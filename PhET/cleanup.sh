#!/bin/bash

shopt -s extglob

cd zh_TW/simulation

# remove legacy link in HTML5 simulations
for i in *.html; do
	cp $i $i.2
	sed -z '
	  s:@:@A:g; s:}:@B:g; s:</a>:}:g;
	  s:<a[^<>]* href="legacy/[^}]*}:<!--&-->:g;
	  s:}:</a>:g; s:@B:}:g; s:@A:@:g
	' < $i.2 > $i
	rm -f $i.2
done

# Find out the legacy simulations which should be reserved
#FILES=`grep legacy simulations.html | awk ' { print $2; } ' | sed -r 's/href="(.+\.html)"/\1/' | xargs grep -e "\.\./sims.*jar" | sed -r 's/.+href="(.+.jar)".+$/\1/' | tr '\r\n' ' '` 
#
#for j in $FILES; do
#	echo $j >> reserve.list
#done

cd ../simulations/translated
find ./*.html ! -name "zh_TW.html" -a ! -name "en.html" -exec rm -f {} \;

cd ../../../sims

# Remove unnecessary old simulations
rm -rf \
	acid-base-solutions/ \
	arithmatic/ \
	balance-and-torque/ \
	balancing-chemical-equations/ \
	balloons/ \
	beers-law-lab/ \
	bending-light/ \
	build-an-atom/ \
	build-tools/ \
	charges-and-fields/ \
	charges-and-fields-scala/ \
	color-vision/ \
	energy-forms-and-changes/ \
	equation-grapher/ \
	faradays-law/ \
	fluid-pressure-and-flow/under-pressure* \
	force-law-lab/ \
	forces-and-motion-basics/ \
	fractions/ \
	friction/ \
	gene-expression-basics/ \
	gravity-and-orbits/ \
	line-graphing/ \
	mass-spring-lab/ \
	molarity/ \
	molecule-polarity/ \
	molecules-and-light/ \
	molecule-shapes/ \
	mvc-example/ \
	neuron/ \
	ohms-law/ \
	old-blackbody-spectrum/ \
	pendulum-lab/ \
	ph-scale/ \
	plinko-probability/ \
	projectile-motion/ \
	reactants-products-and-leftovers/ \
	resistance-in-a-wire/ \
	rutherford-scattering/ \
	sim-example/ \
	sim-template/ \
	states-of-matter/ \
	test-flash-project/ \
	test-java-project/ \
	test-project/ \
	travoltage/ \
	wave-interference/ \
	wave-on-a-string/ \
	index????.html

cd ../files
rm -rf \
	world-photos/ \
	team-photos/

cd ..
cd en/simulations/translated
find ./*.html ! -name "zh_TW.html" -a ! -name "en.html" -exec rm -f {} \;

cd ../../..

find . -name "index????.html" -delete
