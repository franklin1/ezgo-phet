#!/bin/bash

echo "signing ./faraday/faraday_es.jar"
sign-jar ./faraday/faraday_es.jar
echo "signing ./faraday/faraday_ar.jar"
sign-jar ./faraday/faraday_ar.jar
echo "signing ./faraday/generator_ar.jar"
sign-jar ./faraday/generator_ar.jar
echo "signing ./faraday/generator_en.jar"
sign-jar ./faraday/generator_en.jar
echo "signing ./faraday/generator_nl.jar"
sign-jar ./faraday/generator_nl.jar
echo "signing ./faraday/magnet-and-compass_fa.jar"
sign-jar ./faraday/magnet-and-compass_fa.jar
echo "signing ./faraday/magnet-and-compass_iw.jar"
sign-jar ./faraday/magnet-and-compass_iw.jar
echo "signing ./faraday/magnets-and-electromagnets_ru.jar"
sign-jar ./faraday/magnets-and-electromagnets_ru.jar
echo "signing ./faraday/faraday_da.jar"
sign-jar ./faraday/faraday_da.jar
echo "signing ./faraday/faraday_ku_TR.jar"
sign-jar ./faraday/faraday_ku_TR.jar
echo "signing ./faraday/magnet-and-compass_es_PE.jar"
sign-jar ./faraday/magnet-and-compass_es_PE.jar
echo "signing ./faraday/magnet-and-compass_vi.jar"
sign-jar ./faraday/magnet-and-compass_vi.jar
echo "signing ./faraday/faraday_iw.jar"
sign-jar ./faraday/faraday_iw.jar
echo "signing ./faraday/magnets-and-electromagnets_nl.jar"
sign-jar ./faraday/magnets-and-electromagnets_nl.jar
echo "signing ./faraday/magnets-and-electromagnets_fr.jar"
sign-jar ./faraday/magnets-and-electromagnets_fr.jar
echo "signing ./faraday/generator_mk.jar"
sign-jar ./faraday/generator_mk.jar
echo "signing ./faraday/magnet-and-compass_uk.jar"
sign-jar ./faraday/magnet-and-compass_uk.jar
echo "signing ./faraday/magnet-and-compass_it.jar"
sign-jar ./faraday/magnet-and-compass_it.jar
echo "signing ./faraday/magnet-and-compass_sk.jar"
sign-jar ./faraday/magnet-and-compass_sk.jar
echo "signing ./faraday/faraday_et.jar"
sign-jar ./faraday/faraday_et.jar
echo "signing ./faraday/magnets-and-electromagnets_sv.jar"
sign-jar ./faraday/magnets-and-electromagnets_sv.jar
echo "signing ./faraday/generator_be.jar"
sign-jar ./faraday/generator_be.jar
echo "signing ./faraday/magnets-and-electromagnets_km.jar"
sign-jar ./faraday/magnets-and-electromagnets_km.jar
echo "signing ./faraday/magnet-and-compass_sr.jar"
sign-jar ./faraday/magnet-and-compass_sr.jar
echo "signing ./faraday/faraday_tk.jar"
sign-jar ./faraday/faraday_tk.jar
echo "signing ./faraday/magnet-and-compass_zh_TW.jar"
sign-jar ./faraday/magnet-and-compass_zh_TW.jar
echo "signing ./faraday/generator_sk.jar"
sign-jar ./faraday/generator_sk.jar
echo "signing ./faraday/faraday_el.jar"
sign-jar ./faraday/faraday_el.jar
echo "signing ./faraday/magnets-and-electromagnets_pl.jar"
sign-jar ./faraday/magnets-and-electromagnets_pl.jar
echo "signing ./faraday/faraday_tr.jar"
sign-jar ./faraday/faraday_tr.jar
echo "signing ./faraday/generator_bg.jar"
sign-jar ./faraday/generator_bg.jar
echo "signing ./faraday/magnet-and-compass_ka.jar"
sign-jar ./faraday/magnet-and-compass_ka.jar
echo "signing ./faraday/generator_ru.jar"
sign-jar ./faraday/generator_ru.jar
echo "signing ./faraday/generator_fi.jar"
sign-jar ./faraday/generator_fi.jar
echo "signing ./faraday/faraday_hr.jar"
sign-jar ./faraday/faraday_hr.jar
echo "signing ./faraday/magnet-and-compass_tr.jar"
sign-jar ./faraday/magnet-and-compass_tr.jar
echo "signing ./faraday/faraday_cs.jar"
sign-jar ./faraday/faraday_cs.jar
echo "signing ./faraday/faraday_ko.jar"
sign-jar ./faraday/faraday_ko.jar
echo "signing ./faraday/magnet-and-compass_hy.jar"
sign-jar ./faraday/magnet-and-compass_hy.jar
echo "signing ./faraday/faraday_uk.jar"
sign-jar ./faraday/faraday_uk.jar
echo "signing ./faraday/magnet-and-compass_sl.jar"
sign-jar ./faraday/magnet-and-compass_sl.jar
echo "signing ./faraday/faraday_all.jar"
sign-jar ./faraday/faraday_all.jar
echo "signing ./faraday/magnets-and-electromagnets_fi.jar"
sign-jar ./faraday/magnets-and-electromagnets_fi.jar
echo "signing ./faraday/magnet-and-compass_de.jar"
sign-jar ./faraday/magnet-and-compass_de.jar
echo "signing ./faraday/magnet-and-compass_tk.jar"
sign-jar ./faraday/magnet-and-compass_tk.jar
echo "signing ./faraday/faraday_lo.jar"
sign-jar ./faraday/faraday_lo.jar
echo "signing ./faraday/faraday_bg.jar"
sign-jar ./faraday/faraday_bg.jar
echo "signing ./faraday/magnets-and-electromagnets_en.jar"
sign-jar ./faraday/magnets-and-electromagnets_en.jar
echo "signing ./faraday/faraday_ht.jar"
sign-jar ./faraday/faraday_ht.jar
echo "signing ./faraday/generator_tr.jar"
sign-jar ./faraday/generator_tr.jar
echo "signing ./faraday/magnet-and-compass_sq.jar"
sign-jar ./faraday/magnet-and-compass_sq.jar
echo "signing ./faraday/faraday_ka.jar"
sign-jar ./faraday/faraday_ka.jar
echo "signing ./faraday/magnets-and-electromagnets_gl.jar"
sign-jar ./faraday/magnets-and-electromagnets_gl.jar
echo "signing ./faraday/magnet-and-compass_lt.jar"
sign-jar ./faraday/magnet-and-compass_lt.jar
echo "signing ./faraday/magnets-and-electromagnets_ku_TR.jar"
sign-jar ./faraday/magnets-and-electromagnets_ku_TR.jar
echo "signing ./faraday/magnets-and-electromagnets_ko.jar"
sign-jar ./faraday/magnets-and-electromagnets_ko.jar
echo "signing ./faraday/magnet-and-compass_mr.jar"
sign-jar ./faraday/magnet-and-compass_mr.jar
echo "signing ./faraday/generator_mr.jar"
sign-jar ./faraday/generator_mr.jar
echo "signing ./faraday/generator_ko.jar"
sign-jar ./faraday/generator_ko.jar
echo "signing ./faraday/generator_iw.jar"
sign-jar ./faraday/generator_iw.jar
echo "signing ./faraday/generator_lv.jar"
sign-jar ./faraday/generator_lv.jar
echo "signing ./faraday/magnet-and-compass_es.jar"
sign-jar ./faraday/magnet-and-compass_es.jar
echo "signing ./faraday/generator_it.jar"
sign-jar ./faraday/generator_it.jar
echo "signing ./faraday/generator_et.jar"
sign-jar ./faraday/generator_et.jar
echo "signing ./faraday/magnets-and-electromagnets_ar.jar"
sign-jar ./faraday/magnets-and-electromagnets_ar.jar
echo "signing ./faraday/magnets-and-electromagnets_hr.jar"
sign-jar ./faraday/magnets-and-electromagnets_hr.jar
echo "signing ./faraday/magnets-and-electromagnets_th.jar"
sign-jar ./faraday/magnets-and-electromagnets_th.jar
echo "signing ./faraday/magnets-and-electromagnets_ro.jar"
sign-jar ./faraday/magnets-and-electromagnets_ro.jar
echo "signing ./faraday/generator_de.jar"
sign-jar ./faraday/generator_de.jar
echo "signing ./faraday/generator_fr.jar"
sign-jar ./faraday/generator_fr.jar
echo "signing ./faraday/faraday_ru.jar"
sign-jar ./faraday/faraday_ru.jar
echo "signing ./faraday/generator_cs.jar"
sign-jar ./faraday/generator_cs.jar
echo "signing ./faraday/generator_hr.jar"
sign-jar ./faraday/generator_hr.jar
echo "signing ./faraday/magnet-and-compass_sv.jar"
sign-jar ./faraday/magnet-and-compass_sv.jar
echo "signing ./faraday/magnets-and-electromagnets_de.jar"
sign-jar ./faraday/magnets-and-electromagnets_de.jar
echo "signing ./faraday/generator_sq.jar"
sign-jar ./faraday/generator_sq.jar
echo "signing ./faraday/magnets-and-electromagnets_et.jar"
sign-jar ./faraday/magnets-and-electromagnets_et.jar
echo "signing ./faraday/magnets-and-electromagnets_es_PE.jar"
sign-jar ./faraday/magnets-and-electromagnets_es_PE.jar
echo "signing ./faraday/faraday_fa.jar"
sign-jar ./faraday/faraday_fa.jar
echo "signing ./faraday/magnet-and-compass_in.jar"
sign-jar ./faraday/magnet-and-compass_in.jar
echo "signing ./faraday/generator_tk.jar"
sign-jar ./faraday/generator_tk.jar
echo "signing ./faraday/magnet-and-compass_gl.jar"
sign-jar ./faraday/magnet-and-compass_gl.jar
echo "signing ./faraday/magnet-and-compass_kk.jar"
sign-jar ./faraday/magnet-and-compass_kk.jar
echo "signing ./faraday/faraday_sr.jar"
sign-jar ./faraday/faraday_sr.jar
echo "signing ./faraday/magnet-and-compass_en.jar"
sign-jar ./faraday/magnet-and-compass_en.jar
echo "signing ./faraday/magnet-and-compass_bg.jar"
sign-jar ./faraday/magnet-and-compass_bg.jar
echo "signing ./faraday/faraday_en.jar"
sign-jar ./faraday/faraday_en.jar
echo "signing ./faraday/magnets-and-electromagnets_cs.jar"
sign-jar ./faraday/magnets-and-electromagnets_cs.jar
echo "signing ./faraday/magnet-and-compass_bs.jar"
sign-jar ./faraday/magnet-and-compass_bs.jar
echo "signing ./faraday/magnets-and-electromagnets_hy.jar"
sign-jar ./faraday/magnets-and-electromagnets_hy.jar
echo "signing ./faraday/magnets-and-electromagnets_in.jar"
sign-jar ./faraday/magnets-and-electromagnets_in.jar
echo "signing ./faraday/faraday_it.jar"
sign-jar ./faraday/faraday_it.jar
echo "signing ./faraday/magnets-and-electromagnets_sr.jar"
sign-jar ./faraday/magnets-and-electromagnets_sr.jar
echo "signing ./faraday/generator_sl.jar"
sign-jar ./faraday/generator_sl.jar
echo "signing ./faraday/magnet-and-compass_ko.jar"
sign-jar ./faraday/magnet-and-compass_ko.jar
echo "signing ./faraday/generator_pt_BR.jar"
sign-jar ./faraday/generator_pt_BR.jar
echo "signing ./faraday/magnet-and-compass_eu.jar"
sign-jar ./faraday/magnet-and-compass_eu.jar
echo "signing ./faraday/generator_ku_TR.jar"
sign-jar ./faraday/generator_ku_TR.jar
echo "signing ./faraday/magnet-and-compass_nl.jar"
sign-jar ./faraday/magnet-and-compass_nl.jar
echo "signing ./faraday/generator_uk.jar"
sign-jar ./faraday/generator_uk.jar
echo "signing ./faraday/magnet-and-compass_kn.jar"
sign-jar ./faraday/magnet-and-compass_kn.jar
echo "signing ./faraday/magnets-and-electromagnets_kk.jar"
sign-jar ./faraday/magnets-and-electromagnets_kk.jar
echo "signing ./faraday/magnet-and-compass_el.jar"
sign-jar ./faraday/magnet-and-compass_el.jar
echo "signing ./faraday/faraday_ca.jar"
sign-jar ./faraday/faraday_ca.jar
echo "signing ./faraday/generator_el.jar"
sign-jar ./faraday/generator_el.jar
echo "signing ./faraday/faraday_sq.jar"
sign-jar ./faraday/faraday_sq.jar
echo "signing ./faraday/magnets-and-electromagnets_ka.jar"
sign-jar ./faraday/magnets-and-electromagnets_ka.jar
echo "signing ./faraday/faraday_ja.jar"
sign-jar ./faraday/faraday_ja.jar
echo "signing ./faraday/faraday_sl.jar"
sign-jar ./faraday/faraday_sl.jar
echo "signing ./faraday/magnet-and-compass_ca.jar"
sign-jar ./faraday/magnet-and-compass_ca.jar
echo "signing ./faraday/magnets-and-electromagnets_zh_CN.jar"
sign-jar ./faraday/magnets-and-electromagnets_zh_CN.jar
echo "signing ./faraday/faraday_nl.jar"
sign-jar ./faraday/faraday_nl.jar
echo "signing ./faraday/generator_es_PE.jar"
sign-jar ./faraday/generator_es_PE.jar
echo "signing ./faraday/magnet-and-compass_km.jar"
sign-jar ./faraday/magnet-and-compass_km.jar
echo "signing ./faraday/generator_gl.jar"
sign-jar ./faraday/generator_gl.jar
echo "signing ./faraday/magnet-and-compass_et.jar"
sign-jar ./faraday/magnet-and-compass_et.jar
echo "signing ./faraday/magnets-and-electromagnets_lo.jar"
sign-jar ./faraday/magnets-and-electromagnets_lo.jar
echo "signing ./faraday/generator_hy.jar"
sign-jar ./faraday/generator_hy.jar
echo "signing ./faraday/magnet-and-compass_lo.jar"
sign-jar ./faraday/magnet-and-compass_lo.jar
echo "signing ./faraday/magnets-and-electromagnets_kn.jar"
sign-jar ./faraday/magnets-and-electromagnets_kn.jar
echo "signing ./faraday/magnets-and-electromagnets_ja.jar"
sign-jar ./faraday/magnets-and-electromagnets_ja.jar
echo "signing ./faraday/faraday_es_PE.jar"
sign-jar ./faraday/faraday_es_PE.jar
echo "signing ./faraday/magnets-and-electromagnets_mk.jar"
sign-jar ./faraday/magnets-and-electromagnets_mk.jar
echo "signing ./faraday/faraday_hu.jar"
sign-jar ./faraday/faraday_hu.jar
echo "signing ./faraday/magnets-and-electromagnets_sq.jar"
sign-jar ./faraday/magnets-and-electromagnets_sq.jar
echo "signing ./faraday/magnets-and-electromagnets_be.jar"
sign-jar ./faraday/magnets-and-electromagnets_be.jar
echo "signing ./faraday/magnet-and-compass_be.jar"
sign-jar ./faraday/magnet-and-compass_be.jar
echo "signing ./faraday/generator_ht.jar"
sign-jar ./faraday/generator_ht.jar
echo "signing ./faraday/faraday_lt.jar"
sign-jar ./faraday/faraday_lt.jar
echo "signing ./faraday/magnets-and-electromagnets_hu.jar"
sign-jar ./faraday/magnets-and-electromagnets_hu.jar
echo "signing ./faraday/magnet-and-compass_pt_BR.jar"
sign-jar ./faraday/magnet-and-compass_pt_BR.jar
echo "signing ./faraday/magnets-and-electromagnets_bg.jar"
sign-jar ./faraday/magnets-and-electromagnets_bg.jar
echo "signing ./faraday/magnet-and-compass_da.jar"
sign-jar ./faraday/magnet-and-compass_da.jar
echo "signing ./faraday/magnets-and-electromagnets_pt.jar"
sign-jar ./faraday/magnets-and-electromagnets_pt.jar
echo "signing ./faraday/magnet-and-compass_ht.jar"
sign-jar ./faraday/magnet-and-compass_ht.jar
echo "signing ./faraday/faraday_zh_TW.jar"
sign-jar ./faraday/faraday_zh_TW.jar
echo "signing ./faraday/magnets-and-electromagnets_tr.jar"
sign-jar ./faraday/magnets-and-electromagnets_tr.jar
echo "signing ./faraday/faraday_ro.jar"
sign-jar ./faraday/faraday_ro.jar
echo "signing ./faraday/magnets-and-electromagnets_sl.jar"
sign-jar ./faraday/magnets-and-electromagnets_sl.jar
echo "signing ./faraday/faraday_de.jar"
sign-jar ./faraday/faraday_de.jar
echo "signing ./faraday/magnets-and-electromagnets_fa.jar"
sign-jar ./faraday/magnets-and-electromagnets_fa.jar
echo "signing ./faraday/faraday_gl.jar"
sign-jar ./faraday/faraday_gl.jar
echo "signing ./faraday/generator_eu.jar"
sign-jar ./faraday/generator_eu.jar
echo "signing ./faraday/generator_th.jar"
sign-jar ./faraday/generator_th.jar
echo "signing ./faraday/magnet-and-compass_ru.jar"
sign-jar ./faraday/magnet-and-compass_ru.jar
echo "signing ./faraday/magnet-and-compass_ar.jar"
sign-jar ./faraday/magnet-and-compass_ar.jar
echo "signing ./faraday/magnets-and-electromagnets_lv.jar"
sign-jar ./faraday/magnets-and-electromagnets_lv.jar
echo "signing ./faraday/faraday_mr.jar"
sign-jar ./faraday/faraday_mr.jar
echo "signing ./faraday/generator_sv.jar"
sign-jar ./faraday/generator_sv.jar
echo "signing ./faraday/magnet-and-compass_cs.jar"
sign-jar ./faraday/magnet-and-compass_cs.jar
echo "signing ./faraday/generator_ca.jar"
sign-jar ./faraday/generator_ca.jar
echo "signing ./faraday/generator_in.jar"
sign-jar ./faraday/generator_in.jar
echo "signing ./faraday/faraday_in.jar"
sign-jar ./faraday/faraday_in.jar
echo "signing ./faraday/generator_ja.jar"
sign-jar ./faraday/generator_ja.jar
echo "signing ./faraday/faraday_all_installer.jar"
sign-jar ./faraday/faraday_all_installer.jar
echo "signing ./faraday/generator_kn.jar"
sign-jar ./faraday/generator_kn.jar
echo "signing ./faraday/faraday_pl.jar"
sign-jar ./faraday/faraday_pl.jar
echo "signing ./faraday/generator_ro.jar"
sign-jar ./faraday/generator_ro.jar
echo "signing ./faraday/magnet-and-compass_ro.jar"
sign-jar ./faraday/magnet-and-compass_ro.jar
echo "signing ./faraday/magnets-and-electromagnets_ca.jar"
sign-jar ./faraday/magnets-and-electromagnets_ca.jar
echo "signing ./faraday/generator_km.jar"
sign-jar ./faraday/generator_km.jar
echo "signing ./faraday/generator_zh_CN.jar"
sign-jar ./faraday/generator_zh_CN.jar
echo "signing ./faraday/faraday_lv.jar"
sign-jar ./faraday/faraday_lv.jar
echo "signing ./faraday/generator_pt.jar"
sign-jar ./faraday/generator_pt.jar
echo "signing ./faraday/faraday_km.jar"
sign-jar ./faraday/faraday_km.jar
echo "signing ./faraday/magnets-and-electromagnets_mr.jar"
sign-jar ./faraday/magnets-and-electromagnets_mr.jar
echo "signing ./faraday/magnet-and-compass_mk.jar"
sign-jar ./faraday/magnet-and-compass_mk.jar
echo "signing ./faraday/generator_da.jar"
sign-jar ./faraday/generator_da.jar
echo "signing ./faraday/faraday_zh_CN.jar"
sign-jar ./faraday/faraday_zh_CN.jar
echo "signing ./faraday/magnets-and-electromagnets_eu.jar"
sign-jar ./faraday/magnets-and-electromagnets_eu.jar
echo "signing ./faraday/magnet-and-compass_hu.jar"
sign-jar ./faraday/magnet-and-compass_hu.jar
echo "signing ./faraday/generator_vi.jar"
sign-jar ./faraday/generator_vi.jar
echo "signing ./faraday/magnets-and-electromagnets_it.jar"
sign-jar ./faraday/magnets-and-electromagnets_it.jar
echo "signing ./faraday/magnets-and-electromagnets_tk.jar"
sign-jar ./faraday/magnets-and-electromagnets_tk.jar
echo "signing ./faraday/generator_lt.jar"
sign-jar ./faraday/generator_lt.jar
echo "signing ./faraday/faraday_sv.jar"
sign-jar ./faraday/faraday_sv.jar
echo "signing ./faraday/generator_bs.jar"
sign-jar ./faraday/generator_bs.jar
echo "signing ./faraday/generator_lo.jar"
sign-jar ./faraday/generator_lo.jar
echo "signing ./faraday/magnet-and-compass_fr.jar"
sign-jar ./faraday/magnet-and-compass_fr.jar
echo "signing ./faraday/magnet-and-compass_hr.jar"
sign-jar ./faraday/magnet-and-compass_hr.jar
echo "signing ./faraday/generator_fa.jar"
sign-jar ./faraday/generator_fa.jar
echo "signing ./faraday/magnet-and-compass_th.jar"
sign-jar ./faraday/magnet-and-compass_th.jar
echo "signing ./faraday/magnet-and-compass_pl.jar"
sign-jar ./faraday/magnet-and-compass_pl.jar
echo "signing ./faraday/magnet-and-compass_lv.jar"
sign-jar ./faraday/magnet-and-compass_lv.jar
echo "signing ./faraday/magnets-and-electromagnets_es.jar"
sign-jar ./faraday/magnets-and-electromagnets_es.jar
echo "signing ./faraday/faraday_kn.jar"
sign-jar ./faraday/faraday_kn.jar
echo "signing ./faraday/magnets-and-electromagnets_ht.jar"
sign-jar ./faraday/magnets-and-electromagnets_ht.jar
echo "signing ./faraday/magnets-and-electromagnets_el.jar"
sign-jar ./faraday/magnets-and-electromagnets_el.jar
echo "signing ./faraday/faraday_vi.jar"
sign-jar ./faraday/faraday_vi.jar
echo "signing ./faraday/magnet-and-compass_pt.jar"
sign-jar ./faraday/magnet-and-compass_pt.jar
echo "signing ./faraday/faraday_pt.jar"
sign-jar ./faraday/faraday_pt.jar
echo "signing ./faraday/magnets-and-electromagnets_uk.jar"
sign-jar ./faraday/magnets-and-electromagnets_uk.jar
echo "signing ./faraday/magnet-and-compass_ku_TR.jar"
sign-jar ./faraday/magnet-and-compass_ku_TR.jar
echo "signing ./faraday/generator_pl.jar"
sign-jar ./faraday/generator_pl.jar
echo "signing ./faraday/magnets-and-electromagnets_zh_TW.jar"
sign-jar ./faraday/magnets-and-electromagnets_zh_TW.jar
echo "signing ./faraday/magnets-and-electromagnets_sk.jar"
sign-jar ./faraday/magnets-and-electromagnets_sk.jar
echo "signing ./faraday/magnets-and-electromagnets_bs.jar"
sign-jar ./faraday/magnets-and-electromagnets_bs.jar
echo "signing ./faraday/faraday_fi.jar"
sign-jar ./faraday/faraday_fi.jar
echo "signing ./faraday/generator_kk.jar"
sign-jar ./faraday/generator_kk.jar
echo "signing ./faraday/generator_es.jar"
sign-jar ./faraday/generator_es.jar
echo "signing ./faraday/faraday_hy.jar"
sign-jar ./faraday/faraday_hy.jar
echo "signing ./faraday/magnet-and-compass_zh_CN.jar"
sign-jar ./faraday/magnet-and-compass_zh_CN.jar
echo "signing ./faraday/generator_sr.jar"
sign-jar ./faraday/generator_sr.jar
echo "signing ./faraday/magnet-and-compass_fi.jar"
sign-jar ./faraday/magnet-and-compass_fi.jar
echo "signing ./faraday/generator_hu.jar"
sign-jar ./faraday/generator_hu.jar
echo "signing ./faraday/faraday_sk.jar"
sign-jar ./faraday/faraday_sk.jar
echo "signing ./faraday/faraday_kk.jar"
sign-jar ./faraday/faraday_kk.jar
echo "signing ./faraday/generator_ka.jar"
sign-jar ./faraday/generator_ka.jar
echo "signing ./faraday/faraday_fr.jar"
sign-jar ./faraday/faraday_fr.jar
echo "signing ./faraday/magnets-and-electromagnets_da.jar"
sign-jar ./faraday/magnets-and-electromagnets_da.jar
echo "signing ./faraday/generator_zh_TW.jar"
sign-jar ./faraday/generator_zh_TW.jar
echo "signing ./faraday/faraday_be.jar"
sign-jar ./faraday/faraday_be.jar
echo "signing ./faraday/magnet-and-compass_ja.jar"
sign-jar ./faraday/magnet-and-compass_ja.jar
echo "signing ./faraday/magnets-and-electromagnets_pt_BR.jar"
sign-jar ./faraday/magnets-and-electromagnets_pt_BR.jar
echo "signing ./faraday/faraday_mk.jar"
sign-jar ./faraday/faraday_mk.jar
echo "signing ./faraday/faraday_eu.jar"
sign-jar ./faraday/faraday_eu.jar
echo "signing ./faraday/magnets-and-electromagnets_vi.jar"
sign-jar ./faraday/magnets-and-electromagnets_vi.jar
echo "signing ./faraday/faraday_pt_BR.jar"
sign-jar ./faraday/faraday_pt_BR.jar
echo "signing ./faraday/magnets-and-electromagnets_iw.jar"
sign-jar ./faraday/magnets-and-electromagnets_iw.jar
echo "signing ./faraday/faraday_th.jar"
sign-jar ./faraday/faraday_th.jar
echo "signing ./faraday/magnets-and-electromagnets_lt.jar"
sign-jar ./faraday/magnets-and-electromagnets_lt.jar
echo "signing ./faraday/faraday_bs.jar"
sign-jar ./faraday/faraday_bs.jar
echo "signing ./ohms-law/ohms-law_th.jar"
sign-jar ./ohms-law/ohms-law_th.jar
echo "signing ./ohms-law/ohms-law_ka.jar"
sign-jar ./ohms-law/ohms-law_ka.jar
echo "signing ./ohms-law/ohms-law_tr.jar"
sign-jar ./ohms-law/ohms-law_tr.jar
echo "signing ./ohms-law/ohms-law_sr.jar"
sign-jar ./ohms-law/ohms-law_sr.jar
echo "signing ./ohms-law/ohms-law_lv.jar"
sign-jar ./ohms-law/ohms-law_lv.jar
echo "signing ./ohms-law/ohms-law_nl.jar"
sign-jar ./ohms-law/ohms-law_nl.jar
echo "signing ./ohms-law/ohms-law_be.jar"
sign-jar ./ohms-law/ohms-law_be.jar
echo "signing ./ohms-law/ohms-law_et.jar"
sign-jar ./ohms-law/ohms-law_et.jar
echo "signing ./ohms-law/ohms-law_kn.jar"
sign-jar ./ohms-law/ohms-law_kn.jar
echo "signing ./ohms-law/ohms-law_mn.jar"
sign-jar ./ohms-law/ohms-law_mn.jar
echo "signing ./ohms-law/ohms-law_da.jar"
sign-jar ./ohms-law/ohms-law_da.jar
echo "signing ./ohms-law/ohms-law_ku_TR.jar"
sign-jar ./ohms-law/ohms-law_ku_TR.jar
echo "signing ./ohms-law/ohms-law_ko.jar"
sign-jar ./ohms-law/ohms-law_ko.jar
echo "signing ./ohms-law/ohms-law_in.jar"
sign-jar ./ohms-law/ohms-law_in.jar
echo "signing ./ohms-law/ohms-law_bs.jar"
sign-jar ./ohms-law/ohms-law_bs.jar
echo "signing ./ohms-law/ohms-law_eu.jar"
sign-jar ./ohms-law/ohms-law_eu.jar
echo "signing ./ohms-law/ohms-law_fi.jar"
sign-jar ./ohms-law/ohms-law_fi.jar
echo "signing ./ohms-law/ohms-law_uk.jar"
sign-jar ./ohms-law/ohms-law_uk.jar
echo "signing ./ohms-law/ohms-law_kk.jar"
sign-jar ./ohms-law/ohms-law_kk.jar
echo "signing ./ohms-law/ohms-law_fa.jar"
sign-jar ./ohms-law/ohms-law_fa.jar
echo "signing ./ohms-law/ohms-law_zh_TW.jar"
sign-jar ./ohms-law/ohms-law_zh_TW.jar
echo "signing ./ohms-law/ohms-law_ms.jar"
sign-jar ./ohms-law/ohms-law_ms.jar
echo "signing ./ohms-law/ohms-law_el.jar"
sign-jar ./ohms-law/ohms-law_el.jar
echo "signing ./ohms-law/ohms-law_ar_SA.jar"
sign-jar ./ohms-law/ohms-law_ar_SA.jar
echo "signing ./ohms-law/ohms-law_hu.jar"
sign-jar ./ohms-law/ohms-law_hu.jar
echo "signing ./ohms-law/ohms-law_sk.jar"
sign-jar ./ohms-law/ohms-law_sk.jar
echo "signing ./ohms-law/ohms-law_de.jar"
sign-jar ./ohms-law/ohms-law_de.jar
echo "signing ./ohms-law/ohms-law_en.jar"
sign-jar ./ohms-law/ohms-law_en.jar
echo "signing ./ohms-law/ohms-law_mr.jar"
sign-jar ./ohms-law/ohms-law_mr.jar
echo "signing ./ohms-law/ohms-law_pl.jar"
sign-jar ./ohms-law/ohms-law_pl.jar
echo "signing ./ohms-law/ohms-law_tk.jar"
sign-jar ./ohms-law/ohms-law_tk.jar
echo "signing ./ohms-law/ohms-law_iw.jar"
sign-jar ./ohms-law/ohms-law_iw.jar
echo "signing ./ohms-law/ohms-law_cs.jar"
sign-jar ./ohms-law/ohms-law_cs.jar
echo "signing ./ohms-law/ohms-law_bg.jar"
sign-jar ./ohms-law/ohms-law_bg.jar
echo "signing ./ohms-law/ohms-law_pt.jar"
sign-jar ./ohms-law/ohms-law_pt.jar
echo "signing ./ohms-law/ohms-law_es.jar"
sign-jar ./ohms-law/ohms-law_es.jar
echo "signing ./ohms-law/ohms-law_vi.jar"
sign-jar ./ohms-law/ohms-law_vi.jar
echo "signing ./ohms-law/ohms-law_fr.jar"
sign-jar ./ohms-law/ohms-law_fr.jar
echo "signing ./ohms-law/ohms-law_sl.jar"
sign-jar ./ohms-law/ohms-law_sl.jar
echo "signing ./ohms-law/ohms-law_ro.jar"
sign-jar ./ohms-law/ohms-law_ro.jar
echo "signing ./ohms-law/ohms-law_gl.jar"
sign-jar ./ohms-law/ohms-law_gl.jar
echo "signing ./ohms-law/ohms-law_ja.jar"
sign-jar ./ohms-law/ohms-law_ja.jar
echo "signing ./ohms-law/ohms-law_ru.jar"
sign-jar ./ohms-law/ohms-law_ru.jar
echo "signing ./ohms-law/ohms-law_nb.jar"
sign-jar ./ohms-law/ohms-law_nb.jar
echo "signing ./ohms-law/ohms-law_pt_BR.jar"
sign-jar ./ohms-law/ohms-law_pt_BR.jar
echo "signing ./ohms-law/ohms-law_ur.jar"
sign-jar ./ohms-law/ohms-law_ur.jar
echo "signing ./ohms-law/ohms-law_zh_CN.jar"
sign-jar ./ohms-law/ohms-law_zh_CN.jar
echo "signing ./ohms-law/ohms-law_it.jar"
sign-jar ./ohms-law/ohms-law_it.jar
echo "signing ./ohms-law/ohms-law_ht.jar"
sign-jar ./ohms-law/ohms-law_ht.jar
echo "signing ./ohms-law/ohms-law_mk.jar"
sign-jar ./ohms-law/ohms-law_mk.jar
echo "signing ./ohms-law/ohms-law_ta.jar"
sign-jar ./ohms-law/ohms-law_ta.jar
echo "signing ./ohms-law/ohms-law_es_PE.jar"
sign-jar ./ohms-law/ohms-law_es_PE.jar
echo "signing ./ohms-law/ohms-law_hr.jar"
sign-jar ./ohms-law/ohms-law_hr.jar
echo "signing ./ohms-law/ohms-law_sv.jar"
sign-jar ./ohms-law/ohms-law_sv.jar
echo "signing ./ohms-law/ohms-law_ar.jar"
sign-jar ./ohms-law/ohms-law_ar.jar
echo "signing ./lasers/lasers_all_installer.jar"
sign-jar ./lasers/lasers_all_installer.jar
echo "signing ./lasers/lasers_sq.jar"
sign-jar ./lasers/lasers_sq.jar
echo "signing ./lasers/lasers_bs.jar"
sign-jar ./lasers/lasers_bs.jar
echo "signing ./lasers/lasers_cs.jar"
sign-jar ./lasers/lasers_cs.jar
echo "signing ./lasers/lasers_mn.jar"
sign-jar ./lasers/lasers_mn.jar
echo "signing ./lasers/lasers_sr.jar"
sign-jar ./lasers/lasers_sr.jar
echo "signing ./lasers/lasers_in.jar"
sign-jar ./lasers/lasers_in.jar
echo "signing ./lasers/lasers_th.jar"
sign-jar ./lasers/lasers_th.jar
echo "signing ./lasers/lasers_zh_CN.jar"
sign-jar ./lasers/lasers_zh_CN.jar
echo "signing ./lasers/lasers_en.jar"
sign-jar ./lasers/lasers_en.jar
echo "signing ./lasers/lasers_lt.jar"
sign-jar ./lasers/lasers_lt.jar
echo "signing ./lasers/lasers_pt_BR.jar"
sign-jar ./lasers/lasers_pt_BR.jar
echo "signing ./lasers/lasers_et.jar"
sign-jar ./lasers/lasers_et.jar
echo "signing ./lasers/lasers_be.jar"
sign-jar ./lasers/lasers_be.jar
echo "signing ./lasers/lasers_tr.jar"
sign-jar ./lasers/lasers_tr.jar
echo "signing ./lasers/lasers_de.jar"
sign-jar ./lasers/lasers_de.jar
echo "signing ./lasers/lasers_tk.jar"
sign-jar ./lasers/lasers_tk.jar
echo "signing ./lasers/lasers_gl.jar"
sign-jar ./lasers/lasers_gl.jar
echo "signing ./lasers/lasers_all.jar"
sign-jar ./lasers/lasers_all.jar
echo "signing ./lasers/lasers_ar.jar"
sign-jar ./lasers/lasers_ar.jar
echo "signing ./lasers/lasers_pt.jar"
sign-jar ./lasers/lasers_pt.jar
echo "signing ./lasers/lasers_fa.jar"
sign-jar ./lasers/lasers_fa.jar
echo "signing ./lasers/lasers_vi.jar"
sign-jar ./lasers/lasers_vi.jar
echo "signing ./lasers/lasers_nl.jar"
sign-jar ./lasers/lasers_nl.jar
echo "signing ./lasers/lasers_hu.jar"
sign-jar ./lasers/lasers_hu.jar
echo "signing ./lasers/lasers_km.jar"
sign-jar ./lasers/lasers_km.jar
echo "signing ./lasers/lasers_pl.jar"
sign-jar ./lasers/lasers_pl.jar
echo "signing ./lasers/lasers_mk.jar"
sign-jar ./lasers/lasers_mk.jar
echo "signing ./lasers/lasers_iw.jar"
sign-jar ./lasers/lasers_iw.jar
echo "signing ./lasers/lasers_ko.jar"
sign-jar ./lasers/lasers_ko.jar
echo "signing ./lasers/lasers_sk.jar"
sign-jar ./lasers/lasers_sk.jar
echo "signing ./lasers/lasers_zh_TW.jar"
sign-jar ./lasers/lasers_zh_TW.jar
echo "signing ./lasers/lasers_uk.jar"
sign-jar ./lasers/lasers_uk.jar
echo "signing ./lasers/lasers_ka.jar"
sign-jar ./lasers/lasers_ka.jar
echo "signing ./lasers/lasers_fi.jar"
sign-jar ./lasers/lasers_fi.jar
echo "signing ./lasers/lasers_ar_SA.jar"
sign-jar ./lasers/lasers_ar_SA.jar
echo "signing ./lasers/lasers_ja.jar"
sign-jar ./lasers/lasers_ja.jar
echo "signing ./lasers/lasers_es_PE.jar"
sign-jar ./lasers/lasers_es_PE.jar
echo "signing ./lasers/lasers_it.jar"
sign-jar ./lasers/lasers_it.jar
echo "signing ./lasers/lasers_kn.jar"
sign-jar ./lasers/lasers_kn.jar
echo "signing ./lasers/lasers_eu.jar"
sign-jar ./lasers/lasers_eu.jar
echo "signing ./lasers/lasers_kk.jar"
sign-jar ./lasers/lasers_kk.jar
echo "signing ./lasers/lasers_hr.jar"
sign-jar ./lasers/lasers_hr.jar
echo "signing ./lasers/lasers_ru.jar"
sign-jar ./lasers/lasers_ru.jar
echo "signing ./lasers/lasers_fr.jar"
sign-jar ./lasers/lasers_fr.jar
echo "signing ./lasers/lasers_el.jar"
sign-jar ./lasers/lasers_el.jar
echo "signing ./lasers/lasers_es.jar"
sign-jar ./lasers/lasers_es.jar
echo "signing ./lasers/lasers_da.jar"
sign-jar ./lasers/lasers_da.jar
echo "signing ./efield/efield_sv.jar"
sign-jar ./efield/efield_sv.jar
echo "signing ./efield/efield_pt_BR.jar"
sign-jar ./efield/efield_pt_BR.jar
echo "signing ./efield/efield_in.jar"
sign-jar ./efield/efield_in.jar
echo "signing ./efield/efield_ku_TR.jar"
sign-jar ./efield/efield_ku_TR.jar
echo "signing ./efield/efield_ja.jar"
sign-jar ./efield/efield_ja.jar
echo "signing ./efield/efield_fa.jar"
sign-jar ./efield/efield_fa.jar
echo "signing ./efield/efield_pt.jar"
sign-jar ./efield/efield_pt.jar
echo "signing ./efield/efield_iw.jar"
sign-jar ./efield/efield_iw.jar
echo "signing ./efield/efield_hu.jar"
sign-jar ./efield/efield_hu.jar
echo "signing ./efield/efield_ko.jar"
sign-jar ./efield/efield_ko.jar
echo "signing ./efield/efield_sl.jar"
sign-jar ./efield/efield_sl.jar
echo "signing ./efield/efield_it.jar"
sign-jar ./efield/efield_it.jar
echo "signing ./efield/efield_km.jar"
sign-jar ./efield/efield_km.jar
echo "signing ./efield/efield_pl.jar"
sign-jar ./efield/efield_pl.jar
echo "signing ./efield/efield_hy.jar"
sign-jar ./efield/efield_hy.jar
echo "signing ./efield/efield_tk.jar"
sign-jar ./efield/efield_tk.jar
echo "signing ./efield/efield_de.jar"
sign-jar ./efield/efield_de.jar
echo "signing ./efield/efield_ro.jar"
sign-jar ./efield/efield_ro.jar
echo "signing ./efield/efield_kn.jar"
sign-jar ./efield/efield_kn.jar
echo "signing ./efield/efield_eu.jar"
sign-jar ./efield/efield_eu.jar
echo "signing ./efield/efield_et.jar"
sign-jar ./efield/efield_et.jar
echo "signing ./efield/efield_sk.jar"
sign-jar ./efield/efield_sk.jar
echo "signing ./efield/efield_nl.jar"
sign-jar ./efield/efield_nl.jar
echo "signing ./efield/efield_cs.jar"
sign-jar ./efield/efield_cs.jar
echo "signing ./efield/efield_ar.jar"
sign-jar ./efield/efield_ar.jar
echo "signing ./efield/efield_tr.jar"
sign-jar ./efield/efield_tr.jar
echo "signing ./efield/efield_vi.jar"
sign-jar ./efield/efield_vi.jar
echo "signing ./efield/efield_hr.jar"
sign-jar ./efield/efield_hr.jar
echo "signing ./efield/efield_ka.jar"
sign-jar ./efield/efield_ka.jar
echo "signing ./efield/efield_en.jar"
sign-jar ./efield/efield_en.jar
echo "signing ./efield/efield_es.jar"
sign-jar ./efield/efield_es.jar
echo "signing ./efield/efield_mk.jar"
sign-jar ./efield/efield_mk.jar
echo "signing ./efield/efield_fr.jar"
sign-jar ./efield/efield_fr.jar
echo "signing ./efield/efield_uk.jar"
sign-jar ./efield/efield_uk.jar
echo "signing ./efield/efield_all_installer.jar"
sign-jar ./efield/efield_all_installer.jar
echo "signing ./efield/efield_sr.jar"
sign-jar ./efield/efield_sr.jar
echo "signing ./efield/efield_bs.jar"
sign-jar ./efield/efield_bs.jar
echo "signing ./efield/efield_el.jar"
sign-jar ./efield/efield_el.jar
echo "signing ./efield/efield_bg.jar"
sign-jar ./efield/efield_bg.jar
echo "signing ./efield/efield_all.jar"
sign-jar ./efield/efield_all.jar
echo "signing ./efield/efield_da.jar"
sign-jar ./efield/efield_da.jar
echo "signing ./efield/efield_zh_TW.jar"
sign-jar ./efield/efield_zh_TW.jar
echo "signing ./efield/efield_es_PE.jar"
sign-jar ./efield/efield_es_PE.jar
echo "signing ./efield/efield_be.jar"
sign-jar ./efield/efield_be.jar
echo "signing ./efield/efield_zh_CN.jar"
sign-jar ./efield/efield_zh_CN.jar
echo "signing ./efield/efield_kk.jar"
sign-jar ./efield/efield_kk.jar
echo "signing ./efield/efield_th.jar"
sign-jar ./efield/efield_th.jar
echo "signing ./density-and-buoyancy/buoyancy_pt.jar"
sign-jar ./density-and-buoyancy/buoyancy_pt.jar
echo "signing ./density-and-buoyancy/density_sk.jar"
sign-jar ./density-and-buoyancy/density_sk.jar
echo "signing ./density-and-buoyancy/buoyancy_zh_TW.jar"
sign-jar ./density-and-buoyancy/buoyancy_zh_TW.jar
echo "signing ./density-and-buoyancy/density_ku_TR.jar"
sign-jar ./density-and-buoyancy/density_ku_TR.jar
echo "signing ./density-and-buoyancy/density_ca.jar"
sign-jar ./density-and-buoyancy/density_ca.jar
echo "signing ./density-and-buoyancy/density_bs.jar"
sign-jar ./density-and-buoyancy/density_bs.jar
echo "signing ./density-and-buoyancy/density_mk.jar"
sign-jar ./density-and-buoyancy/density_mk.jar
echo "signing ./density-and-buoyancy/buoyancy_nl.jar"
sign-jar ./density-and-buoyancy/buoyancy_nl.jar
echo "signing ./density-and-buoyancy/buoyancy_hr.jar"
sign-jar ./density-and-buoyancy/buoyancy_hr.jar
echo "signing ./density-and-buoyancy/density_sr.jar"
sign-jar ./density-and-buoyancy/density_sr.jar
echo "signing ./density-and-buoyancy/buoyancy_sk.jar"
sign-jar ./density-and-buoyancy/buoyancy_sk.jar
echo "signing ./density-and-buoyancy/density_pl.jar"
sign-jar ./density-and-buoyancy/density_pl.jar
echo "signing ./density-and-buoyancy/buoyancy_zh_CN.jar"
sign-jar ./density-and-buoyancy/buoyancy_zh_CN.jar
echo "signing ./density-and-buoyancy/density_th.jar"
sign-jar ./density-and-buoyancy/density_th.jar
echo "signing ./density-and-buoyancy/density_ta.jar"
sign-jar ./density-and-buoyancy/density_ta.jar
echo "signing ./density-and-buoyancy/density_es.jar"
sign-jar ./density-and-buoyancy/density_es.jar
echo "signing ./density-and-buoyancy/density_it.jar"
sign-jar ./density-and-buoyancy/density_it.jar
echo "signing ./density-and-buoyancy/buoyancy_tk.jar"
sign-jar ./density-and-buoyancy/buoyancy_tk.jar
echo "signing ./density-and-buoyancy/density_el.jar"
sign-jar ./density-and-buoyancy/density_el.jar
echo "signing ./density-and-buoyancy/buoyancy_it.jar"
sign-jar ./density-and-buoyancy/buoyancy_it.jar
echo "signing ./density-and-buoyancy/buoyancy_mk.jar"
sign-jar ./density-and-buoyancy/buoyancy_mk.jar
echo "signing ./density-and-buoyancy/buoyancy_kn.jar"
sign-jar ./density-and-buoyancy/buoyancy_kn.jar
echo "signing ./density-and-buoyancy/density_hr.jar"
sign-jar ./density-and-buoyancy/density_hr.jar
echo "signing ./density-and-buoyancy/buoyancy_km.jar"
sign-jar ./density-and-buoyancy/buoyancy_km.jar
echo "signing ./density-and-buoyancy/density_bg.jar"
sign-jar ./density-and-buoyancy/density_bg.jar
echo "signing ./density-and-buoyancy/buoyancy_kk.jar"
sign-jar ./density-and-buoyancy/buoyancy_kk.jar
echo "signing ./density-and-buoyancy/density_in.jar"
sign-jar ./density-and-buoyancy/density_in.jar
echo "signing ./density-and-buoyancy/density_eu.jar"
sign-jar ./density-and-buoyancy/density_eu.jar
echo "signing ./density-and-buoyancy/density_pt_BR.jar"
sign-jar ./density-and-buoyancy/density_pt_BR.jar
echo "signing ./density-and-buoyancy/density_mr.jar"
sign-jar ./density-and-buoyancy/density_mr.jar
echo "signing ./density-and-buoyancy/buoyancy_ar_SA.jar"
sign-jar ./density-and-buoyancy/buoyancy_ar_SA.jar
echo "signing ./density-and-buoyancy/density_tr.jar"
sign-jar ./density-and-buoyancy/density_tr.jar
echo "signing ./density-and-buoyancy/buoyancy_mr.jar"
sign-jar ./density-and-buoyancy/buoyancy_mr.jar
echo "signing ./density-and-buoyancy/density_sl.jar"
sign-jar ./density-and-buoyancy/density_sl.jar
echo "signing ./density-and-buoyancy/density_af.jar"
sign-jar ./density-and-buoyancy/density_af.jar
echo "signing ./density-and-buoyancy/buoyancy_el.jar"
sign-jar ./density-and-buoyancy/buoyancy_el.jar
echo "signing ./density-and-buoyancy/density_fa.jar"
sign-jar ./density-and-buoyancy/density_fa.jar
echo "signing ./density-and-buoyancy/buoyancy_ht.jar"
sign-jar ./density-and-buoyancy/buoyancy_ht.jar
echo "signing ./density-and-buoyancy/buoyancy_cs.jar"
sign-jar ./density-and-buoyancy/buoyancy_cs.jar
echo "signing ./density-and-buoyancy/buoyancy_ru.jar"
sign-jar ./density-and-buoyancy/buoyancy_ru.jar
echo "signing ./density-and-buoyancy/density_gl.jar"
sign-jar ./density-and-buoyancy/density_gl.jar
echo "signing ./density-and-buoyancy/buoyancy_bg.jar"
sign-jar ./density-and-buoyancy/buoyancy_bg.jar
echo "signing ./density-and-buoyancy/buoyancy_es_PE.jar"
sign-jar ./density-and-buoyancy/buoyancy_es_PE.jar
echo "signing ./density-and-buoyancy/density_de.jar"
sign-jar ./density-and-buoyancy/density_de.jar
echo "signing ./density-and-buoyancy/buoyancy_es.jar"
sign-jar ./density-and-buoyancy/buoyancy_es.jar
echo "signing ./density-and-buoyancy/density_vi.jar"
sign-jar ./density-and-buoyancy/density_vi.jar
echo "signing ./density-and-buoyancy/density_es_PE.jar"
sign-jar ./density-and-buoyancy/density_es_PE.jar
echo "signing ./density-and-buoyancy/buoyancy_eu.jar"
sign-jar ./density-and-buoyancy/buoyancy_eu.jar
echo "signing ./density-and-buoyancy/buoyancy_sv.jar"
sign-jar ./density-and-buoyancy/buoyancy_sv.jar
echo "signing ./density-and-buoyancy/density_da.jar"
sign-jar ./density-and-buoyancy/density_da.jar
echo "signing ./density-and-buoyancy/buoyancy_af.jar"
sign-jar ./density-and-buoyancy/buoyancy_af.jar
echo "signing ./density-and-buoyancy/buoyancy_hu.jar"
sign-jar ./density-and-buoyancy/buoyancy_hu.jar
echo "signing ./density-and-buoyancy/buoyancy_ko.jar"
sign-jar ./density-and-buoyancy/buoyancy_ko.jar
echo "signing ./density-and-buoyancy/buoyancy_ja.jar"
sign-jar ./density-and-buoyancy/buoyancy_ja.jar
echo "signing ./density-and-buoyancy/density_ar.jar"
sign-jar ./density-and-buoyancy/density_ar.jar
echo "signing ./density-and-buoyancy/buoyancy_sl.jar"
sign-jar ./density-and-buoyancy/buoyancy_sl.jar
echo "signing ./density-and-buoyancy/density_cs.jar"
sign-jar ./density-and-buoyancy/density_cs.jar
echo "signing ./density-and-buoyancy/density_fi.jar"
sign-jar ./density-and-buoyancy/density_fi.jar
echo "signing ./density-and-buoyancy/buoyancy_th.jar"
sign-jar ./density-and-buoyancy/buoyancy_th.jar
echo "signing ./density-and-buoyancy/density_fr.jar"
sign-jar ./density-and-buoyancy/density_fr.jar
echo "signing ./density-and-buoyancy/buoyancy_uk.jar"
sign-jar ./density-and-buoyancy/buoyancy_uk.jar
echo "signing ./density-and-buoyancy/buoyancy_pt_BR.jar"
sign-jar ./density-and-buoyancy/buoyancy_pt_BR.jar
echo "signing ./density-and-buoyancy/density_zh_CN.jar"
sign-jar ./density-and-buoyancy/density_zh_CN.jar
echo "signing ./density-and-buoyancy/buoyancy_sr.jar"
sign-jar ./density-and-buoyancy/buoyancy_sr.jar
echo "signing ./density-and-buoyancy/buoyancy_ta.jar"
sign-jar ./density-and-buoyancy/buoyancy_ta.jar
echo "signing ./density-and-buoyancy/buoyancy_tr.jar"
sign-jar ./density-and-buoyancy/buoyancy_tr.jar
echo "signing ./density-and-buoyancy/buoyancy_gl.jar"
sign-jar ./density-and-buoyancy/buoyancy_gl.jar
echo "signing ./density-and-buoyancy/density_kk.jar"
sign-jar ./density-and-buoyancy/density_kk.jar
echo "signing ./density-and-buoyancy/density_ru.jar"
sign-jar ./density-and-buoyancy/density_ru.jar
echo "signing ./density-and-buoyancy/density_nl.jar"
sign-jar ./density-and-buoyancy/density_nl.jar
echo "signing ./density-and-buoyancy/buoyancy_ku_TR.jar"
sign-jar ./density-and-buoyancy/buoyancy_ku_TR.jar
echo "signing ./density-and-buoyancy/buoyancy_bs.jar"
sign-jar ./density-and-buoyancy/buoyancy_bs.jar
echo "signing ./density-and-buoyancy/density_hu.jar"
sign-jar ./density-and-buoyancy/density_hu.jar
echo "signing ./density-and-buoyancy/density_ka.jar"
sign-jar ./density-and-buoyancy/density_ka.jar
echo "signing ./density-and-buoyancy/density_km.jar"
sign-jar ./density-and-buoyancy/density_km.jar
echo "signing ./density-and-buoyancy/buoyancy_fi.jar"
sign-jar ./density-and-buoyancy/buoyancy_fi.jar
echo "signing ./density-and-buoyancy/buoyancy_pl.jar"
sign-jar ./density-and-buoyancy/buoyancy_pl.jar
echo "signing ./density-and-buoyancy/density_ko.jar"
sign-jar ./density-and-buoyancy/density_ko.jar
echo "signing ./density-and-buoyancy/buoyancy_en.jar"
sign-jar ./density-and-buoyancy/buoyancy_en.jar
echo "signing ./density-and-buoyancy/density_ar_SA.jar"
sign-jar ./density-and-buoyancy/density_ar_SA.jar
echo "signing ./density-and-buoyancy/buoyancy_in.jar"
sign-jar ./density-and-buoyancy/buoyancy_in.jar
echo "signing ./density-and-buoyancy/buoyancy_fr.jar"
sign-jar ./density-and-buoyancy/buoyancy_fr.jar
echo "signing ./density-and-buoyancy/density_is.jar"
sign-jar ./density-and-buoyancy/density_is.jar
echo "signing ./density-and-buoyancy/density_ja.jar"
sign-jar ./density-and-buoyancy/density_ja.jar
echo "signing ./density-and-buoyancy/buoyancy_iw.jar"
sign-jar ./density-and-buoyancy/buoyancy_iw.jar
echo "signing ./density-and-buoyancy/buoyancy_fa.jar"
sign-jar ./density-and-buoyancy/buoyancy_fa.jar
echo "signing ./density-and-buoyancy/density_ht.jar"
sign-jar ./density-and-buoyancy/density_ht.jar
echo "signing ./density-and-buoyancy/density_sv.jar"
sign-jar ./density-and-buoyancy/density_sv.jar
echo "signing ./density-and-buoyancy/buoyancy_de.jar"
sign-jar ./density-and-buoyancy/buoyancy_de.jar
echo "signing ./density-and-buoyancy/buoyancy_ca.jar"
sign-jar ./density-and-buoyancy/buoyancy_ca.jar
echo "signing ./density-and-buoyancy/density_tk.jar"
sign-jar ./density-and-buoyancy/density_tk.jar
echo "signing ./density-and-buoyancy/density_iw.jar"
sign-jar ./density-and-buoyancy/density_iw.jar
echo "signing ./density-and-buoyancy/density_zh_TW.jar"
sign-jar ./density-and-buoyancy/density_zh_TW.jar
echo "signing ./density-and-buoyancy/buoyancy_is.jar"
sign-jar ./density-and-buoyancy/buoyancy_is.jar
echo "signing ./density-and-buoyancy/density_kn.jar"
sign-jar ./density-and-buoyancy/density_kn.jar
echo "signing ./density-and-buoyancy/buoyancy_da.jar"
sign-jar ./density-and-buoyancy/buoyancy_da.jar
echo "signing ./density-and-buoyancy/density_uk.jar"
sign-jar ./density-and-buoyancy/density_uk.jar
echo "signing ./density-and-buoyancy/density_en.jar"
sign-jar ./density-and-buoyancy/density_en.jar
echo "signing ./density-and-buoyancy/buoyancy_ka.jar"
sign-jar ./density-and-buoyancy/buoyancy_ka.jar
echo "signing ./density-and-buoyancy/buoyancy_et.jar"
sign-jar ./density-and-buoyancy/buoyancy_et.jar
echo "signing ./density-and-buoyancy/buoyancy_vi.jar"
sign-jar ./density-and-buoyancy/buoyancy_vi.jar
echo "signing ./density-and-buoyancy/density_pt.jar"
sign-jar ./density-and-buoyancy/density_pt.jar
echo "signing ./density-and-buoyancy/buoyancy_ar.jar"
sign-jar ./density-and-buoyancy/buoyancy_ar.jar
echo "signing ./density-and-buoyancy/density_et.jar"
sign-jar ./density-and-buoyancy/density_et.jar
echo "signing ./nuclear-physics/nuclear-fission_et.jar"
sign-jar ./nuclear-physics/nuclear-fission_et.jar
echo "signing ./nuclear-physics/nuclear-fission_nn.jar"
sign-jar ./nuclear-physics/nuclear-fission_nn.jar
echo "signing ./nuclear-physics/radioactive-dating-game_en.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_en.jar
echo "signing ./nuclear-physics/beta-decay_pt_BR.jar"
sign-jar ./nuclear-physics/beta-decay_pt_BR.jar
echo "signing ./nuclear-physics/nuclear-fission_ja.jar"
sign-jar ./nuclear-physics/nuclear-fission_ja.jar
echo "signing ./nuclear-physics/alpha-decay_eu.jar"
sign-jar ./nuclear-physics/alpha-decay_eu.jar
echo "signing ./nuclear-physics/beta-decay_et.jar"
sign-jar ./nuclear-physics/beta-decay_et.jar
echo "signing ./nuclear-physics/beta-decay_zh_CN.jar"
sign-jar ./nuclear-physics/beta-decay_zh_CN.jar
echo "signing ./nuclear-physics/alpha-decay_de.jar"
sign-jar ./nuclear-physics/alpha-decay_de.jar
echo "signing ./nuclear-physics/alpha-decay_mk.jar"
sign-jar ./nuclear-physics/alpha-decay_mk.jar
echo "signing ./nuclear-physics/alpha-decay_nb.jar"
sign-jar ./nuclear-physics/alpha-decay_nb.jar
echo "signing ./nuclear-physics/alpha-decay_kk.jar"
sign-jar ./nuclear-physics/alpha-decay_kk.jar
echo "signing ./nuclear-physics/alpha-decay_kn.jar"
sign-jar ./nuclear-physics/alpha-decay_kn.jar
echo "signing ./nuclear-physics/nuclear-physics_all_installer.jar"
sign-jar ./nuclear-physics/nuclear-physics_all_installer.jar
echo "signing ./nuclear-physics/radioactive-dating-game_sr.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_sr.jar
echo "signing ./nuclear-physics/beta-decay_en.jar"
sign-jar ./nuclear-physics/beta-decay_en.jar
echo "signing ./nuclear-physics/alpha-decay_gl.jar"
sign-jar ./nuclear-physics/alpha-decay_gl.jar
echo "signing ./nuclear-physics/radioactive-dating-game_eu.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_eu.jar
echo "signing ./nuclear-physics/nuclear-fission_sl.jar"
sign-jar ./nuclear-physics/nuclear-fission_sl.jar
echo "signing ./nuclear-physics/alpha-decay_da.jar"
sign-jar ./nuclear-physics/alpha-decay_da.jar
echo "signing ./nuclear-physics/alpha-decay_zh_CN.jar"
sign-jar ./nuclear-physics/alpha-decay_zh_CN.jar
echo "signing ./nuclear-physics/nuclear-fission_sr.jar"
sign-jar ./nuclear-physics/nuclear-fission_sr.jar
echo "signing ./nuclear-physics/radioactive-dating-game_el.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_el.jar
echo "signing ./nuclear-physics/radioactive-dating-game_es_PE.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_es_PE.jar
echo "signing ./nuclear-physics/beta-decay_nn.jar"
sign-jar ./nuclear-physics/beta-decay_nn.jar
echo "signing ./nuclear-physics/alpha-decay_sk.jar"
sign-jar ./nuclear-physics/alpha-decay_sk.jar
echo "signing ./nuclear-physics/alpha-decay_pt_BR.jar"
sign-jar ./nuclear-physics/alpha-decay_pt_BR.jar
echo "signing ./nuclear-physics/beta-decay_eu.jar"
sign-jar ./nuclear-physics/beta-decay_eu.jar
echo "signing ./nuclear-physics/radioactive-dating-game_es.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_es.jar
echo "signing ./nuclear-physics/beta-decay_fr.jar"
sign-jar ./nuclear-physics/beta-decay_fr.jar
echo "signing ./nuclear-physics/radioactive-dating-game_iw.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_iw.jar
echo "signing ./nuclear-physics/beta-decay_sq.jar"
sign-jar ./nuclear-physics/beta-decay_sq.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ja.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ja.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ko.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ko.jar
echo "signing ./nuclear-physics/nuclear-fission_zh_TW.jar"
sign-jar ./nuclear-physics/nuclear-fission_zh_TW.jar
echo "signing ./nuclear-physics/alpha-decay_bs.jar"
sign-jar ./nuclear-physics/alpha-decay_bs.jar
echo "signing ./nuclear-physics/alpha-decay_vi.jar"
sign-jar ./nuclear-physics/alpha-decay_vi.jar
echo "signing ./nuclear-physics/beta-decay_gl.jar"
sign-jar ./nuclear-physics/beta-decay_gl.jar
echo "signing ./nuclear-physics/nuclear-fission_eu.jar"
sign-jar ./nuclear-physics/nuclear-fission_eu.jar
echo "signing ./nuclear-physics/radioactive-dating-game_bg.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_bg.jar
echo "signing ./nuclear-physics/nuclear-fission_bg.jar"
sign-jar ./nuclear-physics/nuclear-fission_bg.jar
echo "signing ./nuclear-physics/nuclear-fission_gl.jar"
sign-jar ./nuclear-physics/nuclear-fission_gl.jar
echo "signing ./nuclear-physics/alpha-decay_in.jar"
sign-jar ./nuclear-physics/alpha-decay_in.jar
echo "signing ./nuclear-physics/nuclear-fission_in.jar"
sign-jar ./nuclear-physics/nuclear-fission_in.jar
echo "signing ./nuclear-physics/radioactive-dating-game_vi.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_vi.jar
echo "signing ./nuclear-physics/nuclear-fission_tr.jar"
sign-jar ./nuclear-physics/nuclear-fission_tr.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ar.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ar.jar
echo "signing ./nuclear-physics/nuclear-fission_ka.jar"
sign-jar ./nuclear-physics/nuclear-fission_ka.jar
echo "signing ./nuclear-physics/beta-decay_pl.jar"
sign-jar ./nuclear-physics/beta-decay_pl.jar
echo "signing ./nuclear-physics/beta-decay_ca.jar"
sign-jar ./nuclear-physics/beta-decay_ca.jar
echo "signing ./nuclear-physics/alpha-decay_ca.jar"
sign-jar ./nuclear-physics/alpha-decay_ca.jar
echo "signing ./nuclear-physics/nuclear-fission_de.jar"
sign-jar ./nuclear-physics/nuclear-fission_de.jar
echo "signing ./nuclear-physics/radioactive-dating-game_pt.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_pt.jar
echo "signing ./nuclear-physics/radioactive-dating-game_gl.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_gl.jar
echo "signing ./nuclear-physics/alpha-decay_ko.jar"
sign-jar ./nuclear-physics/alpha-decay_ko.jar
echo "signing ./nuclear-physics/beta-decay_es_PE.jar"
sign-jar ./nuclear-physics/beta-decay_es_PE.jar
echo "signing ./nuclear-physics/beta-decay_vi.jar"
sign-jar ./nuclear-physics/beta-decay_vi.jar
echo "signing ./nuclear-physics/nuclear-fission_hr.jar"
sign-jar ./nuclear-physics/nuclear-fission_hr.jar
echo "signing ./nuclear-physics/beta-decay_de.jar"
sign-jar ./nuclear-physics/beta-decay_de.jar
echo "signing ./nuclear-physics/nuclear-fission_hu.jar"
sign-jar ./nuclear-physics/nuclear-fission_hu.jar
echo "signing ./nuclear-physics/radioactive-dating-game_kn.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_kn.jar
echo "signing ./nuclear-physics/nuclear-fission_es_PE.jar"
sign-jar ./nuclear-physics/nuclear-fission_es_PE.jar
echo "signing ./nuclear-physics/alpha-decay_ar.jar"
sign-jar ./nuclear-physics/alpha-decay_ar.jar
echo "signing ./nuclear-physics/radioactive-dating-game_cs.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_cs.jar
echo "signing ./nuclear-physics/nuclear-fission_es.jar"
sign-jar ./nuclear-physics/nuclear-fission_es.jar
echo "signing ./nuclear-physics/beta-decay_it.jar"
sign-jar ./nuclear-physics/beta-decay_it.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ka.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ka.jar
echo "signing ./nuclear-physics/beta-decay_ko.jar"
sign-jar ./nuclear-physics/beta-decay_ko.jar
echo "signing ./nuclear-physics/alpha-decay_cs.jar"
sign-jar ./nuclear-physics/alpha-decay_cs.jar
echo "signing ./nuclear-physics/beta-decay_ja.jar"
sign-jar ./nuclear-physics/beta-decay_ja.jar
echo "signing ./nuclear-physics/beta-decay_cs.jar"
sign-jar ./nuclear-physics/beta-decay_cs.jar
echo "signing ./nuclear-physics/beta-decay_ro.jar"
sign-jar ./nuclear-physics/beta-decay_ro.jar
echo "signing ./nuclear-physics/radioactive-dating-game_be.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_be.jar
echo "signing ./nuclear-physics/radioactive-dating-game_in.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_in.jar
echo "signing ./nuclear-physics/alpha-decay_fr.jar"
sign-jar ./nuclear-physics/alpha-decay_fr.jar
echo "signing ./nuclear-physics/nuclear-fission_fa.jar"
sign-jar ./nuclear-physics/nuclear-fission_fa.jar
echo "signing ./nuclear-physics/beta-decay_iw.jar"
sign-jar ./nuclear-physics/beta-decay_iw.jar
echo "signing ./nuclear-physics/beta-decay_ru.jar"
sign-jar ./nuclear-physics/beta-decay_ru.jar
echo "signing ./nuclear-physics/alpha-decay_hr.jar"
sign-jar ./nuclear-physics/alpha-decay_hr.jar
echo "signing ./nuclear-physics/alpha-decay_ro.jar"
sign-jar ./nuclear-physics/alpha-decay_ro.jar
echo "signing ./nuclear-physics/alpha-decay_sr.jar"
sign-jar ./nuclear-physics/alpha-decay_sr.jar
echo "signing ./nuclear-physics/alpha-decay_bg.jar"
sign-jar ./nuclear-physics/alpha-decay_bg.jar
echo "signing ./nuclear-physics/alpha-decay_fi.jar"
sign-jar ./nuclear-physics/alpha-decay_fi.jar
echo "signing ./nuclear-physics/alpha-decay_ja.jar"
sign-jar ./nuclear-physics/alpha-decay_ja.jar
echo "signing ./nuclear-physics/beta-decay_tk.jar"
sign-jar ./nuclear-physics/beta-decay_tk.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ar_SA.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ar_SA.jar
echo "signing ./nuclear-physics/radioactive-dating-game_fa.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_fa.jar
echo "signing ./nuclear-physics/beta-decay_tr.jar"
sign-jar ./nuclear-physics/beta-decay_tr.jar
echo "signing ./nuclear-physics/nuclear-fission_vi.jar"
sign-jar ./nuclear-physics/nuclear-fission_vi.jar
echo "signing ./nuclear-physics/beta-decay_sv.jar"
sign-jar ./nuclear-physics/beta-decay_sv.jar
echo "signing ./nuclear-physics/nuclear-fission_kn.jar"
sign-jar ./nuclear-physics/nuclear-fission_kn.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ro.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ro.jar
echo "signing ./nuclear-physics/radioactive-dating-game_da.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_da.jar
echo "signing ./nuclear-physics/nuclear-fission_en.jar"
sign-jar ./nuclear-physics/nuclear-fission_en.jar
echo "signing ./nuclear-physics/nuclear-fission_zh_CN.jar"
sign-jar ./nuclear-physics/nuclear-fission_zh_CN.jar
echo "signing ./nuclear-physics/beta-decay_kn.jar"
sign-jar ./nuclear-physics/beta-decay_kn.jar
echo "signing ./nuclear-physics/radioactive-dating-game_hr.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_hr.jar
echo "signing ./nuclear-physics/nuclear-fission_fi.jar"
sign-jar ./nuclear-physics/nuclear-fission_fi.jar
echo "signing ./nuclear-physics/nuclear-fission_bs.jar"
sign-jar ./nuclear-physics/nuclear-fission_bs.jar
echo "signing ./nuclear-physics/radioactive-dating-game_zh_CN.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_zh_CN.jar
echo "signing ./nuclear-physics/nuclear-fission_iw.jar"
sign-jar ./nuclear-physics/nuclear-fission_iw.jar
echo "signing ./nuclear-physics/radioactive-dating-game_tk.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_tk.jar
echo "signing ./nuclear-physics/radioactive-dating-game_de.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_de.jar
echo "signing ./nuclear-physics/beta-decay_ka.jar"
sign-jar ./nuclear-physics/beta-decay_ka.jar
echo "signing ./nuclear-physics/alpha-decay_en.jar"
sign-jar ./nuclear-physics/alpha-decay_en.jar
echo "signing ./nuclear-physics/beta-decay_nl.jar"
sign-jar ./nuclear-physics/beta-decay_nl.jar
echo "signing ./nuclear-physics/radioactive-dating-game_th.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_th.jar
echo "signing ./nuclear-physics/alpha-decay_ht.jar"
sign-jar ./nuclear-physics/alpha-decay_ht.jar
echo "signing ./nuclear-physics/nuclear-fission_ca.jar"
sign-jar ./nuclear-physics/nuclear-fission_ca.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ht.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ht.jar
echo "signing ./nuclear-physics/nuclear-fission_pt_BR.jar"
sign-jar ./nuclear-physics/nuclear-fission_pt_BR.jar
echo "signing ./nuclear-physics/alpha-decay_be.jar"
sign-jar ./nuclear-physics/alpha-decay_be.jar
echo "signing ./nuclear-physics/alpha-decay_zh_TW.jar"
sign-jar ./nuclear-physics/alpha-decay_zh_TW.jar
echo "signing ./nuclear-physics/beta-decay_ht.jar"
sign-jar ./nuclear-physics/beta-decay_ht.jar
echo "signing ./nuclear-physics/alpha-decay_es.jar"
sign-jar ./nuclear-physics/alpha-decay_es.jar
echo "signing ./nuclear-physics/radioactive-dating-game_nl.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_nl.jar
echo "signing ./nuclear-physics/radioactive-dating-game_sk.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_sk.jar
echo "signing ./nuclear-physics/beta-decay_sr.jar"
sign-jar ./nuclear-physics/beta-decay_sr.jar
echo "signing ./nuclear-physics/radioactive-dating-game_nb.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_nb.jar
echo "signing ./nuclear-physics/nuclear-physics_all.jar"
sign-jar ./nuclear-physics/nuclear-physics_all.jar
echo "signing ./nuclear-physics/beta-decay_sl.jar"
sign-jar ./nuclear-physics/beta-decay_sl.jar
echo "signing ./nuclear-physics/nuclear-fission_nl.jar"
sign-jar ./nuclear-physics/nuclear-fission_nl.jar
echo "signing ./nuclear-physics/alpha-decay_nl.jar"
sign-jar ./nuclear-physics/alpha-decay_nl.jar
echo "signing ./nuclear-physics/alpha-decay_sv.jar"
sign-jar ./nuclear-physics/alpha-decay_sv.jar
echo "signing ./nuclear-physics/alpha-decay_et.jar"
sign-jar ./nuclear-physics/alpha-decay_et.jar
echo "signing ./nuclear-physics/alpha-decay_th.jar"
sign-jar ./nuclear-physics/alpha-decay_th.jar
echo "signing ./nuclear-physics/beta-decay_ar.jar"
sign-jar ./nuclear-physics/beta-decay_ar.jar
echo "signing ./nuclear-physics/beta-decay_sk.jar"
sign-jar ./nuclear-physics/beta-decay_sk.jar
echo "signing ./nuclear-physics/radioactive-dating-game_kk.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_kk.jar
echo "signing ./nuclear-physics/alpha-decay_uk.jar"
sign-jar ./nuclear-physics/alpha-decay_uk.jar
echo "signing ./nuclear-physics/beta-decay_pt.jar"
sign-jar ./nuclear-physics/beta-decay_pt.jar
echo "signing ./nuclear-physics/alpha-decay_pl.jar"
sign-jar ./nuclear-physics/alpha-decay_pl.jar
echo "signing ./nuclear-physics/beta-decay_bs.jar"
sign-jar ./nuclear-physics/beta-decay_bs.jar
echo "signing ./nuclear-physics/alpha-decay_fa.jar"
sign-jar ./nuclear-physics/alpha-decay_fa.jar
echo "signing ./nuclear-physics/nuclear-fission_pl.jar"
sign-jar ./nuclear-physics/nuclear-fission_pl.jar
echo "signing ./nuclear-physics/radioactive-dating-game_sl.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_sl.jar
echo "signing ./nuclear-physics/beta-decay_kk.jar"
sign-jar ./nuclear-physics/beta-decay_kk.jar
echo "signing ./nuclear-physics/beta-decay_mk.jar"
sign-jar ./nuclear-physics/beta-decay_mk.jar
echo "signing ./nuclear-physics/nuclear-fission_ro.jar"
sign-jar ./nuclear-physics/nuclear-fission_ro.jar
echo "signing ./nuclear-physics/beta-decay_fa.jar"
sign-jar ./nuclear-physics/beta-decay_fa.jar
echo "signing ./nuclear-physics/nuclear-fission_da.jar"
sign-jar ./nuclear-physics/nuclear-fission_da.jar
echo "signing ./nuclear-physics/radioactive-dating-game_et.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_et.jar
echo "signing ./nuclear-physics/alpha-decay_tk.jar"
sign-jar ./nuclear-physics/alpha-decay_tk.jar
echo "signing ./nuclear-physics/alpha-decay_nn.jar"
sign-jar ./nuclear-physics/alpha-decay_nn.jar
echo "signing ./nuclear-physics/radioactive-dating-game_mk.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_mk.jar
echo "signing ./nuclear-physics/alpha-decay_ar_SA.jar"
sign-jar ./nuclear-physics/alpha-decay_ar_SA.jar
echo "signing ./nuclear-physics/radioactive-dating-game_hu.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_hu.jar
echo "signing ./nuclear-physics/nuclear-fission_ar.jar"
sign-jar ./nuclear-physics/nuclear-fission_ar.jar
echo "signing ./nuclear-physics/nuclear-fission_tk.jar"
sign-jar ./nuclear-physics/nuclear-fission_tk.jar
echo "signing ./nuclear-physics/nuclear-fission_sk.jar"
sign-jar ./nuclear-physics/nuclear-fission_sk.jar
echo "signing ./nuclear-physics/alpha-decay_pt.jar"
sign-jar ./nuclear-physics/alpha-decay_pt.jar
echo "signing ./nuclear-physics/nuclear-fission_ht.jar"
sign-jar ./nuclear-physics/nuclear-fission_ht.jar
echo "signing ./nuclear-physics/nuclear-fission_uk.jar"
sign-jar ./nuclear-physics/nuclear-fission_uk.jar
echo "signing ./nuclear-physics/radioactive-dating-game_fi.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_fi.jar
echo "signing ./nuclear-physics/radioactive-dating-game_zh_TW.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_zh_TW.jar
echo "signing ./nuclear-physics/beta-decay_in.jar"
sign-jar ./nuclear-physics/beta-decay_in.jar
echo "signing ./nuclear-physics/alpha-decay_ru.jar"
sign-jar ./nuclear-physics/alpha-decay_ru.jar
echo "signing ./nuclear-physics/alpha-decay_hu.jar"
sign-jar ./nuclear-physics/alpha-decay_hu.jar
echo "signing ./nuclear-physics/nuclear-fission_th.jar"
sign-jar ./nuclear-physics/nuclear-fission_th.jar
echo "signing ./nuclear-physics/alpha-decay_es_PE.jar"
sign-jar ./nuclear-physics/alpha-decay_es_PE.jar
echo "signing ./nuclear-physics/beta-decay_uk.jar"
sign-jar ./nuclear-physics/beta-decay_uk.jar
echo "signing ./nuclear-physics/beta-decay_fi.jar"
sign-jar ./nuclear-physics/beta-decay_fi.jar
echo "signing ./nuclear-physics/nuclear-fission_be.jar"
sign-jar ./nuclear-physics/nuclear-fission_be.jar
echo "signing ./nuclear-physics/beta-decay_hu.jar"
sign-jar ./nuclear-physics/beta-decay_hu.jar
echo "signing ./nuclear-physics/nuclear-fission_ar_SA.jar"
sign-jar ./nuclear-physics/nuclear-fission_ar_SA.jar
echo "signing ./nuclear-physics/beta-decay_be.jar"
sign-jar ./nuclear-physics/beta-decay_be.jar
echo "signing ./nuclear-physics/beta-decay_bg.jar"
sign-jar ./nuclear-physics/beta-decay_bg.jar
echo "signing ./nuclear-physics/alpha-decay_iw.jar"
sign-jar ./nuclear-physics/alpha-decay_iw.jar
echo "signing ./nuclear-physics/beta-decay_nb.jar"
sign-jar ./nuclear-physics/beta-decay_nb.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ru.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ru.jar
echo "signing ./nuclear-physics/radioactive-dating-game_sq.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_sq.jar
echo "signing ./nuclear-physics/beta-decay_da.jar"
sign-jar ./nuclear-physics/beta-decay_da.jar
echo "signing ./nuclear-physics/radioactive-dating-game_sv.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_sv.jar
echo "signing ./nuclear-physics/alpha-decay_sq.jar"
sign-jar ./nuclear-physics/alpha-decay_sq.jar
echo "signing ./nuclear-physics/radioactive-dating-game_nn.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_nn.jar
echo "signing ./nuclear-physics/radioactive-dating-game_it.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_it.jar
echo "signing ./nuclear-physics/alpha-decay_el.jar"
sign-jar ./nuclear-physics/alpha-decay_el.jar
echo "signing ./nuclear-physics/nuclear-fission_sq.jar"
sign-jar ./nuclear-physics/nuclear-fission_sq.jar
echo "signing ./nuclear-physics/nuclear-fission_nb.jar"
sign-jar ./nuclear-physics/nuclear-fission_nb.jar
echo "signing ./nuclear-physics/nuclear-fission_fr.jar"
sign-jar ./nuclear-physics/nuclear-fission_fr.jar
echo "signing ./nuclear-physics/nuclear-fission_ru.jar"
sign-jar ./nuclear-physics/nuclear-fission_ru.jar
echo "signing ./nuclear-physics/alpha-decay_tr.jar"
sign-jar ./nuclear-physics/alpha-decay_tr.jar
echo "signing ./nuclear-physics/beta-decay_es.jar"
sign-jar ./nuclear-physics/beta-decay_es.jar
echo "signing ./nuclear-physics/radioactive-dating-game_uk.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_uk.jar
echo "signing ./nuclear-physics/alpha-decay_it.jar"
sign-jar ./nuclear-physics/alpha-decay_it.jar
echo "signing ./nuclear-physics/alpha-decay_ka.jar"
sign-jar ./nuclear-physics/alpha-decay_ka.jar
echo "signing ./nuclear-physics/radioactive-dating-game_bs.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_bs.jar
echo "signing ./nuclear-physics/nuclear-fission_el.jar"
sign-jar ./nuclear-physics/nuclear-fission_el.jar
echo "signing ./nuclear-physics/nuclear-fission_mk.jar"
sign-jar ./nuclear-physics/nuclear-fission_mk.jar
echo "signing ./nuclear-physics/beta-decay_ar_SA.jar"
sign-jar ./nuclear-physics/beta-decay_ar_SA.jar
echo "signing ./nuclear-physics/nuclear-fission_sv.jar"
sign-jar ./nuclear-physics/nuclear-fission_sv.jar
echo "signing ./nuclear-physics/beta-decay_el.jar"
sign-jar ./nuclear-physics/beta-decay_el.jar
echo "signing ./nuclear-physics/radioactive-dating-game_pl.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_pl.jar
echo "signing ./nuclear-physics/alpha-decay_sl.jar"
sign-jar ./nuclear-physics/alpha-decay_sl.jar
echo "signing ./nuclear-physics/nuclear-fission_cs.jar"
sign-jar ./nuclear-physics/nuclear-fission_cs.jar
echo "signing ./nuclear-physics/beta-decay_hr.jar"
sign-jar ./nuclear-physics/beta-decay_hr.jar
echo "signing ./nuclear-physics/radioactive-dating-game_pt_BR.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_pt_BR.jar
echo "signing ./nuclear-physics/nuclear-fission_pt.jar"
sign-jar ./nuclear-physics/nuclear-fission_pt.jar
echo "signing ./nuclear-physics/radioactive-dating-game_ca.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_ca.jar
echo "signing ./nuclear-physics/beta-decay_th.jar"
sign-jar ./nuclear-physics/beta-decay_th.jar
echo "signing ./nuclear-physics/nuclear-fission_it.jar"
sign-jar ./nuclear-physics/nuclear-fission_it.jar
echo "signing ./nuclear-physics/beta-decay_zh_TW.jar"
sign-jar ./nuclear-physics/beta-decay_zh_TW.jar
echo "signing ./nuclear-physics/radioactive-dating-game_fr.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_fr.jar
echo "signing ./nuclear-physics/radioactive-dating-game_tr.jar"
sign-jar ./nuclear-physics/radioactive-dating-game_tr.jar
echo "signing ./nuclear-physics/nuclear-fission_kk.jar"
sign-jar ./nuclear-physics/nuclear-fission_kk.jar
echo "signing ./nuclear-physics/nuclear-fission_ko.jar"
sign-jar ./nuclear-physics/nuclear-fission_ko.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_pt.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_pt.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_zh_CN.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_zh_CN.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_fr.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_fr.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_nl.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_nl.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_zh_TW.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_zh_TW.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_tr.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_tr.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_km.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_km.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_mi.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_mi.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_uk.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_uk.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_mr.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_mr.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ta.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ta.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ja.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ja.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_vi.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_vi.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_en.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_en.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_it.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_it.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_gl.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_gl.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_pl.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_pl.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_sq.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_sq.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_de.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_de.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_el.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_el.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_be.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_be.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_hr.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_hr.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ro.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ro.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_mn.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_mn.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_mk.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_mk.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_eu.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_eu.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_bg.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_bg.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ka.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ka.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_hu.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_hu.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_fi.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_fi.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_es_PE.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_es_PE.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ko.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ko.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ku_TR.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ku_TR.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ru.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ru.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_cs.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_cs.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_fa.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_fa.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_all_installer.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_all_installer.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_tk.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_tk.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_sl.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_sl.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_iw.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_iw.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_et.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_et.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_th.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_th.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_sr.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_sr.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_pt_BR.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_pt_BR.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_es.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_es.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_hy.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_hy.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_cy.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_cy.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_sk.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_sk.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_in.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_in.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_da.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_da.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_lv.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_lv.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_all.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_all.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_kn.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_kn.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_bs.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_bs.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_ar.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_ar.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_kk.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_kk.jar
echo "signing ./battery-resistor-circuit/battery-resistor-circuit_sv.jar"
sign-jar ./battery-resistor-circuit/battery-resistor-circuit_sv.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_zh_CN.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_zh_CN.jar
echo "signing ./quantum-wave-interference/davisson-germer_el.jar"
sign-jar ./quantum-wave-interference/davisson-germer_el.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_tr.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_tr.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_hu.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_hu.jar
echo "signing ./quantum-wave-interference/davisson-germer_de.jar"
sign-jar ./quantum-wave-interference/davisson-germer_de.jar
echo "signing ./quantum-wave-interference/davisson-germer_es_PE.jar"
sign-jar ./quantum-wave-interference/davisson-germer_es_PE.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_in.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_in.jar
echo "signing ./quantum-wave-interference/davisson-germer_fr.jar"
sign-jar ./quantum-wave-interference/davisson-germer_fr.jar
echo "signing ./quantum-wave-interference/davisson-germer_ja.jar"
sign-jar ./quantum-wave-interference/davisson-germer_ja.jar
echo "signing ./quantum-wave-interference/davisson-germer_nl.jar"
sign-jar ./quantum-wave-interference/davisson-germer_nl.jar
echo "signing ./quantum-wave-interference/davisson-germer_gl.jar"
sign-jar ./quantum-wave-interference/davisson-germer_gl.jar
echo "signing ./quantum-wave-interference/davisson-germer_pl.jar"
sign-jar ./quantum-wave-interference/davisson-germer_pl.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_el.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_el.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_gl.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_gl.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_pl.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_pl.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_ko.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_ko.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_be.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_be.jar
echo "signing ./quantum-wave-interference/davisson-germer_eu.jar"
sign-jar ./quantum-wave-interference/davisson-germer_eu.jar
echo "signing ./quantum-wave-interference/davisson-germer_iw.jar"
sign-jar ./quantum-wave-interference/davisson-germer_iw.jar
echo "signing ./quantum-wave-interference/davisson-germer_pt.jar"
sign-jar ./quantum-wave-interference/davisson-germer_pt.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_nl.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_nl.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_de.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_de.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_es_PE.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_es_PE.jar
echo "signing ./quantum-wave-interference/davisson-germer_th.jar"
sign-jar ./quantum-wave-interference/davisson-germer_th.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_ka.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_ka.jar
echo "signing ./quantum-wave-interference/davisson-germer_it.jar"
sign-jar ./quantum-wave-interference/davisson-germer_it.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_hr.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_hr.jar
echo "signing ./quantum-wave-interference/davisson-germer_ka.jar"
sign-jar ./quantum-wave-interference/davisson-germer_ka.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_ru.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_ru.jar
echo "signing ./quantum-wave-interference/davisson-germer_mk.jar"
sign-jar ./quantum-wave-interference/davisson-germer_mk.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_zh_TW.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_zh_TW.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_all.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_all.jar
echo "signing ./quantum-wave-interference/davisson-germer_hu.jar"
sign-jar ./quantum-wave-interference/davisson-germer_hu.jar
echo "signing ./quantum-wave-interference/davisson-germer_kk.jar"
sign-jar ./quantum-wave-interference/davisson-germer_kk.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_vi.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_vi.jar
echo "signing ./quantum-wave-interference/davisson-germer_sk.jar"
sign-jar ./quantum-wave-interference/davisson-germer_sk.jar
echo "signing ./quantum-wave-interference/davisson-germer_ru.jar"
sign-jar ./quantum-wave-interference/davisson-germer_ru.jar
echo "signing ./quantum-wave-interference/davisson-germer_hr.jar"
sign-jar ./quantum-wave-interference/davisson-germer_hr.jar
echo "signing ./quantum-wave-interference/davisson-germer_in.jar"
sign-jar ./quantum-wave-interference/davisson-germer_in.jar
echo "signing ./quantum-wave-interference/davisson-germer_zh_TW.jar"
sign-jar ./quantum-wave-interference/davisson-germer_zh_TW.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_it.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_it.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_fa.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_fa.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_mk.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_mk.jar
echo "signing ./quantum-wave-interference/davisson-germer_sr.jar"
sign-jar ./quantum-wave-interference/davisson-germer_sr.jar
echo "signing ./quantum-wave-interference/davisson-germer_ko.jar"
sign-jar ./quantum-wave-interference/davisson-germer_ko.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_fr.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_fr.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_sk.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_sk.jar
echo "signing ./quantum-wave-interference/davisson-germer_zh_CN.jar"
sign-jar ./quantum-wave-interference/davisson-germer_zh_CN.jar
echo "signing ./quantum-wave-interference/davisson-germer_bs.jar"
sign-jar ./quantum-wave-interference/davisson-germer_bs.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_da.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_da.jar
echo "signing ./quantum-wave-interference/davisson-germer_en.jar"
sign-jar ./quantum-wave-interference/davisson-germer_en.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_all_installer.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_all_installer.jar
echo "signing ./quantum-wave-interference/davisson-germer_be.jar"
sign-jar ./quantum-wave-interference/davisson-germer_be.jar
echo "signing ./quantum-wave-interference/davisson-germer_tr.jar"
sign-jar ./quantum-wave-interference/davisson-germer_tr.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_bs.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_bs.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_iw.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_iw.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_tk.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_tk.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_es.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_es.jar
echo "signing ./quantum-wave-interference/davisson-germer_fa.jar"
sign-jar ./quantum-wave-interference/davisson-germer_fa.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_eu.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_eu.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_en.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_en.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_ja.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_ja.jar
echo "signing ./quantum-wave-interference/davisson-germer_tk.jar"
sign-jar ./quantum-wave-interference/davisson-germer_tk.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_pt.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_pt.jar
echo "signing ./quantum-wave-interference/davisson-germer_es.jar"
sign-jar ./quantum-wave-interference/davisson-germer_es.jar
echo "signing ./quantum-wave-interference/davisson-germer_vi.jar"
sign-jar ./quantum-wave-interference/davisson-germer_vi.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_kk.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_kk.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_th.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_th.jar
echo "signing ./quantum-wave-interference/davisson-germer_da.jar"
sign-jar ./quantum-wave-interference/davisson-germer_da.jar
echo "signing ./quantum-wave-interference/quantum-wave-interference_sr.jar"
sign-jar ./quantum-wave-interference/quantum-wave-interference_sr.jar
echo "signing ./conductivity/conductivity_el.jar"
sign-jar ./conductivity/conductivity_el.jar
echo "signing ./conductivity/conductivity_de.jar"
sign-jar ./conductivity/conductivity_de.jar
echo "signing ./conductivity/conductivity_bs.jar"
sign-jar ./conductivity/conductivity_bs.jar
echo "signing ./conductivity/conductivity_ru.jar"
sign-jar ./conductivity/conductivity_ru.jar
echo "signing ./conductivity/conductivity_et.jar"
sign-jar ./conductivity/conductivity_et.jar
echo "signing ./conductivity/conductivity_it.jar"
sign-jar ./conductivity/conductivity_it.jar
echo "signing ./conductivity/conductivity_zh_TW.jar"
sign-jar ./conductivity/conductivity_zh_TW.jar
echo "signing ./conductivity/conductivity_nl.jar"
sign-jar ./conductivity/conductivity_nl.jar
echo "signing ./conductivity/conductivity_pl.jar"
sign-jar ./conductivity/conductivity_pl.jar
echo "signing ./conductivity/conductivity_tk.jar"
sign-jar ./conductivity/conductivity_tk.jar
echo "signing ./conductivity/conductivity_kn.jar"
sign-jar ./conductivity/conductivity_kn.jar
echo "signing ./conductivity/conductivity_ka.jar"
sign-jar ./conductivity/conductivity_ka.jar
echo "signing ./conductivity/conductivity_fa.jar"
sign-jar ./conductivity/conductivity_fa.jar
echo "signing ./conductivity/conductivity_es.jar"
sign-jar ./conductivity/conductivity_es.jar
echo "signing ./conductivity/conductivity_sq.jar"
sign-jar ./conductivity/conductivity_sq.jar
echo "signing ./conductivity/conductivity_in.jar"
sign-jar ./conductivity/conductivity_in.jar
echo "signing ./conductivity/conductivity_kk.jar"
sign-jar ./conductivity/conductivity_kk.jar
echo "signing ./conductivity/conductivity_hy.jar"
sign-jar ./conductivity/conductivity_hy.jar
echo "signing ./conductivity/conductivity_pt_BR.jar"
sign-jar ./conductivity/conductivity_pt_BR.jar
echo "signing ./conductivity/conductivity_ku.jar"
sign-jar ./conductivity/conductivity_ku.jar
echo "signing ./conductivity/conductivity_hu.jar"
sign-jar ./conductivity/conductivity_hu.jar
echo "signing ./conductivity/conductivity_mk.jar"
sign-jar ./conductivity/conductivity_mk.jar
echo "signing ./conductivity/conductivity_pt.jar"
sign-jar ./conductivity/conductivity_pt.jar
echo "signing ./conductivity/conductivity_zh_CN.jar"
sign-jar ./conductivity/conductivity_zh_CN.jar
echo "signing ./conductivity/conductivity_km.jar"
sign-jar ./conductivity/conductivity_km.jar
echo "signing ./conductivity/conductivity_all.jar"
sign-jar ./conductivity/conductivity_all.jar
echo "signing ./conductivity/conductivity_vi.jar"
sign-jar ./conductivity/conductivity_vi.jar
echo "signing ./conductivity/conductivity_gl.jar"
sign-jar ./conductivity/conductivity_gl.jar
echo "signing ./conductivity/conductivity_uk.jar"
sign-jar ./conductivity/conductivity_uk.jar
echo "signing ./conductivity/conductivity_ro.jar"
sign-jar ./conductivity/conductivity_ro.jar
echo "signing ./conductivity/conductivity_cs.jar"
sign-jar ./conductivity/conductivity_cs.jar
echo "signing ./conductivity/conductivity_eu.jar"
sign-jar ./conductivity/conductivity_eu.jar
echo "signing ./conductivity/conductivity_fr.jar"
sign-jar ./conductivity/conductivity_fr.jar
echo "signing ./conductivity/conductivity_da.jar"
sign-jar ./conductivity/conductivity_da.jar
echo "signing ./conductivity/conductivity_ja.jar"
sign-jar ./conductivity/conductivity_ja.jar
echo "signing ./conductivity/conductivity_ar_SA.jar"
sign-jar ./conductivity/conductivity_ar_SA.jar
echo "signing ./conductivity/conductivity_th.jar"
sign-jar ./conductivity/conductivity_th.jar
echo "signing ./conductivity/conductivity_sr.jar"
sign-jar ./conductivity/conductivity_sr.jar
echo "signing ./conductivity/conductivity_ko.jar"
sign-jar ./conductivity/conductivity_ko.jar
echo "signing ./conductivity/conductivity_bg.jar"
sign-jar ./conductivity/conductivity_bg.jar
echo "signing ./conductivity/conductivity_hr.jar"
sign-jar ./conductivity/conductivity_hr.jar
echo "signing ./conductivity/conductivity_all_installer.jar"
sign-jar ./conductivity/conductivity_all_installer.jar
echo "signing ./conductivity/conductivity_iw.jar"
sign-jar ./conductivity/conductivity_iw.jar
echo "signing ./conductivity/conductivity_en.jar"
sign-jar ./conductivity/conductivity_en.jar
echo "signing ./conductivity/conductivity_tr.jar"
sign-jar ./conductivity/conductivity_tr.jar
echo "signing ./conductivity/conductivity_es_PE.jar"
sign-jar ./conductivity/conductivity_es_PE.jar
echo "signing ./conductivity/conductivity_ar.jar"
sign-jar ./conductivity/conductivity_ar.jar
echo "signing ./conductivity/conductivity_ku_TR.jar"
sign-jar ./conductivity/conductivity_ku_TR.jar
echo "signing ./conductivity/conductivity_sv.jar"
sign-jar ./conductivity/conductivity_sv.jar
echo "signing ./conductivity/conductivity_sl.jar"
sign-jar ./conductivity/conductivity_sl.jar
echo "signing ./conductivity/conductivity_sk.jar"
sign-jar ./conductivity/conductivity_sk.jar
echo "signing ./conductivity/conductivity_mr.jar"
sign-jar ./conductivity/conductivity_mr.jar
echo "signing ./phetgraphics-demo/phetgraphics-demo_all_installer.jar"
sign-jar ./phetgraphics-demo/phetgraphics-demo_all_installer.jar
echo "signing ./phetgraphics-demo/phetgraphics-demo_pt.jar"
sign-jar ./phetgraphics-demo/phetgraphics-demo_pt.jar
echo "signing ./phetgraphics-demo/phetgraphics-demo_en.jar"
sign-jar ./phetgraphics-demo/phetgraphics-demo_en.jar
echo "signing ./phetgraphics-demo/phetgraphics-demo_all.jar"
sign-jar ./phetgraphics-demo/phetgraphics-demo_all.jar
echo "signing ./travoltage/travoltage_zh_TW.jar"
sign-jar ./travoltage/travoltage_zh_TW.jar
echo "signing ./travoltage/travoltage_ko.jar"
sign-jar ./travoltage/travoltage_ko.jar
echo "signing ./travoltage/travoltage_it.jar"
sign-jar ./travoltage/travoltage_it.jar
echo "signing ./travoltage/travoltage_sl.jar"
sign-jar ./travoltage/travoltage_sl.jar
echo "signing ./travoltage/travoltage_bs.jar"
sign-jar ./travoltage/travoltage_bs.jar
echo "signing ./travoltage/travoltage_sk.jar"
sign-jar ./travoltage/travoltage_sk.jar
echo "signing ./travoltage/travoltage_zh_CN.jar"
sign-jar ./travoltage/travoltage_zh_CN.jar
echo "signing ./travoltage/travoltage_ru.jar"
sign-jar ./travoltage/travoltage_ru.jar
echo "signing ./travoltage/travoltage_all_installer.jar"
sign-jar ./travoltage/travoltage_all_installer.jar
echo "signing ./travoltage/travoltage_es.jar"
sign-jar ./travoltage/travoltage_es.jar
echo "signing ./travoltage/travoltage_iw.jar"
sign-jar ./travoltage/travoltage_iw.jar
echo "signing ./travoltage/travoltage_fi.jar"
sign-jar ./travoltage/travoltage_fi.jar
echo "signing ./travoltage/travoltage_es_PE.jar"
sign-jar ./travoltage/travoltage_es_PE.jar
echo "signing ./travoltage/travoltage_km.jar"
sign-jar ./travoltage/travoltage_km.jar
echo "signing ./travoltage/travoltage_fa.jar"
sign-jar ./travoltage/travoltage_fa.jar
echo "signing ./travoltage/travoltage_be.jar"
sign-jar ./travoltage/travoltage_be.jar
echo "signing ./travoltage/travoltage_tr.jar"
sign-jar ./travoltage/travoltage_tr.jar
echo "signing ./travoltage/travoltage_ku_TR.jar"
sign-jar ./travoltage/travoltage_ku_TR.jar
echo "signing ./travoltage/travoltage_fr.jar"
sign-jar ./travoltage/travoltage_fr.jar
echo "signing ./travoltage/travoltage_ar.jar"
sign-jar ./travoltage/travoltage_ar.jar
echo "signing ./travoltage/travoltage_tk.jar"
sign-jar ./travoltage/travoltage_tk.jar
echo "signing ./travoltage/travoltage_hy.jar"
sign-jar ./travoltage/travoltage_hy.jar
echo "signing ./travoltage/travoltage_sv.jar"
sign-jar ./travoltage/travoltage_sv.jar
echo "signing ./travoltage/travoltage_kk.jar"
sign-jar ./travoltage/travoltage_kk.jar
echo "signing ./travoltage/travoltage_da.jar"
sign-jar ./travoltage/travoltage_da.jar
echo "signing ./travoltage/travoltage_pt_BR.jar"
sign-jar ./travoltage/travoltage_pt_BR.jar
echo "signing ./travoltage/travoltage_uk.jar"
sign-jar ./travoltage/travoltage_uk.jar
echo "signing ./travoltage/travoltage_pt.jar"
sign-jar ./travoltage/travoltage_pt.jar
echo "signing ./travoltage/travoltage_pl.jar"
sign-jar ./travoltage/travoltage_pl.jar
echo "signing ./travoltage/travoltage_sr.jar"
sign-jar ./travoltage/travoltage_sr.jar
echo "signing ./travoltage/travoltage_in.jar"
sign-jar ./travoltage/travoltage_in.jar
echo "signing ./travoltage/travoltage_ro.jar"
sign-jar ./travoltage/travoltage_ro.jar
echo "signing ./travoltage/travoltage_mk.jar"
sign-jar ./travoltage/travoltage_mk.jar
echo "signing ./travoltage/travoltage_hr.jar"
sign-jar ./travoltage/travoltage_hr.jar
echo "signing ./travoltage/travoltage_gl.jar"
sign-jar ./travoltage/travoltage_gl.jar
echo "signing ./travoltage/travoltage_bg.jar"
sign-jar ./travoltage/travoltage_bg.jar
echo "signing ./travoltage/travoltage_eu.jar"
sign-jar ./travoltage/travoltage_eu.jar
echo "signing ./travoltage/travoltage_ja.jar"
sign-jar ./travoltage/travoltage_ja.jar
echo "signing ./travoltage/travoltage_vi.jar"
sign-jar ./travoltage/travoltage_vi.jar
echo "signing ./travoltage/travoltage_hu.jar"
sign-jar ./travoltage/travoltage_hu.jar
echo "signing ./travoltage/travoltage_de.jar"
sign-jar ./travoltage/travoltage_de.jar
echo "signing ./travoltage/travoltage_all.jar"
sign-jar ./travoltage/travoltage_all.jar
echo "signing ./travoltage/travoltage_et.jar"
sign-jar ./travoltage/travoltage_et.jar
echo "signing ./travoltage/travoltage_el.jar"
sign-jar ./travoltage/travoltage_el.jar
echo "signing ./travoltage/travoltage_nl.jar"
sign-jar ./travoltage/travoltage_nl.jar
echo "signing ./travoltage/travoltage_en.jar"
sign-jar ./travoltage/travoltage_en.jar
echo "signing ./travoltage/travoltage_th.jar"
sign-jar ./travoltage/travoltage_th.jar
echo "signing ./radio-waves/radio-waves_hr.jar"
sign-jar ./radio-waves/radio-waves_hr.jar
echo "signing ./radio-waves/radio-waves_nl.jar"
sign-jar ./radio-waves/radio-waves_nl.jar
echo "signing ./radio-waves/radio-waves_el.jar"
sign-jar ./radio-waves/radio-waves_el.jar
echo "signing ./radio-waves/radio-waves_gl.jar"
sign-jar ./radio-waves/radio-waves_gl.jar
echo "signing ./radio-waves/radio-waves_pl.jar"
sign-jar ./radio-waves/radio-waves_pl.jar
echo "signing ./radio-waves/radio-waves_pt_BR.jar"
sign-jar ./radio-waves/radio-waves_pt_BR.jar
echo "signing ./radio-waves/radio-waves_eu.jar"
sign-jar ./radio-waves/radio-waves_eu.jar
echo "signing ./radio-waves/radio-waves_fi.jar"
sign-jar ./radio-waves/radio-waves_fi.jar
echo "signing ./radio-waves/radio-waves_bs.jar"
sign-jar ./radio-waves/radio-waves_bs.jar
echo "signing ./radio-waves/radio-waves_km.jar"
sign-jar ./radio-waves/radio-waves_km.jar
echo "signing ./radio-waves/radio-waves_ar_SA.jar"
sign-jar ./radio-waves/radio-waves_ar_SA.jar
echo "signing ./radio-waves/radio-waves_ro.jar"
sign-jar ./radio-waves/radio-waves_ro.jar
echo "signing ./radio-waves/radio-waves_th.jar"
sign-jar ./radio-waves/radio-waves_th.jar
echo "signing ./radio-waves/radio-waves_en.jar"
sign-jar ./radio-waves/radio-waves_en.jar
echo "signing ./radio-waves/radio-waves_be.jar"
sign-jar ./radio-waves/radio-waves_be.jar
echo "signing ./radio-waves/radio-waves_zh_CN.jar"
sign-jar ./radio-waves/radio-waves_zh_CN.jar
echo "signing ./radio-waves/radio-waves_ru.jar"
sign-jar ./radio-waves/radio-waves_ru.jar
echo "signing ./radio-waves/radio-waves_ka.jar"
sign-jar ./radio-waves/radio-waves_ka.jar
echo "signing ./radio-waves/radio-waves_sv.jar"
sign-jar ./radio-waves/radio-waves_sv.jar
echo "signing ./radio-waves/radio-waves_tk.jar"
sign-jar ./radio-waves/radio-waves_tk.jar
echo "signing ./radio-waves/radio-waves_all.jar"
sign-jar ./radio-waves/radio-waves_all.jar
echo "signing ./radio-waves/radio-waves_ca.jar"
sign-jar ./radio-waves/radio-waves_ca.jar
echo "signing ./radio-waves/radio-waves_in.jar"
sign-jar ./radio-waves/radio-waves_in.jar
echo "signing ./radio-waves/radio-waves_all_installer.jar"
sign-jar ./radio-waves/radio-waves_all_installer.jar
echo "signing ./radio-waves/radio-waves_de.jar"
sign-jar ./radio-waves/radio-waves_de.jar
echo "signing ./radio-waves/radio-waves_sk.jar"
sign-jar ./radio-waves/radio-waves_sk.jar
echo "signing ./radio-waves/radio-waves_iw.jar"
sign-jar ./radio-waves/radio-waves_iw.jar
echo "signing ./radio-waves/radio-waves_hu.jar"
sign-jar ./radio-waves/radio-waves_hu.jar
echo "signing ./radio-waves/radio-waves_ja.jar"
sign-jar ./radio-waves/radio-waves_ja.jar
echo "signing ./radio-waves/radio-waves_es_PE.jar"
sign-jar ./radio-waves/radio-waves_es_PE.jar
echo "signing ./radio-waves/radio-waves_kk.jar"
sign-jar ./radio-waves/radio-waves_kk.jar
echo "signing ./radio-waves/radio-waves_fa.jar"
sign-jar ./radio-waves/radio-waves_fa.jar
echo "signing ./radio-waves/radio-waves_fr.jar"
sign-jar ./radio-waves/radio-waves_fr.jar
echo "signing ./radio-waves/radio-waves_tr.jar"
sign-jar ./radio-waves/radio-waves_tr.jar
echo "signing ./radio-waves/radio-waves_cs.jar"
sign-jar ./radio-waves/radio-waves_cs.jar
echo "signing ./radio-waves/radio-waves_mr.jar"
sign-jar ./radio-waves/radio-waves_mr.jar
echo "signing ./radio-waves/radio-waves_ar.jar"
sign-jar ./radio-waves/radio-waves_ar.jar
echo "signing ./radio-waves/radio-waves_da.jar"
sign-jar ./radio-waves/radio-waves_da.jar
echo "signing ./radio-waves/radio-waves_ko.jar"
sign-jar ./radio-waves/radio-waves_ko.jar
echo "signing ./radio-waves/radio-waves_vi.jar"
sign-jar ./radio-waves/radio-waves_vi.jar
echo "signing ./radio-waves/radio-waves_pt.jar"
sign-jar ./radio-waves/radio-waves_pt.jar
echo "signing ./radio-waves/radio-waves_it.jar"
sign-jar ./radio-waves/radio-waves_it.jar
echo "signing ./radio-waves/radio-waves_es.jar"
sign-jar ./radio-waves/radio-waves_es.jar
echo "signing ./radio-waves/radio-waves_sr.jar"
sign-jar ./radio-waves/radio-waves_sr.jar
echo "signing ./radio-waves/radio-waves_mk.jar"
sign-jar ./radio-waves/radio-waves_mk.jar
echo "signing ./radio-waves/radio-waves_zh_TW.jar"
sign-jar ./radio-waves/radio-waves_zh_TW.jar
echo "signing ./natural-selection/natural-selection_eu.jar"
sign-jar ./natural-selection/natural-selection_eu.jar
echo "signing ./natural-selection/natural-selection_kk.jar"
sign-jar ./natural-selection/natural-selection_kk.jar
echo "signing ./natural-selection/natural-selection_fa.jar"
sign-jar ./natural-selection/natural-selection_fa.jar
echo "signing ./natural-selection/natural-selection_sv.jar"
sign-jar ./natural-selection/natural-selection_sv.jar
echo "signing ./natural-selection/natural-selection_th.jar"
sign-jar ./natural-selection/natural-selection_th.jar
echo "signing ./natural-selection/natural-selection_vi.jar"
sign-jar ./natural-selection/natural-selection_vi.jar
echo "signing ./natural-selection/natural-selection_et.jar"
sign-jar ./natural-selection/natural-selection_et.jar
echo "signing ./natural-selection/natural-selection_ko.jar"
sign-jar ./natural-selection/natural-selection_ko.jar
echo "signing ./natural-selection/natural-selection_cs.jar"
sign-jar ./natural-selection/natural-selection_cs.jar
echo "signing ./natural-selection/natural-selection_zh_CN.jar"
sign-jar ./natural-selection/natural-selection_zh_CN.jar
echo "signing ./natural-selection/natural-selection_mk.jar"
sign-jar ./natural-selection/natural-selection_mk.jar
echo "signing ./natural-selection/natural-selection_tr.jar"
sign-jar ./natural-selection/natural-selection_tr.jar
echo "signing ./natural-selection/natural-selection_pt_BR.jar"
sign-jar ./natural-selection/natural-selection_pt_BR.jar
echo "signing ./natural-selection/natural-selection_all.jar"
sign-jar ./natural-selection/natural-selection_all.jar
echo "signing ./natural-selection/natural-selection_ru.jar"
sign-jar ./natural-selection/natural-selection_ru.jar
echo "signing ./natural-selection/natural-selection_es_PE.jar"
sign-jar ./natural-selection/natural-selection_es_PE.jar
echo "signing ./natural-selection/natural-selection_en.jar"
sign-jar ./natural-selection/natural-selection_en.jar
echo "signing ./natural-selection/natural-selection_el.jar"
sign-jar ./natural-selection/natural-selection_el.jar
echo "signing ./natural-selection/natural-selection_ka.jar"
sign-jar ./natural-selection/natural-selection_ka.jar
echo "signing ./natural-selection/natural-selection_nl.jar"
sign-jar ./natural-selection/natural-selection_nl.jar
echo "signing ./natural-selection/natural-selection_pl.jar"
sign-jar ./natural-selection/natural-selection_pl.jar
echo "signing ./natural-selection/natural-selection_sr.jar"
sign-jar ./natural-selection/natural-selection_sr.jar
echo "signing ./natural-selection/natural-selection_nb.jar"
sign-jar ./natural-selection/natural-selection_nb.jar
echo "signing ./natural-selection/natural-selection_hu.jar"
sign-jar ./natural-selection/natural-selection_hu.jar
echo "signing ./natural-selection/natural-selection_all_installer.jar"
sign-jar ./natural-selection/natural-selection_all_installer.jar
echo "signing ./natural-selection/natural-selection_tk.jar"
sign-jar ./natural-selection/natural-selection_tk.jar
echo "signing ./natural-selection/natural-selection_fr.jar"
sign-jar ./natural-selection/natural-selection_fr.jar
echo "signing ./natural-selection/natural-selection_iw.jar"
sign-jar ./natural-selection/natural-selection_iw.jar
echo "signing ./natural-selection/natural-selection_it.jar"
sign-jar ./natural-selection/natural-selection_it.jar
echo "signing ./natural-selection/natural-selection_es.jar"
sign-jar ./natural-selection/natural-selection_es.jar
echo "signing ./natural-selection/natural-selection_sk.jar"
sign-jar ./natural-selection/natural-selection_sk.jar
echo "signing ./natural-selection/natural-selection_zh_TW.jar"
sign-jar ./natural-selection/natural-selection_zh_TW.jar
echo "signing ./natural-selection/natural-selection_da.jar"
sign-jar ./natural-selection/natural-selection_da.jar
echo "signing ./natural-selection/natural-selection_de.jar"
sign-jar ./natural-selection/natural-selection_de.jar
echo "signing ./natural-selection/natural-selection_lt.jar"
sign-jar ./natural-selection/natural-selection_lt.jar
echo "signing ./natural-selection/natural-selection_ja.jar"
sign-jar ./natural-selection/natural-selection_ja.jar
echo "signing ./natural-selection/natural-selection_sq.jar"
sign-jar ./natural-selection/natural-selection_sq.jar
echo "signing ./natural-selection/natural-selection_ca.jar"
sign-jar ./natural-selection/natural-selection_ca.jar
echo "signing ./natural-selection/natural-selection_km.jar"
sign-jar ./natural-selection/natural-selection_km.jar
echo "signing ./natural-selection/natural-selection_nn.jar"
sign-jar ./natural-selection/natural-selection_nn.jar
echo "signing ./forces-1d/forces-1d_de.jar"
sign-jar ./forces-1d/forces-1d_de.jar
echo "signing ./forces-1d/forces-1d_tr.jar"
sign-jar ./forces-1d/forces-1d_tr.jar
echo "signing ./forces-1d/forces-1d_af.jar"
sign-jar ./forces-1d/forces-1d_af.jar
echo "signing ./forces-1d/forces-1d_ko.jar"
sign-jar ./forces-1d/forces-1d_ko.jar
echo "signing ./forces-1d/forces-1d_nl.jar"
sign-jar ./forces-1d/forces-1d_nl.jar
echo "signing ./forces-1d/forces-1d_sr.jar"
sign-jar ./forces-1d/forces-1d_sr.jar
echo "signing ./forces-1d/forces-1d_ar.jar"
sign-jar ./forces-1d/forces-1d_ar.jar
echo "signing ./forces-1d/forces-1d_es_CO.jar"
sign-jar ./forces-1d/forces-1d_es_CO.jar
echo "signing ./forces-1d/forces-1d_pl.jar"
sign-jar ./forces-1d/forces-1d_pl.jar
echo "signing ./forces-1d/forces-1d_fa.jar"
sign-jar ./forces-1d/forces-1d_fa.jar
echo "signing ./forces-1d/forces-1d_es_PE.jar"
sign-jar ./forces-1d/forces-1d_es_PE.jar
echo "signing ./forces-1d/forces-1d_pt_BR.jar"
sign-jar ./forces-1d/forces-1d_pt_BR.jar
echo "signing ./forces-1d/forces-1d_fr.jar"
sign-jar ./forces-1d/forces-1d_fr.jar
echo "signing ./forces-1d/forces-1d_vi.jar"
sign-jar ./forces-1d/forces-1d_vi.jar
echo "signing ./forces-1d/forces-1d_cs.jar"
sign-jar ./forces-1d/forces-1d_cs.jar
echo "signing ./forces-1d/forces-1d_ms.jar"
sign-jar ./forces-1d/forces-1d_ms.jar
echo "signing ./forces-1d/forces-1d_it.jar"
sign-jar ./forces-1d/forces-1d_it.jar
echo "signing ./forces-1d/forces-1d_bs.jar"
sign-jar ./forces-1d/forces-1d_bs.jar
echo "signing ./forces-1d/forces-1d_ca.jar"
sign-jar ./forces-1d/forces-1d_ca.jar
echo "signing ./forces-1d/forces-1d_et.jar"
sign-jar ./forces-1d/forces-1d_et.jar
echo "signing ./forces-1d/forces-1d_all_installer.jar"
sign-jar ./forces-1d/forces-1d_all_installer.jar
echo "signing ./forces-1d/forces-1d_da.jar"
sign-jar ./forces-1d/forces-1d_da.jar
echo "signing ./forces-1d/forces-1d_sv.jar"
sign-jar ./forces-1d/forces-1d_sv.jar
echo "signing ./forces-1d/forces-1d_ja.jar"
sign-jar ./forces-1d/forces-1d_ja.jar
echo "signing ./forces-1d/forces-1d_mk.jar"
sign-jar ./forces-1d/forces-1d_mk.jar
echo "signing ./forces-1d/forces-1d_all.jar"
sign-jar ./forces-1d/forces-1d_all.jar
echo "signing ./forces-1d/forces-1d_km.jar"
sign-jar ./forces-1d/forces-1d_km.jar
echo "signing ./forces-1d/forces-1d_en.jar"
sign-jar ./forces-1d/forces-1d_en.jar
echo "signing ./forces-1d/forces-1d_th.jar"
sign-jar ./forces-1d/forces-1d_th.jar
echo "signing ./forces-1d/forces-1d_es.jar"
sign-jar ./forces-1d/forces-1d_es.jar
echo "signing ./forces-1d/forces-1d_uk.jar"
sign-jar ./forces-1d/forces-1d_uk.jar
echo "signing ./forces-1d/forces-1d_zh_CN.jar"
sign-jar ./forces-1d/forces-1d_zh_CN.jar
echo "signing ./forces-1d/forces-1d_sk.jar"
sign-jar ./forces-1d/forces-1d_sk.jar
echo "signing ./forces-1d/forces-1d_mr.jar"
sign-jar ./forces-1d/forces-1d_mr.jar
echo "signing ./forces-1d/forces-1d_eu.jar"
sign-jar ./forces-1d/forces-1d_eu.jar
echo "signing ./forces-1d/forces-1d_tk.jar"
sign-jar ./forces-1d/forces-1d_tk.jar
echo "signing ./forces-1d/forces-1d_hr.jar"
sign-jar ./forces-1d/forces-1d_hr.jar
echo "signing ./forces-1d/forces-1d_in.jar"
sign-jar ./forces-1d/forces-1d_in.jar
echo "signing ./forces-1d/forces-1d_hu.jar"
sign-jar ./forces-1d/forces-1d_hu.jar
echo "signing ./forces-1d/forces-1d_kn.jar"
sign-jar ./forces-1d/forces-1d_kn.jar
echo "signing ./forces-1d/forces-1d_sl.jar"
sign-jar ./forces-1d/forces-1d_sl.jar
echo "signing ./forces-1d/forces-1d_gl.jar"
sign-jar ./forces-1d/forces-1d_gl.jar
echo "signing ./forces-1d/forces-1d_zh_TW.jar"
sign-jar ./forces-1d/forces-1d_zh_TW.jar
echo "signing ./forces-1d/forces-1d_el.jar"
sign-jar ./forces-1d/forces-1d_el.jar
echo "signing ./forces-1d/forces-1d_iw.jar"
sign-jar ./forces-1d/forces-1d_iw.jar
echo "signing ./forces-1d/forces-1d_ku_TR.jar"
sign-jar ./forces-1d/forces-1d_ku_TR.jar
echo "signing ./forces-1d/forces-1d_pt.jar"
sign-jar ./forces-1d/forces-1d_pt.jar
echo "signing ./mvc-example/mvc-example_all.jar"
sign-jar ./mvc-example/mvc-example_all.jar
echo "signing ./mvc-example/mvc-example_pt.jar"
sign-jar ./mvc-example/mvc-example_pt.jar
echo "signing ./mvc-example/mvc-example_all_installer.jar"
sign-jar ./mvc-example/mvc-example_all_installer.jar
echo "signing ./mvc-example/mvc-example_en.jar"
sign-jar ./mvc-example/mvc-example_en.jar
echo "signing ./test-java-project/sim1_eu.jar"
sign-jar ./test-java-project/sim1_eu.jar
echo "signing ./test-java-project/test-java-project_all.jar"
sign-jar ./test-java-project/test-java-project_all.jar
echo "signing ./test-java-project/sim1_en.jar"
sign-jar ./test-java-project/sim1_en.jar
echo "signing ./test-java-project/sim1_in.jar"
sign-jar ./test-java-project/sim1_in.jar
echo "signing ./test-java-project/sim1_bs.jar"
sign-jar ./test-java-project/sim1_bs.jar
echo "signing ./test-java-project/sim1_ar.jar"
sign-jar ./test-java-project/sim1_ar.jar
echo "signing ./test-java-project/sim1_el.jar"
sign-jar ./test-java-project/sim1_el.jar
echo "signing ./test-java-project/sim1_fr.jar"
sign-jar ./test-java-project/sim1_fr.jar
echo "signing ./test-java-project/test-java-project_all_installer.jar"
sign-jar ./test-java-project/test-java-project_all_installer.jar
echo "signing ./test-java-project/sim1_hu.jar"
sign-jar ./test-java-project/sim1_hu.jar
echo "signing ./test-java-project/sim1_ka.jar"
sign-jar ./test-java-project/sim1_ka.jar
echo "signing ./test-java-project/sim1_gl.jar"
sign-jar ./test-java-project/sim1_gl.jar
echo "signing ./test-java-project/sim1_it.jar"
sign-jar ./test-java-project/sim1_it.jar
echo "signing ./test-java-project/sim1_zh_TW.jar"
sign-jar ./test-java-project/sim1_zh_TW.jar
echo "signing ./test-java-project/sim1_km.jar"
sign-jar ./test-java-project/sim1_km.jar
echo "signing ./test-java-project/sim1_fa.jar"
sign-jar ./test-java-project/sim1_fa.jar
echo "signing ./test-java-project/sim1_da.jar"
sign-jar ./test-java-project/sim1_da.jar
echo "signing ./test-java-project/sim1_de.jar"
sign-jar ./test-java-project/sim1_de.jar
echo "signing ./test-java-project/sim1_iw.jar"
sign-jar ./test-java-project/sim1_iw.jar
echo "signing ./test-java-project/sim1_es.jar"
sign-jar ./test-java-project/sim1_es.jar
echo "signing ./test-java-project/sim1_ak.jar"
sign-jar ./test-java-project/sim1_ak.jar
echo "signing ./charges-and-fields-scala/charges-and-fields-scala_en.jar"
sign-jar ./charges-and-fields-scala/charges-and-fields-scala_en.jar
echo "signing ./charges-and-fields-scala/charges-and-fields-scala_all_installer.jar"
sign-jar ./charges-and-fields-scala/charges-and-fields-scala_all_installer.jar
echo "signing ./charges-and-fields-scala/charges-and-fields-scala_all.jar"
sign-jar ./charges-and-fields-scala/charges-and-fields-scala_all.jar
echo "signing ./collision-lab/collision-lab_nb.jar"
sign-jar ./collision-lab/collision-lab_nb.jar
echo "signing ./collision-lab/collision-lab_fa.jar"
sign-jar ./collision-lab/collision-lab_fa.jar
echo "signing ./collision-lab/collision-lab_eu.jar"
sign-jar ./collision-lab/collision-lab_eu.jar
echo "signing ./collision-lab/collision-lab_es.jar"
sign-jar ./collision-lab/collision-lab_es.jar
echo "signing ./collision-lab/collision-lab_mr.jar"
sign-jar ./collision-lab/collision-lab_mr.jar
echo "signing ./collision-lab/collision-lab_in.jar"
sign-jar ./collision-lab/collision-lab_in.jar
echo "signing ./collision-lab/collision-lab_pl.jar"
sign-jar ./collision-lab/collision-lab_pl.jar
echo "signing ./collision-lab/collision-lab_ht.jar"
sign-jar ./collision-lab/collision-lab_ht.jar
echo "signing ./collision-lab/collision-lab_mk.jar"
sign-jar ./collision-lab/collision-lab_mk.jar
echo "signing ./collision-lab/collision-lab_th.jar"
sign-jar ./collision-lab/collision-lab_th.jar
echo "signing ./collision-lab/collision-lab_pt_BR.jar"
sign-jar ./collision-lab/collision-lab_pt_BR.jar
echo "signing ./collision-lab/collision-lab_de.jar"
sign-jar ./collision-lab/collision-lab_de.jar
echo "signing ./collision-lab/collision-lab_hu.jar"
sign-jar ./collision-lab/collision-lab_hu.jar
echo "signing ./collision-lab/collision-lab_gl.jar"
sign-jar ./collision-lab/collision-lab_gl.jar
echo "signing ./collision-lab/collision-lab_nl.jar"
sign-jar ./collision-lab/collision-lab_nl.jar
echo "signing ./collision-lab/collision-lab_cy.jar"
sign-jar ./collision-lab/collision-lab_cy.jar
echo "signing ./collision-lab/collision-lab_ku_TR.jar"
sign-jar ./collision-lab/collision-lab_ku_TR.jar
echo "signing ./collision-lab/collision-lab_ar.jar"
sign-jar ./collision-lab/collision-lab_ar.jar
echo "signing ./collision-lab/collision-lab_ko.jar"
sign-jar ./collision-lab/collision-lab_ko.jar
echo "signing ./collision-lab/collision-lab_kn.jar"
sign-jar ./collision-lab/collision-lab_kn.jar
echo "signing ./collision-lab/collision-lab_tr.jar"
sign-jar ./collision-lab/collision-lab_tr.jar
echo "signing ./collision-lab/collision-lab_ru.jar"
sign-jar ./collision-lab/collision-lab_ru.jar
echo "signing ./collision-lab/collision-lab_zh_CN.jar"
sign-jar ./collision-lab/collision-lab_zh_CN.jar
echo "signing ./collision-lab/collision-lab_et.jar"
sign-jar ./collision-lab/collision-lab_et.jar
echo "signing ./collision-lab/collision-lab_iw.jar"
sign-jar ./collision-lab/collision-lab_iw.jar
echo "signing ./collision-lab/collision-lab_ja.jar"
sign-jar ./collision-lab/collision-lab_ja.jar
echo "signing ./collision-lab/collision-lab_sk.jar"
sign-jar ./collision-lab/collision-lab_sk.jar
echo "signing ./collision-lab/collision-lab_en.jar"
sign-jar ./collision-lab/collision-lab_en.jar
echo "signing ./collision-lab/collision-lab_ms.jar"
sign-jar ./collision-lab/collision-lab_ms.jar
echo "signing ./collision-lab/collision-lab_ta.jar"
sign-jar ./collision-lab/collision-lab_ta.jar
echo "signing ./collision-lab/collision-lab_it.jar"
sign-jar ./collision-lab/collision-lab_it.jar
echo "signing ./collision-lab/collision-lab_es_PE.jar"
sign-jar ./collision-lab/collision-lab_es_PE.jar
echo "signing ./collision-lab/collision-lab_da.jar"
sign-jar ./collision-lab/collision-lab_da.jar
echo "signing ./collision-lab/collision-lab_cs.jar"
sign-jar ./collision-lab/collision-lab_cs.jar
echo "signing ./collision-lab/collision-lab_vi.jar"
sign-jar ./collision-lab/collision-lab_vi.jar
echo "signing ./collision-lab/collision-lab_tk.jar"
sign-jar ./collision-lab/collision-lab_tk.jar
echo "signing ./collision-lab/collision-lab_km.jar"
sign-jar ./collision-lab/collision-lab_km.jar
echo "signing ./collision-lab/collision-lab_sv.jar"
sign-jar ./collision-lab/collision-lab_sv.jar
echo "signing ./collision-lab/collision-lab_fr.jar"
sign-jar ./collision-lab/collision-lab_fr.jar
echo "signing ./collision-lab/collision-lab_zh_TW.jar"
sign-jar ./collision-lab/collision-lab_zh_TW.jar
echo "signing ./collision-lab/collision-lab_el.jar"
sign-jar ./collision-lab/collision-lab_el.jar
echo "signing ./collision-lab/collision-lab_be.jar"
sign-jar ./collision-lab/collision-lab_be.jar
echo "signing ./collision-lab/collision-lab_fi.jar"
sign-jar ./collision-lab/collision-lab_fi.jar
echo "signing ./collision-lab/collision-lab_sr.jar"
sign-jar ./collision-lab/collision-lab_sr.jar
echo "signing ./collision-lab/collision-lab_bs.jar"
sign-jar ./collision-lab/collision-lab_bs.jar
echo "signing ./soluble-salts/soluble-salts_tk.jar"
sign-jar ./soluble-salts/soluble-salts_tk.jar
echo "signing ./soluble-salts/soluble-salts_cs.jar"
sign-jar ./soluble-salts/soluble-salts_cs.jar
echo "signing ./soluble-salts/soluble-salts_pt_BR.jar"
sign-jar ./soluble-salts/soluble-salts_pt_BR.jar
echo "signing ./soluble-salts/soluble-salts_iw.jar"
sign-jar ./soluble-salts/soluble-salts_iw.jar
echo "signing ./soluble-salts/soluble-salts_hu.jar"
sign-jar ./soluble-salts/soluble-salts_hu.jar
echo "signing ./soluble-salts/soluble-salts_th.jar"
sign-jar ./soluble-salts/soluble-salts_th.jar
echo "signing ./soluble-salts/soluble-salts_gl.jar"
sign-jar ./soluble-salts/soluble-salts_gl.jar
echo "signing ./soluble-salts/soluble-salts_en.jar"
sign-jar ./soluble-salts/soluble-salts_en.jar
echo "signing ./soluble-salts/soluble-salts_fi.jar"
sign-jar ./soluble-salts/soluble-salts_fi.jar
echo "signing ./soluble-salts/soluble-salts_sq.jar"
sign-jar ./soluble-salts/soluble-salts_sq.jar
echo "signing ./soluble-salts/soluble-salts_hr.jar"
sign-jar ./soluble-salts/soluble-salts_hr.jar
echo "signing ./soluble-salts/soluble-salts_ko.jar"
sign-jar ./soluble-salts/soluble-salts_ko.jar
echo "signing ./soluble-salts/soluble-salts_ja.jar"
sign-jar ./soluble-salts/soluble-salts_ja.jar
echo "signing ./soluble-salts/soluble-salts_eu.jar"
sign-jar ./soluble-salts/soluble-salts_eu.jar
echo "signing ./soluble-salts/soluble-salts_fa.jar"
sign-jar ./soluble-salts/soluble-salts_fa.jar
echo "signing ./soluble-salts/soluble-salts_ar.jar"
sign-jar ./soluble-salts/soluble-salts_ar.jar
echo "signing ./soluble-salts/soluble-salts_sl.jar"
sign-jar ./soluble-salts/soluble-salts_sl.jar
echo "signing ./soluble-salts/soluble-salts_it.jar"
sign-jar ./soluble-salts/soluble-salts_it.jar
echo "signing ./soluble-salts/soluble-salts_zh_CN.jar"
sign-jar ./soluble-salts/soluble-salts_zh_CN.jar
echo "signing ./soluble-salts/soluble-salts_all_installer.jar"
sign-jar ./soluble-salts/soluble-salts_all_installer.jar
echo "signing ./soluble-salts/soluble-salts_zh_TW.jar"
sign-jar ./soluble-salts/soluble-salts_zh_TW.jar
echo "signing ./soluble-salts/soluble-salts_ka.jar"
sign-jar ./soluble-salts/soluble-salts_ka.jar
echo "signing ./soluble-salts/soluble-salts_km.jar"
sign-jar ./soluble-salts/soluble-salts_km.jar
echo "signing ./soluble-salts/soluble-salts_ar_SA.jar"
sign-jar ./soluble-salts/soluble-salts_ar_SA.jar
echo "signing ./soluble-salts/soluble-salts_lo.jar"
sign-jar ./soluble-salts/soluble-salts_lo.jar
echo "signing ./soluble-salts/soluble-salts_de.jar"
sign-jar ./soluble-salts/soluble-salts_de.jar
echo "signing ./soluble-salts/soluble-salts_el.jar"
sign-jar ./soluble-salts/soluble-salts_el.jar
echo "signing ./soluble-salts/soluble-salts_pt.jar"
sign-jar ./soluble-salts/soluble-salts_pt.jar
echo "signing ./soluble-salts/soluble-salts_bs.jar"
sign-jar ./soluble-salts/soluble-salts_bs.jar
echo "signing ./soluble-salts/soluble-salts_fr.jar"
sign-jar ./soluble-salts/soluble-salts_fr.jar
echo "signing ./soluble-salts/soluble-salts_mk.jar"
sign-jar ./soluble-salts/soluble-salts_mk.jar
echo "signing ./soluble-salts/soluble-salts_all.jar"
sign-jar ./soluble-salts/soluble-salts_all.jar
echo "signing ./soluble-salts/soluble-salts_da.jar"
sign-jar ./soluble-salts/soluble-salts_da.jar
echo "signing ./soluble-salts/soluble-salts_es_PE.jar"
sign-jar ./soluble-salts/soluble-salts_es_PE.jar
echo "signing ./soluble-salts/soluble-salts_ru.jar"
sign-jar ./soluble-salts/soluble-salts_ru.jar
echo "signing ./soluble-salts/soluble-salts_pl.jar"
sign-jar ./soluble-salts/soluble-salts_pl.jar
echo "signing ./soluble-salts/soluble-salts_uz.jar"
sign-jar ./soluble-salts/soluble-salts_uz.jar
echo "signing ./soluble-salts/soluble-salts_kk.jar"
sign-jar ./soluble-salts/soluble-salts_kk.jar
echo "signing ./soluble-salts/soluble-salts_vi.jar"
sign-jar ./soluble-salts/soluble-salts_vi.jar
echo "signing ./soluble-salts/soluble-salts_es.jar"
sign-jar ./soluble-salts/soluble-salts_es.jar
echo "signing ./soluble-salts/soluble-salts_in.jar"
sign-jar ./soluble-salts/soluble-salts_in.jar
echo "signing ./soluble-salts/soluble-salts_sk.jar"
sign-jar ./soluble-salts/soluble-salts_sk.jar
echo "signing ./soluble-salts/soluble-salts_nl.jar"
sign-jar ./soluble-salts/soluble-salts_nl.jar
echo "signing ./soluble-salts/soluble-salts_et.jar"
sign-jar ./soluble-salts/soluble-salts_et.jar
echo "signing ./soluble-salts/soluble-salts_sr.jar"
sign-jar ./soluble-salts/soluble-salts_sr.jar
echo "signing ./soluble-salts/soluble-salts_sv.jar"
sign-jar ./soluble-salts/soluble-salts_sv.jar
echo "signing ./soluble-salts/soluble-salts_nb.jar"
sign-jar ./soluble-salts/soluble-salts_nb.jar
echo "signing ./soluble-salts/soluble-salts_tr.jar"
sign-jar ./soluble-salts/soluble-salts_tr.jar
echo "signing ./arithmetic/arithmetic_bs.jar"
sign-jar ./arithmetic/arithmetic_bs.jar
echo "signing ./arithmetic/arithmetic_ja.jar"
sign-jar ./arithmetic/arithmetic_ja.jar
echo "signing ./arithmetic/arithmetic_ar_SA.jar"
sign-jar ./arithmetic/arithmetic_ar_SA.jar
echo "signing ./arithmetic/arithmetic_sr.jar"
sign-jar ./arithmetic/arithmetic_sr.jar
echo "signing ./arithmetic/arithmetic_ru.jar"
sign-jar ./arithmetic/arithmetic_ru.jar
echo "signing ./arithmetic/arithmetic_nl.jar"
sign-jar ./arithmetic/arithmetic_nl.jar
echo "signing ./arithmetic/arithmetic_pl.jar"
sign-jar ./arithmetic/arithmetic_pl.jar
echo "signing ./arithmetic/arithmetic_eu.jar"
sign-jar ./arithmetic/arithmetic_eu.jar
echo "signing ./arithmetic/arithmetic_fa.jar"
sign-jar ./arithmetic/arithmetic_fa.jar
echo "signing ./arithmetic/arithmetic_hr.jar"
sign-jar ./arithmetic/arithmetic_hr.jar
echo "signing ./arithmetic/arithmetic_ro.jar"
sign-jar ./arithmetic/arithmetic_ro.jar
echo "signing ./arithmetic/arithmetic_ar.jar"
sign-jar ./arithmetic/arithmetic_ar.jar
echo "signing ./arithmetic/arithmetic_zh_TW.jar"
sign-jar ./arithmetic/arithmetic_zh_TW.jar
echo "signing ./arithmetic/arithmetic_fr.jar"
sign-jar ./arithmetic/arithmetic_fr.jar
echo "signing ./arithmetic/arithmetic_in.jar"
sign-jar ./arithmetic/arithmetic_in.jar
echo "signing ./arithmetic/arithmetic_et.jar"
sign-jar ./arithmetic/arithmetic_et.jar
echo "signing ./arithmetic/arithmetic_kk.jar"
sign-jar ./arithmetic/arithmetic_kk.jar
echo "signing ./arithmetic/arithmetic_vi.jar"
sign-jar ./arithmetic/arithmetic_vi.jar
echo "signing ./arithmetic/arithmetic_af.jar"
sign-jar ./arithmetic/arithmetic_af.jar
echo "signing ./arithmetic/arithmetic_lv.jar"
sign-jar ./arithmetic/arithmetic_lv.jar
echo "signing ./arithmetic/arithmetic_tk.jar"
sign-jar ./arithmetic/arithmetic_tk.jar
echo "signing ./arithmetic/arithmetic_tr.jar"
sign-jar ./arithmetic/arithmetic_tr.jar
echo "signing ./arithmetic/arithmetic_de.jar"
sign-jar ./arithmetic/arithmetic_de.jar
echo "signing ./arithmetic/arithmetic_it.jar"
sign-jar ./arithmetic/arithmetic_it.jar
echo "signing ./arithmetic/arithmetic_iw.jar"
sign-jar ./arithmetic/arithmetic_iw.jar
echo "signing ./arithmetic/arithmetic_hu.jar"
sign-jar ./arithmetic/arithmetic_hu.jar
echo "signing ./arithmetic/arithmetic_el.jar"
sign-jar ./arithmetic/arithmetic_el.jar
echo "signing ./arithmetic/arithmetic_ko.jar"
sign-jar ./arithmetic/arithmetic_ko.jar
echo "signing ./arithmetic/arithmetic_mk.jar"
sign-jar ./arithmetic/arithmetic_mk.jar
echo "signing ./arithmetic/arithmetic_da.jar"
sign-jar ./arithmetic/arithmetic_da.jar
echo "signing ./arithmetic/arithmetic_ka.jar"
sign-jar ./arithmetic/arithmetic_ka.jar
echo "signing ./arithmetic/arithmetic_es.jar"
sign-jar ./arithmetic/arithmetic_es.jar
echo "signing ./arithmetic/arithmetic_sq.jar"
sign-jar ./arithmetic/arithmetic_sq.jar
echo "signing ./arithmetic/arithmetic_sv.jar"
sign-jar ./arithmetic/arithmetic_sv.jar
echo "signing ./arithmetic/arithmetic_en.jar"
sign-jar ./arithmetic/arithmetic_en.jar
echo "signing ./arithmetic/arithmetic_es_PE.jar"
sign-jar ./arithmetic/arithmetic_es_PE.jar
echo "signing ./arithmetic/arithmetic_pt_BR.jar"
sign-jar ./arithmetic/arithmetic_pt_BR.jar
echo "signing ./arithmetic/arithmetic_sw.jar"
sign-jar ./arithmetic/arithmetic_sw.jar
echo "signing ./arithmetic/arithmetic_zh_CN.jar"
sign-jar ./arithmetic/arithmetic_zh_CN.jar
echo "signing ./arithmetic/arithmetic_sk.jar"
sign-jar ./arithmetic/arithmetic_sk.jar
echo "signing ./arithmetic/arithmetic_mr.jar"
sign-jar ./arithmetic/arithmetic_mr.jar
echo "signing ./the-ramp/the-ramp_et.jar"
sign-jar ./the-ramp/the-ramp_et.jar
echo "signing ./the-ramp/the-ramp_da.jar"
sign-jar ./the-ramp/the-ramp_da.jar
echo "signing ./the-ramp/the-ramp_ka.jar"
sign-jar ./the-ramp/the-ramp_ka.jar
echo "signing ./the-ramp/the-ramp_ja.jar"
sign-jar ./the-ramp/the-ramp_ja.jar
echo "signing ./the-ramp/the-ramp_sv.jar"
sign-jar ./the-ramp/the-ramp_sv.jar
echo "signing ./the-ramp/the-ramp_zh_CN.jar"
sign-jar ./the-ramp/the-ramp_zh_CN.jar
echo "signing ./the-ramp/the-ramp_in.jar"
sign-jar ./the-ramp/the-ramp_in.jar
echo "signing ./the-ramp/the-ramp_pt_BR.jar"
sign-jar ./the-ramp/the-ramp_pt_BR.jar
echo "signing ./the-ramp/the-ramp_mk.jar"
sign-jar ./the-ramp/the-ramp_mk.jar
echo "signing ./the-ramp/the-ramp_vi.jar"
sign-jar ./the-ramp/the-ramp_vi.jar
echo "signing ./the-ramp/the-ramp_iw.jar"
sign-jar ./the-ramp/the-ramp_iw.jar
echo "signing ./the-ramp/the-ramp_gl.jar"
sign-jar ./the-ramp/the-ramp_gl.jar
echo "signing ./the-ramp/the-ramp_zh_TW.jar"
sign-jar ./the-ramp/the-ramp_zh_TW.jar
echo "signing ./the-ramp/the-ramp_cs.jar"
sign-jar ./the-ramp/the-ramp_cs.jar
echo "signing ./the-ramp/the-ramp_kk.jar"
sign-jar ./the-ramp/the-ramp_kk.jar
echo "signing ./the-ramp/the-ramp_ko.jar"
sign-jar ./the-ramp/the-ramp_ko.jar
echo "signing ./the-ramp/the-ramp_tk.jar"
sign-jar ./the-ramp/the-ramp_tk.jar
echo "signing ./the-ramp/the-ramp_eu.jar"
sign-jar ./the-ramp/the-ramp_eu.jar
echo "signing ./the-ramp/the-ramp_en.jar"
sign-jar ./the-ramp/the-ramp_en.jar
echo "signing ./the-ramp/the-ramp_pl.jar"
sign-jar ./the-ramp/the-ramp_pl.jar
echo "signing ./the-ramp/the-ramp_hr.jar"
sign-jar ./the-ramp/the-ramp_hr.jar
echo "signing ./the-ramp/the-ramp_it.jar"
sign-jar ./the-ramp/the-ramp_it.jar
echo "signing ./the-ramp/the-ramp_kn.jar"
sign-jar ./the-ramp/the-ramp_kn.jar
echo "signing ./the-ramp/the-ramp_ku_TR.jar"
sign-jar ./the-ramp/the-ramp_ku_TR.jar
echo "signing ./the-ramp/the-ramp_fr.jar"
sign-jar ./the-ramp/the-ramp_fr.jar
echo "signing ./the-ramp/the-ramp_sl.jar"
sign-jar ./the-ramp/the-ramp_sl.jar
echo "signing ./the-ramp/the-ramp_sk.jar"
sign-jar ./the-ramp/the-ramp_sk.jar
echo "signing ./the-ramp/the-ramp_ar_SA.jar"
sign-jar ./the-ramp/the-ramp_ar_SA.jar
echo "signing ./the-ramp/the-ramp_el.jar"
sign-jar ./the-ramp/the-ramp_el.jar
echo "signing ./the-ramp/the-ramp_es_PE.jar"
sign-jar ./the-ramp/the-ramp_es_PE.jar
echo "signing ./the-ramp/the-ramp_nl.jar"
sign-jar ./the-ramp/the-ramp_nl.jar
echo "signing ./the-ramp/the-ramp_all.jar"
sign-jar ./the-ramp/the-ramp_all.jar
echo "signing ./the-ramp/the-ramp_uk.jar"
sign-jar ./the-ramp/the-ramp_uk.jar
echo "signing ./the-ramp/the-ramp_all_installer.jar"
sign-jar ./the-ramp/the-ramp_all_installer.jar
echo "signing ./the-ramp/the-ramp_hu.jar"
sign-jar ./the-ramp/the-ramp_hu.jar
echo "signing ./the-ramp/the-ramp_fa.jar"
sign-jar ./the-ramp/the-ramp_fa.jar
echo "signing ./the-ramp/the-ramp_th.jar"
sign-jar ./the-ramp/the-ramp_th.jar
echo "signing ./the-ramp/the-ramp_ar.jar"
sign-jar ./the-ramp/the-ramp_ar.jar
echo "signing ./the-ramp/the-ramp_fi.jar"
sign-jar ./the-ramp/the-ramp_fi.jar
echo "signing ./the-ramp/the-ramp_km.jar"
sign-jar ./the-ramp/the-ramp_km.jar
echo "signing ./the-ramp/the-ramp_es.jar"
sign-jar ./the-ramp/the-ramp_es.jar
echo "signing ./the-ramp/the-ramp_tr.jar"
sign-jar ./the-ramp/the-ramp_tr.jar
echo "signing ./the-ramp/the-ramp_lt.jar"
sign-jar ./the-ramp/the-ramp_lt.jar
echo "signing ./the-ramp/the-ramp_ms.jar"
sign-jar ./the-ramp/the-ramp_ms.jar
echo "signing ./the-ramp/the-ramp_nb.jar"
sign-jar ./the-ramp/the-ramp_nb.jar
echo "signing ./the-ramp/the-ramp_ro.jar"
sign-jar ./the-ramp/the-ramp_ro.jar
echo "signing ./the-ramp/the-ramp_de.jar"
sign-jar ./the-ramp/the-ramp_de.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_km.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_km.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_pt_BR.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_pt_BR.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_zh_TW.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_zh_TW.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_es.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_es.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ka.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ka.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_el.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_el.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_de.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_de.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_kn.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_kn.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ja.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ja.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ro.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ro.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_nl.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_nl.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_sk.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_sk.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_it.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_it.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_hr.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_hr.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_fi.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_fi.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_iw.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_iw.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_fa.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_fa.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_sq.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_sq.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_da.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_da.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_mr.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_mr.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_all.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_all.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ku_TR.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ku_TR.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_hu.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_hu.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_cs.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_cs.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_tk.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_tk.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_lv.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_lv.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_tr.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_tr.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_eu.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_eu.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_en.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_en.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_in.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_in.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ar_SA.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ar_SA.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_all_installer.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_all_installer.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_es_PE.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_es_PE.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ar.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ar.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_pt.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_pt.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_fr.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_fr.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_et.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_et.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_sr.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_sr.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ko.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ko.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_th.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_th.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_sv.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_sv.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_bs.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_bs.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_be.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_be.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_gl.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_gl.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ru.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ru.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_zh_CN.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_zh_CN.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_kk.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_kk.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_pl.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_pl.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_mk.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_mk.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_bg.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_bg.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_ti.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_ti.jar
echo "signing ./gravity-and-orbits/gravity-and-orbits_vi.jar"
sign-jar ./gravity-and-orbits/gravity-and-orbits_vi.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_ku_TR.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_ku_TR.jar
echo "signing ./ladybug-motion-2d/aphid-maze_tr.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_tr.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_eu.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_eu.jar
echo "signing ./ladybug-motion-2d/aphid-maze_ar.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_ar.jar
echo "signing ./ladybug-motion-2d/aphid-maze_vi.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_vi.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_km.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_km.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_sr.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_sr.jar
echo "signing ./ladybug-motion-2d/aphid-maze_bs.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_bs.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_pt.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_pt.jar
echo "signing ./ladybug-motion-2d/aphid-maze_pl.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_pl.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_nl.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_nl.jar
echo "signing ./ladybug-motion-2d/aphid-maze_el.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_el.jar
echo "signing ./ladybug-motion-2d/aphid-maze_it.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_it.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_fr.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_fr.jar
echo "signing ./ladybug-motion-2d/aphid-maze_tk.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_tk.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_sv.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_sv.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_de.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_de.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_all.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_all.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_zh_TW.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_zh_TW.jar
echo "signing ./ladybug-motion-2d/aphid-maze_en.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_en.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_en.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_en.jar
echo "signing ./ladybug-motion-2d/aphid-maze_sv.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_sv.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_iw.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_iw.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_tr.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_tr.jar
echo "signing ./ladybug-motion-2d/aphid-maze_de.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_de.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_da.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_da.jar
echo "signing ./ladybug-motion-2d/aphid-maze_et.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_et.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_ja.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_ja.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_ar.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_ar.jar
echo "signing ./ladybug-motion-2d/aphid-maze_es_PE.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_es_PE.jar
echo "signing ./ladybug-motion-2d/aphid-maze_be.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_be.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_ko.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_ko.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_gl.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_gl.jar
echo "signing ./ladybug-motion-2d/aphid-maze_ku_TR.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_ku_TR.jar
echo "signing ./ladybug-motion-2d/aphid-maze_es_CO.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_es_CO.jar
echo "signing ./ladybug-motion-2d/aphid-maze_da.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_da.jar
echo "signing ./ladybug-motion-2d/aphid-maze_iw.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_iw.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_hu.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_hu.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_it.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_it.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_th.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_th.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_es_CO.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_es_CO.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_fa.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_fa.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_tk.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_tk.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_pt_BR.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_pt_BR.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_pl.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_pl.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_ms.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_ms.jar
echo "signing ./ladybug-motion-2d/aphid-maze_hu.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_hu.jar
echo "signing ./ladybug-motion-2d/aphid-maze_mk.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_mk.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_vi.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_vi.jar
echo "signing ./ladybug-motion-2d/aphid-maze_zh_CN.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_zh_CN.jar
echo "signing ./ladybug-motion-2d/aphid-maze_kk.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_kk.jar
echo "signing ./ladybug-motion-2d/aphid-maze_sr.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_sr.jar
echo "signing ./ladybug-motion-2d/aphid-maze_fa.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_fa.jar
echo "signing ./ladybug-motion-2d/aphid-maze_ms.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_ms.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_sl.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_sl.jar
echo "signing ./ladybug-motion-2d/aphid-maze_km.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_km.jar
echo "signing ./ladybug-motion-2d/aphid-maze_zh_TW.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_zh_TW.jar
echo "signing ./ladybug-motion-2d/aphid-maze_fr.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_fr.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_es.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_es.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_all_installer.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_all_installer.jar
echo "signing ./ladybug-motion-2d/aphid-maze_pt_BR.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_pt_BR.jar
echo "signing ./ladybug-motion-2d/aphid-maze_ko.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_ko.jar
echo "signing ./ladybug-motion-2d/aphid-maze_sk.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_sk.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_el.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_el.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_et.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_et.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_kk.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_kk.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_be.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_be.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_bs.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_bs.jar
echo "signing ./ladybug-motion-2d/aphid-maze_th.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_th.jar
echo "signing ./ladybug-motion-2d/aphid-maze_eu.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_eu.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_mk.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_mk.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_es_PE.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_es_PE.jar
echo "signing ./ladybug-motion-2d/aphid-maze_sl.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_sl.jar
echo "signing ./ladybug-motion-2d/aphid-maze_ja.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_ja.jar
echo "signing ./ladybug-motion-2d/aphid-maze_gl.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_gl.jar
echo "signing ./ladybug-motion-2d/aphid-maze_es.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_es.jar
echo "signing ./ladybug-motion-2d/aphid-maze_pt.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_pt.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_zh_CN.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_zh_CN.jar
echo "signing ./ladybug-motion-2d/ladybug-motion-2d_sk.jar"
sign-jar ./ladybug-motion-2d/ladybug-motion-2d_sk.jar
echo "signing ./ladybug-motion-2d/aphid-maze_nl.jar"
sign-jar ./ladybug-motion-2d/aphid-maze_nl.jar
echo "signing ./eating-and-exercise/eating-and-exercise_pt_BR.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_pt_BR.jar
echo "signing ./eating-and-exercise/eating-and-exercise_fa.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_fa.jar
echo "signing ./eating-and-exercise/eating-and-exercise_mr.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_mr.jar
echo "signing ./eating-and-exercise/eating-and-exercise_mi.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_mi.jar
echo "signing ./eating-and-exercise/eating-and-exercise_zh_CN.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_zh_CN.jar
echo "signing ./eating-and-exercise/eating-and-exercise_all_installer.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_all_installer.jar
echo "signing ./eating-and-exercise/eating-and-exercise_da.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_da.jar
echo "signing ./eating-and-exercise/eating-and-exercise_de.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_de.jar
echo "signing ./eating-and-exercise/eating-and-exercise_af.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_af.jar
echo "signing ./eating-and-exercise/eating-and-exercise_fr.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_fr.jar
echo "signing ./eating-and-exercise/eating-and-exercise_hu.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_hu.jar
echo "signing ./eating-and-exercise/eating-and-exercise_iw.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_iw.jar
echo "signing ./eating-and-exercise/eating-and-exercise_cs.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_cs.jar
echo "signing ./eating-and-exercise/eating-and-exercise_sk.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_sk.jar
echo "signing ./eating-and-exercise/eating-and-exercise_ar.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_ar.jar
echo "signing ./eating-and-exercise/eating-and-exercise_es_PE.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_es_PE.jar
echo "signing ./eating-and-exercise/eating-and-exercise_pl.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_pl.jar
echo "signing ./eating-and-exercise/eating-and-exercise_mk.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_mk.jar
echo "signing ./eating-and-exercise/eating-and-exercise_sr.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_sr.jar
echo "signing ./eating-and-exercise/eating-and-exercise_tr.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_tr.jar
echo "signing ./eating-and-exercise/eating-and-exercise_vi.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_vi.jar
echo "signing ./eating-and-exercise/eating-and-exercise_ka.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_ka.jar
echo "signing ./eating-and-exercise/eating-and-exercise_es.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_es.jar
echo "signing ./eating-and-exercise/eating-and-exercise_nl.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_nl.jar
echo "signing ./eating-and-exercise/eating-and-exercise_et.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_et.jar
echo "signing ./eating-and-exercise/eating-and-exercise_ko.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_ko.jar
echo "signing ./eating-and-exercise/eating-and-exercise_km.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_km.jar
echo "signing ./eating-and-exercise/eating-and-exercise_en.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_en.jar
echo "signing ./eating-and-exercise/eating-and-exercise_eu.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_eu.jar
echo "signing ./eating-and-exercise/eating-and-exercise_th.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_th.jar
echo "signing ./eating-and-exercise/eating-and-exercise_ja.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_ja.jar
echo "signing ./eating-and-exercise/eating-and-exercise_tk.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_tk.jar
echo "signing ./eating-and-exercise/eating-and-exercise_it.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_it.jar
echo "signing ./eating-and-exercise/eating-and-exercise_el.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_el.jar
echo "signing ./eating-and-exercise/eating-and-exercise_zh_TW.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_zh_TW.jar
echo "signing ./eating-and-exercise/eating-and-exercise_all.jar"
sign-jar ./eating-and-exercise/eating-and-exercise_all.jar
echo "signing ./signal-circuit/signal-circuit_zh_CN.jar"
sign-jar ./signal-circuit/signal-circuit_zh_CN.jar
echo "signing ./signal-circuit/signal-circuit_kk.jar"
sign-jar ./signal-circuit/signal-circuit_kk.jar
echo "signing ./signal-circuit/signal-circuit_da.jar"
sign-jar ./signal-circuit/signal-circuit_da.jar
echo "signing ./signal-circuit/signal-circuit_pt.jar"
sign-jar ./signal-circuit/signal-circuit_pt.jar
echo "signing ./signal-circuit/signal-circuit_all.jar"
sign-jar ./signal-circuit/signal-circuit_all.jar
echo "signing ./signal-circuit/signal-circuit_sr.jar"
sign-jar ./signal-circuit/signal-circuit_sr.jar
echo "signing ./signal-circuit/signal-circuit_vi.jar"
sign-jar ./signal-circuit/signal-circuit_vi.jar
echo "signing ./signal-circuit/signal-circuit_pt_BR.jar"
sign-jar ./signal-circuit/signal-circuit_pt_BR.jar
echo "signing ./signal-circuit/signal-circuit_kn.jar"
sign-jar ./signal-circuit/signal-circuit_kn.jar
echo "signing ./signal-circuit/signal-circuit_ru.jar"
sign-jar ./signal-circuit/signal-circuit_ru.jar
echo "signing ./signal-circuit/signal-circuit_ar_SA.jar"
sign-jar ./signal-circuit/signal-circuit_ar_SA.jar
echo "signing ./signal-circuit/signal-circuit_bs.jar"
sign-jar ./signal-circuit/signal-circuit_bs.jar
echo "signing ./signal-circuit/signal-circuit_hr.jar"
sign-jar ./signal-circuit/signal-circuit_hr.jar
echo "signing ./signal-circuit/signal-circuit_ro.jar"
sign-jar ./signal-circuit/signal-circuit_ro.jar
echo "signing ./signal-circuit/signal-circuit_tr.jar"
sign-jar ./signal-circuit/signal-circuit_tr.jar
echo "signing ./signal-circuit/signal-circuit_fa.jar"
sign-jar ./signal-circuit/signal-circuit_fa.jar
echo "signing ./signal-circuit/signal-circuit_pl.jar"
sign-jar ./signal-circuit/signal-circuit_pl.jar
echo "signing ./signal-circuit/signal-circuit_cs.jar"
sign-jar ./signal-circuit/signal-circuit_cs.jar
echo "signing ./signal-circuit/signal-circuit_fr.jar"
sign-jar ./signal-circuit/signal-circuit_fr.jar
echo "signing ./signal-circuit/signal-circuit_iw.jar"
sign-jar ./signal-circuit/signal-circuit_iw.jar
echo "signing ./signal-circuit/signal-circuit_uk.jar"
sign-jar ./signal-circuit/signal-circuit_uk.jar
echo "signing ./signal-circuit/signal-circuit_ka.jar"
sign-jar ./signal-circuit/signal-circuit_ka.jar
echo "signing ./signal-circuit/signal-circuit_sk.jar"
sign-jar ./signal-circuit/signal-circuit_sk.jar
echo "signing ./signal-circuit/signal-circuit_mk.jar"
sign-jar ./signal-circuit/signal-circuit_mk.jar
echo "signing ./signal-circuit/signal-circuit_th.jar"
sign-jar ./signal-circuit/signal-circuit_th.jar
echo "signing ./signal-circuit/signal-circuit_zh_TW.jar"
sign-jar ./signal-circuit/signal-circuit_zh_TW.jar
echo "signing ./signal-circuit/signal-circuit_gl.jar"
sign-jar ./signal-circuit/signal-circuit_gl.jar
echo "signing ./signal-circuit/signal-circuit_eu.jar"
sign-jar ./signal-circuit/signal-circuit_eu.jar
echo "signing ./signal-circuit/signal-circuit_tk.jar"
sign-jar ./signal-circuit/signal-circuit_tk.jar
echo "signing ./signal-circuit/signal-circuit_de.jar"
sign-jar ./signal-circuit/signal-circuit_de.jar
echo "signing ./signal-circuit/signal-circuit_es_PE.jar"
sign-jar ./signal-circuit/signal-circuit_es_PE.jar
echo "signing ./signal-circuit/signal-circuit_en.jar"
sign-jar ./signal-circuit/signal-circuit_en.jar
echo "signing ./signal-circuit/signal-circuit_nl.jar"
sign-jar ./signal-circuit/signal-circuit_nl.jar
echo "signing ./signal-circuit/signal-circuit_hu.jar"
sign-jar ./signal-circuit/signal-circuit_hu.jar
echo "signing ./signal-circuit/signal-circuit_ja.jar"
sign-jar ./signal-circuit/signal-circuit_ja.jar
echo "signing ./signal-circuit/signal-circuit_ar.jar"
sign-jar ./signal-circuit/signal-circuit_ar.jar
echo "signing ./signal-circuit/signal-circuit_lv.jar"
sign-jar ./signal-circuit/signal-circuit_lv.jar
echo "signing ./signal-circuit/signal-circuit_it.jar"
sign-jar ./signal-circuit/signal-circuit_it.jar
echo "signing ./signal-circuit/signal-circuit_hy.jar"
sign-jar ./signal-circuit/signal-circuit_hy.jar
echo "signing ./signal-circuit/signal-circuit_es.jar"
sign-jar ./signal-circuit/signal-circuit_es.jar
echo "signing ./signal-circuit/signal-circuit_ku_TR.jar"
sign-jar ./signal-circuit/signal-circuit_ku_TR.jar
echo "signing ./signal-circuit/signal-circuit_ko.jar"
sign-jar ./signal-circuit/signal-circuit_ko.jar
echo "signing ./signal-circuit/signal-circuit_all_installer.jar"
sign-jar ./signal-circuit/signal-circuit_all_installer.jar
echo "signing ./signal-circuit/signal-circuit_bg.jar"
sign-jar ./signal-circuit/signal-circuit_bg.jar
echo "signing ./signal-circuit/signal-circuit_sv.jar"
sign-jar ./signal-circuit/signal-circuit_sv.jar
echo "signing ./signal-circuit/signal-circuit_el.jar"
sign-jar ./signal-circuit/signal-circuit_el.jar
echo "signing ./signal-circuit/signal-circuit_in.jar"
sign-jar ./signal-circuit/signal-circuit_in.jar
echo "signing ./test-flash-project/test-flash-project_en.jar"
sign-jar ./test-flash-project/test-flash-project_en.jar
echo "signing ./test-flash-project/test-flash-project_en_US.jar"
sign-jar ./test-flash-project/test-flash-project_en_US.jar
echo "signing ./test-flash-project/test-flash-project_es.jar"
sign-jar ./test-flash-project/test-flash-project_es.jar
echo "signing ./test-flash-project/test-flash-project_zh_CN.jar"
sign-jar ./test-flash-project/test-flash-project_zh_CN.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_pt_BR.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_pt_BR.jar
echo "signing ./beers-law-lab/concentration_nl.jar"
sign-jar ./beers-law-lab/concentration_nl.jar
echo "signing ./beers-law-lab/beers-law-lab_gl.jar"
sign-jar ./beers-law-lab/beers-law-lab_gl.jar
echo "signing ./beers-law-lab/concentration_es.jar"
sign-jar ./beers-law-lab/concentration_es.jar
echo "signing ./beers-law-lab/beers-law-lab_fr.jar"
sign-jar ./beers-law-lab/beers-law-lab_fr.jar
echo "signing ./beers-law-lab/beers-law-lab_zh_TW.jar"
sign-jar ./beers-law-lab/beers-law-lab_zh_TW.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_ru.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_ru.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_sk.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_sk.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_it.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_it.jar
echo "signing ./beers-law-lab/concentration_mk.jar"
sign-jar ./beers-law-lab/concentration_mk.jar
echo "signing ./beers-law-lab/beers-law-lab_da.jar"
sign-jar ./beers-law-lab/beers-law-lab_da.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_ka.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_ka.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_es_PE.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_es_PE.jar
echo "signing ./beers-law-lab/concentration-interviews_fr.jar"
sign-jar ./beers-law-lab/concentration-interviews_fr.jar
echo "signing ./beers-law-lab/beers-law-lab_nb.jar"
sign-jar ./beers-law-lab/beers-law-lab_nb.jar
echo "signing ./beers-law-lab/beers-law-lab_ja.jar"
sign-jar ./beers-law-lab/beers-law-lab_ja.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_th.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_th.jar
echo "signing ./beers-law-lab/beers-law-lab_nl.jar"
sign-jar ./beers-law-lab/beers-law-lab_nl.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_sr.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_sr.jar
echo "signing ./beers-law-lab/beers-law-lab_bg.jar"
sign-jar ./beers-law-lab/beers-law-lab_bg.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_ta.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_ta.jar
echo "signing ./beers-law-lab/concentration-interviews_ru.jar"
sign-jar ./beers-law-lab/concentration-interviews_ru.jar
echo "signing ./beers-law-lab/concentration-interviews_en.jar"
sign-jar ./beers-law-lab/concentration-interviews_en.jar
echo "signing ./beers-law-lab/beers-law-lab_ko.jar"
sign-jar ./beers-law-lab/beers-law-lab_ko.jar
echo "signing ./beers-law-lab/concentration_vi.jar"
sign-jar ./beers-law-lab/concentration_vi.jar
echo "signing ./beers-law-lab/concentration-interviews_nb.jar"
sign-jar ./beers-law-lab/concentration-interviews_nb.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_es.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_es.jar
echo "signing ./beers-law-lab/beers-law-lab_es_PE.jar"
sign-jar ./beers-law-lab/beers-law-lab_es_PE.jar
echo "signing ./beers-law-lab/concentration_it.jar"
sign-jar ./beers-law-lab/concentration_it.jar
echo "signing ./beers-law-lab/concentration_ko.jar"
sign-jar ./beers-law-lab/concentration_ko.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_nl.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_nl.jar
echo "signing ./beers-law-lab/concentration_kk.jar"
sign-jar ./beers-law-lab/concentration_kk.jar
echo "signing ./beers-law-lab/concentration-interviews_sk.jar"
sign-jar ./beers-law-lab/concentration-interviews_sk.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_mk.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_mk.jar
echo "signing ./beers-law-lab/concentration_gl.jar"
sign-jar ./beers-law-lab/concentration_gl.jar
echo "signing ./beers-law-lab/beers-law-lab_zh_CN.jar"
sign-jar ./beers-law-lab/beers-law-lab_zh_CN.jar
echo "signing ./beers-law-lab/concentration-interviews_vi.jar"
sign-jar ./beers-law-lab/concentration-interviews_vi.jar
echo "signing ./beers-law-lab/concentration_mr.jar"
sign-jar ./beers-law-lab/concentration_mr.jar
echo "signing ./beers-law-lab/concentration-interviews_gl.jar"
sign-jar ./beers-law-lab/concentration-interviews_gl.jar
echo "signing ./beers-law-lab/beers-law-lab_pl.jar"
sign-jar ./beers-law-lab/beers-law-lab_pl.jar
echo "signing ./beers-law-lab/concentration_nb.jar"
sign-jar ./beers-law-lab/concentration_nb.jar
echo "signing ./beers-law-lab/beers-law-lab_kk.jar"
sign-jar ./beers-law-lab/beers-law-lab_kk.jar
echo "signing ./beers-law-lab/beers-law-lab_pt_BR.jar"
sign-jar ./beers-law-lab/beers-law-lab_pt_BR.jar
echo "signing ./beers-law-lab/beers-law-lab_tk.jar"
sign-jar ./beers-law-lab/beers-law-lab_tk.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_ko.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_ko.jar
echo "signing ./beers-law-lab/beers-law-lab_iw.jar"
sign-jar ./beers-law-lab/beers-law-lab_iw.jar
echo "signing ./beers-law-lab/beers-law-lab_mk.jar"
sign-jar ./beers-law-lab/beers-law-lab_mk.jar
echo "signing ./beers-law-lab/concentration_in.jar"
sign-jar ./beers-law-lab/concentration_in.jar
echo "signing ./beers-law-lab/concentration_th.jar"
sign-jar ./beers-law-lab/concentration_th.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_nb.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_nb.jar
echo "signing ./beers-law-lab/concentration_sk.jar"
sign-jar ./beers-law-lab/concentration_sk.jar
echo "signing ./beers-law-lab/concentration-interviews_zh_TW.jar"
sign-jar ./beers-law-lab/concentration-interviews_zh_TW.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_de.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_de.jar
echo "signing ./beers-law-lab/concentration_pl.jar"
sign-jar ./beers-law-lab/concentration_pl.jar
echo "signing ./beers-law-lab/concentration_ar.jar"
sign-jar ./beers-law-lab/concentration_ar.jar
echo "signing ./beers-law-lab/beers-law-lab_ru.jar"
sign-jar ./beers-law-lab/beers-law-lab_ru.jar
echo "signing ./beers-law-lab/beers-law-lab_de.jar"
sign-jar ./beers-law-lab/beers-law-lab_de.jar
echo "signing ./beers-law-lab/beers-law-lab_all_installer.jar"
sign-jar ./beers-law-lab/beers-law-lab_all_installer.jar
echo "signing ./beers-law-lab/concentration_eu.jar"
sign-jar ./beers-law-lab/concentration_eu.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_fr.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_fr.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_ja.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_ja.jar
echo "signing ./beers-law-lab/concentration-interviews_de.jar"
sign-jar ./beers-law-lab/concentration-interviews_de.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_bs.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_bs.jar
echo "signing ./beers-law-lab/beers-law-lab_vi.jar"
sign-jar ./beers-law-lab/beers-law-lab_vi.jar
echo "signing ./beers-law-lab/beers-law-lab_tr.jar"
sign-jar ./beers-law-lab/beers-law-lab_tr.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_cs.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_cs.jar
echo "signing ./beers-law-lab/concentration-interviews_pl.jar"
sign-jar ./beers-law-lab/concentration-interviews_pl.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_en.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_en.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_fa.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_fa.jar
echo "signing ./beers-law-lab/concentration-interviews_tr.jar"
sign-jar ./beers-law-lab/concentration-interviews_tr.jar
echo "signing ./beers-law-lab/concentration-interviews_el.jar"
sign-jar ./beers-law-lab/concentration-interviews_el.jar
echo "signing ./beers-law-lab/beers-law-lab_bs.jar"
sign-jar ./beers-law-lab/beers-law-lab_bs.jar
echo "signing ./beers-law-lab/concentration-interviews_sr.jar"
sign-jar ./beers-law-lab/concentration-interviews_sr.jar
echo "signing ./beers-law-lab/beers-law-lab_ka.jar"
sign-jar ./beers-law-lab/beers-law-lab_ka.jar
echo "signing ./beers-law-lab/concentration-interviews_cs.jar"
sign-jar ./beers-law-lab/concentration-interviews_cs.jar
echo "signing ./beers-law-lab/concentration-interviews_ja.jar"
sign-jar ./beers-law-lab/concentration-interviews_ja.jar
echo "signing ./beers-law-lab/beers-law-lab_el.jar"
sign-jar ./beers-law-lab/beers-law-lab_el.jar
echo "signing ./beers-law-lab/beers-law-lab_it.jar"
sign-jar ./beers-law-lab/beers-law-lab_it.jar
echo "signing ./beers-law-lab/concentration-interviews_tk.jar"
sign-jar ./beers-law-lab/concentration-interviews_tk.jar
echo "signing ./beers-law-lab/concentration_zh_CN.jar"
sign-jar ./beers-law-lab/concentration_zh_CN.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_pl.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_pl.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_hu.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_hu.jar
echo "signing ./beers-law-lab/beers-law-lab_th.jar"
sign-jar ./beers-law-lab/beers-law-lab_th.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_ar.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_ar.jar
echo "signing ./beers-law-lab/concentration_sr.jar"
sign-jar ./beers-law-lab/concentration_sr.jar
echo "signing ./beers-law-lab/beers-law-lab_es.jar"
sign-jar ./beers-law-lab/beers-law-lab_es.jar
echo "signing ./beers-law-lab/beers-law-lab_sv.jar"
sign-jar ./beers-law-lab/beers-law-lab_sv.jar
echo "signing ./beers-law-lab/concentration-interviews_es.jar"
sign-jar ./beers-law-lab/concentration-interviews_es.jar
echo "signing ./beers-law-lab/beers-law-lab_all.jar"
sign-jar ./beers-law-lab/beers-law-lab_all.jar
echo "signing ./beers-law-lab/concentration_cs.jar"
sign-jar ./beers-law-lab/concentration_cs.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_mr.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_mr.jar
echo "signing ./beers-law-lab/concentration_da.jar"
sign-jar ./beers-law-lab/concentration_da.jar
echo "signing ./beers-law-lab/concentration-interviews_ko.jar"
sign-jar ./beers-law-lab/concentration-interviews_ko.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_zh_CN.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_zh_CN.jar
echo "signing ./beers-law-lab/concentration_el.jar"
sign-jar ./beers-law-lab/concentration_el.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_vi.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_vi.jar
echo "signing ./beers-law-lab/concentration-interviews_kk.jar"
sign-jar ./beers-law-lab/concentration-interviews_kk.jar
echo "signing ./beers-law-lab/beers-law-lab_sk.jar"
sign-jar ./beers-law-lab/beers-law-lab_sk.jar
echo "signing ./beers-law-lab/concentration_ru.jar"
sign-jar ./beers-law-lab/concentration_ru.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_eu.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_eu.jar
echo "signing ./beers-law-lab/concentration-interviews_it.jar"
sign-jar ./beers-law-lab/concentration-interviews_it.jar
echo "signing ./beers-law-lab/beers-law-lab_sr.jar"
sign-jar ./beers-law-lab/beers-law-lab_sr.jar
echo "signing ./beers-law-lab/concentration-interviews_fa.jar"
sign-jar ./beers-law-lab/concentration-interviews_fa.jar
echo "signing ./beers-law-lab/concentration-interviews_nl.jar"
sign-jar ./beers-law-lab/concentration-interviews_nl.jar
echo "signing ./beers-law-lab/concentration-interviews_th.jar"
sign-jar ./beers-law-lab/concentration-interviews_th.jar
echo "signing ./beers-law-lab/beers-law-lab_en.jar"
sign-jar ./beers-law-lab/beers-law-lab_en.jar
echo "signing ./beers-law-lab/concentration-interviews_bs.jar"
sign-jar ./beers-law-lab/concentration-interviews_bs.jar
echo "signing ./beers-law-lab/concentration_ta.jar"
sign-jar ./beers-law-lab/concentration_ta.jar
echo "signing ./beers-law-lab/beers-law-lab_cs.jar"
sign-jar ./beers-law-lab/beers-law-lab_cs.jar
echo "signing ./beers-law-lab/concentration-interviews_sv.jar"
sign-jar ./beers-law-lab/concentration-interviews_sv.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_tk.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_tk.jar
echo "signing ./beers-law-lab/concentration_iw.jar"
sign-jar ./beers-law-lab/concentration_iw.jar
echo "signing ./beers-law-lab/concentration_zh_TW.jar"
sign-jar ./beers-law-lab/concentration_zh_TW.jar
echo "signing ./beers-law-lab/concentration_tr.jar"
sign-jar ./beers-law-lab/concentration_tr.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_tr.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_tr.jar
echo "signing ./beers-law-lab/concentration-interviews_pt_BR.jar"
sign-jar ./beers-law-lab/concentration-interviews_pt_BR.jar
echo "signing ./beers-law-lab/concentration-interviews_ka.jar"
sign-jar ./beers-law-lab/concentration-interviews_ka.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_kk.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_kk.jar
echo "signing ./beers-law-lab/beers-law-lab_fa.jar"
sign-jar ./beers-law-lab/beers-law-lab_fa.jar
echo "signing ./beers-law-lab/concentration_sv.jar"
sign-jar ./beers-law-lab/concentration_sv.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_sv.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_sv.jar
echo "signing ./beers-law-lab/beers-law-lab_ta.jar"
sign-jar ./beers-law-lab/beers-law-lab_ta.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_bg.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_bg.jar
echo "signing ./beers-law-lab/concentration_bs.jar"
sign-jar ./beers-law-lab/concentration_bs.jar
echo "signing ./beers-law-lab/concentration_en.jar"
sign-jar ./beers-law-lab/concentration_en.jar
echo "signing ./beers-law-lab/concentration_de.jar"
sign-jar ./beers-law-lab/concentration_de.jar
echo "signing ./beers-law-lab/beers-law-lab_eu.jar"
sign-jar ./beers-law-lab/beers-law-lab_eu.jar
echo "signing ./beers-law-lab/concentration_hu.jar"
sign-jar ./beers-law-lab/concentration_hu.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_iw.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_iw.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_in.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_in.jar
echo "signing ./beers-law-lab/concentration-interviews_eu.jar"
sign-jar ./beers-law-lab/concentration-interviews_eu.jar
echo "signing ./beers-law-lab/beers-law-lab_in.jar"
sign-jar ./beers-law-lab/beers-law-lab_in.jar
echo "signing ./beers-law-lab/concentration-interviews_in.jar"
sign-jar ./beers-law-lab/concentration-interviews_in.jar
echo "signing ./beers-law-lab/beers-law-lab_hu.jar"
sign-jar ./beers-law-lab/beers-law-lab_hu.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_zh_TW.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_zh_TW.jar
echo "signing ./beers-law-lab/concentration_tk.jar"
sign-jar ./beers-law-lab/concentration_tk.jar
echo "signing ./beers-law-lab/concentration_ja.jar"
sign-jar ./beers-law-lab/concentration_ja.jar
echo "signing ./beers-law-lab/concentration-interviews_hu.jar"
sign-jar ./beers-law-lab/concentration-interviews_hu.jar
echo "signing ./beers-law-lab/concentration-interviews_bg.jar"
sign-jar ./beers-law-lab/concentration-interviews_bg.jar
echo "signing ./beers-law-lab/concentration_pt_BR.jar"
sign-jar ./beers-law-lab/concentration_pt_BR.jar
echo "signing ./beers-law-lab/concentration_bg.jar"
sign-jar ./beers-law-lab/concentration_bg.jar
echo "signing ./beers-law-lab/concentration-interviews_mk.jar"
sign-jar ./beers-law-lab/concentration-interviews_mk.jar
echo "signing ./beers-law-lab/concentration_fr.jar"
sign-jar ./beers-law-lab/concentration_fr.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_gl.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_gl.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_el.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_el.jar
echo "signing ./beers-law-lab/beers-law-lab_ar.jar"
sign-jar ./beers-law-lab/beers-law-lab_ar.jar
echo "signing ./beers-law-lab/concentration_ka.jar"
sign-jar ./beers-law-lab/concentration_ka.jar
echo "signing ./beers-law-lab/beers-law-lab-interviews_da.jar"
sign-jar ./beers-law-lab/beers-law-lab-interviews_da.jar
echo "signing ./beers-law-lab/concentration-interviews_ar.jar"
sign-jar ./beers-law-lab/concentration-interviews_ar.jar
echo "signing ./beers-law-lab/concentration-interviews_da.jar"
sign-jar ./beers-law-lab/concentration-interviews_da.jar
echo "signing ./beers-law-lab/concentration-interviews_mr.jar"
sign-jar ./beers-law-lab/concentration-interviews_mr.jar
echo "signing ./beers-law-lab/concentration-interviews_ta.jar"
sign-jar ./beers-law-lab/concentration-interviews_ta.jar
echo "signing ./beers-law-lab/concentration-interviews_zh_CN.jar"
sign-jar ./beers-law-lab/concentration-interviews_zh_CN.jar
echo "signing ./beers-law-lab/concentration-interviews_iw.jar"
sign-jar ./beers-law-lab/concentration-interviews_iw.jar
echo "signing ./beers-law-lab/concentration-interviews_es_PE.jar"
sign-jar ./beers-law-lab/concentration-interviews_es_PE.jar
echo "signing ./beers-law-lab/concentration_fa.jar"
sign-jar ./beers-law-lab/concentration_fa.jar
echo "signing ./beers-law-lab/concentration_es_PE.jar"
sign-jar ./beers-law-lab/concentration_es_PE.jar
echo "signing ./beers-law-lab/beers-law-lab_mr.jar"
sign-jar ./beers-law-lab/beers-law-lab_mr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_nn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_nn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_pt_BR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_pt_BR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ht.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ht.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ta.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ta.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_be.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_be.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_kn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_kn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_it.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_it.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_af.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_af.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_eu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_eu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ko.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ko.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_zh_CN.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_zh_CN.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_be.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_be.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sq.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sq.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_en.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_en.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ms.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ms.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_sl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_sl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ga.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ga.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_fi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_fi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_bs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_bs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_da.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_da.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_it.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_it.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ht.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ht.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ko.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ko.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_gl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_gl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_hy.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_hy.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_hr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_hr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_es_PE.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_es_PE.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_cs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_cs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ru.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ru.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_km.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_km.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ar.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ar.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_eu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_eu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_nb.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_nb.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_sr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_sr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_cs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_cs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_af.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_af.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_sv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_sv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ka.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ka.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_be.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_be.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_pt_BR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_pt_BR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_it.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_it.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_bs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_bs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ca.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ca.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_km.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_km.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_hy.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_hy.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ru.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ru.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ku.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ku.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_vi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_vi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_iw.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_iw.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_en.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_en.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ga.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ga.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_et.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_et.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_tr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_tr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_pt.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_pt.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_iw.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_iw.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_nb.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_nb.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_th.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_th.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_vi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_vi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_nl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_nl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ms.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ms.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_hy.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_hy.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_zh_TW.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_zh_TW.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_bg.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_bg.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_mk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_mk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_zh_CN.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_zh_CN.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_eu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_eu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_zh_CN.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_zh_CN.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_es.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_es.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ga.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ga.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_fr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_fr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_nn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_nn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_pl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_pl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_de.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_de.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_mr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_mr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_tk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_tk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_fr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_fr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_bs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_bs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ar.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ar.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_kn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_kn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_vi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_vi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_km.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_km.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_fa.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_fa.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ro.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ro.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_sk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_sk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_uk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_uk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_pt.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_pt.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_mk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_mk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_in.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_in.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ro.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ro.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_et.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_et.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ku_TR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ku_TR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_mk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_mk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_lv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_lv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ka.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ka.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_mn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_mn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_da.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_da.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_fr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_fr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_de.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_de.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_cs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_cs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ta.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ta.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_pt_BR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_pt_BR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_pl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_pl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ja.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ja.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_mn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_mn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_fa.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_fa.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ko.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ko.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_th.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_th.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_mn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_mn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_sl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_sl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_cs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_cs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_nb.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_nb.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_es.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_es.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_sv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_sv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_et.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_et.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_pt_BR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_pt_BR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_gl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_gl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_et.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_et.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_tr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_tr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_en.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_en.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_hr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_hr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ja.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ja.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_bg.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_bg.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_fi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_fi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ka.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ka.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_pl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_pl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_el.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_el.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_fa.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_fa.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_af.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_af.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit_all_installer.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit_all_installer.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ku.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ku.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_hy.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_hy.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_mk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_mk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_tr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_tr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ga.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ga.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_nn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_nn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_de.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_de.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit_all.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit_all.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ru.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ru.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_de.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_de.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_kn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_kn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_es_PE.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_es_PE.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ko.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ko.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_hu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_hu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_fi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_fi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_af.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_af.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_nn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_nn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_mr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_mr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_sr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_sr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ja.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ja.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_pl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_pl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_sq.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_sq.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_th.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_th.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_lv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_lv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ca.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ca.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_sq.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_sq.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_es_PE.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_es_PE.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_sv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ro.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ro.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_fa.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_fa.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_fi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_fi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_es.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_es.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ku.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ku.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_tk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_tk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_es_PE.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_es_PE.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_uk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_uk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ro.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ro.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_mr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_mr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ht.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ht.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_kn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_kn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_hr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_hr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_in.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_in.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_gl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_gl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_tk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_tk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_tk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_tk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_el.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_el.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ja.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ja.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_in.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_in.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_mr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_mr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_es.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_es.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_el.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_el.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_nl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_nl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_it.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_it.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_km.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_km.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_be.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_be.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ku.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ku.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_sk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_sk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ar.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ar.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ka.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ka.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_iw.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_iw.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ht.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ht.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_bg.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_bg.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_hr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_hr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_gl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_gl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_nb.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_nb.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_pt.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_pt.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_en.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_en.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_th.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_th.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_zh_TW.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_zh_TW.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_uk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_uk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_nl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_nl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ta.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ta.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_da.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_da.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_vi.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_vi.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ms.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ms.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ku_TR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ku_TR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_eu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_eu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_mn.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_mn.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ta.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ta.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ms.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ms.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ar.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ar.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ku_TR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ku_TR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_in.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_in.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ca.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_ca.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_fr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_fr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sq.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_sq.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ku_TR.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_ku_TR.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_pt.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_pt.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_lv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_lv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_zh_CN.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_zh_CN.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_da.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_da.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_ru.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_ru.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_bs.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_bs.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_tr.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_tr.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_zh_TW.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_zh_TW.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_hu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_hu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_lv.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_lv.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_zh_TW.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_zh_TW.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_nl.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac-virtual-lab_nl.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_hu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_hu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_uk.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_uk.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_el.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc-virtual-lab_el.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_bg.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_bg.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-ac_iw.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-ac_iw.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_hu.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_hu.jar
echo "signing ./circuit-construction-kit/circuit-construction-kit-dc_ca.jar"
sign-jar ./circuit-construction-kit/circuit-construction-kit-dc_ca.jar
echo "signing ./wave-on-a-string/wave-on-a-string_tk.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_tk.jar
echo "signing ./wave-on-a-string/wave-on-a-string_nl.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_nl.jar
echo "signing ./wave-on-a-string/wave-on-a-string_da.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_da.jar
echo "signing ./wave-on-a-string/wave-on-a-string_es.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_es.jar
echo "signing ./wave-on-a-string/wave-on-a-string_pt.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_pt.jar
echo "signing ./wave-on-a-string/wave-on-a-string_pl.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_pl.jar
echo "signing ./wave-on-a-string/wave-on-a-string_in.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_in.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ko.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ko.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ru.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ru.jar
echo "signing ./wave-on-a-string/wave-on-a-string_fr.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_fr.jar
echo "signing ./wave-on-a-string/wave-on-a-string_kk.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_kk.jar
echo "signing ./wave-on-a-string/wave-on-a-string_gl.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_gl.jar
echo "signing ./wave-on-a-string/wave-on-a-string_sl.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_sl.jar
echo "signing ./wave-on-a-string/wave-on-a-string_et.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_et.jar
echo "signing ./wave-on-a-string/wave-on-a-string_bs.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_bs.jar
echo "signing ./wave-on-a-string/wave-on-a-string_sk.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_sk.jar
echo "signing ./wave-on-a-string/wave-on-a-string_be.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_be.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ja.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ja.jar
echo "signing ./wave-on-a-string/wave-on-a-string_tr.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_tr.jar
echo "signing ./wave-on-a-string/wave-on-a-string_sq.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_sq.jar
echo "signing ./wave-on-a-string/wave-on-a-string_it.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_it.jar
echo "signing ./wave-on-a-string/wave-on-a-string_vi.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_vi.jar
echo "signing ./wave-on-a-string/wave-on-a-string_km.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_km.jar
echo "signing ./wave-on-a-string/wave-on-a-string_fa.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_fa.jar
echo "signing ./wave-on-a-string/wave-on-a-string_el.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_el.jar
echo "signing ./wave-on-a-string/wave-on-a-string_nb.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_nb.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ku_TR.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ku_TR.jar
echo "signing ./wave-on-a-string/wave-on-a-string_eu.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_eu.jar
echo "signing ./wave-on-a-string/wave-on-a-string_sv.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_sv.jar
echo "signing ./wave-on-a-string/wave-on-a-string_hr.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_hr.jar
echo "signing ./wave-on-a-string/wave-on-a-string_sr.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_sr.jar
echo "signing ./wave-on-a-string/wave-on-a-string_mr.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_mr.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ar.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ar.jar
echo "signing ./wave-on-a-string/wave-on-a-string_es_PE.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_es_PE.jar
echo "signing ./wave-on-a-string/wave-on-a-string_hu.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_hu.jar
echo "signing ./wave-on-a-string/wave-on-a-string_th.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_th.jar
echo "signing ./wave-on-a-string/wave-on-a-string_es_ES.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_es_ES.jar
echo "signing ./wave-on-a-string/wave-on-a-string_fi.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_fi.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ka.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ka.jar
echo "signing ./wave-on-a-string/wave-on-a-string_ar_SA.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_ar_SA.jar
echo "signing ./wave-on-a-string/wave-on-a-string_pt_BR.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_pt_BR.jar
echo "signing ./wave-on-a-string/wave-on-a-string_cs.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_cs.jar
echo "signing ./wave-on-a-string/wave-on-a-string_de.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_de.jar
echo "signing ./wave-on-a-string/wave-on-a-string_iw.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_iw.jar
echo "signing ./wave-on-a-string/wave-on-a-string_mk.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_mk.jar
echo "signing ./wave-on-a-string/wave-on-a-string_zh_CN.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_zh_CN.jar
echo "signing ./wave-on-a-string/wave-on-a-string_zh_TW.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_zh_TW.jar
echo "signing ./wave-on-a-string/wave-on-a-string_en.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_en.jar
echo "signing ./wave-on-a-string/wave-on-a-string_kn.jar"
sign-jar ./wave-on-a-string/wave-on-a-string_kn.jar
echo "signing ./sim-example/sim-example_all_installer.jar"
sign-jar ./sim-example/sim-example_all_installer.jar
echo "signing ./sim-example/sim-example_en.jar"
sign-jar ./sim-example/sim-example_en.jar
echo "signing ./sim-example/sim-example_es.jar"
sign-jar ./sim-example/sim-example_es.jar
echo "signing ./sim-example/sim-example_ar.jar"
sign-jar ./sim-example/sim-example_ar.jar
echo "signing ./sim-example/sim-example_all.jar"
sign-jar ./sim-example/sim-example_all.jar
echo "signing ./equation-grapher/equation-grapher_en.jar"
sign-jar ./equation-grapher/equation-grapher_en.jar
echo "signing ./equation-grapher/equation-grapher_hu.jar"
sign-jar ./equation-grapher/equation-grapher_hu.jar
echo "signing ./equation-grapher/equation-grapher_it.jar"
sign-jar ./equation-grapher/equation-grapher_it.jar
echo "signing ./equation-grapher/equation-grapher_ar_SA.jar"
sign-jar ./equation-grapher/equation-grapher_ar_SA.jar
echo "signing ./equation-grapher/equation-grapher_kk.jar"
sign-jar ./equation-grapher/equation-grapher_kk.jar
echo "signing ./equation-grapher/equation-grapher_mk.jar"
sign-jar ./equation-grapher/equation-grapher_mk.jar
echo "signing ./equation-grapher/equation-grapher_nl.jar"
sign-jar ./equation-grapher/equation-grapher_nl.jar
echo "signing ./equation-grapher/equation-grapher_zh_CN.jar"
sign-jar ./equation-grapher/equation-grapher_zh_CN.jar
echo "signing ./equation-grapher/equation-grapher_tr.jar"
sign-jar ./equation-grapher/equation-grapher_tr.jar
echo "signing ./equation-grapher/equation-grapher_ko.jar"
sign-jar ./equation-grapher/equation-grapher_ko.jar
echo "signing ./equation-grapher/equation-grapher_sq.jar"
sign-jar ./equation-grapher/equation-grapher_sq.jar
echo "signing ./equation-grapher/equation-grapher_da.jar"
sign-jar ./equation-grapher/equation-grapher_da.jar
echo "signing ./equation-grapher/equation-grapher_in.jar"
sign-jar ./equation-grapher/equation-grapher_in.jar
echo "signing ./equation-grapher/equation-grapher_pl.jar"
sign-jar ./equation-grapher/equation-grapher_pl.jar
echo "signing ./equation-grapher/equation-grapher_vi.jar"
sign-jar ./equation-grapher/equation-grapher_vi.jar
echo "signing ./equation-grapher/equation-grapher_fa.jar"
sign-jar ./equation-grapher/equation-grapher_fa.jar
echo "signing ./equation-grapher/equation-grapher_et.jar"
sign-jar ./equation-grapher/equation-grapher_et.jar
echo "signing ./equation-grapher/equation-grapher_sv.jar"
sign-jar ./equation-grapher/equation-grapher_sv.jar
echo "signing ./equation-grapher/equation-grapher_ru.jar"
sign-jar ./equation-grapher/equation-grapher_ru.jar
echo "signing ./equation-grapher/equation-grapher_de.jar"
sign-jar ./equation-grapher/equation-grapher_de.jar
echo "signing ./equation-grapher/equation-grapher_sk.jar"
sign-jar ./equation-grapher/equation-grapher_sk.jar
echo "signing ./equation-grapher/equation-grapher_ar.jar"
sign-jar ./equation-grapher/equation-grapher_ar.jar
echo "signing ./equation-grapher/equation-grapher_iw.jar"
sign-jar ./equation-grapher/equation-grapher_iw.jar
echo "signing ./equation-grapher/equation-grapher_tk.jar"
sign-jar ./equation-grapher/equation-grapher_tk.jar
echo "signing ./equation-grapher/equation-grapher_lv.jar"
sign-jar ./equation-grapher/equation-grapher_lv.jar
echo "signing ./equation-grapher/equation-grapher_bs.jar"
sign-jar ./equation-grapher/equation-grapher_bs.jar
echo "signing ./equation-grapher/equation-grapher_es_CO.jar"
sign-jar ./equation-grapher/equation-grapher_es_CO.jar
echo "signing ./equation-grapher/equation-grapher_zh_TW.jar"
sign-jar ./equation-grapher/equation-grapher_zh_TW.jar
echo "signing ./equation-grapher/equation-grapher_ka.jar"
sign-jar ./equation-grapher/equation-grapher_ka.jar
echo "signing ./equation-grapher/equation-grapher_pt_BR.jar"
sign-jar ./equation-grapher/equation-grapher_pt_BR.jar
echo "signing ./equation-grapher/equation-grapher_el.jar"
sign-jar ./equation-grapher/equation-grapher_el.jar
echo "signing ./equation-grapher/equation-grapher_ja.jar"
sign-jar ./equation-grapher/equation-grapher_ja.jar
echo "signing ./equation-grapher/equation-grapher_fr.jar"
sign-jar ./equation-grapher/equation-grapher_fr.jar
echo "signing ./equation-grapher/equation-grapher_es.jar"
sign-jar ./equation-grapher/equation-grapher_es.jar
echo "signing ./equation-grapher/equation-grapher_hr.jar"
sign-jar ./equation-grapher/equation-grapher_hr.jar
echo "signing ./equation-grapher/equation-grapher_si.jar"
sign-jar ./equation-grapher/equation-grapher_si.jar
echo "signing ./equation-grapher/equation-grapher_es_PE.jar"
sign-jar ./equation-grapher/equation-grapher_es_PE.jar
echo "signing ./equation-grapher/equation-grapher_eu.jar"
sign-jar ./equation-grapher/equation-grapher_eu.jar
echo "signing ./equation-grapher/equation-grapher_sr.jar"
sign-jar ./equation-grapher/equation-grapher_sr.jar
echo "signing ./motion-2d/motion-2d_fa.jar"
sign-jar ./motion-2d/motion-2d_fa.jar
echo "signing ./motion-2d/motion-2d_be.jar"
sign-jar ./motion-2d/motion-2d_be.jar
echo "signing ./motion-2d/motion-2d_fr.jar"
sign-jar ./motion-2d/motion-2d_fr.jar
echo "signing ./motion-2d/motion-2d_ar.jar"
sign-jar ./motion-2d/motion-2d_ar.jar
echo "signing ./motion-2d/motion-2d_kn.jar"
sign-jar ./motion-2d/motion-2d_kn.jar
echo "signing ./motion-2d/motion-2d_el.jar"
sign-jar ./motion-2d/motion-2d_el.jar
echo "signing ./motion-2d/motion-2d_gl.jar"
sign-jar ./motion-2d/motion-2d_gl.jar
echo "signing ./motion-2d/motion-2d_sk.jar"
sign-jar ./motion-2d/motion-2d_sk.jar
echo "signing ./motion-2d/motion-2d_in.jar"
sign-jar ./motion-2d/motion-2d_in.jar
echo "signing ./motion-2d/motion-2d_km.jar"
sign-jar ./motion-2d/motion-2d_km.jar
echo "signing ./motion-2d/motion-2d_eu.jar"
sign-jar ./motion-2d/motion-2d_eu.jar
echo "signing ./motion-2d/motion-2d_zh_TW.jar"
sign-jar ./motion-2d/motion-2d_zh_TW.jar
echo "signing ./motion-2d/motion-2d_bg.jar"
sign-jar ./motion-2d/motion-2d_bg.jar
echo "signing ./motion-2d/motion-2d_th.jar"
sign-jar ./motion-2d/motion-2d_th.jar
echo "signing ./motion-2d/motion-2d_tr.jar"
sign-jar ./motion-2d/motion-2d_tr.jar
echo "signing ./motion-2d/motion-2d_ms.jar"
sign-jar ./motion-2d/motion-2d_ms.jar
echo "signing ./motion-2d/motion-2d_nb.jar"
sign-jar ./motion-2d/motion-2d_nb.jar
echo "signing ./motion-2d/motion-2d_ru.jar"
sign-jar ./motion-2d/motion-2d_ru.jar
echo "signing ./motion-2d/motion-2d_vi.jar"
sign-jar ./motion-2d/motion-2d_vi.jar
echo "signing ./motion-2d/motion-2d_ko.jar"
sign-jar ./motion-2d/motion-2d_ko.jar
echo "signing ./motion-2d/motion-2d_cs.jar"
sign-jar ./motion-2d/motion-2d_cs.jar
echo "signing ./motion-2d/motion-2d_da.jar"
sign-jar ./motion-2d/motion-2d_da.jar
echo "signing ./motion-2d/motion-2d_mk.jar"
sign-jar ./motion-2d/motion-2d_mk.jar
echo "signing ./motion-2d/motion-2d_it.jar"
sign-jar ./motion-2d/motion-2d_it.jar
echo "signing ./motion-2d/motion-2d_kk.jar"
sign-jar ./motion-2d/motion-2d_kk.jar
echo "signing ./motion-2d/motion-2d_en.jar"
sign-jar ./motion-2d/motion-2d_en.jar
echo "signing ./motion-2d/motion-2d_sr.jar"
sign-jar ./motion-2d/motion-2d_sr.jar
echo "signing ./motion-2d/motion-2d_all.jar"
sign-jar ./motion-2d/motion-2d_all.jar
echo "signing ./motion-2d/motion-2d_tk.jar"
sign-jar ./motion-2d/motion-2d_tk.jar
echo "signing ./motion-2d/motion-2d_zh_CN.jar"
sign-jar ./motion-2d/motion-2d_zh_CN.jar
echo "signing ./motion-2d/motion-2d_pt.jar"
sign-jar ./motion-2d/motion-2d_pt.jar
echo "signing ./motion-2d/motion-2d_bs.jar"
sign-jar ./motion-2d/motion-2d_bs.jar
echo "signing ./motion-2d/motion-2d_et.jar"
sign-jar ./motion-2d/motion-2d_et.jar
echo "signing ./motion-2d/motion-2d_hu.jar"
sign-jar ./motion-2d/motion-2d_hu.jar
echo "signing ./motion-2d/motion-2d_ar_SA.jar"
sign-jar ./motion-2d/motion-2d_ar_SA.jar
echo "signing ./motion-2d/motion-2d_uk.jar"
sign-jar ./motion-2d/motion-2d_uk.jar
echo "signing ./motion-2d/motion-2d_sv.jar"
sign-jar ./motion-2d/motion-2d_sv.jar
echo "signing ./motion-2d/motion-2d_es_PE.jar"
sign-jar ./motion-2d/motion-2d_es_PE.jar
echo "signing ./motion-2d/motion-2d_de.jar"
sign-jar ./motion-2d/motion-2d_de.jar
echo "signing ./motion-2d/motion-2d_ja.jar"
sign-jar ./motion-2d/motion-2d_ja.jar
echo "signing ./motion-2d/motion-2d_mr.jar"
sign-jar ./motion-2d/motion-2d_mr.jar
echo "signing ./motion-2d/motion-2d_pt_BR.jar"
sign-jar ./motion-2d/motion-2d_pt_BR.jar
echo "signing ./motion-2d/motion-2d_all_installer.jar"
sign-jar ./motion-2d/motion-2d_all_installer.jar
echo "signing ./motion-2d/motion-2d_lv.jar"
sign-jar ./motion-2d/motion-2d_lv.jar
echo "signing ./motion-2d/motion-2d_nl.jar"
sign-jar ./motion-2d/motion-2d_nl.jar
echo "signing ./motion-2d/motion-2d_iw.jar"
sign-jar ./motion-2d/motion-2d_iw.jar
echo "signing ./motion-2d/motion-2d_hr.jar"
sign-jar ./motion-2d/motion-2d_hr.jar
echo "signing ./motion-2d/motion-2d_es.jar"
sign-jar ./motion-2d/motion-2d_es.jar
echo "signing ./motion-2d/motion-2d_pl.jar"
sign-jar ./motion-2d/motion-2d_pl.jar
echo "signing ./motion-2d/motion-2d_ku_TR.jar"
sign-jar ./motion-2d/motion-2d_ku_TR.jar
echo "signing ./build-a-molecule/build-a-molecule_all.jar"
sign-jar ./build-a-molecule/build-a-molecule_all.jar
echo "signing ./build-a-molecule/build-a-molecule_sl.jar"
sign-jar ./build-a-molecule/build-a-molecule_sl.jar
echo "signing ./build-a-molecule/build-a-molecule_el.jar"
sign-jar ./build-a-molecule/build-a-molecule_el.jar
echo "signing ./build-a-molecule/build-a-molecule_pt_BR.jar"
sign-jar ./build-a-molecule/build-a-molecule_pt_BR.jar
echo "signing ./build-a-molecule/build-a-molecule_ko.jar"
sign-jar ./build-a-molecule/build-a-molecule_ko.jar
echo "signing ./build-a-molecule/build-a-molecule_eu.jar"
sign-jar ./build-a-molecule/build-a-molecule_eu.jar
echo "signing ./build-a-molecule/build-a-molecule_nb.jar"
sign-jar ./build-a-molecule/build-a-molecule_nb.jar
echo "signing ./build-a-molecule/build-a-molecule_all_installer.jar"
sign-jar ./build-a-molecule/build-a-molecule_all_installer.jar
echo "signing ./build-a-molecule/build-a-molecule_tk.jar"
sign-jar ./build-a-molecule/build-a-molecule_tk.jar
echo "signing ./build-a-molecule/build-a-molecule_tr.jar"
sign-jar ./build-a-molecule/build-a-molecule_tr.jar
echo "signing ./build-a-molecule/build-a-molecule_nn.jar"
sign-jar ./build-a-molecule/build-a-molecule_nn.jar
echo "signing ./build-a-molecule/build-a-molecule_kk.jar"
sign-jar ./build-a-molecule/build-a-molecule_kk.jar
echo "signing ./build-a-molecule/build-a-molecule_ru.jar"
sign-jar ./build-a-molecule/build-a-molecule_ru.jar
echo "signing ./build-a-molecule/build-a-molecule_it.jar"
sign-jar ./build-a-molecule/build-a-molecule_it.jar
echo "signing ./build-a-molecule/build-a-molecule_nl.jar"
sign-jar ./build-a-molecule/build-a-molecule_nl.jar
echo "signing ./build-a-molecule/build-a-molecule_et.jar"
sign-jar ./build-a-molecule/build-a-molecule_et.jar
echo "signing ./build-a-molecule/build-a-molecule_ca.jar"
sign-jar ./build-a-molecule/build-a-molecule_ca.jar
echo "signing ./build-a-molecule/build-a-molecule_km.jar"
sign-jar ./build-a-molecule/build-a-molecule_km.jar
echo "signing ./build-a-molecule/build-a-molecule_da.jar"
sign-jar ./build-a-molecule/build-a-molecule_da.jar
echo "signing ./build-a-molecule/build-a-molecule_sk.jar"
sign-jar ./build-a-molecule/build-a-molecule_sk.jar
echo "signing ./build-a-molecule/build-a-molecule_ar.jar"
sign-jar ./build-a-molecule/build-a-molecule_ar.jar
echo "signing ./build-a-molecule/build-a-molecule_en.jar"
sign-jar ./build-a-molecule/build-a-molecule_en.jar
echo "signing ./build-a-molecule/build-a-molecule_mk.jar"
sign-jar ./build-a-molecule/build-a-molecule_mk.jar
echo "signing ./build-a-molecule/build-a-molecule_iw.jar"
sign-jar ./build-a-molecule/build-a-molecule_iw.jar
echo "signing ./build-a-molecule/build-a-molecule_fi.jar"
sign-jar ./build-a-molecule/build-a-molecule_fi.jar
echo "signing ./build-a-molecule/build-a-molecule_es.jar"
sign-jar ./build-a-molecule/build-a-molecule_es.jar
echo "signing ./build-a-molecule/build-a-molecule_hu.jar"
sign-jar ./build-a-molecule/build-a-molecule_hu.jar
echo "signing ./build-a-molecule/build-a-molecule_vi.jar"
sign-jar ./build-a-molecule/build-a-molecule_vi.jar
echo "signing ./build-a-molecule/build-a-molecule_gl.jar"
sign-jar ./build-a-molecule/build-a-molecule_gl.jar
echo "signing ./build-a-molecule/build-a-molecule_fa.jar"
sign-jar ./build-a-molecule/build-a-molecule_fa.jar
echo "signing ./build-a-molecule/build-a-molecule_bs.jar"
sign-jar ./build-a-molecule/build-a-molecule_bs.jar
echo "signing ./build-a-molecule/build-a-molecule_de.jar"
sign-jar ./build-a-molecule/build-a-molecule_de.jar
echo "signing ./build-a-molecule/build-a-molecule_pl.jar"
sign-jar ./build-a-molecule/build-a-molecule_pl.jar
echo "signing ./build-a-molecule/build-a-molecule_mr.jar"
sign-jar ./build-a-molecule/build-a-molecule_mr.jar
echo "signing ./build-a-molecule/build-a-molecule_th.jar"
sign-jar ./build-a-molecule/build-a-molecule_th.jar
echo "signing ./build-a-molecule/build-a-molecule_sr.jar"
sign-jar ./build-a-molecule/build-a-molecule_sr.jar
echo "signing ./build-a-molecule/build-a-molecule_cs.jar"
sign-jar ./build-a-molecule/build-a-molecule_cs.jar
echo "signing ./build-a-molecule/build-a-molecule_zh_CN.jar"
sign-jar ./build-a-molecule/build-a-molecule_zh_CN.jar
echo "signing ./build-a-molecule/build-a-molecule_sv.jar"
sign-jar ./build-a-molecule/build-a-molecule_sv.jar
echo "signing ./build-a-molecule/build-a-molecule_zh_TW.jar"
sign-jar ./build-a-molecule/build-a-molecule_zh_TW.jar
echo "signing ./build-a-molecule/build-a-molecule_fr.jar"
sign-jar ./build-a-molecule/build-a-molecule_fr.jar
echo "signing ./build-a-molecule/build-a-molecule_es_PE.jar"
sign-jar ./build-a-molecule/build-a-molecule_es_PE.jar
echo "signing ./build-a-molecule/build-a-molecule_ja.jar"
sign-jar ./build-a-molecule/build-a-molecule_ja.jar
echo "signing ./flash-common-strings/flash-common-strings_en.jar"
sign-jar ./flash-common-strings/flash-common-strings_en.jar
echo "signing ./faradays-law/faradays-law_et.jar"
sign-jar ./faradays-law/faradays-law_et.jar
echo "signing ./faradays-law/faradays-law_uk.jar"
sign-jar ./faradays-law/faradays-law_uk.jar
echo "signing ./faradays-law/faradays-law_mr.jar"
sign-jar ./faradays-law/faradays-law_mr.jar
echo "signing ./faradays-law/faradays-law_ca.jar"
sign-jar ./faradays-law/faradays-law_ca.jar
echo "signing ./faradays-law/faradays-law_pt_BR.jar"
sign-jar ./faradays-law/faradays-law_pt_BR.jar
echo "signing ./faradays-law/faradays-law_fr.jar"
sign-jar ./faradays-law/faradays-law_fr.jar
echo "signing ./faradays-law/faradays-law_hr.jar"
sign-jar ./faradays-law/faradays-law_hr.jar
echo "signing ./faradays-law/faradays-law_ka.jar"
sign-jar ./faradays-law/faradays-law_ka.jar
echo "signing ./faradays-law/faradays-law_eu.jar"
sign-jar ./faradays-law/faradays-law_eu.jar
echo "signing ./faradays-law/faradays-law_be.jar"
sign-jar ./faradays-law/faradays-law_be.jar
echo "signing ./faradays-law/faradays-law_sq.jar"
sign-jar ./faradays-law/faradays-law_sq.jar
echo "signing ./faradays-law/faradays-law_de.jar"
sign-jar ./faradays-law/faradays-law_de.jar
echo "signing ./faradays-law/faradays-law_da.jar"
sign-jar ./faradays-law/faradays-law_da.jar
echo "signing ./faradays-law/faradays-law_lv.jar"
sign-jar ./faradays-law/faradays-law_lv.jar
echo "signing ./faradays-law/faradays-law_in.jar"
sign-jar ./faradays-law/faradays-law_in.jar
echo "signing ./faradays-law/faradays-law_pt.jar"
sign-jar ./faradays-law/faradays-law_pt.jar
echo "signing ./faradays-law/faradays-law_km.jar"
sign-jar ./faradays-law/faradays-law_km.jar
echo "signing ./faradays-law/faradays-law_es.jar"
sign-jar ./faradays-law/faradays-law_es.jar
echo "signing ./faradays-law/faradays-law_mk.jar"
sign-jar ./faradays-law/faradays-law_mk.jar
echo "signing ./faradays-law/faradays-law_th.jar"
sign-jar ./faradays-law/faradays-law_th.jar
echo "signing ./faradays-law/faradays-law_es_PE.jar"
sign-jar ./faradays-law/faradays-law_es_PE.jar
echo "signing ./faradays-law/faradays-law_tr.jar"
sign-jar ./faradays-law/faradays-law_tr.jar
echo "signing ./faradays-law/faradays-law_sr.jar"
sign-jar ./faradays-law/faradays-law_sr.jar
echo "signing ./faradays-law/faradays-law_sv.jar"
sign-jar ./faradays-law/faradays-law_sv.jar
echo "signing ./faradays-law/faradays-law_kk.jar"
sign-jar ./faradays-law/faradays-law_kk.jar
echo "signing ./faradays-law/faradays-law_bg.jar"
sign-jar ./faradays-law/faradays-law_bg.jar
echo "signing ./faradays-law/faradays-law_nl.jar"
sign-jar ./faradays-law/faradays-law_nl.jar
echo "signing ./faradays-law/faradays-law_kn.jar"
sign-jar ./faradays-law/faradays-law_kn.jar
echo "signing ./faradays-law/faradays-law_cs.jar"
sign-jar ./faradays-law/faradays-law_cs.jar
echo "signing ./faradays-law/faradays-law_ko.jar"
sign-jar ./faradays-law/faradays-law_ko.jar
echo "signing ./faradays-law/faradays-law_ta.jar"
sign-jar ./faradays-law/faradays-law_ta.jar
echo "signing ./faradays-law/faradays-law_ar.jar"
sign-jar ./faradays-law/faradays-law_ar.jar
echo "signing ./faradays-law/faradays-law_bs.jar"
sign-jar ./faradays-law/faradays-law_bs.jar
echo "signing ./faradays-law/faradays-law_gl.jar"
sign-jar ./faradays-law/faradays-law_gl.jar
echo "signing ./faradays-law/faradays-law_el.jar"
sign-jar ./faradays-law/faradays-law_el.jar
echo "signing ./faradays-law/faradays-law_hu.jar"
sign-jar ./faradays-law/faradays-law_hu.jar
echo "signing ./faradays-law/faradays-law_fa.jar"
sign-jar ./faradays-law/faradays-law_fa.jar
echo "signing ./faradays-law/faradays-law_zh_TW.jar"
sign-jar ./faradays-law/faradays-law_zh_TW.jar
echo "signing ./faradays-law/faradays-law_it.jar"
sign-jar ./faradays-law/faradays-law_it.jar
echo "signing ./faradays-law/faradays-law_sk.jar"
sign-jar ./faradays-law/faradays-law_sk.jar
echo "signing ./faradays-law/faradays-law_pl.jar"
sign-jar ./faradays-law/faradays-law_pl.jar
echo "signing ./faradays-law/faradays-law_ar_SA.jar"
sign-jar ./faradays-law/faradays-law_ar_SA.jar
echo "signing ./faradays-law/faradays-law_ja.jar"
sign-jar ./faradays-law/faradays-law_ja.jar
echo "signing ./faradays-law/faradays-law_en.jar"
sign-jar ./faradays-law/faradays-law_en.jar
echo "signing ./faradays-law/faradays-law_vi.jar"
sign-jar ./faradays-law/faradays-law_vi.jar
echo "signing ./faradays-law/faradays-law_zh_CN.jar"
sign-jar ./faradays-law/faradays-law_zh_CN.jar
echo "signing ./faradays-law/faradays-law_sl.jar"
sign-jar ./faradays-law/faradays-law_sl.jar
echo "signing ./faradays-law/faradays-law_ro.jar"
sign-jar ./faradays-law/faradays-law_ro.jar
echo "signing ./faradays-law/faradays-law_iw.jar"
sign-jar ./faradays-law/faradays-law_iw.jar
echo "signing ./faradays-law/faradays-law_tk.jar"
sign-jar ./faradays-law/faradays-law_tk.jar
echo "signing ./faradays-law/faradays-law_ms.jar"
sign-jar ./faradays-law/faradays-law_ms.jar
echo "signing ./line-graphing/graphing-lines_pl.jar"
sign-jar ./line-graphing/graphing-lines_pl.jar
echo "signing ./line-graphing/graphing-lines_in.jar"
sign-jar ./line-graphing/graphing-lines_in.jar
echo "signing ./line-graphing/graphing-lines_cs.jar"
sign-jar ./line-graphing/graphing-lines_cs.jar
echo "signing ./line-graphing/line-graphing_all_installer.jar"
sign-jar ./line-graphing/line-graphing_all_installer.jar
echo "signing ./line-graphing/graphing-lines_bs.jar"
sign-jar ./line-graphing/graphing-lines_bs.jar
echo "signing ./line-graphing/graphing-lines_tr.jar"
sign-jar ./line-graphing/graphing-lines_tr.jar
echo "signing ./line-graphing/graphing-lines_sr.jar"
sign-jar ./line-graphing/graphing-lines_sr.jar
echo "signing ./line-graphing/graphing-lines_ar.jar"
sign-jar ./line-graphing/graphing-lines_ar.jar
echo "signing ./line-graphing/graphing-lines_sk.jar"
sign-jar ./line-graphing/graphing-lines_sk.jar
echo "signing ./line-graphing/graphing-lines_zh_CN.jar"
sign-jar ./line-graphing/graphing-lines_zh_CN.jar
echo "signing ./line-graphing/graphing-lines_da.jar"
sign-jar ./line-graphing/graphing-lines_da.jar
echo "signing ./line-graphing/graphing-lines_vi.jar"
sign-jar ./line-graphing/graphing-lines_vi.jar
echo "signing ./line-graphing/graphing-lines_nl.jar"
sign-jar ./line-graphing/graphing-lines_nl.jar
echo "signing ./line-graphing/graphing-lines_pt_BR.jar"
sign-jar ./line-graphing/graphing-lines_pt_BR.jar
echo "signing ./line-graphing/graphing-lines_fa.jar"
sign-jar ./line-graphing/graphing-lines_fa.jar
echo "signing ./line-graphing/line-graphing_all.jar"
sign-jar ./line-graphing/line-graphing_all.jar
echo "signing ./line-graphing/graphing-lines_en.jar"
sign-jar ./line-graphing/graphing-lines_en.jar
echo "signing ./line-graphing/graphing-lines_sv.jar"
sign-jar ./line-graphing/graphing-lines_sv.jar
echo "signing ./line-graphing/graphing-lines_nb.jar"
sign-jar ./line-graphing/graphing-lines_nb.jar
echo "signing ./line-graphing/graphing-lines_it.jar"
sign-jar ./line-graphing/graphing-lines_it.jar
echo "signing ./line-graphing/graphing-lines_zh_TW.jar"
sign-jar ./line-graphing/graphing-lines_zh_TW.jar
echo "signing ./line-graphing/graphing-lines_iw.jar"
sign-jar ./line-graphing/graphing-lines_iw.jar
echo "signing ./line-graphing/graphing-lines_sl.jar"
sign-jar ./line-graphing/graphing-lines_sl.jar
echo "signing ./line-graphing/graphing-lines_fr.jar"
sign-jar ./line-graphing/graphing-lines_fr.jar
echo "signing ./line-graphing/graphing-lines_nn.jar"
sign-jar ./line-graphing/graphing-lines_nn.jar
echo "signing ./line-graphing/graphing-lines_el.jar"
sign-jar ./line-graphing/graphing-lines_el.jar
echo "signing ./line-graphing/graphing-lines_eu.jar"
sign-jar ./line-graphing/graphing-lines_eu.jar
echo "signing ./line-graphing/graphing-lines_ko.jar"
sign-jar ./line-graphing/graphing-lines_ko.jar
echo "signing ./line-graphing/graphing-lines_es_PE.jar"
sign-jar ./line-graphing/graphing-lines_es_PE.jar
echo "signing ./line-graphing/graphing-lines_kk.jar"
sign-jar ./line-graphing/graphing-lines_kk.jar
echo "signing ./line-graphing/graphing-lines_es.jar"
sign-jar ./line-graphing/graphing-lines_es.jar
echo "signing ./line-graphing/graphing-lines_hu.jar"
sign-jar ./line-graphing/graphing-lines_hu.jar
echo "signing ./line-graphing/graphing-lines_de.jar"
sign-jar ./line-graphing/graphing-lines_de.jar
echo "signing ./microwaves/microwaves_gl.jar"
sign-jar ./microwaves/microwaves_gl.jar
echo "signing ./microwaves/microwaves_sv.jar"
sign-jar ./microwaves/microwaves_sv.jar
echo "signing ./microwaves/microwaves_de.jar"
sign-jar ./microwaves/microwaves_de.jar
echo "signing ./microwaves/microwaves_fr.jar"
sign-jar ./microwaves/microwaves_fr.jar
echo "signing ./microwaves/microwaves_pt_BR.jar"
sign-jar ./microwaves/microwaves_pt_BR.jar
echo "signing ./microwaves/microwaves_ar_SA.jar"
sign-jar ./microwaves/microwaves_ar_SA.jar
echo "signing ./microwaves/microwaves_en.jar"
sign-jar ./microwaves/microwaves_en.jar
echo "signing ./microwaves/microwaves_eu.jar"
sign-jar ./microwaves/microwaves_eu.jar
echo "signing ./microwaves/microwaves_kn.jar"
sign-jar ./microwaves/microwaves_kn.jar
echo "signing ./microwaves/microwaves_tr.jar"
sign-jar ./microwaves/microwaves_tr.jar
echo "signing ./microwaves/microwaves_mk.jar"
sign-jar ./microwaves/microwaves_mk.jar
echo "signing ./microwaves/microwaves_nl.jar"
sign-jar ./microwaves/microwaves_nl.jar
echo "signing ./microwaves/microwaves_pt.jar"
sign-jar ./microwaves/microwaves_pt.jar
echo "signing ./microwaves/microwaves_es_PE.jar"
sign-jar ./microwaves/microwaves_es_PE.jar
echo "signing ./microwaves/microwaves_zh_CN.jar"
sign-jar ./microwaves/microwaves_zh_CN.jar
echo "signing ./microwaves/microwaves_hr.jar"
sign-jar ./microwaves/microwaves_hr.jar
echo "signing ./microwaves/microwaves_ru.jar"
sign-jar ./microwaves/microwaves_ru.jar
echo "signing ./microwaves/microwaves_mr.jar"
sign-jar ./microwaves/microwaves_mr.jar
echo "signing ./microwaves/microwaves_es.jar"
sign-jar ./microwaves/microwaves_es.jar
echo "signing ./microwaves/microwaves_zh_TW.jar"
sign-jar ./microwaves/microwaves_zh_TW.jar
echo "signing ./microwaves/microwaves_fa.jar"
sign-jar ./microwaves/microwaves_fa.jar
echo "signing ./microwaves/microwaves_in.jar"
sign-jar ./microwaves/microwaves_in.jar
echo "signing ./microwaves/microwaves_th.jar"
sign-jar ./microwaves/microwaves_th.jar
echo "signing ./microwaves/microwaves_km.jar"
sign-jar ./microwaves/microwaves_km.jar
echo "signing ./microwaves/microwaves_pl.jar"
sign-jar ./microwaves/microwaves_pl.jar
echo "signing ./microwaves/microwaves_lv.jar"
sign-jar ./microwaves/microwaves_lv.jar
echo "signing ./microwaves/microwaves_be.jar"
sign-jar ./microwaves/microwaves_be.jar
echo "signing ./microwaves/microwaves_it.jar"
sign-jar ./microwaves/microwaves_it.jar
echo "signing ./microwaves/microwaves_da.jar"
sign-jar ./microwaves/microwaves_da.jar
echo "signing ./microwaves/microwaves_sr.jar"
sign-jar ./microwaves/microwaves_sr.jar
echo "signing ./microwaves/microwaves_el.jar"
sign-jar ./microwaves/microwaves_el.jar
echo "signing ./microwaves/microwaves_ko.jar"
sign-jar ./microwaves/microwaves_ko.jar
echo "signing ./microwaves/microwaves_bs.jar"
sign-jar ./microwaves/microwaves_bs.jar
echo "signing ./microwaves/microwaves_kk.jar"
sign-jar ./microwaves/microwaves_kk.jar
echo "signing ./microwaves/microwaves_sk.jar"
sign-jar ./microwaves/microwaves_sk.jar
echo "signing ./microwaves/microwaves_tk.jar"
sign-jar ./microwaves/microwaves_tk.jar
echo "signing ./microwaves/microwaves_vi.jar"
sign-jar ./microwaves/microwaves_vi.jar
echo "signing ./microwaves/microwaves_all_installer.jar"
sign-jar ./microwaves/microwaves_all_installer.jar
echo "signing ./microwaves/microwaves_cs.jar"
sign-jar ./microwaves/microwaves_cs.jar
echo "signing ./microwaves/microwaves_ja.jar"
sign-jar ./microwaves/microwaves_ja.jar
echo "signing ./microwaves/microwaves_all.jar"
sign-jar ./microwaves/microwaves_all.jar
echo "signing ./microwaves/microwaves_iw.jar"
sign-jar ./microwaves/microwaves_iw.jar
echo "signing ./microwaves/microwaves_hu.jar"
sign-jar ./microwaves/microwaves_hu.jar
echo "signing ./microwaves/microwaves_ka.jar"
sign-jar ./microwaves/microwaves_ka.jar
echo "signing ./cavendish-experiment/cavendish-experiment_all.jar"
sign-jar ./cavendish-experiment/cavendish-experiment_all.jar
echo "signing ./cavendish-experiment/cavendish-experiment_en.jar"
sign-jar ./cavendish-experiment/cavendish-experiment_en.jar
echo "signing ./cavendish-experiment/cavendish-experiment_all_installer.jar"
sign-jar ./cavendish-experiment/cavendish-experiment_all_installer.jar
echo "signing ./motion-series/ramp-forces-and-motion_kn.jar"
sign-jar ./motion-series/ramp-forces-and-motion_kn.jar
echo "signing ./motion-series/ramp-forces-and-motion_sk.jar"
sign-jar ./motion-series/ramp-forces-and-motion_sk.jar
echo "signing ./motion-series/ramp-forces-and-motion_mn.jar"
sign-jar ./motion-series/ramp-forces-and-motion_mn.jar
echo "signing ./motion-series/ramp-forces-and-motion_mi.jar"
sign-jar ./motion-series/ramp-forces-and-motion_mi.jar
echo "signing ./motion-series/forces-and-motion_zh_TW.jar"
sign-jar ./motion-series/forces-and-motion_zh_TW.jar
echo "signing ./motion-series/forces-and-motion_tr.jar"
sign-jar ./motion-series/forces-and-motion_tr.jar
echo "signing ./motion-series/ramp-forces-and-motion_fi.jar"
sign-jar ./motion-series/ramp-forces-and-motion_fi.jar
echo "signing ./motion-series/motion-series_all_installer.jar"
sign-jar ./motion-series/motion-series_all_installer.jar
echo "signing ./motion-series/forces-and-motion_de.jar"
sign-jar ./motion-series/forces-and-motion_de.jar
echo "signing ./motion-series/forces-and-motion_eu.jar"
sign-jar ./motion-series/forces-and-motion_eu.jar
echo "signing ./motion-series/forces-and-motion_in.jar"
sign-jar ./motion-series/forces-and-motion_in.jar
echo "signing ./motion-series/forces-and-motion_nl.jar"
sign-jar ./motion-series/forces-and-motion_nl.jar
echo "signing ./motion-series/forces-and-motion_ht.jar"
sign-jar ./motion-series/forces-and-motion_ht.jar
echo "signing ./motion-series/ramp-forces-and-motion_eu.jar"
sign-jar ./motion-series/ramp-forces-and-motion_eu.jar
echo "signing ./motion-series/ramp-forces-and-motion_de.jar"
sign-jar ./motion-series/ramp-forces-and-motion_de.jar
echo "signing ./motion-series/forces-and-motion_uk.jar"
sign-jar ./motion-series/forces-and-motion_uk.jar
echo "signing ./motion-series/motion-series_all.jar"
sign-jar ./motion-series/motion-series_all.jar
echo "signing ./motion-series/forces-and-motion_sl.jar"
sign-jar ./motion-series/forces-and-motion_sl.jar
echo "signing ./motion-series/forces-and-motion_pl.jar"
sign-jar ./motion-series/forces-and-motion_pl.jar
echo "signing ./motion-series/ramp-forces-and-motion_ro.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ro.jar
echo "signing ./motion-series/forces-and-motion_af.jar"
sign-jar ./motion-series/forces-and-motion_af.jar
echo "signing ./motion-series/ramp-forces-and-motion_et.jar"
sign-jar ./motion-series/ramp-forces-and-motion_et.jar
echo "signing ./motion-series/ramp-forces-and-motion_ru.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ru.jar
echo "signing ./motion-series/forces-and-motion_fa.jar"
sign-jar ./motion-series/forces-and-motion_fa.jar
echo "signing ./motion-series/ramp-forces-and-motion_pt_BR.jar"
sign-jar ./motion-series/ramp-forces-and-motion_pt_BR.jar
echo "signing ./motion-series/ramp-forces-and-motion_nl.jar"
sign-jar ./motion-series/ramp-forces-and-motion_nl.jar
echo "signing ./motion-series/forces-and-motion_iw.jar"
sign-jar ./motion-series/forces-and-motion_iw.jar
echo "signing ./motion-series/ramp-forces-and-motion_hu.jar"
sign-jar ./motion-series/ramp-forces-and-motion_hu.jar
echo "signing ./motion-series/ramp-forces-and-motion_tr.jar"
sign-jar ./motion-series/ramp-forces-and-motion_tr.jar
echo "signing ./motion-series/forces-and-motion_ar.jar"
sign-jar ./motion-series/forces-and-motion_ar.jar
echo "signing ./motion-series/ramp-forces-and-motion_iw.jar"
sign-jar ./motion-series/ramp-forces-and-motion_iw.jar
echo "signing ./motion-series/ramp-forces-and-motion_el.jar"
sign-jar ./motion-series/ramp-forces-and-motion_el.jar
echo "signing ./motion-series/forces-and-motion_gl.jar"
sign-jar ./motion-series/forces-and-motion_gl.jar
echo "signing ./motion-series/ramp-forces-and-motion_ja.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ja.jar
echo "signing ./motion-series/ramp-forces-and-motion_in.jar"
sign-jar ./motion-series/ramp-forces-and-motion_in.jar
echo "signing ./motion-series/forces-and-motion_es.jar"
sign-jar ./motion-series/forces-and-motion_es.jar
echo "signing ./motion-series/ramp-forces-and-motion_da.jar"
sign-jar ./motion-series/ramp-forces-and-motion_da.jar
echo "signing ./motion-series/ramp-forces-and-motion_be.jar"
sign-jar ./motion-series/ramp-forces-and-motion_be.jar
echo "signing ./motion-series/forces-and-motion_et.jar"
sign-jar ./motion-series/forces-and-motion_et.jar
echo "signing ./motion-series/ramp-forces-and-motion_es.jar"
sign-jar ./motion-series/ramp-forces-and-motion_es.jar
echo "signing ./motion-series/forces-and-motion_fr.jar"
sign-jar ./motion-series/forces-and-motion_fr.jar
echo "signing ./motion-series/ramp-forces-and-motion_ar.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ar.jar
echo "signing ./motion-series/ramp-forces-and-motion_pt.jar"
sign-jar ./motion-series/ramp-forces-and-motion_pt.jar
echo "signing ./motion-series/forces-and-motion_ko.jar"
sign-jar ./motion-series/forces-and-motion_ko.jar
echo "signing ./motion-series/forces-and-motion_sr.jar"
sign-jar ./motion-series/forces-and-motion_sr.jar
echo "signing ./motion-series/forces-and-motion_hu.jar"
sign-jar ./motion-series/forces-and-motion_hu.jar
echo "signing ./motion-series/forces-and-motion_mi.jar"
sign-jar ./motion-series/forces-and-motion_mi.jar
echo "signing ./motion-series/forces-and-motion_mk.jar"
sign-jar ./motion-series/forces-and-motion_mk.jar
echo "signing ./motion-series/forces-and-motion_ru.jar"
sign-jar ./motion-series/forces-and-motion_ru.jar
echo "signing ./motion-series/forces-and-motion_en.jar"
sign-jar ./motion-series/forces-and-motion_en.jar
echo "signing ./motion-series/ramp-forces-and-motion_es_PE.jar"
sign-jar ./motion-series/ramp-forces-and-motion_es_PE.jar
echo "signing ./motion-series/forces-and-motion_ku_TR.jar"
sign-jar ./motion-series/forces-and-motion_ku_TR.jar
echo "signing ./motion-series/forces-and-motion_sk.jar"
sign-jar ./motion-series/forces-and-motion_sk.jar
echo "signing ./motion-series/forces-and-motion_bs.jar"
sign-jar ./motion-series/forces-and-motion_bs.jar
echo "signing ./motion-series/ramp-forces-and-motion_ht.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ht.jar
echo "signing ./motion-series/forces-and-motion_kk.jar"
sign-jar ./motion-series/forces-and-motion_kk.jar
echo "signing ./motion-series/ramp-forces-and-motion_fr.jar"
sign-jar ./motion-series/ramp-forces-and-motion_fr.jar
echo "signing ./motion-series/ramp-forces-and-motion_th.jar"
sign-jar ./motion-series/ramp-forces-and-motion_th.jar
echo "signing ./motion-series/forces-and-motion_hr.jar"
sign-jar ./motion-series/forces-and-motion_hr.jar
echo "signing ./motion-series/ramp-forces-and-motion_vi.jar"
sign-jar ./motion-series/ramp-forces-and-motion_vi.jar
echo "signing ./motion-series/ramp-forces-and-motion_zh_CN.jar"
sign-jar ./motion-series/ramp-forces-and-motion_zh_CN.jar
echo "signing ./motion-series/forces-and-motion_mr.jar"
sign-jar ./motion-series/forces-and-motion_mr.jar
echo "signing ./motion-series/forces-and-motion_kn.jar"
sign-jar ./motion-series/forces-and-motion_kn.jar
echo "signing ./motion-series/ramp-forces-and-motion_ko.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ko.jar
echo "signing ./motion-series/ramp-forces-and-motion_sr.jar"
sign-jar ./motion-series/ramp-forces-and-motion_sr.jar
echo "signing ./motion-series/forces-and-motion_tk.jar"
sign-jar ./motion-series/forces-and-motion_tk.jar
echo "signing ./motion-series/forces-and-motion_pt_BR.jar"
sign-jar ./motion-series/forces-and-motion_pt_BR.jar
echo "signing ./motion-series/forces-and-motion_ms.jar"
sign-jar ./motion-series/forces-and-motion_ms.jar
echo "signing ./motion-series/ramp-forces-and-motion_zh_TW.jar"
sign-jar ./motion-series/ramp-forces-and-motion_zh_TW.jar
echo "signing ./motion-series/ramp-forces-and-motion_en.jar"
sign-jar ./motion-series/ramp-forces-and-motion_en.jar
echo "signing ./motion-series/ramp-forces-and-motion_kk.jar"
sign-jar ./motion-series/ramp-forces-and-motion_kk.jar
echo "signing ./motion-series/ramp-forces-and-motion_lt.jar"
sign-jar ./motion-series/ramp-forces-and-motion_lt.jar
echo "signing ./motion-series/ramp-forces-and-motion_mr.jar"
sign-jar ./motion-series/ramp-forces-and-motion_mr.jar
echo "signing ./motion-series/ramp-forces-and-motion_mk.jar"
sign-jar ./motion-series/ramp-forces-and-motion_mk.jar
echo "signing ./motion-series/forces-and-motion_vi.jar"
sign-jar ./motion-series/forces-and-motion_vi.jar
echo "signing ./motion-series/forces-and-motion_pt.jar"
sign-jar ./motion-series/forces-and-motion_pt.jar
echo "signing ./motion-series/forces-and-motion_zh_CN.jar"
sign-jar ./motion-series/forces-and-motion_zh_CN.jar
echo "signing ./motion-series/ramp-forces-and-motion_af.jar"
sign-jar ./motion-series/ramp-forces-and-motion_af.jar
echo "signing ./motion-series/ramp-forces-and-motion_tk.jar"
sign-jar ./motion-series/ramp-forces-and-motion_tk.jar
echo "signing ./motion-series/forces-and-motion_cs.jar"
sign-jar ./motion-series/forces-and-motion_cs.jar
echo "signing ./motion-series/ramp-forces-and-motion_uk.jar"
sign-jar ./motion-series/ramp-forces-and-motion_uk.jar
echo "signing ./motion-series/ramp-forces-and-motion_km.jar"
sign-jar ./motion-series/ramp-forces-and-motion_km.jar
echo "signing ./motion-series/forces-and-motion_ro.jar"
sign-jar ./motion-series/forces-and-motion_ro.jar
echo "signing ./motion-series/forces-and-motion_el.jar"
sign-jar ./motion-series/forces-and-motion_el.jar
echo "signing ./motion-series/forces-and-motion_lt.jar"
sign-jar ./motion-series/forces-and-motion_lt.jar
echo "signing ./motion-series/ramp-forces-and-motion_ms.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ms.jar
echo "signing ./motion-series/ramp-forces-and-motion_it.jar"
sign-jar ./motion-series/ramp-forces-and-motion_it.jar
echo "signing ./motion-series/ramp-forces-and-motion_cs.jar"
sign-jar ./motion-series/ramp-forces-and-motion_cs.jar
echo "signing ./motion-series/forces-and-motion_be.jar"
sign-jar ./motion-series/forces-and-motion_be.jar
echo "signing ./motion-series/forces-and-motion_da.jar"
sign-jar ./motion-series/forces-and-motion_da.jar
echo "signing ./motion-series/forces-and-motion_sv.jar"
sign-jar ./motion-series/forces-and-motion_sv.jar
echo "signing ./motion-series/forces-and-motion_it.jar"
sign-jar ./motion-series/forces-and-motion_it.jar
echo "signing ./motion-series/ramp-forces-and-motion_gl.jar"
sign-jar ./motion-series/ramp-forces-and-motion_gl.jar
echo "signing ./motion-series/ramp-forces-and-motion_bs.jar"
sign-jar ./motion-series/ramp-forces-and-motion_bs.jar
echo "signing ./motion-series/ramp-forces-and-motion_hr.jar"
sign-jar ./motion-series/ramp-forces-and-motion_hr.jar
echo "signing ./motion-series/ramp-forces-and-motion_sl.jar"
sign-jar ./motion-series/ramp-forces-and-motion_sl.jar
echo "signing ./motion-series/ramp-forces-and-motion_ku_TR.jar"
sign-jar ./motion-series/ramp-forces-and-motion_ku_TR.jar
echo "signing ./motion-series/ramp-forces-and-motion_fa.jar"
sign-jar ./motion-series/ramp-forces-and-motion_fa.jar
echo "signing ./motion-series/forces-and-motion_es_PE.jar"
sign-jar ./motion-series/forces-and-motion_es_PE.jar
echo "signing ./motion-series/forces-and-motion_th.jar"
sign-jar ./motion-series/forces-and-motion_th.jar
echo "signing ./motion-series/forces-and-motion_mn.jar"
sign-jar ./motion-series/forces-and-motion_mn.jar
echo "signing ./motion-series/forces-and-motion_fi.jar"
sign-jar ./motion-series/forces-and-motion_fi.jar
echo "signing ./motion-series/forces-and-motion_ja.jar"
sign-jar ./motion-series/forces-and-motion_ja.jar
echo "signing ./motion-series/ramp-forces-and-motion_sv.jar"
sign-jar ./motion-series/ramp-forces-and-motion_sv.jar
echo "signing ./motion-series/forces-and-motion_km.jar"
sign-jar ./motion-series/forces-and-motion_km.jar
echo "signing ./motion-series/ramp-forces-and-motion_pl.jar"
sign-jar ./motion-series/ramp-forces-and-motion_pl.jar
echo "signing ./capacitor-lab/capacitor-lab_mk.jar"
sign-jar ./capacitor-lab/capacitor-lab_mk.jar
echo "signing ./capacitor-lab/capacitor-lab_af.jar"
sign-jar ./capacitor-lab/capacitor-lab_af.jar
echo "signing ./capacitor-lab/capacitor-lab_pt_BR.jar"
sign-jar ./capacitor-lab/capacitor-lab_pt_BR.jar
echo "signing ./capacitor-lab/capacitor-lab_hu.jar"
sign-jar ./capacitor-lab/capacitor-lab_hu.jar
echo "signing ./capacitor-lab/capacitor-lab_iw.jar"
sign-jar ./capacitor-lab/capacitor-lab_iw.jar
echo "signing ./capacitor-lab/capacitor-lab_kk.jar"
sign-jar ./capacitor-lab/capacitor-lab_kk.jar
echo "signing ./capacitor-lab/capacitor-lab_ku_TR.jar"
sign-jar ./capacitor-lab/capacitor-lab_ku_TR.jar
echo "signing ./capacitor-lab/capacitor-lab_all.jar"
sign-jar ./capacitor-lab/capacitor-lab_all.jar
echo "signing ./capacitor-lab/capacitor-lab_th.jar"
sign-jar ./capacitor-lab/capacitor-lab_th.jar
echo "signing ./capacitor-lab/capacitor-lab_fa.jar"
sign-jar ./capacitor-lab/capacitor-lab_fa.jar
echo "signing ./capacitor-lab/capacitor-lab_tr.jar"
sign-jar ./capacitor-lab/capacitor-lab_tr.jar
echo "signing ./capacitor-lab/capacitor-lab_ru.jar"
sign-jar ./capacitor-lab/capacitor-lab_ru.jar
echo "signing ./capacitor-lab/capacitor-lab_fr.jar"
sign-jar ./capacitor-lab/capacitor-lab_fr.jar
echo "signing ./capacitor-lab/capacitor-lab_ko.jar"
sign-jar ./capacitor-lab/capacitor-lab_ko.jar
echo "signing ./capacitor-lab/capacitor-lab_all_installer.jar"
sign-jar ./capacitor-lab/capacitor-lab_all_installer.jar
echo "signing ./capacitor-lab/capacitor-lab_sq.jar"
sign-jar ./capacitor-lab/capacitor-lab_sq.jar
echo "signing ./capacitor-lab/capacitor-lab_bg.jar"
sign-jar ./capacitor-lab/capacitor-lab_bg.jar
echo "signing ./capacitor-lab/capacitor-lab_eu.jar"
sign-jar ./capacitor-lab/capacitor-lab_eu.jar
echo "signing ./capacitor-lab/capacitor-lab_be.jar"
sign-jar ./capacitor-lab/capacitor-lab_be.jar
echo "signing ./capacitor-lab/capacitor-lab_fi.jar"
sign-jar ./capacitor-lab/capacitor-lab_fi.jar
echo "signing ./capacitor-lab/capacitor-lab_el.jar"
sign-jar ./capacitor-lab/capacitor-lab_el.jar
echo "signing ./capacitor-lab/capacitor-lab_ht.jar"
sign-jar ./capacitor-lab/capacitor-lab_ht.jar
echo "signing ./capacitor-lab/capacitor-lab_in.jar"
sign-jar ./capacitor-lab/capacitor-lab_in.jar
echo "signing ./capacitor-lab/capacitor-lab_de.jar"
sign-jar ./capacitor-lab/capacitor-lab_de.jar
echo "signing ./capacitor-lab/capacitor-lab_gl.jar"
sign-jar ./capacitor-lab/capacitor-lab_gl.jar
echo "signing ./capacitor-lab/capacitor-lab_nl.jar"
sign-jar ./capacitor-lab/capacitor-lab_nl.jar
echo "signing ./capacitor-lab/capacitor-lab_ka.jar"
sign-jar ./capacitor-lab/capacitor-lab_ka.jar
echo "signing ./capacitor-lab/capacitor-lab_ar.jar"
sign-jar ./capacitor-lab/capacitor-lab_ar.jar
echo "signing ./capacitor-lab/capacitor-lab_tk.jar"
sign-jar ./capacitor-lab/capacitor-lab_tk.jar
echo "signing ./capacitor-lab/capacitor-lab_en.jar"
sign-jar ./capacitor-lab/capacitor-lab_en.jar
echo "signing ./capacitor-lab/capacitor-lab_zh_CN.jar"
sign-jar ./capacitor-lab/capacitor-lab_zh_CN.jar
echo "signing ./capacitor-lab/capacitor-lab_vi.jar"
sign-jar ./capacitor-lab/capacitor-lab_vi.jar
echo "signing ./capacitor-lab/capacitor-lab_da.jar"
sign-jar ./capacitor-lab/capacitor-lab_da.jar
echo "signing ./capacitor-lab/capacitor-lab_it.jar"
sign-jar ./capacitor-lab/capacitor-lab_it.jar
echo "signing ./capacitor-lab/capacitor-lab_lv.jar"
sign-jar ./capacitor-lab/capacitor-lab_lv.jar
echo "signing ./capacitor-lab/capacitor-lab_cs.jar"
sign-jar ./capacitor-lab/capacitor-lab_cs.jar
echo "signing ./capacitor-lab/capacitor-lab_uk.jar"
sign-jar ./capacitor-lab/capacitor-lab_uk.jar
echo "signing ./capacitor-lab/capacitor-lab_ro.jar"
sign-jar ./capacitor-lab/capacitor-lab_ro.jar
echo "signing ./capacitor-lab/capacitor-lab_hy.jar"
sign-jar ./capacitor-lab/capacitor-lab_hy.jar
echo "signing ./capacitor-lab/capacitor-lab_ku.jar"
sign-jar ./capacitor-lab/capacitor-lab_ku.jar
echo "signing ./capacitor-lab/capacitor-lab_mr.jar"
sign-jar ./capacitor-lab/capacitor-lab_mr.jar
echo "signing ./capacitor-lab/capacitor-lab_bs.jar"
sign-jar ./capacitor-lab/capacitor-lab_bs.jar
echo "signing ./capacitor-lab/capacitor-lab_hr.jar"
sign-jar ./capacitor-lab/capacitor-lab_hr.jar
echo "signing ./capacitor-lab/capacitor-lab_es.jar"
sign-jar ./capacitor-lab/capacitor-lab_es.jar
echo "signing ./capacitor-lab/capacitor-lab_et.jar"
sign-jar ./capacitor-lab/capacitor-lab_et.jar
echo "signing ./capacitor-lab/capacitor-lab_es_PE.jar"
sign-jar ./capacitor-lab/capacitor-lab_es_PE.jar
echo "signing ./capacitor-lab/capacitor-lab_ja.jar"
sign-jar ./capacitor-lab/capacitor-lab_ja.jar
echo "signing ./capacitor-lab/capacitor-lab_sv.jar"
sign-jar ./capacitor-lab/capacitor-lab_sv.jar
echo "signing ./capacitor-lab/capacitor-lab_zh_TW.jar"
sign-jar ./capacitor-lab/capacitor-lab_zh_TW.jar
echo "signing ./capacitor-lab/capacitor-lab_km.jar"
sign-jar ./capacitor-lab/capacitor-lab_km.jar
echo "signing ./capacitor-lab/capacitor-lab_kn.jar"
sign-jar ./capacitor-lab/capacitor-lab_kn.jar
echo "signing ./capacitor-lab/capacitor-lab_sr.jar"
sign-jar ./capacitor-lab/capacitor-lab_sr.jar
echo "signing ./capacitor-lab/capacitor-lab_pl.jar"
sign-jar ./capacitor-lab/capacitor-lab_pl.jar
echo "signing ./capacitor-lab/capacitor-lab_sk.jar"
sign-jar ./capacitor-lab/capacitor-lab_sk.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_el.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_el.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_eu.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_eu.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ru.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ru.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_be.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_be.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_iw.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_iw.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_vi.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_vi.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_de.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_de.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_bs.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_bs.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_it.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_it.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_tr.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_tr.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_tk.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_tk.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ja.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ja.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_mr.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_mr.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_iw.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_iw.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ca.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ca.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_it.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_it.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_fr.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_fr.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ko.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ko.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_hy.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_hy.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_nn.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_nn.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_th.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_th.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_cs.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_cs.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_nn.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_nn.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_de.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_de.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_sv.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_sv.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_zh_TW.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_zh_TW.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ja.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ja.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ar.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ar.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_zh_TW.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_zh_TW.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_zh_CN.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_zh_CN.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_sr.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_sr.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_th.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_th.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_pl.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_pl.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_eu.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_eu.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_hu.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_hu.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_es_PE.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_es_PE.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_zh_CN.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_zh_CN.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_cs.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_cs.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_fa.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_fa.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_kk.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_kk.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_sq.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_sq.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_pt_BR.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_pt_BR.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_da.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_da.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ar.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ar.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_es_PE.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_es_PE.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ca.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ca.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_kn.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_kn.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_gl.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_gl.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_sk.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_sk.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_all.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_all.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_da.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_da.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ku_TR.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ku_TR.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_kn.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_kn.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_sq.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_sq.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_nl.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_nl.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_mr.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_mr.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_en.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_en.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_be.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_be.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_in.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_in.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_in.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_in.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_sr.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_sr.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_nb.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_nb.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_tr.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_tr.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_sk.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_sk.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ht.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ht.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_es.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_es.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_tk.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_tk.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_nl.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_nl.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_hu.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_hu.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_hy.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_hy.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_uz.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_uz.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_bs.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_bs.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_el.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_el.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_mk.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_mk.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_vi.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_vi.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_es.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_es.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_uz.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_uz.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_fa.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_fa.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ku_TR.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ku_TR.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_mk.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_mk.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_pl.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_pl.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_sv.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_sv.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ko.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ko.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_ht.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_ht.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_en.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_en.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_fr.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_fr.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_all_installer.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_all_installer.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_kk.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_kk.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_ru.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_ru.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_gl.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_gl.jar
echo "signing ./fluid-pressure-and-flow/fluid-pressure-and-flow_pt_BR.jar"
sign-jar ./fluid-pressure-and-flow/fluid-pressure-and-flow_pt_BR.jar
echo "signing ./fluid-pressure-and-flow/under-pressure_nb.jar"
sign-jar ./fluid-pressure-and-flow/under-pressure_nb.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_af.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_af.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_hu.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_hu.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_cs.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_cs.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_es_PE.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_es_PE.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_it.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_it.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_mr.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_mr.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_eu.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_eu.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_da.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_da.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_sv.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_sv.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_kk.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_kk.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_th.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_th.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_kn.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_kn.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_gl.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_gl.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_km.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_km.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_sr.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_sr.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_be.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_be.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_zh_TW.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_zh_TW.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_pt_BR.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_pt_BR.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_et.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_et.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_en.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_en.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_pt.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_pt.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_tk.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_tk.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_hr.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_hr.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_iw.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_iw.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_fa.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_fa.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_in.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_in.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_de.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_de.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_vi.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_vi.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_bs.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_bs.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_es.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_es.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_mk.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_mk.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_fr.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_fr.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_sk.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_sk.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_fi.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_fi.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_zh_CN.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_zh_CN.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_pl.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_pl.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_el.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_el.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_nl.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_nl.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_ja.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_ja.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_ko.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_ko.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_ar.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_ar.jar
echo "signing ./blackbody-spectrum/blackbody-spectrum_tr.jar"
sign-jar ./blackbody-spectrum/blackbody-spectrum_tr.jar
echo "signing ./stern-gerlach/stern-gerlach_hu.jar"
sign-jar ./stern-gerlach/stern-gerlach_hu.jar
echo "signing ./stern-gerlach/stern-gerlach_eu.jar"
sign-jar ./stern-gerlach/stern-gerlach_eu.jar
echo "signing ./stern-gerlach/stern-gerlach_ja.jar"
sign-jar ./stern-gerlach/stern-gerlach_ja.jar
echo "signing ./stern-gerlach/stern-gerlach_nl.jar"
sign-jar ./stern-gerlach/stern-gerlach_nl.jar
echo "signing ./stern-gerlach/stern-gerlach_zh_CN.jar"
sign-jar ./stern-gerlach/stern-gerlach_zh_CN.jar
echo "signing ./stern-gerlach/stern-gerlach_be.jar"
sign-jar ./stern-gerlach/stern-gerlach_be.jar
echo "signing ./stern-gerlach/stern-gerlach_iw.jar"
sign-jar ./stern-gerlach/stern-gerlach_iw.jar
echo "signing ./stern-gerlach/stern-gerlach_zh_TW.jar"
sign-jar ./stern-gerlach/stern-gerlach_zh_TW.jar
echo "signing ./stern-gerlach/stern-gerlach_es_PE.jar"
sign-jar ./stern-gerlach/stern-gerlach_es_PE.jar
echo "signing ./stern-gerlach/stern-gerlach_de.jar"
sign-jar ./stern-gerlach/stern-gerlach_de.jar
echo "signing ./stern-gerlach/stern-gerlach_mk.jar"
sign-jar ./stern-gerlach/stern-gerlach_mk.jar
echo "signing ./stern-gerlach/stern-gerlach_da.jar"
sign-jar ./stern-gerlach/stern-gerlach_da.jar
echo "signing ./stern-gerlach/stern-gerlach_tr.jar"
sign-jar ./stern-gerlach/stern-gerlach_tr.jar
echo "signing ./stern-gerlach/stern-gerlach_en.jar"
sign-jar ./stern-gerlach/stern-gerlach_en.jar
echo "signing ./stern-gerlach/stern-gerlach_bs.jar"
sign-jar ./stern-gerlach/stern-gerlach_bs.jar
echo "signing ./stern-gerlach/stern-gerlach_hr.jar"
sign-jar ./stern-gerlach/stern-gerlach_hr.jar
echo "signing ./stern-gerlach/stern-gerlach_pl.jar"
sign-jar ./stern-gerlach/stern-gerlach_pl.jar
echo "signing ./stern-gerlach/stern-gerlach_gl.jar"
sign-jar ./stern-gerlach/stern-gerlach_gl.jar
echo "signing ./stern-gerlach/stern-gerlach_th.jar"
sign-jar ./stern-gerlach/stern-gerlach_th.jar
echo "signing ./stern-gerlach/stern-gerlach_es.jar"
sign-jar ./stern-gerlach/stern-gerlach_es.jar
echo "signing ./stern-gerlach/stern-gerlach_fa.jar"
sign-jar ./stern-gerlach/stern-gerlach_fa.jar
echo "signing ./stern-gerlach/stern-gerlach_cs.jar"
sign-jar ./stern-gerlach/stern-gerlach_cs.jar
echo "signing ./stern-gerlach/stern-gerlach_ko.jar"
sign-jar ./stern-gerlach/stern-gerlach_ko.jar
echo "signing ./stern-gerlach/stern-gerlach_sr.jar"
sign-jar ./stern-gerlach/stern-gerlach_sr.jar
echo "signing ./stern-gerlach/stern-gerlach_kk.jar"
sign-jar ./stern-gerlach/stern-gerlach_kk.jar
echo "signing ./stern-gerlach/stern-gerlach_fr.jar"
sign-jar ./stern-gerlach/stern-gerlach_fr.jar
echo "signing ./stern-gerlach/stern-gerlach_it.jar"
sign-jar ./stern-gerlach/stern-gerlach_it.jar
echo "signing ./stern-gerlach/stern-gerlach_sk.jar"
sign-jar ./stern-gerlach/stern-gerlach_sk.jar
echo "signing ./stern-gerlach/stern-gerlach_in.jar"
sign-jar ./stern-gerlach/stern-gerlach_in.jar
echo "signing ./stern-gerlach/stern-gerlach_pt_BR.jar"
sign-jar ./stern-gerlach/stern-gerlach_pt_BR.jar
echo "signing ./stern-gerlach/stern-gerlach_vi.jar"
sign-jar ./stern-gerlach/stern-gerlach_vi.jar
echo "signing ./stern-gerlach/stern-gerlach_el.jar"
sign-jar ./stern-gerlach/stern-gerlach_el.jar
echo "signing ./stern-gerlach/stern-gerlach_tk.jar"
sign-jar ./stern-gerlach/stern-gerlach_tk.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_fi.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_fi.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_hu.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_hu.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_th.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_th.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_pl.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_pl.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_pt_BR.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_pt_BR.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_sk.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_sk.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_fr.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_fr.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_kk.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_kk.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_all.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_all.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_zh_TW.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_zh_TW.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_mr.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_mr.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_tk.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_tk.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_eu.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_eu.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_fa.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_fa.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_el.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_el.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_bs.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_bs.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_et.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_et.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_lv.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_lv.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_gl.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_gl.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_vi.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_vi.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_da.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_da.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_tr.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_tr.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_en.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_en.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_es.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_es.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_iw.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_iw.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_sl.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_sl.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_ru.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_ru.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_ar.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_ar.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_zh_CN.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_zh_CN.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_ka.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_ka.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_ko.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_ko.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_all_installer.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_all_installer.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_km.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_km.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_cs.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_cs.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_mk.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_mk.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_de.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_de.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_es_PE.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_es_PE.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_sr.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_sr.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_it.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_it.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_sv.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_sv.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_ja.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_ja.jar
echo "signing ./balancing-chemical-equations/balancing-chemical-equations_nl.jar"
sign-jar ./balancing-chemical-equations/balancing-chemical-equations_nl.jar
echo "signing ./bound-states/band-structure_da.jar"
sign-jar ./bound-states/band-structure_da.jar
echo "signing ./bound-states/band-structure_it.jar"
sign-jar ./bound-states/band-structure_it.jar
echo "signing ./bound-states/covalent-bonds_zh_TW.jar"
sign-jar ./bound-states/covalent-bonds_zh_TW.jar
echo "signing ./bound-states/bound-states_ro.jar"
sign-jar ./bound-states/bound-states_ro.jar
echo "signing ./bound-states/bound-states_zh_TW.jar"
sign-jar ./bound-states/bound-states_zh_TW.jar
echo "signing ./bound-states/band-structure_ko.jar"
sign-jar ./bound-states/band-structure_ko.jar
echo "signing ./bound-states/band-structure_pt.jar"
sign-jar ./bound-states/band-structure_pt.jar
echo "signing ./bound-states/covalent-bonds_nl.jar"
sign-jar ./bound-states/covalent-bonds_nl.jar
echo "signing ./bound-states/bound-states_iw.jar"
sign-jar ./bound-states/bound-states_iw.jar
echo "signing ./bound-states/bound-states_eu.jar"
sign-jar ./bound-states/bound-states_eu.jar
echo "signing ./bound-states/band-structure_bs.jar"
sign-jar ./bound-states/band-structure_bs.jar
echo "signing ./bound-states/bound-states_tr.jar"
sign-jar ./bound-states/bound-states_tr.jar
echo "signing ./bound-states/covalent-bonds_fr.jar"
sign-jar ./bound-states/covalent-bonds_fr.jar
echo "signing ./bound-states/bound-states_da.jar"
sign-jar ./bound-states/bound-states_da.jar
echo "signing ./bound-states/band-structure_ar.jar"
sign-jar ./bound-states/band-structure_ar.jar
echo "signing ./bound-states/band-structure_vi.jar"
sign-jar ./bound-states/band-structure_vi.jar
echo "signing ./bound-states/covalent-bonds_ar.jar"
sign-jar ./bound-states/covalent-bonds_ar.jar
echo "signing ./bound-states/band-structure_en.jar"
sign-jar ./bound-states/band-structure_en.jar
echo "signing ./bound-states/covalent-bonds_et.jar"
sign-jar ./bound-states/covalent-bonds_et.jar
echo "signing ./bound-states/covalent-bonds_vi.jar"
sign-jar ./bound-states/covalent-bonds_vi.jar
echo "signing ./bound-states/bound-states_ko.jar"
sign-jar ./bound-states/bound-states_ko.jar
echo "signing ./bound-states/covalent-bonds_iw.jar"
sign-jar ./bound-states/covalent-bonds_iw.jar
echo "signing ./bound-states/band-structure_es.jar"
sign-jar ./bound-states/band-structure_es.jar
echo "signing ./bound-states/band-structure_pl.jar"
sign-jar ./bound-states/band-structure_pl.jar
echo "signing ./bound-states/band-structure_hu.jar"
sign-jar ./bound-states/band-structure_hu.jar
echo "signing ./bound-states/covalent-bonds_de.jar"
sign-jar ./bound-states/covalent-bonds_de.jar
echo "signing ./bound-states/covalent-bonds_sr.jar"
sign-jar ./bound-states/covalent-bonds_sr.jar
echo "signing ./bound-states/covalent-bonds_pl.jar"
sign-jar ./bound-states/covalent-bonds_pl.jar
echo "signing ./bound-states/bound-states_et.jar"
sign-jar ./bound-states/bound-states_et.jar
echo "signing ./bound-states/covalent-bonds_in.jar"
sign-jar ./bound-states/covalent-bonds_in.jar
echo "signing ./bound-states/covalent-bonds_da.jar"
sign-jar ./bound-states/covalent-bonds_da.jar
echo "signing ./bound-states/covalent-bonds_tr.jar"
sign-jar ./bound-states/covalent-bonds_tr.jar
echo "signing ./bound-states/bound-states_tk.jar"
sign-jar ./bound-states/bound-states_tk.jar
echo "signing ./bound-states/covalent-bonds_es_PE.jar"
sign-jar ./bound-states/covalent-bonds_es_PE.jar
echo "signing ./bound-states/band-structure_ar_SA.jar"
sign-jar ./bound-states/band-structure_ar_SA.jar
echo "signing ./bound-states/band-structure_hr.jar"
sign-jar ./bound-states/band-structure_hr.jar
echo "signing ./bound-states/covalent-bonds_th.jar"
sign-jar ./bound-states/covalent-bonds_th.jar
echo "signing ./bound-states/bound-states_ar_SA.jar"
sign-jar ./bound-states/bound-states_ar_SA.jar
echo "signing ./bound-states/band-structure_mk.jar"
sign-jar ./bound-states/band-structure_mk.jar
echo "signing ./bound-states/covalent-bonds_tk.jar"
sign-jar ./bound-states/covalent-bonds_tk.jar
echo "signing ./bound-states/bound-states_bs.jar"
sign-jar ./bound-states/bound-states_bs.jar
echo "signing ./bound-states/band-structure_de.jar"
sign-jar ./bound-states/band-structure_de.jar
echo "signing ./bound-states/bound-states_th.jar"
sign-jar ./bound-states/bound-states_th.jar
echo "signing ./bound-states/covalent-bonds_ko.jar"
sign-jar ./bound-states/covalent-bonds_ko.jar
echo "signing ./bound-states/covalent-bonds_sk.jar"
sign-jar ./bound-states/covalent-bonds_sk.jar
echo "signing ./bound-states/covalent-bonds_it.jar"
sign-jar ./bound-states/covalent-bonds_it.jar
echo "signing ./bound-states/band-structure_in.jar"
sign-jar ./bound-states/band-structure_in.jar
echo "signing ./bound-states/band-structure_tk.jar"
sign-jar ./bound-states/band-structure_tk.jar
echo "signing ./bound-states/covalent-bonds_hu.jar"
sign-jar ./bound-states/covalent-bonds_hu.jar
echo "signing ./bound-states/band-structure_kk.jar"
sign-jar ./bound-states/band-structure_kk.jar
echo "signing ./bound-states/band-structure_eu.jar"
sign-jar ./bound-states/band-structure_eu.jar
echo "signing ./bound-states/bound-states_nl.jar"
sign-jar ./bound-states/bound-states_nl.jar
echo "signing ./bound-states/covalent-bonds_bs.jar"
sign-jar ./bound-states/covalent-bonds_bs.jar
echo "signing ./bound-states/bound-states_es.jar"
sign-jar ./bound-states/bound-states_es.jar
echo "signing ./bound-states/band-structure_ja.jar"
sign-jar ./bound-states/band-structure_ja.jar
echo "signing ./bound-states/covalent-bonds_eu.jar"
sign-jar ./bound-states/covalent-bonds_eu.jar
echo "signing ./bound-states/band-structure_sq.jar"
sign-jar ./bound-states/band-structure_sq.jar
echo "signing ./bound-states/bound-states_it.jar"
sign-jar ./bound-states/bound-states_it.jar
echo "signing ./bound-states/band-structure_sr.jar"
sign-jar ./bound-states/band-structure_sr.jar
echo "signing ./bound-states/bound-states_kk.jar"
sign-jar ./bound-states/bound-states_kk.jar
echo "signing ./bound-states/band-structure_sk.jar"
sign-jar ./bound-states/band-structure_sk.jar
echo "signing ./bound-states/bound-states_pt_BR.jar"
sign-jar ./bound-states/bound-states_pt_BR.jar
echo "signing ./bound-states/covalent-bonds_ar_SA.jar"
sign-jar ./bound-states/covalent-bonds_ar_SA.jar
echo "signing ./bound-states/covalent-bonds_mn.jar"
sign-jar ./bound-states/covalent-bonds_mn.jar
echo "signing ./bound-states/band-structure_zh_CN.jar"
sign-jar ./bound-states/band-structure_zh_CN.jar
echo "signing ./bound-states/covalent-bonds_en.jar"
sign-jar ./bound-states/covalent-bonds_en.jar
echo "signing ./bound-states/covalent-bonds_fa.jar"
sign-jar ./bound-states/covalent-bonds_fa.jar
echo "signing ./bound-states/bound-states_en.jar"
sign-jar ./bound-states/bound-states_en.jar
echo "signing ./bound-states/bound-states_mn.jar"
sign-jar ./bound-states/bound-states_mn.jar
echo "signing ./bound-states/bound-states_zh_CN.jar"
sign-jar ./bound-states/bound-states_zh_CN.jar
echo "signing ./bound-states/covalent-bonds_pt_BR.jar"
sign-jar ./bound-states/covalent-bonds_pt_BR.jar
echo "signing ./bound-states/band-structure_es_PE.jar"
sign-jar ./bound-states/band-structure_es_PE.jar
echo "signing ./bound-states/band-structure_tr.jar"
sign-jar ./bound-states/band-structure_tr.jar
echo "signing ./bound-states/bound-states_sq.jar"
sign-jar ./bound-states/bound-states_sq.jar
echo "signing ./bound-states/band-structure_th.jar"
sign-jar ./bound-states/band-structure_th.jar
echo "signing ./bound-states/bound-states_de.jar"
sign-jar ./bound-states/bound-states_de.jar
echo "signing ./bound-states/band-structure_el.jar"
sign-jar ./bound-states/band-structure_el.jar
echo "signing ./bound-states/covalent-bonds_mk.jar"
sign-jar ./bound-states/covalent-bonds_mk.jar
echo "signing ./bound-states/band-structure_nl.jar"
sign-jar ./bound-states/band-structure_nl.jar
echo "signing ./bound-states/band-structure_pt_BR.jar"
sign-jar ./bound-states/band-structure_pt_BR.jar
echo "signing ./bound-states/bound-states_fa.jar"
sign-jar ./bound-states/bound-states_fa.jar
echo "signing ./bound-states/covalent-bonds_el.jar"
sign-jar ./bound-states/covalent-bonds_el.jar
echo "signing ./bound-states/band-structure_iw.jar"
sign-jar ./bound-states/band-structure_iw.jar
echo "signing ./bound-states/bound-states_fr.jar"
sign-jar ./bound-states/bound-states_fr.jar
echo "signing ./bound-states/bound-states_hu.jar"
sign-jar ./bound-states/bound-states_hu.jar
echo "signing ./bound-states/covalent-bonds_kk.jar"
sign-jar ./bound-states/covalent-bonds_kk.jar
echo "signing ./bound-states/bound-states_sr.jar"
sign-jar ./bound-states/bound-states_sr.jar
echo "signing ./bound-states/bound-states_es_PE.jar"
sign-jar ./bound-states/bound-states_es_PE.jar
echo "signing ./bound-states/bound-states_el.jar"
sign-jar ./bound-states/bound-states_el.jar
echo "signing ./bound-states/bound-states_in.jar"
sign-jar ./bound-states/bound-states_in.jar
echo "signing ./bound-states/bound-states_hr.jar"
sign-jar ./bound-states/bound-states_hr.jar
echo "signing ./bound-states/band-structure_ro.jar"
sign-jar ./bound-states/band-structure_ro.jar
echo "signing ./bound-states/covalent-bonds_sq.jar"
sign-jar ./bound-states/covalent-bonds_sq.jar
echo "signing ./bound-states/bound-states_sk.jar"
sign-jar ./bound-states/bound-states_sk.jar
echo "signing ./bound-states/bound-states_vi.jar"
sign-jar ./bound-states/bound-states_vi.jar
echo "signing ./bound-states/bound-states_ja.jar"
sign-jar ./bound-states/bound-states_ja.jar
echo "signing ./bound-states/bound-states_mk.jar"
sign-jar ./bound-states/bound-states_mk.jar
echo "signing ./bound-states/bound-states_ar.jar"
sign-jar ./bound-states/bound-states_ar.jar
echo "signing ./bound-states/covalent-bonds_es.jar"
sign-jar ./bound-states/covalent-bonds_es.jar
echo "signing ./bound-states/covalent-bonds_pt.jar"
sign-jar ./bound-states/covalent-bonds_pt.jar
echo "signing ./bound-states/bound-states_all.jar"
sign-jar ./bound-states/bound-states_all.jar
echo "signing ./bound-states/covalent-bonds_ro.jar"
sign-jar ./bound-states/covalent-bonds_ro.jar
echo "signing ./bound-states/covalent-bonds_ja.jar"
sign-jar ./bound-states/covalent-bonds_ja.jar
echo "signing ./bound-states/band-structure_mn.jar"
sign-jar ./bound-states/band-structure_mn.jar
echo "signing ./bound-states/bound-states_all_installer.jar"
sign-jar ./bound-states/bound-states_all_installer.jar
echo "signing ./bound-states/band-structure_et.jar"
sign-jar ./bound-states/band-structure_et.jar
echo "signing ./bound-states/bound-states_pl.jar"
sign-jar ./bound-states/bound-states_pl.jar
echo "signing ./bound-states/covalent-bonds_hr.jar"
sign-jar ./bound-states/covalent-bonds_hr.jar
echo "signing ./bound-states/covalent-bonds_zh_CN.jar"
sign-jar ./bound-states/covalent-bonds_zh_CN.jar
echo "signing ./bound-states/band-structure_fr.jar"
sign-jar ./bound-states/band-structure_fr.jar
echo "signing ./bound-states/band-structure_fa.jar"
sign-jar ./bound-states/band-structure_fa.jar
echo "signing ./bound-states/band-structure_zh_TW.jar"
sign-jar ./bound-states/band-structure_zh_TW.jar
echo "signing ./bound-states/bound-states_pt.jar"
sign-jar ./bound-states/bound-states_pt.jar
echo "signing ./optical-quantum-control/optical-quantum-control_mk.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_mk.jar
echo "signing ./optical-quantum-control/optical-quantum-control_pl.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_pl.jar
echo "signing ./optical-quantum-control/optical-quantum-control_el.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_el.jar
echo "signing ./optical-quantum-control/optical-quantum-control_hr.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_hr.jar
echo "signing ./optical-quantum-control/optical-quantum-control_ka.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_ka.jar
echo "signing ./optical-quantum-control/optical-quantum-control_ja.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_ja.jar
echo "signing ./optical-quantum-control/optical-quantum-control_fr.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_fr.jar
echo "signing ./optical-quantum-control/optical-quantum-control_da.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_da.jar
echo "signing ./optical-quantum-control/optical-quantum-control_eu.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_eu.jar
echo "signing ./optical-quantum-control/optical-quantum-control_es.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_es.jar
echo "signing ./optical-quantum-control/optical-quantum-control_sv.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_sv.jar
echo "signing ./optical-quantum-control/optical-quantum-control_all_installer.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_all_installer.jar
echo "signing ./optical-quantum-control/optical-quantum-control_pt.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_pt.jar
echo "signing ./optical-quantum-control/optical-quantum-control_th.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_th.jar
echo "signing ./optical-quantum-control/optical-quantum-control_ru.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_ru.jar
echo "signing ./optical-quantum-control/optical-quantum-control_sk.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_sk.jar
echo "signing ./optical-quantum-control/optical-quantum-control_in.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_in.jar
echo "signing ./optical-quantum-control/optical-quantum-control_hu.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_hu.jar
echo "signing ./optical-quantum-control/optical-quantum-control_tr.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_tr.jar
echo "signing ./optical-quantum-control/optical-quantum-control_fa.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_fa.jar
echo "signing ./optical-quantum-control/optical-quantum-control_vi.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_vi.jar
echo "signing ./optical-quantum-control/optical-quantum-control_bs.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_bs.jar
echo "signing ./optical-quantum-control/optical-quantum-control_sr.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_sr.jar
echo "signing ./optical-quantum-control/optical-quantum-control_de.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_de.jar
echo "signing ./optical-quantum-control/optical-quantum-control_nl.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_nl.jar
echo "signing ./optical-quantum-control/optical-quantum-control_tk.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_tk.jar
echo "signing ./optical-quantum-control/optical-quantum-control_pt_BR.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_pt_BR.jar
echo "signing ./optical-quantum-control/optical-quantum-control_es_PE.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_es_PE.jar
echo "signing ./optical-quantum-control/optical-quantum-control_en.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_en.jar
echo "signing ./optical-quantum-control/optical-quantum-control_kk.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_kk.jar
echo "signing ./optical-quantum-control/optical-quantum-control_ko.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_ko.jar
echo "signing ./optical-quantum-control/optical-quantum-control_zh_TW.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_zh_TW.jar
echo "signing ./optical-quantum-control/optical-quantum-control_zh_CN.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_zh_CN.jar
echo "signing ./optical-quantum-control/optical-quantum-control_all.jar"
sign-jar ./optical-quantum-control/optical-quantum-control_all.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_zh_TW.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_zh_TW.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_sr.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_sr.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_nn.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_nn.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_in.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_in.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_fa.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_fa.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_tr.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_tr.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_mk.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_mk.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_nb.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_nb.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_fr.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_fr.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_hu.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_hu.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_mi.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_mi.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_ku_TR.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_ku_TR.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_en.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_en.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_eu.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_eu.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_iw.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_iw.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_sk.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_sk.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_sv.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_sv.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_ko.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_ko.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_es.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_es.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_pt.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_pt.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_all.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_all.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_ro.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_ro.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_pt_BR.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_pt_BR.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_el.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_el.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_nl.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_nl.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_vi.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_vi.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_kk.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_kk.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_ku.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_ku.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_da.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_da.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_be.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_be.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_zh_CN.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_zh_CN.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_ar.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_ar.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_es_PE.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_es_PE.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_bs.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_bs.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_cs.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_cs.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_th.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_th.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_de.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_de.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_hr.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_hr.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_it.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_it.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_all_installer.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_all_installer.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_pl.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_pl.jar
echo "signing ./energy-forms-and-changes/energy-forms-and-changes_ja.jar"
sign-jar ./energy-forms-and-changes/energy-forms-and-changes_ja.jar
echo "signing ./my-solar-system/my-solar-system_in.jar"
sign-jar ./my-solar-system/my-solar-system_in.jar
echo "signing ./my-solar-system/my-solar-system_nl.jar"
sign-jar ./my-solar-system/my-solar-system_nl.jar
echo "signing ./my-solar-system/my-solar-system_zh_CN.jar"
sign-jar ./my-solar-system/my-solar-system_zh_CN.jar
echo "signing ./my-solar-system/my-solar-system_tr.jar"
sign-jar ./my-solar-system/my-solar-system_tr.jar
echo "signing ./my-solar-system/my-solar-system_en.jar"
sign-jar ./my-solar-system/my-solar-system_en.jar
echo "signing ./my-solar-system/my-solar-system_bs.jar"
sign-jar ./my-solar-system/my-solar-system_bs.jar
echo "signing ./my-solar-system/my-solar-system_zh_TW.jar"
sign-jar ./my-solar-system/my-solar-system_zh_TW.jar
echo "signing ./my-solar-system/my-solar-system_cs.jar"
sign-jar ./my-solar-system/my-solar-system_cs.jar
echo "signing ./my-solar-system/my-solar-system_hu.jar"
sign-jar ./my-solar-system/my-solar-system_hu.jar
echo "signing ./my-solar-system/my-solar-system_ka.jar"
sign-jar ./my-solar-system/my-solar-system_ka.jar
echo "signing ./my-solar-system/my-solar-system_sr.jar"
sign-jar ./my-solar-system/my-solar-system_sr.jar
echo "signing ./my-solar-system/my-solar-system_es.jar"
sign-jar ./my-solar-system/my-solar-system_es.jar
echo "signing ./my-solar-system/my-solar-system_kn.jar"
sign-jar ./my-solar-system/my-solar-system_kn.jar
echo "signing ./my-solar-system/my-solar-system_be.jar"
sign-jar ./my-solar-system/my-solar-system_be.jar
echo "signing ./my-solar-system/my-solar-system_fr.jar"
sign-jar ./my-solar-system/my-solar-system_fr.jar
echo "signing ./my-solar-system/my-solar-system_eu.jar"
sign-jar ./my-solar-system/my-solar-system_eu.jar
echo "signing ./my-solar-system/my-solar-system_uk.jar"
sign-jar ./my-solar-system/my-solar-system_uk.jar
echo "signing ./my-solar-system/my-solar-system_fa.jar"
sign-jar ./my-solar-system/my-solar-system_fa.jar
echo "signing ./my-solar-system/my-solar-system_mk.jar"
sign-jar ./my-solar-system/my-solar-system_mk.jar
echo "signing ./my-solar-system/my-solar-system_hr.jar"
sign-jar ./my-solar-system/my-solar-system_hr.jar
echo "signing ./my-solar-system/my-solar-system_nb.jar"
sign-jar ./my-solar-system/my-solar-system_nb.jar
echo "signing ./my-solar-system/my-solar-system_sk.jar"
sign-jar ./my-solar-system/my-solar-system_sk.jar
echo "signing ./my-solar-system/my-solar-system_pl.jar"
sign-jar ./my-solar-system/my-solar-system_pl.jar
echo "signing ./my-solar-system/my-solar-system_tk.jar"
sign-jar ./my-solar-system/my-solar-system_tk.jar
echo "signing ./my-solar-system/my-solar-system_ku_TR.jar"
sign-jar ./my-solar-system/my-solar-system_ku_TR.jar
echo "signing ./my-solar-system/my-solar-system_ca.jar"
sign-jar ./my-solar-system/my-solar-system_ca.jar
echo "signing ./my-solar-system/my-solar-system_ko.jar"
sign-jar ./my-solar-system/my-solar-system_ko.jar
echo "signing ./my-solar-system/my-solar-system_es_ES.jar"
sign-jar ./my-solar-system/my-solar-system_es_ES.jar
echo "signing ./my-solar-system/my-solar-system_pt_BR.jar"
sign-jar ./my-solar-system/my-solar-system_pt_BR.jar
echo "signing ./my-solar-system/my-solar-system_es_PE.jar"
sign-jar ./my-solar-system/my-solar-system_es_PE.jar
echo "signing ./my-solar-system/my-solar-system_es_CO.jar"
sign-jar ./my-solar-system/my-solar-system_es_CO.jar
echo "signing ./my-solar-system/my-solar-system_th.jar"
sign-jar ./my-solar-system/my-solar-system_th.jar
echo "signing ./my-solar-system/my-solar-system_km.jar"
sign-jar ./my-solar-system/my-solar-system_km.jar
echo "signing ./my-solar-system/my-solar-system_sv.jar"
sign-jar ./my-solar-system/my-solar-system_sv.jar
echo "signing ./my-solar-system/my-solar-system_ar_SA.jar"
sign-jar ./my-solar-system/my-solar-system_ar_SA.jar
echo "signing ./my-solar-system/my-solar-system_iw.jar"
sign-jar ./my-solar-system/my-solar-system_iw.jar
echo "signing ./my-solar-system/my-solar-system_de.jar"
sign-jar ./my-solar-system/my-solar-system_de.jar
echo "signing ./my-solar-system/my-solar-system_et.jar"
sign-jar ./my-solar-system/my-solar-system_et.jar
echo "signing ./my-solar-system/my-solar-system_gl.jar"
sign-jar ./my-solar-system/my-solar-system_gl.jar
echo "signing ./my-solar-system/my-solar-system_ar.jar"
sign-jar ./my-solar-system/my-solar-system_ar.jar
echo "signing ./my-solar-system/my-solar-system_vi.jar"
sign-jar ./my-solar-system/my-solar-system_vi.jar
echo "signing ./my-solar-system/my-solar-system_it.jar"
sign-jar ./my-solar-system/my-solar-system_it.jar
echo "signing ./my-solar-system/my-solar-system_da.jar"
sign-jar ./my-solar-system/my-solar-system_da.jar
echo "signing ./my-solar-system/my-solar-system_el.jar"
sign-jar ./my-solar-system/my-solar-system_el.jar
echo "signing ./my-solar-system/my-solar-system_ja.jar"
sign-jar ./my-solar-system/my-solar-system_ja.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_tk.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_tk.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_pt_BR.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_pt_BR.jar
echo "signing ./balance-and-torque/balancing-act_fi.jar"
sign-jar ./balance-and-torque/balancing-act_fi.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_mk.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_mk.jar
echo "signing ./balance-and-torque/balancing-act_in.jar"
sign-jar ./balance-and-torque/balancing-act_in.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_th.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_th.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ro.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ro.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_zh_CN.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_zh_CN.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_iw.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_iw.jar
echo "signing ./balance-and-torque/balancing-act_fr.jar"
sign-jar ./balance-and-torque/balancing-act_fr.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_fa.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_fa.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_uk.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_uk.jar
echo "signing ./balance-and-torque/balancing-act_ru.jar"
sign-jar ./balance-and-torque/balancing-act_ru.jar
echo "signing ./balance-and-torque/balancing-act_th.jar"
sign-jar ./balance-and-torque/balancing-act_th.jar
echo "signing ./balance-and-torque/balancing-act_pt.jar"
sign-jar ./balance-and-torque/balancing-act_pt.jar
echo "signing ./balance-and-torque/balancing-act_tk.jar"
sign-jar ./balance-and-torque/balancing-act_tk.jar
echo "signing ./balance-and-torque/balancing-act_pl.jar"
sign-jar ./balance-and-torque/balancing-act_pl.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_tr.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_tr.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_es_PE.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_es_PE.jar
echo "signing ./balance-and-torque/balancing-act_sk.jar"
sign-jar ./balance-and-torque/balancing-act_sk.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_pl.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_pl.jar
echo "signing ./balance-and-torque/balancing-act_ar.jar"
sign-jar ./balance-and-torque/balancing-act_ar.jar
echo "signing ./balance-and-torque/balancing-act_it.jar"
sign-jar ./balance-and-torque/balancing-act_it.jar
echo "signing ./balance-and-torque/balance-and-torque_all.jar"
sign-jar ./balance-and-torque/balance-and-torque_all.jar
echo "signing ./balance-and-torque/balancing-act_da.jar"
sign-jar ./balance-and-torque/balancing-act_da.jar
echo "signing ./balance-and-torque/balancing-act_el.jar"
sign-jar ./balance-and-torque/balancing-act_el.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_cs.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_cs.jar
echo "signing ./balance-and-torque/balancing-act_sq.jar"
sign-jar ./balance-and-torque/balancing-act_sq.jar
echo "signing ./balance-and-torque/balancing-act_gl.jar"
sign-jar ./balance-and-torque/balancing-act_gl.jar
echo "signing ./balance-and-torque/balancing-act_es_PE.jar"
sign-jar ./balance-and-torque/balancing-act_es_PE.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_in.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_in.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_hy.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_hy.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_eu.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_eu.jar
echo "signing ./balance-and-torque/balancing-act_ku_TR.jar"
sign-jar ./balance-and-torque/balancing-act_ku_TR.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ja.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ja.jar
echo "signing ./balance-and-torque/balancing-act_lv.jar"
sign-jar ./balance-and-torque/balancing-act_lv.jar
echo "signing ./balance-and-torque/balancing-act_be.jar"
sign-jar ./balance-and-torque/balancing-act_be.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_az.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_az.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_uz.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_uz.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_nl.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_nl.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_el.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_el.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ru.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ru.jar
echo "signing ./balance-and-torque/balancing-act_hy.jar"
sign-jar ./balance-and-torque/balancing-act_hy.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_es.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_es.jar
echo "signing ./balance-and-torque/balancing-act_vi.jar"
sign-jar ./balance-and-torque/balancing-act_vi.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_lv.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_lv.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ku.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ku.jar
echo "signing ./balance-and-torque/balancing-act_tr.jar"
sign-jar ./balance-and-torque/balancing-act_tr.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_gl.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_gl.jar
echo "signing ./balance-and-torque/balancing-act_zh_CN.jar"
sign-jar ./balance-and-torque/balancing-act_zh_CN.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_zh_TW.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_zh_TW.jar
echo "signing ./balance-and-torque/balancing-act_de.jar"
sign-jar ./balance-and-torque/balancing-act_de.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_bs.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_bs.jar
echo "signing ./balance-and-torque/balancing-act_ku.jar"
sign-jar ./balance-and-torque/balancing-act_ku.jar
echo "signing ./balance-and-torque/balancing-act_uk.jar"
sign-jar ./balance-and-torque/balancing-act_uk.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_it.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_it.jar
echo "signing ./balance-and-torque/balancing-act_pt_BR.jar"
sign-jar ./balance-and-torque/balancing-act_pt_BR.jar
echo "signing ./balance-and-torque/balancing-act_kk.jar"
sign-jar ./balance-and-torque/balancing-act_kk.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_be.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_be.jar
echo "signing ./balance-and-torque/balancing-act_ja.jar"
sign-jar ./balance-and-torque/balancing-act_ja.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_fi.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_fi.jar
echo "signing ./balance-and-torque/balancing-act_fa.jar"
sign-jar ./balance-and-torque/balancing-act_fa.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_sr.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_sr.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ar.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ar.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ku_TR.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ku_TR.jar
echo "signing ./balance-and-torque/balancing-act_es_ES.jar"
sign-jar ./balance-and-torque/balancing-act_es_ES.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_km.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_km.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_bn.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_bn.jar
echo "signing ./balance-and-torque/balancing-act_iw.jar"
sign-jar ./balance-and-torque/balancing-act_iw.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_sk.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_sk.jar
echo "signing ./balance-and-torque/balancing-act_sr.jar"
sign-jar ./balance-and-torque/balancing-act_sr.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_kk.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_kk.jar
echo "signing ./balance-and-torque/balancing-act_az.jar"
sign-jar ./balance-and-torque/balancing-act_az.jar
echo "signing ./balance-and-torque/balancing-act_hu.jar"
sign-jar ./balance-and-torque/balancing-act_hu.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_da.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_da.jar
echo "signing ./balance-and-torque/balancing-act_sv.jar"
sign-jar ./balance-and-torque/balancing-act_sv.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_mr.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_mr.jar
echo "signing ./balance-and-torque/balancing-act_eu.jar"
sign-jar ./balance-and-torque/balancing-act_eu.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_pt.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_pt.jar
echo "signing ./balance-and-torque/balancing-act_bn.jar"
sign-jar ./balance-and-torque/balancing-act_bn.jar
echo "signing ./balance-and-torque/balancing-act_nl.jar"
sign-jar ./balance-and-torque/balancing-act_nl.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_hu.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_hu.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_de.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_de.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_sq.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_sq.jar
echo "signing ./balance-and-torque/balancing-act_ko.jar"
sign-jar ./balance-and-torque/balancing-act_ko.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_ko.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_ko.jar
echo "signing ./balance-and-torque/balance-and-torque_all_installer.jar"
sign-jar ./balance-and-torque/balance-and-torque_all_installer.jar
echo "signing ./balance-and-torque/balancing-act_en.jar"
sign-jar ./balance-and-torque/balancing-act_en.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_vi.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_vi.jar
echo "signing ./balance-and-torque/balancing-act_zh_TW.jar"
sign-jar ./balance-and-torque/balancing-act_zh_TW.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_en.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_en.jar
echo "signing ./balance-and-torque/balancing-act_ro.jar"
sign-jar ./balance-and-torque/balancing-act_ro.jar
echo "signing ./balance-and-torque/balancing-act_cs.jar"
sign-jar ./balance-and-torque/balancing-act_cs.jar
echo "signing ./balance-and-torque/balancing-act_km.jar"
sign-jar ./balance-and-torque/balancing-act_km.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_fr.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_fr.jar
echo "signing ./balance-and-torque/balancing-act_mk.jar"
sign-jar ./balance-and-torque/balancing-act_mk.jar
echo "signing ./balance-and-torque/balancing-act_es.jar"
sign-jar ./balance-and-torque/balancing-act_es.jar
echo "signing ./balance-and-torque/balancing-act_mr.jar"
sign-jar ./balance-and-torque/balancing-act_mr.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_es_ES.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_es_ES.jar
echo "signing ./balance-and-torque/balancing-act_bs.jar"
sign-jar ./balance-and-torque/balancing-act_bs.jar
echo "signing ./balance-and-torque/balancing-act-study-spring-2012_sv.jar"
sign-jar ./balance-and-torque/balancing-act-study-spring-2012_sv.jar
echo "signing ./balance-and-torque/balancing-act_uz.jar"
sign-jar ./balance-and-torque/balancing-act_uz.jar
echo "signing ./plinko-probability/plinko-probability_zh_TW.jar"
sign-jar ./plinko-probability/plinko-probability_zh_TW.jar
echo "signing ./plinko-probability/plinko-probability_zh_CN.jar"
sign-jar ./plinko-probability/plinko-probability_zh_CN.jar
echo "signing ./plinko-probability/plinko-probability_es_PE.jar"
sign-jar ./plinko-probability/plinko-probability_es_PE.jar
echo "signing ./plinko-probability/plinko-probability_sk.jar"
sign-jar ./plinko-probability/plinko-probability_sk.jar
echo "signing ./plinko-probability/plinko-probability_pt_BR.jar"
sign-jar ./plinko-probability/plinko-probability_pt_BR.jar
echo "signing ./plinko-probability/plinko-probability_de.jar"
sign-jar ./plinko-probability/plinko-probability_de.jar
echo "signing ./plinko-probability/plinko-probability_es.jar"
sign-jar ./plinko-probability/plinko-probability_es.jar
echo "signing ./plinko-probability/plinko-probability_hu.jar"
sign-jar ./plinko-probability/plinko-probability_hu.jar
echo "signing ./plinko-probability/plinko-probability_tk.jar"
sign-jar ./plinko-probability/plinko-probability_tk.jar
echo "signing ./plinko-probability/plinko-probability_mk.jar"
sign-jar ./plinko-probability/plinko-probability_mk.jar
echo "signing ./plinko-probability/plinko-probability_nl.jar"
sign-jar ./plinko-probability/plinko-probability_nl.jar
echo "signing ./plinko-probability/plinko-probability_en.jar"
sign-jar ./plinko-probability/plinko-probability_en.jar
echo "signing ./plinko-probability/plinko-probability_ja.jar"
sign-jar ./plinko-probability/plinko-probability_ja.jar
echo "signing ./plinko-probability/plinko-probability_kk.jar"
sign-jar ./plinko-probability/plinko-probability_kk.jar
echo "signing ./plinko-probability/plinko-probability_sv.jar"
sign-jar ./plinko-probability/plinko-probability_sv.jar
echo "signing ./plinko-probability/plinko-probability_sr.jar"
sign-jar ./plinko-probability/plinko-probability_sr.jar
echo "signing ./plinko-probability/plinko-probability_hr.jar"
sign-jar ./plinko-probability/plinko-probability_hr.jar
echo "signing ./plinko-probability/plinko-probability_fr.jar"
sign-jar ./plinko-probability/plinko-probability_fr.jar
echo "signing ./plinko-probability/plinko-probability_bs.jar"
sign-jar ./plinko-probability/plinko-probability_bs.jar
echo "signing ./plinko-probability/plinko-probability_eu.jar"
sign-jar ./plinko-probability/plinko-probability_eu.jar
echo "signing ./plinko-probability/plinko-probability_ko.jar"
sign-jar ./plinko-probability/plinko-probability_ko.jar
echo "signing ./plinko-probability/plinko-probability_fa.jar"
sign-jar ./plinko-probability/plinko-probability_fa.jar
echo "signing ./plinko-probability/plinko-probability_lv.jar"
sign-jar ./plinko-probability/plinko-probability_lv.jar
echo "signing ./plinko-probability/plinko-probability_et.jar"
sign-jar ./plinko-probability/plinko-probability_et.jar
echo "signing ./plinko-probability/plinko-probability_it.jar"
sign-jar ./plinko-probability/plinko-probability_it.jar
echo "signing ./plinko-probability/plinko-probability_el.jar"
sign-jar ./plinko-probability/plinko-probability_el.jar
echo "signing ./plinko-probability/plinko-probability_vi.jar"
sign-jar ./plinko-probability/plinko-probability_vi.jar
echo "signing ./plinko-probability/plinko-probability_iw.jar"
sign-jar ./plinko-probability/plinko-probability_iw.jar
echo "signing ./plinko-probability/plinko-probability_da.jar"
sign-jar ./plinko-probability/plinko-probability_da.jar
echo "signing ./plinko-probability/plinko-probability_tr.jar"
sign-jar ./plinko-probability/plinko-probability_tr.jar
echo "signing ./plinko-probability/plinko-probability_pl.jar"
sign-jar ./plinko-probability/plinko-probability_pl.jar
echo "signing ./molecules-and-light/molecules-and-light_zh_CN.jar"
sign-jar ./molecules-and-light/molecules-and-light_zh_CN.jar
echo "signing ./molecules-and-light/molecules-and-light_tk.jar"
sign-jar ./molecules-and-light/molecules-and-light_tk.jar
echo "signing ./molecules-and-light/molecules-and-light_bs.jar"
sign-jar ./molecules-and-light/molecules-and-light_bs.jar
echo "signing ./molecules-and-light/molecules-and-light_pt.jar"
sign-jar ./molecules-and-light/molecules-and-light_pt.jar
echo "signing ./molecules-and-light/molecules-and-light_fr.jar"
sign-jar ./molecules-and-light/molecules-and-light_fr.jar
echo "signing ./molecules-and-light/molecules-and-light_fa.jar"
sign-jar ./molecules-and-light/molecules-and-light_fa.jar
echo "signing ./molecules-and-light/molecules-and-light_nb.jar"
sign-jar ./molecules-and-light/molecules-and-light_nb.jar
echo "signing ./molecules-and-light/molecules-and-light_nn.jar"
sign-jar ./molecules-and-light/molecules-and-light_nn.jar
echo "signing ./molecules-and-light/molecules-and-light_km.jar"
sign-jar ./molecules-and-light/molecules-and-light_km.jar
echo "signing ./molecules-and-light/molecules-and-light_pl.jar"
sign-jar ./molecules-and-light/molecules-and-light_pl.jar
echo "signing ./molecules-and-light/molecules-and-light_tr.jar"
sign-jar ./molecules-and-light/molecules-and-light_tr.jar
echo "signing ./molecules-and-light/molecules-and-light_hu.jar"
sign-jar ./molecules-and-light/molecules-and-light_hu.jar
echo "signing ./molecules-and-light/molecules-and-light_ru.jar"
sign-jar ./molecules-and-light/molecules-and-light_ru.jar
echo "signing ./molecules-and-light/molecules-and-light_iw.jar"
sign-jar ./molecules-and-light/molecules-and-light_iw.jar
echo "signing ./molecules-and-light/molecules-and-light_ar.jar"
sign-jar ./molecules-and-light/molecules-and-light_ar.jar
echo "signing ./molecules-and-light/molecules-and-light_ja.jar"
sign-jar ./molecules-and-light/molecules-and-light_ja.jar
echo "signing ./molecules-and-light/molecules-and-light_all_installer.jar"
sign-jar ./molecules-and-light/molecules-and-light_all_installer.jar
echo "signing ./molecules-and-light/molecules-and-light_th.jar"
sign-jar ./molecules-and-light/molecules-and-light_th.jar
echo "signing ./molecules-and-light/molecules-and-light_sv.jar"
sign-jar ./molecules-and-light/molecules-and-light_sv.jar
echo "signing ./molecules-and-light/molecules-and-light_ca.jar"
sign-jar ./molecules-and-light/molecules-and-light_ca.jar
echo "signing ./molecules-and-light/molecules-and-light_sr.jar"
sign-jar ./molecules-and-light/molecules-and-light_sr.jar
echo "signing ./molecules-and-light/molecules-and-light_in.jar"
sign-jar ./molecules-and-light/molecules-and-light_in.jar
echo "signing ./molecules-and-light/molecules-and-light_es_PE.jar"
sign-jar ./molecules-and-light/molecules-and-light_es_PE.jar
echo "signing ./molecules-and-light/molecules-and-light_es.jar"
sign-jar ./molecules-and-light/molecules-and-light_es.jar
echo "signing ./molecules-and-light/molecules-and-light_sk.jar"
sign-jar ./molecules-and-light/molecules-and-light_sk.jar
echo "signing ./molecules-and-light/molecules-and-light_pt_BR.jar"
sign-jar ./molecules-and-light/molecules-and-light_pt_BR.jar
echo "signing ./molecules-and-light/molecules-and-light_lv.jar"
sign-jar ./molecules-and-light/molecules-and-light_lv.jar
echo "signing ./molecules-and-light/molecules-and-light_ko.jar"
sign-jar ./molecules-and-light/molecules-and-light_ko.jar
echo "signing ./molecules-and-light/molecules-and-light_it.jar"
sign-jar ./molecules-and-light/molecules-and-light_it.jar
echo "signing ./molecules-and-light/molecules-and-light_be.jar"
sign-jar ./molecules-and-light/molecules-and-light_be.jar
echo "signing ./molecules-and-light/molecules-and-light_zh_TW.jar"
sign-jar ./molecules-and-light/molecules-and-light_zh_TW.jar
echo "signing ./molecules-and-light/molecules-and-light_cs.jar"
sign-jar ./molecules-and-light/molecules-and-light_cs.jar
echo "signing ./molecules-and-light/molecules-and-light_de.jar"
sign-jar ./molecules-and-light/molecules-and-light_de.jar
echo "signing ./molecules-and-light/molecules-and-light_et.jar"
sign-jar ./molecules-and-light/molecules-and-light_et.jar
echo "signing ./molecules-and-light/molecules-and-light_da.jar"
sign-jar ./molecules-and-light/molecules-and-light_da.jar
echo "signing ./molecules-and-light/molecules-and-light_el.jar"
sign-jar ./molecules-and-light/molecules-and-light_el.jar
echo "signing ./molecules-and-light/molecules-and-light_en.jar"
sign-jar ./molecules-and-light/molecules-and-light_en.jar
echo "signing ./molecules-and-light/molecules-and-light_nl.jar"
sign-jar ./molecules-and-light/molecules-and-light_nl.jar
echo "signing ./molecules-and-light/molecules-and-light_gl.jar"
sign-jar ./molecules-and-light/molecules-and-light_gl.jar
echo "signing ./molecules-and-light/molecules-and-light_vi.jar"
sign-jar ./molecules-and-light/molecules-and-light_vi.jar
echo "signing ./molecules-and-light/molecules-and-light_mk.jar"
sign-jar ./molecules-and-light/molecules-and-light_mk.jar
echo "signing ./molecules-and-light/molecules-and-light_eu.jar"
sign-jar ./molecules-and-light/molecules-and-light_eu.jar
echo "signing ./molecules-and-light/molecules-and-light_all.jar"
sign-jar ./molecules-and-light/molecules-and-light_all.jar
echo "signing ./molecules-and-light/molecules-and-light_ka.jar"
sign-jar ./molecules-and-light/molecules-and-light_ka.jar
echo "signing ./molecules-and-light/molecules-and-light_kk.jar"
sign-jar ./molecules-and-light/molecules-and-light_kk.jar
echo "signing ./molarity/molarity_nl.jar"
sign-jar ./molarity/molarity_nl.jar
echo "signing ./molarity/molarity-interviews_es_PE.jar"
sign-jar ./molarity/molarity-interviews_es_PE.jar
echo "signing ./molarity/molarity_sk.jar"
sign-jar ./molarity/molarity_sk.jar
echo "signing ./molarity/molarity_ar.jar"
sign-jar ./molarity/molarity_ar.jar
echo "signing ./molarity/molarity_fa.jar"
sign-jar ./molarity/molarity_fa.jar
echo "signing ./molarity/molarity-interviews_es_ES.jar"
sign-jar ./molarity/molarity-interviews_es_ES.jar
echo "signing ./molarity/molarity_es.jar"
sign-jar ./molarity/molarity_es.jar
echo "signing ./molarity/molarity-interviews_pt_BR.jar"
sign-jar ./molarity/molarity-interviews_pt_BR.jar
echo "signing ./molarity/molarity-interviews_kk.jar"
sign-jar ./molarity/molarity-interviews_kk.jar
echo "signing ./molarity/molarity_el.jar"
sign-jar ./molarity/molarity_el.jar
echo "signing ./molarity/molarity-interviews_cs.jar"
sign-jar ./molarity/molarity-interviews_cs.jar
echo "signing ./molarity/molarity-interviews_ru.jar"
sign-jar ./molarity/molarity-interviews_ru.jar
echo "signing ./molarity/molarity_eu.jar"
sign-jar ./molarity/molarity_eu.jar
echo "signing ./molarity/molarity-interviews_ja.jar"
sign-jar ./molarity/molarity-interviews_ja.jar
echo "signing ./molarity/molarity_hu.jar"
sign-jar ./molarity/molarity_hu.jar
echo "signing ./molarity/molarity-interviews_ar.jar"
sign-jar ./molarity/molarity-interviews_ar.jar
echo "signing ./molarity/molarity-interviews_hu.jar"
sign-jar ./molarity/molarity-interviews_hu.jar
echo "signing ./molarity/molarity-interviews_in.jar"
sign-jar ./molarity/molarity-interviews_in.jar
echo "signing ./molarity/molarity_gl.jar"
sign-jar ./molarity/molarity_gl.jar
echo "signing ./molarity/molarity_ru.jar"
sign-jar ./molarity/molarity_ru.jar
echo "signing ./molarity/molarity-interviews_es.jar"
sign-jar ./molarity/molarity-interviews_es.jar
echo "signing ./molarity/molarity-interviews_sk.jar"
sign-jar ./molarity/molarity-interviews_sk.jar
echo "signing ./molarity/molarity-interviews_mk.jar"
sign-jar ./molarity/molarity-interviews_mk.jar
echo "signing ./molarity/molarity-interviews_vi.jar"
sign-jar ./molarity/molarity-interviews_vi.jar
echo "signing ./molarity/molarity-interviews_th.jar"
sign-jar ./molarity/molarity-interviews_th.jar
echo "signing ./molarity/molarity-interviews_gl.jar"
sign-jar ./molarity/molarity-interviews_gl.jar
echo "signing ./molarity/molarity_pt_BR.jar"
sign-jar ./molarity/molarity_pt_BR.jar
echo "signing ./molarity/molarity_mk.jar"
sign-jar ./molarity/molarity_mk.jar
echo "signing ./molarity/molarity_tr.jar"
sign-jar ./molarity/molarity_tr.jar
echo "signing ./molarity/molarity_es_CO.jar"
sign-jar ./molarity/molarity_es_CO.jar
echo "signing ./molarity/molarity_tk.jar"
sign-jar ./molarity/molarity_tk.jar
echo "signing ./molarity/molarity-interviews_sr.jar"
sign-jar ./molarity/molarity-interviews_sr.jar
echo "signing ./molarity/molarity_it.jar"
sign-jar ./molarity/molarity_it.jar
echo "signing ./molarity/molarity_cs.jar"
sign-jar ./molarity/molarity_cs.jar
echo "signing ./molarity/molarity_all.jar"
sign-jar ./molarity/molarity_all.jar
echo "signing ./molarity/molarity-interviews_el.jar"
sign-jar ./molarity/molarity-interviews_el.jar
echo "signing ./molarity/molarity_th.jar"
sign-jar ./molarity/molarity_th.jar
echo "signing ./molarity/molarity-interviews_bs.jar"
sign-jar ./molarity/molarity-interviews_bs.jar
echo "signing ./molarity/molarity-interviews_iw.jar"
sign-jar ./molarity/molarity-interviews_iw.jar
echo "signing ./molarity/molarity-interviews_zh_CN.jar"
sign-jar ./molarity/molarity-interviews_zh_CN.jar
echo "signing ./molarity/molarity-interviews_en.jar"
sign-jar ./molarity/molarity-interviews_en.jar
echo "signing ./molarity/molarity-interviews_da.jar"
sign-jar ./molarity/molarity-interviews_da.jar
echo "signing ./molarity/molarity-interviews_zh_TW.jar"
sign-jar ./molarity/molarity-interviews_zh_TW.jar
echo "signing ./molarity/molarity_iw.jar"
sign-jar ./molarity/molarity_iw.jar
echo "signing ./molarity/molarity_kk.jar"
sign-jar ./molarity/molarity_kk.jar
echo "signing ./molarity/molarity-interviews_pl.jar"
sign-jar ./molarity/molarity-interviews_pl.jar
echo "signing ./molarity/molarity-interviews_fr.jar"
sign-jar ./molarity/molarity-interviews_fr.jar
echo "signing ./molarity/molarity_en.jar"
sign-jar ./molarity/molarity_en.jar
echo "signing ./molarity/molarity_sv.jar"
sign-jar ./molarity/molarity_sv.jar
echo "signing ./molarity/molarity_de.jar"
sign-jar ./molarity/molarity_de.jar
echo "signing ./molarity/molarity_es_PE.jar"
sign-jar ./molarity/molarity_es_PE.jar
echo "signing ./molarity/molarity-interviews_tr.jar"
sign-jar ./molarity/molarity-interviews_tr.jar
echo "signing ./molarity/molarity_km.jar"
sign-jar ./molarity/molarity_km.jar
echo "signing ./molarity/molarity-interviews_it.jar"
sign-jar ./molarity/molarity-interviews_it.jar
echo "signing ./molarity/molarity-interviews_nl.jar"
sign-jar ./molarity/molarity-interviews_nl.jar
echo "signing ./molarity/molarity_fr.jar"
sign-jar ./molarity/molarity_fr.jar
echo "signing ./molarity/molarity-interviews_ka.jar"
sign-jar ./molarity/molarity-interviews_ka.jar
echo "signing ./molarity/molarity_pl.jar"
sign-jar ./molarity/molarity_pl.jar
echo "signing ./molarity/molarity_ja.jar"
sign-jar ./molarity/molarity_ja.jar
echo "signing ./molarity/molarity-interviews_tk.jar"
sign-jar ./molarity/molarity-interviews_tk.jar
echo "signing ./molarity/molarity_ko.jar"
sign-jar ./molarity/molarity_ko.jar
echo "signing ./molarity/molarity_in.jar"
sign-jar ./molarity/molarity_in.jar
echo "signing ./molarity/molarity-interviews_es_CO.jar"
sign-jar ./molarity/molarity-interviews_es_CO.jar
echo "signing ./molarity/molarity-interviews_ko.jar"
sign-jar ./molarity/molarity-interviews_ko.jar
echo "signing ./molarity/molarity_es_ES.jar"
sign-jar ./molarity/molarity_es_ES.jar
echo "signing ./molarity/molarity_vi.jar"
sign-jar ./molarity/molarity_vi.jar
echo "signing ./molarity/molarity-interviews_eu.jar"
sign-jar ./molarity/molarity-interviews_eu.jar
echo "signing ./molarity/molarity_bs.jar"
sign-jar ./molarity/molarity_bs.jar
echo "signing ./molarity/molarity-interviews_de.jar"
sign-jar ./molarity/molarity-interviews_de.jar
echo "signing ./molarity/molarity-interviews_km.jar"
sign-jar ./molarity/molarity-interviews_km.jar
echo "signing ./molarity/molarity_ka.jar"
sign-jar ./molarity/molarity_ka.jar
echo "signing ./molarity/molarity_sr.jar"
sign-jar ./molarity/molarity_sr.jar
echo "signing ./molarity/molarity_da.jar"
sign-jar ./molarity/molarity_da.jar
echo "signing ./molarity/molarity-interviews_sv.jar"
sign-jar ./molarity/molarity-interviews_sv.jar
echo "signing ./molarity/molarity_zh_CN.jar"
sign-jar ./molarity/molarity_zh_CN.jar
echo "signing ./molarity/molarity_all_installer.jar"
sign-jar ./molarity/molarity_all_installer.jar
echo "signing ./molarity/molarity-interviews_fa.jar"
sign-jar ./molarity/molarity-interviews_fa.jar
echo "signing ./molarity/molarity_zh_TW.jar"
sign-jar ./molarity/molarity_zh_TW.jar
echo "signing ./semiconductor/semiconductor_hr.jar"
sign-jar ./semiconductor/semiconductor_hr.jar
echo "signing ./semiconductor/semiconductor_de.jar"
sign-jar ./semiconductor/semiconductor_de.jar
echo "signing ./semiconductor/semiconductor_tk.jar"
sign-jar ./semiconductor/semiconductor_tk.jar
echo "signing ./semiconductor/semiconductor_ar.jar"
sign-jar ./semiconductor/semiconductor_ar.jar
echo "signing ./semiconductor/semiconductor_tr.jar"
sign-jar ./semiconductor/semiconductor_tr.jar
echo "signing ./semiconductor/semiconductor_ja.jar"
sign-jar ./semiconductor/semiconductor_ja.jar
echo "signing ./semiconductor/semiconductor_pt_BR.jar"
sign-jar ./semiconductor/semiconductor_pt_BR.jar
echo "signing ./semiconductor/semiconductor_all_installer.jar"
sign-jar ./semiconductor/semiconductor_all_installer.jar
echo "signing ./semiconductor/semiconductor_mk.jar"
sign-jar ./semiconductor/semiconductor_mk.jar
echo "signing ./semiconductor/semiconductor_ru.jar"
sign-jar ./semiconductor/semiconductor_ru.jar
echo "signing ./semiconductor/semiconductor_el.jar"
sign-jar ./semiconductor/semiconductor_el.jar
echo "signing ./semiconductor/semiconductor_all.jar"
sign-jar ./semiconductor/semiconductor_all.jar
echo "signing ./semiconductor/semiconductor_ro.jar"
sign-jar ./semiconductor/semiconductor_ro.jar
echo "signing ./semiconductor/semiconductor_da.jar"
sign-jar ./semiconductor/semiconductor_da.jar
echo "signing ./semiconductor/semiconductor_sv.jar"
sign-jar ./semiconductor/semiconductor_sv.jar
echo "signing ./semiconductor/semiconductor_fr.jar"
sign-jar ./semiconductor/semiconductor_fr.jar
echo "signing ./semiconductor/semiconductor_ko.jar"
sign-jar ./semiconductor/semiconductor_ko.jar
echo "signing ./semiconductor/semiconductor_sk.jar"
sign-jar ./semiconductor/semiconductor_sk.jar
echo "signing ./semiconductor/semiconductor_iw.jar"
sign-jar ./semiconductor/semiconductor_iw.jar
echo "signing ./semiconductor/semiconductor_en.jar"
sign-jar ./semiconductor/semiconductor_en.jar
echo "signing ./semiconductor/semiconductor_es.jar"
sign-jar ./semiconductor/semiconductor_es.jar
echo "signing ./semiconductor/semiconductor_km.jar"
sign-jar ./semiconductor/semiconductor_km.jar
echo "signing ./semiconductor/semiconductor_eu.jar"
sign-jar ./semiconductor/semiconductor_eu.jar
echo "signing ./semiconductor/semiconductor_gl.jar"
sign-jar ./semiconductor/semiconductor_gl.jar
echo "signing ./semiconductor/semiconductor_hy.jar"
sign-jar ./semiconductor/semiconductor_hy.jar
echo "signing ./semiconductor/semiconductor_ar_SA.jar"
sign-jar ./semiconductor/semiconductor_ar_SA.jar
echo "signing ./semiconductor/semiconductor_pl.jar"
sign-jar ./semiconductor/semiconductor_pl.jar
echo "signing ./semiconductor/semiconductor_sr.jar"
sign-jar ./semiconductor/semiconductor_sr.jar
echo "signing ./semiconductor/semiconductor_bs.jar"
sign-jar ./semiconductor/semiconductor_bs.jar
echo "signing ./semiconductor/semiconductor_zh_CN.jar"
sign-jar ./semiconductor/semiconductor_zh_CN.jar
echo "signing ./semiconductor/semiconductor_th.jar"
sign-jar ./semiconductor/semiconductor_th.jar
echo "signing ./semiconductor/semiconductor_hu.jar"
sign-jar ./semiconductor/semiconductor_hu.jar
echo "signing ./semiconductor/semiconductor_kn.jar"
sign-jar ./semiconductor/semiconductor_kn.jar
echo "signing ./semiconductor/semiconductor_sl.jar"
sign-jar ./semiconductor/semiconductor_sl.jar
echo "signing ./semiconductor/semiconductor_kk.jar"
sign-jar ./semiconductor/semiconductor_kk.jar
echo "signing ./semiconductor/semiconductor_it.jar"
sign-jar ./semiconductor/semiconductor_it.jar
echo "signing ./semiconductor/semiconductor_uk.jar"
sign-jar ./semiconductor/semiconductor_uk.jar
echo "signing ./semiconductor/semiconductor_nl.jar"
sign-jar ./semiconductor/semiconductor_nl.jar
echo "signing ./semiconductor/semiconductor_ka.jar"
sign-jar ./semiconductor/semiconductor_ka.jar
echo "signing ./semiconductor/semiconductor_vi.jar"
sign-jar ./semiconductor/semiconductor_vi.jar
echo "signing ./semiconductor/semiconductor_in.jar"
sign-jar ./semiconductor/semiconductor_in.jar
echo "signing ./semiconductor/semiconductor_pt.jar"
sign-jar ./semiconductor/semiconductor_pt.jar
echo "signing ./semiconductor/semiconductor_zh_TW.jar"
sign-jar ./semiconductor/semiconductor_zh_TW.jar
echo "signing ./semiconductor/semiconductor_es_PE.jar"
sign-jar ./semiconductor/semiconductor_es_PE.jar
echo "signing ./semiconductor/semiconductor_fa.jar"
sign-jar ./semiconductor/semiconductor_fa.jar
echo "signing ./rotation/torque_sv.jar"
sign-jar ./rotation/torque_sv.jar
echo "signing ./rotation/rotation_sr.jar"
sign-jar ./rotation/rotation_sr.jar
echo "signing ./rotation/rotation_th.jar"
sign-jar ./rotation/rotation_th.jar
echo "signing ./rotation/torque_iw.jar"
sign-jar ./rotation/torque_iw.jar
echo "signing ./rotation/torque_zh_CN.jar"
sign-jar ./rotation/torque_zh_CN.jar
echo "signing ./rotation/torque_ja.jar"
sign-jar ./rotation/torque_ja.jar
echo "signing ./rotation/torque_sr.jar"
sign-jar ./rotation/torque_sr.jar
echo "signing ./rotation/torque_gl.jar"
sign-jar ./rotation/torque_gl.jar
echo "signing ./rotation/torque_fa.jar"
sign-jar ./rotation/torque_fa.jar
echo "signing ./rotation/rotation_nl.jar"
sign-jar ./rotation/rotation_nl.jar
echo "signing ./rotation/rotation_ja.jar"
sign-jar ./rotation/rotation_ja.jar
echo "signing ./rotation/torque_de.jar"
sign-jar ./rotation/torque_de.jar
echo "signing ./rotation/rotation_es_PE.jar"
sign-jar ./rotation/rotation_es_PE.jar
echo "signing ./rotation/torque_it.jar"
sign-jar ./rotation/torque_it.jar
echo "signing ./rotation/torque_vi.jar"
sign-jar ./rotation/torque_vi.jar
echo "signing ./rotation/torque_mk.jar"
sign-jar ./rotation/torque_mk.jar
echo "signing ./rotation/torque_fr.jar"
sign-jar ./rotation/torque_fr.jar
echo "signing ./rotation/rotation_de.jar"
sign-jar ./rotation/rotation_de.jar
echo "signing ./rotation/torque_sk.jar"
sign-jar ./rotation/torque_sk.jar
echo "signing ./rotation/rotation_bs.jar"
sign-jar ./rotation/rotation_bs.jar
echo "signing ./rotation/torque_th.jar"
sign-jar ./rotation/torque_th.jar
echo "signing ./rotation/rotation_gl.jar"
sign-jar ./rotation/rotation_gl.jar
echo "signing ./rotation/torque_bs.jar"
sign-jar ./rotation/torque_bs.jar
echo "signing ./rotation/rotation_be.jar"
sign-jar ./rotation/rotation_be.jar
echo "signing ./rotation/rotation_et.jar"
sign-jar ./rotation/rotation_et.jar
echo "signing ./rotation/torque_hu.jar"
sign-jar ./rotation/torque_hu.jar
echo "signing ./rotation/rotation_ar_SA.jar"
sign-jar ./rotation/rotation_ar_SA.jar
echo "signing ./rotation/torque_in.jar"
sign-jar ./rotation/torque_in.jar
echo "signing ./rotation/rotation_pt_BR.jar"
sign-jar ./rotation/rotation_pt_BR.jar
echo "signing ./rotation/rotation_da.jar"
sign-jar ./rotation/rotation_da.jar
echo "signing ./rotation/rotation_hr.jar"
sign-jar ./rotation/rotation_hr.jar
echo "signing ./rotation/rotation_hu.jar"
sign-jar ./rotation/rotation_hu.jar
echo "signing ./rotation/torque_be.jar"
sign-jar ./rotation/torque_be.jar
echo "signing ./rotation/rotation_cs.jar"
sign-jar ./rotation/rotation_cs.jar
echo "signing ./rotation/rotation_tk.jar"
sign-jar ./rotation/rotation_tk.jar
echo "signing ./rotation/torque_pt_BR.jar"
sign-jar ./rotation/torque_pt_BR.jar
echo "signing ./rotation/torque_en.jar"
sign-jar ./rotation/torque_en.jar
echo "signing ./rotation/torque_ar_SA.jar"
sign-jar ./rotation/torque_ar_SA.jar
echo "signing ./rotation/torque_es.jar"
sign-jar ./rotation/torque_es.jar
echo "signing ./rotation/torque_et.jar"
sign-jar ./rotation/torque_et.jar
echo "signing ./rotation/rotation_sk.jar"
sign-jar ./rotation/rotation_sk.jar
echo "signing ./rotation/rotation_eu.jar"
sign-jar ./rotation/rotation_eu.jar
echo "signing ./rotation/rotation_km.jar"
sign-jar ./rotation/rotation_km.jar
echo "signing ./rotation/torque_km.jar"
sign-jar ./rotation/torque_km.jar
echo "signing ./rotation/rotation_ku_TR.jar"
sign-jar ./rotation/rotation_ku_TR.jar
echo "signing ./rotation/rotation_all.jar"
sign-jar ./rotation/rotation_all.jar
echo "signing ./rotation/rotation_in.jar"
sign-jar ./rotation/rotation_in.jar
echo "signing ./rotation/torque_es_CO.jar"
sign-jar ./rotation/torque_es_CO.jar
echo "signing ./rotation/rotation_pt.jar"
sign-jar ./rotation/rotation_pt.jar
echo "signing ./rotation/torque_tk.jar"
sign-jar ./rotation/torque_tk.jar
echo "signing ./rotation/torque_pl.jar"
sign-jar ./rotation/torque_pl.jar
echo "signing ./rotation/torque_ko.jar"
sign-jar ./rotation/torque_ko.jar
echo "signing ./rotation/rotation_ar.jar"
sign-jar ./rotation/rotation_ar.jar
echo "signing ./rotation/rotation_fi.jar"
sign-jar ./rotation/rotation_fi.jar
echo "signing ./rotation/torque_ms.jar"
sign-jar ./rotation/torque_ms.jar
echo "signing ./rotation/rotation_fa.jar"
sign-jar ./rotation/rotation_fa.jar
echo "signing ./rotation/torque_zh_TW.jar"
sign-jar ./rotation/torque_zh_TW.jar
echo "signing ./rotation/rotation_it.jar"
sign-jar ./rotation/rotation_it.jar
echo "signing ./rotation/torque_cs.jar"
sign-jar ./rotation/torque_cs.jar
echo "signing ./rotation/torque_es_PE.jar"
sign-jar ./rotation/torque_es_PE.jar
echo "signing ./rotation/rotation_en.jar"
sign-jar ./rotation/rotation_en.jar
echo "signing ./rotation/torque_pt.jar"
sign-jar ./rotation/torque_pt.jar
echo "signing ./rotation/rotation_tr.jar"
sign-jar ./rotation/rotation_tr.jar
echo "signing ./rotation/rotation_pl.jar"
sign-jar ./rotation/rotation_pl.jar
echo "signing ./rotation/rotation_sv.jar"
sign-jar ./rotation/rotation_sv.jar
echo "signing ./rotation/torque_da.jar"
sign-jar ./rotation/torque_da.jar
echo "signing ./rotation/rotation_zh_CN.jar"
sign-jar ./rotation/rotation_zh_CN.jar
echo "signing ./rotation/rotation_ms.jar"
sign-jar ./rotation/rotation_ms.jar
echo "signing ./rotation/rotation_iw.jar"
sign-jar ./rotation/rotation_iw.jar
echo "signing ./rotation/rotation_all_installer.jar"
sign-jar ./rotation/rotation_all_installer.jar
echo "signing ./rotation/rotation_mk.jar"
sign-jar ./rotation/rotation_mk.jar
echo "signing ./rotation/torque_fi.jar"
sign-jar ./rotation/torque_fi.jar
echo "signing ./rotation/rotation_ko.jar"
sign-jar ./rotation/rotation_ko.jar
echo "signing ./rotation/torque_ar.jar"
sign-jar ./rotation/torque_ar.jar
echo "signing ./rotation/rotation_el.jar"
sign-jar ./rotation/rotation_el.jar
echo "signing ./rotation/rotation_vi.jar"
sign-jar ./rotation/rotation_vi.jar
echo "signing ./rotation/torque_nl.jar"
sign-jar ./rotation/torque_nl.jar
echo "signing ./rotation/torque_eu.jar"
sign-jar ./rotation/torque_eu.jar
echo "signing ./rotation/rotation_es.jar"
sign-jar ./rotation/rotation_es.jar
echo "signing ./rotation/torque_tr.jar"
sign-jar ./rotation/torque_tr.jar
echo "signing ./rotation/rotation_fr.jar"
sign-jar ./rotation/rotation_fr.jar
echo "signing ./rotation/torque_el.jar"
sign-jar ./rotation/torque_el.jar
echo "signing ./rotation/torque_hr.jar"
sign-jar ./rotation/torque_hr.jar
echo "signing ./rotation/rotation_es_CO.jar"
sign-jar ./rotation/rotation_es_CO.jar
echo "signing ./rotation/rotation_zh_TW.jar"
sign-jar ./rotation/rotation_zh_TW.jar
echo "signing ./rotation/torque_ku_TR.jar"
sign-jar ./rotation/torque_ku_TR.jar
echo "signing ./molecule-shapes/molecule-shapes_ja.jar"
sign-jar ./molecule-shapes/molecule-shapes_ja.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_en.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_en.jar
echo "signing ./molecule-shapes/molecule-shapes_all.jar"
sign-jar ./molecule-shapes/molecule-shapes_all.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_hu.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_hu.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_fr.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_fr.jar
echo "signing ./molecule-shapes/molecule-shapes_sq.jar"
sign-jar ./molecule-shapes/molecule-shapes_sq.jar
echo "signing ./molecule-shapes/molecule-shapes_sk.jar"
sign-jar ./molecule-shapes/molecule-shapes_sk.jar
echo "signing ./molecule-shapes/molecule-shapes_sr.jar"
sign-jar ./molecule-shapes/molecule-shapes_sr.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_ko.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_ko.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_tk.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_tk.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_ja.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_ja.jar
echo "signing ./molecule-shapes/molecule-shapes_it.jar"
sign-jar ./molecule-shapes/molecule-shapes_it.jar
echo "signing ./molecule-shapes/native_linux.jar"
sign-jar ./molecule-shapes/native_linux.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_da.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_da.jar
echo "signing ./molecule-shapes/molecule-shapes_ko.jar"
sign-jar ./molecule-shapes/molecule-shapes_ko.jar
echo "signing ./molecule-shapes/molecule-shapes_hu.jar"
sign-jar ./molecule-shapes/molecule-shapes_hu.jar
echo "signing ./molecule-shapes/molecule-shapes_fi.jar"
sign-jar ./molecule-shapes/molecule-shapes_fi.jar
echo "signing ./molecule-shapes/molecule-shapes_iw.jar"
sign-jar ./molecule-shapes/molecule-shapes_iw.jar
echo "signing ./molecule-shapes/molecule-shapes_eu.jar"
sign-jar ./molecule-shapes/molecule-shapes_eu.jar
echo "signing ./molecule-shapes/molecule-shapes_pt_BR.jar"
sign-jar ./molecule-shapes/molecule-shapes_pt_BR.jar
echo "signing ./molecule-shapes/molecule-shapes_pl.jar"
sign-jar ./molecule-shapes/molecule-shapes_pl.jar
echo "signing ./molecule-shapes/molecule-shapes_de.jar"
sign-jar ./molecule-shapes/molecule-shapes_de.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_tr.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_tr.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_in.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_in.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_fa.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_fa.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_pl.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_pl.jar
echo "signing ./molecule-shapes/molecule-shapes_da.jar"
sign-jar ./molecule-shapes/molecule-shapes_da.jar
echo "signing ./molecule-shapes/native_macosx.jar"
sign-jar ./molecule-shapes/native_macosx.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_de.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_de.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_zh_TW.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_zh_TW.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_cs.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_cs.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_fi.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_fi.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_sr.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_sr.jar
echo "signing ./molecule-shapes/molecule-shapes_bs.jar"
sign-jar ./molecule-shapes/molecule-shapes_bs.jar
echo "signing ./molecule-shapes/molecule-shapes_sv.jar"
sign-jar ./molecule-shapes/molecule-shapes_sv.jar
echo "signing ./molecule-shapes/molecule-shapes_nl.jar"
sign-jar ./molecule-shapes/molecule-shapes_nl.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_gl.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_gl.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_eu.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_eu.jar
echo "signing ./molecule-shapes/molecule-shapes_fa.jar"
sign-jar ./molecule-shapes/molecule-shapes_fa.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_el.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_el.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_ru.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_ru.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_es_PE.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_es_PE.jar
echo "signing ./molecule-shapes/molecule-shapes_zh_TW.jar"
sign-jar ./molecule-shapes/molecule-shapes_zh_TW.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_sv.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_sv.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_it.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_it.jar
echo "signing ./molecule-shapes/molecule-shapes_th.jar"
sign-jar ./molecule-shapes/molecule-shapes_th.jar
echo "signing ./molecule-shapes/molecule-shapes_zh_CN.jar"
sign-jar ./molecule-shapes/molecule-shapes_zh_CN.jar
echo "signing ./molecule-shapes/molecule-shapes_cs.jar"
sign-jar ./molecule-shapes/molecule-shapes_cs.jar
echo "signing ./molecule-shapes/molecule-shapes_all_installer.jar"
sign-jar ./molecule-shapes/molecule-shapes_all_installer.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_kk.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_kk.jar
echo "signing ./molecule-shapes/molecule-shapes_tr.jar"
sign-jar ./molecule-shapes/molecule-shapes_tr.jar
echo "signing ./molecule-shapes/molecule-shapes_fr.jar"
sign-jar ./molecule-shapes/molecule-shapes_fr.jar
echo "signing ./molecule-shapes/molecule-shapes_vi.jar"
sign-jar ./molecule-shapes/molecule-shapes_vi.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_th.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_th.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_sk.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_sk.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_es.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_es.jar
echo "signing ./molecule-shapes/molecule-shapes_es.jar"
sign-jar ./molecule-shapes/molecule-shapes_es.jar
echo "signing ./molecule-shapes/molecule-shapes_gl.jar"
sign-jar ./molecule-shapes/molecule-shapes_gl.jar
echo "signing ./molecule-shapes/molecule-shapes_el.jar"
sign-jar ./molecule-shapes/molecule-shapes_el.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_pt_BR.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_pt_BR.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_mk.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_mk.jar
echo "signing ./molecule-shapes/molecule-shapes_es_PE.jar"
sign-jar ./molecule-shapes/molecule-shapes_es_PE.jar
echo "signing ./molecule-shapes/molecule-shapes_kk.jar"
sign-jar ./molecule-shapes/molecule-shapes_kk.jar
echo "signing ./molecule-shapes/molecule-shapes_mk.jar"
sign-jar ./molecule-shapes/molecule-shapes_mk.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_bs.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_bs.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_vi.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_vi.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_ar.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_ar.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_nl.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_nl.jar
echo "signing ./molecule-shapes/native_solaris.jar"
sign-jar ./molecule-shapes/native_solaris.jar
echo "signing ./molecule-shapes/molecule-shapes_en.jar"
sign-jar ./molecule-shapes/molecule-shapes_en.jar
echo "signing ./molecule-shapes/molecule-shapes_ru.jar"
sign-jar ./molecule-shapes/molecule-shapes_ru.jar
echo "signing ./molecule-shapes/molecule-shapes_ar.jar"
sign-jar ./molecule-shapes/molecule-shapes_ar.jar
echo "signing ./molecule-shapes/molecule-shapes_tk.jar"
sign-jar ./molecule-shapes/molecule-shapes_tk.jar
echo "signing ./molecule-shapes/molecule-shapes_in.jar"
sign-jar ./molecule-shapes/molecule-shapes_in.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_sq.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_sq.jar
echo "signing ./molecule-shapes/native_windows.jar"
sign-jar ./molecule-shapes/native_windows.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_zh_CN.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_zh_CN.jar
echo "signing ./molecule-shapes/molecule-shapes-basics_iw.jar"
sign-jar ./molecule-shapes/molecule-shapes-basics_iw.jar
echo "signing ./electric-hockey/electric-hockey_es_PE.jar"
sign-jar ./electric-hockey/electric-hockey_es_PE.jar
echo "signing ./electric-hockey/electric-hockey_lv.jar"
sign-jar ./electric-hockey/electric-hockey_lv.jar
echo "signing ./electric-hockey/electric-hockey_bs.jar"
sign-jar ./electric-hockey/electric-hockey_bs.jar
echo "signing ./electric-hockey/electric-hockey_nb.jar"
sign-jar ./electric-hockey/electric-hockey_nb.jar
echo "signing ./electric-hockey/electric-hockey_tr.jar"
sign-jar ./electric-hockey/electric-hockey_tr.jar
echo "signing ./electric-hockey/electric-hockey_pt_BR.jar"
sign-jar ./electric-hockey/electric-hockey_pt_BR.jar
echo "signing ./electric-hockey/electric-hockey_it.jar"
sign-jar ./electric-hockey/electric-hockey_it.jar
echo "signing ./electric-hockey/electric-hockey_et.jar"
sign-jar ./electric-hockey/electric-hockey_et.jar
echo "signing ./electric-hockey/electric-hockey_sk.jar"
sign-jar ./electric-hockey/electric-hockey_sk.jar
echo "signing ./electric-hockey/electric-hockey_de.jar"
sign-jar ./electric-hockey/electric-hockey_de.jar
echo "signing ./electric-hockey/electric-hockey_iw.jar"
sign-jar ./electric-hockey/electric-hockey_iw.jar
echo "signing ./electric-hockey/electric-hockey_ku_TR.jar"
sign-jar ./electric-hockey/electric-hockey_ku_TR.jar
echo "signing ./electric-hockey/electric-hockey_es.jar"
sign-jar ./electric-hockey/electric-hockey_es.jar
echo "signing ./electric-hockey/electric-hockey_vi.jar"
sign-jar ./electric-hockey/electric-hockey_vi.jar
echo "signing ./electric-hockey/electric-hockey_ar.jar"
sign-jar ./electric-hockey/electric-hockey_ar.jar
echo "signing ./electric-hockey/electric-hockey_eu.jar"
sign-jar ./electric-hockey/electric-hockey_eu.jar
echo "signing ./electric-hockey/electric-hockey_all.jar"
sign-jar ./electric-hockey/electric-hockey_all.jar
echo "signing ./electric-hockey/electric-hockey_ar_SA.jar"
sign-jar ./electric-hockey/electric-hockey_ar_SA.jar
echo "signing ./electric-hockey/electric-hockey_hr.jar"
sign-jar ./electric-hockey/electric-hockey_hr.jar
echo "signing ./electric-hockey/electric-hockey_ka.jar"
sign-jar ./electric-hockey/electric-hockey_ka.jar
echo "signing ./electric-hockey/electric-hockey_ru.jar"
sign-jar ./electric-hockey/electric-hockey_ru.jar
echo "signing ./electric-hockey/electric-hockey_uk.jar"
sign-jar ./electric-hockey/electric-hockey_uk.jar
echo "signing ./electric-hockey/electric-hockey_kk.jar"
sign-jar ./electric-hockey/electric-hockey_kk.jar
echo "signing ./electric-hockey/electric-hockey_mn.jar"
sign-jar ./electric-hockey/electric-hockey_mn.jar
echo "signing ./electric-hockey/electric-hockey_mr.jar"
sign-jar ./electric-hockey/electric-hockey_mr.jar
echo "signing ./electric-hockey/electric-hockey_ko.jar"
sign-jar ./electric-hockey/electric-hockey_ko.jar
echo "signing ./electric-hockey/electric-hockey_sr.jar"
sign-jar ./electric-hockey/electric-hockey_sr.jar
echo "signing ./electric-hockey/electric-hockey_fa.jar"
sign-jar ./electric-hockey/electric-hockey_fa.jar
echo "signing ./electric-hockey/electric-hockey_sv.jar"
sign-jar ./electric-hockey/electric-hockey_sv.jar
echo "signing ./electric-hockey/electric-hockey_hu.jar"
sign-jar ./electric-hockey/electric-hockey_hu.jar
echo "signing ./electric-hockey/electric-hockey_gl.jar"
sign-jar ./electric-hockey/electric-hockey_gl.jar
echo "signing ./electric-hockey/electric-hockey_be.jar"
sign-jar ./electric-hockey/electric-hockey_be.jar
echo "signing ./electric-hockey/electric-hockey_th.jar"
sign-jar ./electric-hockey/electric-hockey_th.jar
echo "signing ./electric-hockey/electric-hockey_ja.jar"
sign-jar ./electric-hockey/electric-hockey_ja.jar
echo "signing ./electric-hockey/electric-hockey_el.jar"
sign-jar ./electric-hockey/electric-hockey_el.jar
echo "signing ./electric-hockey/electric-hockey_pt.jar"
sign-jar ./electric-hockey/electric-hockey_pt.jar
echo "signing ./electric-hockey/electric-hockey_tk.jar"
sign-jar ./electric-hockey/electric-hockey_tk.jar
echo "signing ./electric-hockey/electric-hockey_mk.jar"
sign-jar ./electric-hockey/electric-hockey_mk.jar
echo "signing ./electric-hockey/electric-hockey_in.jar"
sign-jar ./electric-hockey/electric-hockey_in.jar
echo "signing ./electric-hockey/electric-hockey_ro.jar"
sign-jar ./electric-hockey/electric-hockey_ro.jar
echo "signing ./electric-hockey/electric-hockey_all_installer.jar"
sign-jar ./electric-hockey/electric-hockey_all_installer.jar
echo "signing ./electric-hockey/electric-hockey_nl.jar"
sign-jar ./electric-hockey/electric-hockey_nl.jar
echo "signing ./electric-hockey/electric-hockey_fr.jar"
sign-jar ./electric-hockey/electric-hockey_fr.jar
echo "signing ./electric-hockey/electric-hockey_pl.jar"
sign-jar ./electric-hockey/electric-hockey_pl.jar
echo "signing ./electric-hockey/electric-hockey_kn.jar"
sign-jar ./electric-hockey/electric-hockey_kn.jar
echo "signing ./electric-hockey/electric-hockey_da.jar"
sign-jar ./electric-hockey/electric-hockey_da.jar
echo "signing ./electric-hockey/electric-hockey_zh_TW.jar"
sign-jar ./electric-hockey/electric-hockey_zh_TW.jar
echo "signing ./electric-hockey/electric-hockey_en.jar"
sign-jar ./electric-hockey/electric-hockey_en.jar
echo "signing ./electric-hockey/electric-hockey_hy.jar"
sign-jar ./electric-hockey/electric-hockey_hy.jar
echo "signing ./electric-hockey/electric-hockey_zh_CN.jar"
sign-jar ./electric-hockey/electric-hockey_zh_CN.jar
echo "signing ./wave-interference/wave-interference_vi.jar"
sign-jar ./wave-interference/wave-interference_vi.jar
echo "signing ./wave-interference/wave-interference_nb.jar"
sign-jar ./wave-interference/wave-interference_nb.jar
echo "signing ./wave-interference/wave-interference_bg.jar"
sign-jar ./wave-interference/wave-interference_bg.jar
echo "signing ./wave-interference/wave-interference_es_PE.jar"
sign-jar ./wave-interference/wave-interference_es_PE.jar
echo "signing ./wave-interference/wave-interference_cs.jar"
sign-jar ./wave-interference/wave-interference_cs.jar
echo "signing ./wave-interference/wave-interference_ka.jar"
sign-jar ./wave-interference/wave-interference_ka.jar
echo "signing ./wave-interference/wave-interference_de.jar"
sign-jar ./wave-interference/wave-interference_de.jar
echo "signing ./wave-interference/wave-interference_be.jar"
sign-jar ./wave-interference/wave-interference_be.jar
echo "signing ./wave-interference/wave-interference_tk.jar"
sign-jar ./wave-interference/wave-interference_tk.jar
echo "signing ./wave-interference/wave-interference_sr.jar"
sign-jar ./wave-interference/wave-interference_sr.jar
echo "signing ./wave-interference/wave-interference_fi.jar"
sign-jar ./wave-interference/wave-interference_fi.jar
echo "signing ./wave-interference/wave-interference_hr.jar"
sign-jar ./wave-interference/wave-interference_hr.jar
echo "signing ./wave-interference/wave-interference_en.jar"
sign-jar ./wave-interference/wave-interference_en.jar
echo "signing ./wave-interference/wave-interference_pt.jar"
sign-jar ./wave-interference/wave-interference_pt.jar
echo "signing ./wave-interference/wave-interference_zh_TW.jar"
sign-jar ./wave-interference/wave-interference_zh_TW.jar
echo "signing ./wave-interference/wave-interference_kn.jar"
sign-jar ./wave-interference/wave-interference_kn.jar
echo "signing ./wave-interference/wave-interference_all_installer.jar"
sign-jar ./wave-interference/wave-interference_all_installer.jar
echo "signing ./wave-interference/wave-interference_in.jar"
sign-jar ./wave-interference/wave-interference_in.jar
echo "signing ./wave-interference/wave-interference_ko.jar"
sign-jar ./wave-interference/wave-interference_ko.jar
echo "signing ./wave-interference/wave-interference_fr.jar"
sign-jar ./wave-interference/wave-interference_fr.jar
echo "signing ./wave-interference/wave-interference_ca.jar"
sign-jar ./wave-interference/wave-interference_ca.jar
echo "signing ./wave-interference/wave-interference_ku_TR.jar"
sign-jar ./wave-interference/wave-interference_ku_TR.jar
echo "signing ./wave-interference/wave-interference_cy.jar"
sign-jar ./wave-interference/wave-interference_cy.jar
echo "signing ./wave-interference/wave-interference_sk.jar"
sign-jar ./wave-interference/wave-interference_sk.jar
echo "signing ./wave-interference/wave-interference_it.jar"
sign-jar ./wave-interference/wave-interference_it.jar
echo "signing ./wave-interference/wave-interference_el.jar"
sign-jar ./wave-interference/wave-interference_el.jar
echo "signing ./wave-interference/wave-interference_ru.jar"
sign-jar ./wave-interference/wave-interference_ru.jar
echo "signing ./wave-interference/wave-interference_mk.jar"
sign-jar ./wave-interference/wave-interference_mk.jar
echo "signing ./wave-interference/wave-interference_tr.jar"
sign-jar ./wave-interference/wave-interference_tr.jar
echo "signing ./wave-interference/wave-interference_ar.jar"
sign-jar ./wave-interference/wave-interference_ar.jar
echo "signing ./wave-interference/wave-interference_fa.jar"
sign-jar ./wave-interference/wave-interference_fa.jar
echo "signing ./wave-interference/wave-interference_iw.jar"
sign-jar ./wave-interference/wave-interference_iw.jar
echo "signing ./wave-interference/wave-interference_sq.jar"
sign-jar ./wave-interference/wave-interference_sq.jar
echo "signing ./wave-interference/wave-interference_mr.jar"
sign-jar ./wave-interference/wave-interference_mr.jar
echo "signing ./wave-interference/wave-interference_sv.jar"
sign-jar ./wave-interference/wave-interference_sv.jar
echo "signing ./wave-interference/wave-interference_pl.jar"
sign-jar ./wave-interference/wave-interference_pl.jar
echo "signing ./wave-interference/wave-interference_nl.jar"
sign-jar ./wave-interference/wave-interference_nl.jar
echo "signing ./wave-interference/wave-interference_bs.jar"
sign-jar ./wave-interference/wave-interference_bs.jar
echo "signing ./wave-interference/wave-interference_da.jar"
sign-jar ./wave-interference/wave-interference_da.jar
echo "signing ./wave-interference/wave-interference_ja.jar"
sign-jar ./wave-interference/wave-interference_ja.jar
echo "signing ./wave-interference/wave-interference_all.jar"
sign-jar ./wave-interference/wave-interference_all.jar
echo "signing ./wave-interference/wave-interference_es.jar"
sign-jar ./wave-interference/wave-interference_es.jar
echo "signing ./wave-interference/wave-interference_mn.jar"
sign-jar ./wave-interference/wave-interference_mn.jar
echo "signing ./wave-interference/wave-interference_uk.jar"
sign-jar ./wave-interference/wave-interference_uk.jar
echo "signing ./wave-interference/wave-interference_hu.jar"
sign-jar ./wave-interference/wave-interference_hu.jar
echo "signing ./wave-interference/wave-interference_eu.jar"
sign-jar ./wave-interference/wave-interference_eu.jar
echo "signing ./wave-interference/wave-interference_pt_BR.jar"
sign-jar ./wave-interference/wave-interference_pt_BR.jar
echo "signing ./wave-interference/wave-interference_gl.jar"
sign-jar ./wave-interference/wave-interference_gl.jar
echo "signing ./wave-interference/wave-interference_zh_CN.jar"
sign-jar ./wave-interference/wave-interference_zh_CN.jar
echo "signing ./wave-interference/wave-interference_km.jar"
sign-jar ./wave-interference/wave-interference_km.jar
echo "signing ./wave-interference/wave-interference_th.jar"
sign-jar ./wave-interference/wave-interference_th.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_en.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_en.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_th.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_th.jar
echo "signing ./energy-skate-park/energy-skate-park_sk.jar"
sign-jar ./energy-skate-park/energy-skate-park_sk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_pl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_pl.jar
echo "signing ./energy-skate-park/energy-skate-park_zh_CN.jar"
sign-jar ./energy-skate-park/energy-skate-park_zh_CN.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ru.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ru.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_gl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_gl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_sr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_sr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_th.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_th.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_et.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_et.jar
echo "signing ./energy-skate-park/energy-skate-park_hr.jar"
sign-jar ./energy-skate-park/energy-skate-park_hr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_de.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_de.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_bs.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_bs.jar
echo "signing ./energy-skate-park/energy-skate-park_sv.jar"
sign-jar ./energy-skate-park/energy-skate-park_sv.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ht.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ht.jar
echo "signing ./energy-skate-park/energy-skate-park_hu.jar"
sign-jar ./energy-skate-park/energy-skate-park_hu.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_kn.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_kn.jar
echo "signing ./energy-skate-park/energy-skate-park_pl.jar"
sign-jar ./energy-skate-park/energy-skate-park_pl.jar
echo "signing ./energy-skate-park/energy-skate-park_ku_TR.jar"
sign-jar ./energy-skate-park/energy-skate-park_ku_TR.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_kk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_kk.jar
echo "signing ./energy-skate-park/energy-skate-park_et.jar"
sign-jar ./energy-skate-park/energy-skate-park_et.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_sv.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_sv.jar
echo "signing ./energy-skate-park/energy-skate-park_zh_TW.jar"
sign-jar ./energy-skate-park/energy-skate-park_zh_TW.jar
echo "signing ./energy-skate-park/energy-skate-park_es_PE.jar"
sign-jar ./energy-skate-park/energy-skate-park_es_PE.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_pt_BR.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_pt_BR.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_pt.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_pt.jar
echo "signing ./energy-skate-park/energy-skate-park_kn.jar"
sign-jar ./energy-skate-park/energy-skate-park_kn.jar
echo "signing ./energy-skate-park/energy-skate-park_kk.jar"
sign-jar ./energy-skate-park/energy-skate-park_kk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_gl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_gl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_nb.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_nb.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_sl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_sl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_de.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_de.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ar_SA.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ar_SA.jar
echo "signing ./energy-skate-park/energy-skate-park_es_CO.jar"
sign-jar ./energy-skate-park/energy-skate-park_es_CO.jar
echo "signing ./energy-skate-park/energy-skate-park_tr.jar"
sign-jar ./energy-skate-park/energy-skate-park_tr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_fa.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_fa.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_en.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_en.jar
echo "signing ./energy-skate-park/energy-skate-park_mk.jar"
sign-jar ./energy-skate-park/energy-skate-park_mk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ko.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ko.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_hr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_hr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_mr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_mr.jar
echo "signing ./energy-skate-park/energy-skate-park_es.jar"
sign-jar ./energy-skate-park/energy-skate-park_es.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_nb.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_nb.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_hu.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_hu.jar
echo "signing ./energy-skate-park/energy-skate-park_in.jar"
sign-jar ./energy-skate-park/energy-skate-park_in.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_mr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_mr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_tr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_tr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_it.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_it.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_cs.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_cs.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_iw.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_iw.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ar_SA.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ar_SA.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_zh_TW.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_zh_TW.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_es_PE.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_es_PE.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_tk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_tk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_pl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_pl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_da.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_da.jar
echo "signing ./energy-skate-park/energy-skate-park_sr.jar"
sign-jar ./energy-skate-park/energy-skate-park_sr.jar
echo "signing ./energy-skate-park/energy-skate-park_de.jar"
sign-jar ./energy-skate-park/energy-skate-park_de.jar
echo "signing ./energy-skate-park/energy-skate-park_en.jar"
sign-jar ./energy-skate-park/energy-skate-park_en.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ku_TR.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ku_TR.jar
echo "signing ./energy-skate-park/energy-skate-park_all_installer.jar"
sign-jar ./energy-skate-park/energy-skate-park_all_installer.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_fr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_fr.jar
echo "signing ./energy-skate-park/energy-skate-park_gl.jar"
sign-jar ./energy-skate-park/energy-skate-park_gl.jar
echo "signing ./energy-skate-park/energy-skate-park_ja.jar"
sign-jar ./energy-skate-park/energy-skate-park_ja.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_vi.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_vi.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_zh_CN.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_zh_CN.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_kn.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_kn.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_hu.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_hu.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ga.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ga.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_mn.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_mn.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ru.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ru.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_mk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_mk.jar
echo "signing ./energy-skate-park/energy-skate-park_el.jar"
sign-jar ./energy-skate-park/energy-skate-park_el.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_mk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_mk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_mn.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_mn.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_sk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_sk.jar
echo "signing ./energy-skate-park/energy-skate-park_tk.jar"
sign-jar ./energy-skate-park/energy-skate-park_tk.jar
echo "signing ./energy-skate-park/energy-skate-park_km.jar"
sign-jar ./energy-skate-park/energy-skate-park_km.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ar.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ar.jar
echo "signing ./energy-skate-park/energy-skate-park_it.jar"
sign-jar ./energy-skate-park/energy-skate-park_it.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_lt.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_lt.jar
echo "signing ./energy-skate-park/energy-skate-park_mr.jar"
sign-jar ./energy-skate-park/energy-skate-park_mr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_it.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_it.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_tk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_tk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_es_CO.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_es_CO.jar
echo "signing ./energy-skate-park/energy-skate-park_be.jar"
sign-jar ./energy-skate-park/energy-skate-park_be.jar
echo "signing ./energy-skate-park/energy-skate-park_nl.jar"
sign-jar ./energy-skate-park/energy-skate-park_nl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_zh_CN.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_zh_CN.jar
echo "signing ./energy-skate-park/energy-skate-park_vi.jar"
sign-jar ./energy-skate-park/energy-skate-park_vi.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_pt.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_pt.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_lt.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_lt.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_eu.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_eu.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_iw.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_iw.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_km.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_km.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_hr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_hr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_pt_BR.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_pt_BR.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_fa.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_fa.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ku_TR.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ku_TR.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_be.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_be.jar
echo "signing ./energy-skate-park/energy-skate-park_fa.jar"
sign-jar ./energy-skate-park/energy-skate-park_fa.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_be.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_be.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_in.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_in.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_es_PE.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_es_PE.jar
echo "signing ./energy-skate-park/energy-skate-park_ar.jar"
sign-jar ./energy-skate-park/energy-skate-park_ar.jar
echo "signing ./energy-skate-park/energy-skate-park_cs.jar"
sign-jar ./energy-skate-park/energy-skate-park_cs.jar
echo "signing ./energy-skate-park/energy-skate-park_iw.jar"
sign-jar ./energy-skate-park/energy-skate-park_iw.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ga.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ga.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_sl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_sl.jar
echo "signing ./energy-skate-park/energy-skate-park_ru.jar"
sign-jar ./energy-skate-park/energy-skate-park_ru.jar
echo "signing ./energy-skate-park/energy-skate-park_eu.jar"
sign-jar ./energy-skate-park/energy-skate-park_eu.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_nl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_nl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_es_CO.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_es_CO.jar
echo "signing ./energy-skate-park/energy-skate-park_mn.jar"
sign-jar ./energy-skate-park/energy-skate-park_mn.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_vi.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_vi.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ja.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ja.jar
echo "signing ./energy-skate-park/energy-skate-park_ht.jar"
sign-jar ./energy-skate-park/energy-skate-park_ht.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_tr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_tr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_el.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_el.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_uk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_uk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ar.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ar.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ko.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ko.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_bs.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_bs.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_es.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_es.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_sv.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_sv.jar
echo "signing ./energy-skate-park/energy-skate-park_all.jar"
sign-jar ./energy-skate-park/energy-skate-park_all.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_et.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_et.jar
echo "signing ./energy-skate-park/energy-skate-park_lt.jar"
sign-jar ./energy-skate-park/energy-skate-park_lt.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_kk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_kk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_cs.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_cs.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_in.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_in.jar
echo "signing ./energy-skate-park/energy-skate-park_fr.jar"
sign-jar ./energy-skate-park/energy-skate-park_fr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_uk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_uk.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_fr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_fr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_nl.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_nl.jar
echo "signing ./energy-skate-park/energy-skate-park_pt_BR.jar"
sign-jar ./energy-skate-park/energy-skate-park_pt_BR.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_ja.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_ja.jar
echo "signing ./energy-skate-park/energy-skate-park_nb.jar"
sign-jar ./energy-skate-park/energy-skate-park_nb.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_da.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_da.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_sr.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_sr.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_es.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_es.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_km.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_km.jar
echo "signing ./energy-skate-park/energy-skate-park_ar_SA.jar"
sign-jar ./energy-skate-park/energy-skate-park_ar_SA.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_eu.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_eu.jar
echo "signing ./energy-skate-park/energy-skate-park_pt.jar"
sign-jar ./energy-skate-park/energy-skate-park_pt.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_el.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_el.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_ht.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_ht.jar
echo "signing ./energy-skate-park/energy-skate-park-basics-study-interviews_sk.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics-study-interviews_sk.jar
echo "signing ./energy-skate-park/energy-skate-park_sl.jar"
sign-jar ./energy-skate-park/energy-skate-park_sl.jar
echo "signing ./energy-skate-park/energy-skate-park-basics_zh_TW.jar"
sign-jar ./energy-skate-park/energy-skate-park-basics_zh_TW.jar
echo "signing ./energy-skate-park/energy-skate-park_th.jar"
sign-jar ./energy-skate-park/energy-skate-park_th.jar
echo "signing ./energy-skate-park/energy-skate-park_ga.jar"
sign-jar ./energy-skate-park/energy-skate-park_ga.jar
echo "signing ./energy-skate-park/energy-skate-park_bs.jar"
sign-jar ./energy-skate-park/energy-skate-park_bs.jar
echo "signing ./energy-skate-park/energy-skate-park_ko.jar"
sign-jar ./energy-skate-park/energy-skate-park_ko.jar
echo "signing ./energy-skate-park/energy-skate-park_da.jar"
sign-jar ./energy-skate-park/energy-skate-park_da.jar
echo "signing ./energy-skate-park/energy-skate-park_uk.jar"
sign-jar ./energy-skate-park/energy-skate-park_uk.jar
echo "signing ./membrane-channels/membrane-channels_kk.jar"
sign-jar ./membrane-channels/membrane-channels_kk.jar
echo "signing ./membrane-channels/membrane-channels_ru.jar"
sign-jar ./membrane-channels/membrane-channels_ru.jar
echo "signing ./membrane-channels/membrane-channels_tr.jar"
sign-jar ./membrane-channels/membrane-channels_tr.jar
echo "signing ./membrane-channels/membrane-channels_it.jar"
sign-jar ./membrane-channels/membrane-channels_it.jar
echo "signing ./membrane-channels/membrane-channels_hu.jar"
sign-jar ./membrane-channels/membrane-channels_hu.jar
echo "signing ./membrane-channels/membrane-channels_all_installer.jar"
sign-jar ./membrane-channels/membrane-channels_all_installer.jar
echo "signing ./membrane-channels/membrane-channels_iw.jar"
sign-jar ./membrane-channels/membrane-channels_iw.jar
echo "signing ./membrane-channels/membrane-channels_el.jar"
sign-jar ./membrane-channels/membrane-channels_el.jar
echo "signing ./membrane-channels/membrane-channels_uk.jar"
sign-jar ./membrane-channels/membrane-channels_uk.jar
echo "signing ./membrane-channels/membrane-channels_de.jar"
sign-jar ./membrane-channels/membrane-channels_de.jar
echo "signing ./membrane-channels/membrane-channels_es.jar"
sign-jar ./membrane-channels/membrane-channels_es.jar
echo "signing ./membrane-channels/membrane-channels_cs.jar"
sign-jar ./membrane-channels/membrane-channels_cs.jar
echo "signing ./membrane-channels/membrane-channels_et.jar"
sign-jar ./membrane-channels/membrane-channels_et.jar
echo "signing ./membrane-channels/membrane-channels_all.jar"
sign-jar ./membrane-channels/membrane-channels_all.jar
echo "signing ./membrane-channels/membrane-channels_fr.jar"
sign-jar ./membrane-channels/membrane-channels_fr.jar
echo "signing ./membrane-channels/membrane-channels_mk.jar"
sign-jar ./membrane-channels/membrane-channels_mk.jar
echo "signing ./membrane-channels/membrane-channels_fa.jar"
sign-jar ./membrane-channels/membrane-channels_fa.jar
echo "signing ./membrane-channels/membrane-channels_ar_SA.jar"
sign-jar ./membrane-channels/membrane-channels_ar_SA.jar
echo "signing ./membrane-channels/membrane-channels_sr.jar"
sign-jar ./membrane-channels/membrane-channels_sr.jar
echo "signing ./membrane-channels/membrane-channels_eu.jar"
sign-jar ./membrane-channels/membrane-channels_eu.jar
echo "signing ./membrane-channels/membrane-channels_zh_CN.jar"
sign-jar ./membrane-channels/membrane-channels_zh_CN.jar
echo "signing ./membrane-channels/membrane-channels_th.jar"
sign-jar ./membrane-channels/membrane-channels_th.jar
echo "signing ./membrane-channels/membrane-channels_ja.jar"
sign-jar ./membrane-channels/membrane-channels_ja.jar
echo "signing ./membrane-channels/membrane-channels_sk.jar"
sign-jar ./membrane-channels/membrane-channels_sk.jar
echo "signing ./membrane-channels/membrane-channels_zh_TW.jar"
sign-jar ./membrane-channels/membrane-channels_zh_TW.jar
echo "signing ./membrane-channels/membrane-channels_ka.jar"
sign-jar ./membrane-channels/membrane-channels_ka.jar
echo "signing ./membrane-channels/membrane-channels_pl.jar"
sign-jar ./membrane-channels/membrane-channels_pl.jar
echo "signing ./membrane-channels/membrane-channels_es_PE.jar"
sign-jar ./membrane-channels/membrane-channels_es_PE.jar
echo "signing ./membrane-channels/membrane-channels_da.jar"
sign-jar ./membrane-channels/membrane-channels_da.jar
echo "signing ./membrane-channels/membrane-channels_en.jar"
sign-jar ./membrane-channels/membrane-channels_en.jar
echo "signing ./membrane-channels/membrane-channels_bs.jar"
sign-jar ./membrane-channels/membrane-channels_bs.jar
echo "signing ./membrane-channels/membrane-channels_sv.jar"
sign-jar ./membrane-channels/membrane-channels_sv.jar
echo "signing ./membrane-channels/membrane-channels_ko.jar"
sign-jar ./membrane-channels/membrane-channels_ko.jar
echo "signing ./membrane-channels/membrane-channels_vi.jar"
sign-jar ./membrane-channels/membrane-channels_vi.jar
echo "signing ./membrane-channels/membrane-channels_tk.jar"
sign-jar ./membrane-channels/membrane-channels_tk.jar
echo "signing ./membrane-channels/membrane-channels_pt_BR.jar"
sign-jar ./membrane-channels/membrane-channels_pt_BR.jar
echo "signing ./balloons/balloons_nb.jar"
sign-jar ./balloons/balloons_nb.jar
echo "signing ./balloons/balloons_it.jar"
sign-jar ./balloons/balloons_it.jar
echo "signing ./balloons/balloons_fr.jar"
sign-jar ./balloons/balloons_fr.jar
echo "signing ./balloons/balloons_all.jar"
sign-jar ./balloons/balloons_all.jar
echo "signing ./balloons/balloons_eu.jar"
sign-jar ./balloons/balloons_eu.jar
echo "signing ./balloons/balloons_hy.jar"
sign-jar ./balloons/balloons_hy.jar
echo "signing ./balloons/balloons_km.jar"
sign-jar ./balloons/balloons_km.jar
echo "signing ./balloons/balloons_iw.jar"
sign-jar ./balloons/balloons_iw.jar
echo "signing ./balloons/balloons_cs.jar"
sign-jar ./balloons/balloons_cs.jar
echo "signing ./balloons/balloons_mk.jar"
sign-jar ./balloons/balloons_mk.jar
echo "signing ./balloons/balloons_be.jar"
sign-jar ./balloons/balloons_be.jar
echo "signing ./balloons/balloons_ga.jar"
sign-jar ./balloons/balloons_ga.jar
echo "signing ./balloons/balloons_pl.jar"
sign-jar ./balloons/balloons_pl.jar
echo "signing ./balloons/balloons_sv.jar"
sign-jar ./balloons/balloons_sv.jar
echo "signing ./balloons/balloons_es_PE.jar"
sign-jar ./balloons/balloons_es_PE.jar
echo "signing ./balloons/balloons_et.jar"
sign-jar ./balloons/balloons_et.jar
echo "signing ./balloons/balloons_af.jar"
sign-jar ./balloons/balloons_af.jar
echo "signing ./balloons/balloons_fa.jar"
sign-jar ./balloons/balloons_fa.jar
echo "signing ./balloons/balloons_mn.jar"
sign-jar ./balloons/balloons_mn.jar
echo "signing ./balloons/balloons_th.jar"
sign-jar ./balloons/balloons_th.jar
echo "signing ./balloons/balloons_pt_BR.jar"
sign-jar ./balloons/balloons_pt_BR.jar
echo "signing ./balloons/balloons_nl.jar"
sign-jar ./balloons/balloons_nl.jar
echo "signing ./balloons/balloons_ro.jar"
sign-jar ./balloons/balloons_ro.jar
echo "signing ./balloons/balloons_fi.jar"
sign-jar ./balloons/balloons_fi.jar
echo "signing ./balloons/balloons_tk.jar"
sign-jar ./balloons/balloons_tk.jar
echo "signing ./balloons/balloons_de.jar"
sign-jar ./balloons/balloons_de.jar
echo "signing ./balloons/balloons_ku_TR.jar"
sign-jar ./balloons/balloons_ku_TR.jar
echo "signing ./balloons/balloons_ja.jar"
sign-jar ./balloons/balloons_ja.jar
echo "signing ./balloons/balloons_zh_TW.jar"
sign-jar ./balloons/balloons_zh_TW.jar
echo "signing ./balloons/balloons_hu.jar"
sign-jar ./balloons/balloons_hu.jar
echo "signing ./balloons/balloons_ar_SA.jar"
sign-jar ./balloons/balloons_ar_SA.jar
echo "signing ./balloons/balloons_da.jar"
sign-jar ./balloons/balloons_da.jar
echo "signing ./balloons/balloons_es.jar"
sign-jar ./balloons/balloons_es.jar
echo "signing ./balloons/balloons_ko.jar"
sign-jar ./balloons/balloons_ko.jar
echo "signing ./balloons/balloons_sq.jar"
sign-jar ./balloons/balloons_sq.jar
echo "signing ./balloons/balloons_bs.jar"
sign-jar ./balloons/balloons_bs.jar
echo "signing ./balloons/balloons_all_installer.jar"
sign-jar ./balloons/balloons_all_installer.jar
echo "signing ./balloons/balloons_sl.jar"
sign-jar ./balloons/balloons_sl.jar
echo "signing ./balloons/balloons_tr.jar"
sign-jar ./balloons/balloons_tr.jar
echo "signing ./balloons/balloons_pt.jar"
sign-jar ./balloons/balloons_pt.jar
echo "signing ./balloons/balloons_el.jar"
sign-jar ./balloons/balloons_el.jar
echo "signing ./balloons/balloons_bg.jar"
sign-jar ./balloons/balloons_bg.jar
echo "signing ./balloons/balloons_hr.jar"
sign-jar ./balloons/balloons_hr.jar
echo "signing ./balloons/balloons_gl.jar"
sign-jar ./balloons/balloons_gl.jar
echo "signing ./balloons/balloons_kk.jar"
sign-jar ./balloons/balloons_kk.jar
echo "signing ./balloons/balloons_uk.jar"
sign-jar ./balloons/balloons_uk.jar
echo "signing ./balloons/balloons_zh_CN.jar"
sign-jar ./balloons/balloons_zh_CN.jar
echo "signing ./balloons/balloons_lo.jar"
sign-jar ./balloons/balloons_lo.jar
echo "signing ./balloons/balloons_ar.jar"
sign-jar ./balloons/balloons_ar.jar
echo "signing ./balloons/balloons_sk.jar"
sign-jar ./balloons/balloons_sk.jar
echo "signing ./balloons/balloons_sr.jar"
sign-jar ./balloons/balloons_sr.jar
echo "signing ./balloons/balloons_kn.jar"
sign-jar ./balloons/balloons_kn.jar
echo "signing ./balloons/balloons_vi.jar"
sign-jar ./balloons/balloons_vi.jar
echo "signing ./balloons/balloons_ru.jar"
sign-jar ./balloons/balloons_ru.jar
echo "signing ./balloons/balloons_in.jar"
sign-jar ./balloons/balloons_in.jar
echo "signing ./balloons/balloons_ka.jar"
sign-jar ./balloons/balloons_ka.jar
echo "signing ./balloons/balloons_lv.jar"
sign-jar ./balloons/balloons_lv.jar
echo "signing ./balloons/balloons_en.jar"
sign-jar ./balloons/balloons_en.jar
echo "signing ./vector-addition/vector-addition_pt_BR.jar"
sign-jar ./vector-addition/vector-addition_pt_BR.jar
echo "signing ./vector-addition/vector-addition_zh_CN.jar"
sign-jar ./vector-addition/vector-addition_zh_CN.jar
echo "signing ./vector-addition/vector-addition_da.jar"
sign-jar ./vector-addition/vector-addition_da.jar
echo "signing ./vector-addition/vector-addition_mk.jar"
sign-jar ./vector-addition/vector-addition_mk.jar
echo "signing ./vector-addition/vector-addition_ku_TR.jar"
sign-jar ./vector-addition/vector-addition_ku_TR.jar
echo "signing ./vector-addition/vector-addition_in.jar"
sign-jar ./vector-addition/vector-addition_in.jar
echo "signing ./vector-addition/vector-addition_th.jar"
sign-jar ./vector-addition/vector-addition_th.jar
echo "signing ./vector-addition/vector-addition_sv.jar"
sign-jar ./vector-addition/vector-addition_sv.jar
echo "signing ./vector-addition/vector-addition_lv.jar"
sign-jar ./vector-addition/vector-addition_lv.jar
echo "signing ./vector-addition/vector-addition_vi.jar"
sign-jar ./vector-addition/vector-addition_vi.jar
echo "signing ./vector-addition/vector-addition_tk.jar"
sign-jar ./vector-addition/vector-addition_tk.jar
echo "signing ./vector-addition/vector-addition_is.jar"
sign-jar ./vector-addition/vector-addition_is.jar
echo "signing ./vector-addition/vector-addition_et.jar"
sign-jar ./vector-addition/vector-addition_et.jar
echo "signing ./vector-addition/vector-addition_ja.jar"
sign-jar ./vector-addition/vector-addition_ja.jar
echo "signing ./vector-addition/vector-addition_fa.jar"
sign-jar ./vector-addition/vector-addition_fa.jar
echo "signing ./vector-addition/vector-addition_zh_TW.jar"
sign-jar ./vector-addition/vector-addition_zh_TW.jar
echo "signing ./vector-addition/vector-addition_fr.jar"
sign-jar ./vector-addition/vector-addition_fr.jar
echo "signing ./vector-addition/vector-addition_bs.jar"
sign-jar ./vector-addition/vector-addition_bs.jar
echo "signing ./vector-addition/vector-addition_bn.jar"
sign-jar ./vector-addition/vector-addition_bn.jar
echo "signing ./vector-addition/vector-addition_mr.jar"
sign-jar ./vector-addition/vector-addition_mr.jar
echo "signing ./vector-addition/vector-addition_pl.jar"
sign-jar ./vector-addition/vector-addition_pl.jar
echo "signing ./vector-addition/vector-addition_ar.jar"
sign-jar ./vector-addition/vector-addition_ar.jar
echo "signing ./vector-addition/vector-addition_sk.jar"
sign-jar ./vector-addition/vector-addition_sk.jar
echo "signing ./vector-addition/vector-addition_iw.jar"
sign-jar ./vector-addition/vector-addition_iw.jar
echo "signing ./vector-addition/vector-addition_ms.jar"
sign-jar ./vector-addition/vector-addition_ms.jar
echo "signing ./vector-addition/vector-addition_ru.jar"
sign-jar ./vector-addition/vector-addition_ru.jar
echo "signing ./vector-addition/vector-addition_es.jar"
sign-jar ./vector-addition/vector-addition_es.jar
echo "signing ./vector-addition/vector-addition_it.jar"
sign-jar ./vector-addition/vector-addition_it.jar
echo "signing ./vector-addition/vector-addition_ko.jar"
sign-jar ./vector-addition/vector-addition_ko.jar
echo "signing ./vector-addition/vector-addition_en.jar"
sign-jar ./vector-addition/vector-addition_en.jar
echo "signing ./vector-addition/vector-addition_nn.jar"
sign-jar ./vector-addition/vector-addition_nn.jar
echo "signing ./vector-addition/vector-addition_cs.jar"
sign-jar ./vector-addition/vector-addition_cs.jar
echo "signing ./vector-addition/vector-addition_ar_SA.jar"
sign-jar ./vector-addition/vector-addition_ar_SA.jar
echo "signing ./vector-addition/vector-addition_gl.jar"
sign-jar ./vector-addition/vector-addition_gl.jar
echo "signing ./vector-addition/vector-addition_hu.jar"
sign-jar ./vector-addition/vector-addition_hu.jar
echo "signing ./vector-addition/vector-addition_sr.jar"
sign-jar ./vector-addition/vector-addition_sr.jar
echo "signing ./vector-addition/vector-addition_eu.jar"
sign-jar ./vector-addition/vector-addition_eu.jar
echo "signing ./vector-addition/vector-addition_es_PE.jar"
sign-jar ./vector-addition/vector-addition_es_PE.jar
echo "signing ./vector-addition/vector-addition_nb.jar"
sign-jar ./vector-addition/vector-addition_nb.jar
echo "signing ./vector-addition/vector-addition_de.jar"
sign-jar ./vector-addition/vector-addition_de.jar
echo "signing ./vector-addition/vector-addition_el.jar"
sign-jar ./vector-addition/vector-addition_el.jar
echo "signing ./vector-addition/vector-addition_tr.jar"
sign-jar ./vector-addition/vector-addition_tr.jar
echo "signing ./vector-addition/vector-addition_ta.jar"
sign-jar ./vector-addition/vector-addition_ta.jar
echo "signing ./vector-addition/vector-addition_ca.jar"
sign-jar ./vector-addition/vector-addition_ca.jar
echo "signing ./vector-addition/vector-addition_kk.jar"
sign-jar ./vector-addition/vector-addition_kk.jar
echo "signing ./vector-addition/vector-addition_nl.jar"
sign-jar ./vector-addition/vector-addition_nl.jar
echo "signing ./vector-addition/vector-addition_hr.jar"
sign-jar ./vector-addition/vector-addition_hr.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_vi.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_vi.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_nb.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_nb.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_th.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_th.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_fr.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_fr.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_it.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_it.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_ka.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_ka.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_es.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_es.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_cs.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_cs.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_de.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_de.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_all.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_all.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_gl.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_gl.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_tk.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_tk.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_ko.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_ko.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_bs.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_bs.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_tr.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_tr.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_el.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_el.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_eu.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_eu.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_nl.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_nl.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_kk.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_kk.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_hu.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_hu.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_pl.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_pl.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_pt.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_pt.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_sv.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_sv.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_sr.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_sr.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_ja.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_ja.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_en.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_en.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_all_installer.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_all_installer.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_fa.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_fa.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_km.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_km.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_sl.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_sl.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_mk.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_mk.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_da.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_da.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_sk.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_sk.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_zh_CN.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_zh_CN.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_es_PE.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_es_PE.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_zh_TW.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_zh_TW.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_pt_BR.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_pt_BR.jar
echo "signing ./reactants-products-and-leftovers/reactants-products-and-leftovers_iw.jar"
sign-jar ./reactants-products-and-leftovers/reactants-products-and-leftovers_iw.jar
echo "signing ./reactions-and-rates/reactions-and-rates_et.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_et.jar
echo "signing ./reactions-and-rates/reactions-and-rates_bs.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_bs.jar
echo "signing ./reactions-and-rates/reactions-and-rates_zh_TW.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_zh_TW.jar
echo "signing ./reactions-and-rates/reactions-and-rates_es.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_es.jar
echo "signing ./reactions-and-rates/reactions-and-rates_tk.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_tk.jar
echo "signing ./reactions-and-rates/reactions-and-rates_vi.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_vi.jar
echo "signing ./reactions-and-rates/reactions-and-rates_mk.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_mk.jar
echo "signing ./reactions-and-rates/reactions-and-rates_cs.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_cs.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ba.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ba.jar
echo "signing ./reactions-and-rates/reactions-and-rates_all.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_all.jar
echo "signing ./reactions-and-rates/reactions-and-rates_da.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_da.jar
echo "signing ./reactions-and-rates/reactions-and-rates_kn.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_kn.jar
echo "signing ./reactions-and-rates/reactions-and-rates_de.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_de.jar
echo "signing ./reactions-and-rates/reactions-and-rates_en.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_en.jar
echo "signing ./reactions-and-rates/reactions-and-rates_th.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_th.jar
echo "signing ./reactions-and-rates/reactions-and-rates_el.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_el.jar
echo "signing ./reactions-and-rates/reactions-and-rates_pt_BR.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_pt_BR.jar
echo "signing ./reactions-and-rates/reactions-and-rates_pt.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_pt.jar
echo "signing ./reactions-and-rates/reactions-and-rates_es_PE.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_es_PE.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ru.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ru.jar
echo "signing ./reactions-and-rates/reactions-and-rates_gl.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_gl.jar
echo "signing ./reactions-and-rates/reactions-and-rates_zh_CN.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_zh_CN.jar
echo "signing ./reactions-and-rates/reactions-and-rates_fr.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_fr.jar
echo "signing ./reactions-and-rates/reactions-and-rates_sl.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_sl.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ko.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ko.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ar.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ar.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ka.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ka.jar
echo "signing ./reactions-and-rates/reactions-and-rates_nl.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_nl.jar
echo "signing ./reactions-and-rates/reactions-and-rates_fa.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_fa.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ja.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ja.jar
echo "signing ./reactions-and-rates/reactions-and-rates_it.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_it.jar
echo "signing ./reactions-and-rates/reactions-and-rates_eu.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_eu.jar
echo "signing ./reactions-and-rates/reactions-and-rates_sv.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_sv.jar
echo "signing ./reactions-and-rates/reactions-and-rates_kk.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_kk.jar
echo "signing ./reactions-and-rates/reactions-and-rates_pl.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_pl.jar
echo "signing ./reactions-and-rates/reactions-and-rates_km.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_km.jar
echo "signing ./reactions-and-rates/reactions-and-rates_ar_SA.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_ar_SA.jar
echo "signing ./reactions-and-rates/reactions-and-rates_sr.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_sr.jar
echo "signing ./reactions-and-rates/reactions-and-rates_all_installer.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_all_installer.jar
echo "signing ./reactions-and-rates/reactions-and-rates_sk.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_sk.jar
echo "signing ./reactions-and-rates/reactions-and-rates_in.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_in.jar
echo "signing ./reactions-and-rates/reactions-and-rates_hu.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_hu.jar
echo "signing ./reactions-and-rates/reactions-and-rates_iw.jar"
sign-jar ./reactions-and-rates/reactions-and-rates_iw.jar
echo "signing ./fractions/build-a-fraction_pt_BR.jar"
sign-jar ./fractions/build-a-fraction_pt_BR.jar
echo "signing ./fractions/build-a-fraction_eu.jar"
sign-jar ./fractions/build-a-fraction_eu.jar
echo "signing ./fractions/fraction-matcher_ja.jar"
sign-jar ./fractions/fraction-matcher_ja.jar
echo "signing ./fractions/fractions-intro_eu.jar"
sign-jar ./fractions/fractions-intro_eu.jar
echo "signing ./fractions/fraction-matcher_sr.jar"
sign-jar ./fractions/fraction-matcher_sr.jar
echo "signing ./fractions/fractions-intro_cs.jar"
sign-jar ./fractions/fractions-intro_cs.jar
echo "signing ./fractions/build-a-fraction_hu.jar"
sign-jar ./fractions/build-a-fraction_hu.jar
echo "signing ./fractions/fraction-matcher_es.jar"
sign-jar ./fractions/fraction-matcher_es.jar
echo "signing ./fractions/build-a-fraction_hr.jar"
sign-jar ./fractions/build-a-fraction_hr.jar
echo "signing ./fractions/fractions-intro_it.jar"
sign-jar ./fractions/fractions-intro_it.jar
echo "signing ./fractions/build-a-fraction_sl.jar"
sign-jar ./fractions/build-a-fraction_sl.jar
echo "signing ./fractions/build-a-fraction_ca.jar"
sign-jar ./fractions/build-a-fraction_ca.jar
echo "signing ./fractions/fractions-intro_sr.jar"
sign-jar ./fractions/fractions-intro_sr.jar
echo "signing ./fractions/build-a-fraction_pt.jar"
sign-jar ./fractions/build-a-fraction_pt.jar
echo "signing ./fractions/build-a-fraction_in.jar"
sign-jar ./fractions/build-a-fraction_in.jar
echo "signing ./fractions/fraction-matcher_kk.jar"
sign-jar ./fractions/fraction-matcher_kk.jar
echo "signing ./fractions/fractions-intro_fr.jar"
sign-jar ./fractions/fractions-intro_fr.jar
echo "signing ./fractions/fraction-matcher_bs.jar"
sign-jar ./fractions/fraction-matcher_bs.jar
echo "signing ./fractions/fraction-matcher_mr.jar"
sign-jar ./fractions/fraction-matcher_mr.jar
echo "signing ./fractions/build-a-fraction_da.jar"
sign-jar ./fractions/build-a-fraction_da.jar
echo "signing ./fractions/build-a-fraction_sv.jar"
sign-jar ./fractions/build-a-fraction_sv.jar
echo "signing ./fractions/fraction-matcher_sq.jar"
sign-jar ./fractions/fraction-matcher_sq.jar
echo "signing ./fractions/fractions-intro_tr.jar"
sign-jar ./fractions/fractions-intro_tr.jar
echo "signing ./fractions/build-a-fraction_iw.jar"
sign-jar ./fractions/build-a-fraction_iw.jar
echo "signing ./fractions/build-a-fraction_fa.jar"
sign-jar ./fractions/build-a-fraction_fa.jar
echo "signing ./fractions/fraction-matcher_sv.jar"
sign-jar ./fractions/fraction-matcher_sv.jar
echo "signing ./fractions/fractions-intro_sv.jar"
sign-jar ./fractions/fractions-intro_sv.jar
echo "signing ./fractions/build-a-fraction_mk.jar"
sign-jar ./fractions/build-a-fraction_mk.jar
echo "signing ./fractions/fractions-intro_es_ES.jar"
sign-jar ./fractions/fractions-intro_es_ES.jar
echo "signing ./fractions/fractions-intro_lo.jar"
sign-jar ./fractions/fractions-intro_lo.jar
echo "signing ./fractions/fractions-intro_ar_SA.jar"
sign-jar ./fractions/fractions-intro_ar_SA.jar
echo "signing ./fractions/build-a-fraction_tr.jar"
sign-jar ./fractions/build-a-fraction_tr.jar
echo "signing ./fractions/build-a-fraction_mr.jar"
sign-jar ./fractions/build-a-fraction_mr.jar
echo "signing ./fractions/fractions-intro_ja.jar"
sign-jar ./fractions/fractions-intro_ja.jar
echo "signing ./fractions/fraction-matcher_vi.jar"
sign-jar ./fractions/fraction-matcher_vi.jar
echo "signing ./fractions/fractions-intro_th.jar"
sign-jar ./fractions/fractions-intro_th.jar
echo "signing ./fractions/fractions_all_installer.jar"
sign-jar ./fractions/fractions_all_installer.jar
echo "signing ./fractions/fraction-matcher_es_ES.jar"
sign-jar ./fractions/fraction-matcher_es_ES.jar
echo "signing ./fractions/fraction-matcher_pl.jar"
sign-jar ./fractions/fraction-matcher_pl.jar
echo "signing ./fractions/fraction-matcher_sk.jar"
sign-jar ./fractions/fraction-matcher_sk.jar
echo "signing ./fractions/build-a-fraction_fr.jar"
sign-jar ./fractions/build-a-fraction_fr.jar
echo "signing ./fractions/fractions-intro_in.jar"
sign-jar ./fractions/fractions-intro_in.jar
echo "signing ./fractions/fraction-matcher_hu.jar"
sign-jar ./fractions/fraction-matcher_hu.jar
echo "signing ./fractions/build-a-fraction_es_PE.jar"
sign-jar ./fractions/build-a-fraction_es_PE.jar
echo "signing ./fractions/fractions-intro_ca.jar"
sign-jar ./fractions/fractions-intro_ca.jar
echo "signing ./fractions/fractions-intro_sk.jar"
sign-jar ./fractions/fractions-intro_sk.jar
echo "signing ./fractions/fraction-matcher_iw.jar"
sign-jar ./fractions/fraction-matcher_iw.jar
echo "signing ./fractions/fractions-intro_pt_BR.jar"
sign-jar ./fractions/fractions-intro_pt_BR.jar
echo "signing ./fractions/fractions-intro_hr.jar"
sign-jar ./fractions/fractions-intro_hr.jar
echo "signing ./fractions/fractions-intro_pt.jar"
sign-jar ./fractions/fractions-intro_pt.jar
echo "signing ./fractions/fraction-matcher_sl.jar"
sign-jar ./fractions/fraction-matcher_sl.jar
echo "signing ./fractions/fractions-intro_sq.jar"
sign-jar ./fractions/fractions-intro_sq.jar
echo "signing ./fractions/build-a-fraction_it.jar"
sign-jar ./fractions/build-a-fraction_it.jar
echo "signing ./fractions/fraction-matcher_zh_TW.jar"
sign-jar ./fractions/fraction-matcher_zh_TW.jar
echo "signing ./fractions/fraction-matcher_hr.jar"
sign-jar ./fractions/fraction-matcher_hr.jar
echo "signing ./fractions/fraction-matcher_es_PE.jar"
sign-jar ./fractions/fraction-matcher_es_PE.jar
echo "signing ./fractions/fraction-matcher_mk.jar"
sign-jar ./fractions/fraction-matcher_mk.jar
echo "signing ./fractions/fractions-intro_zh_CN.jar"
sign-jar ./fractions/fractions-intro_zh_CN.jar
echo "signing ./fractions/fractions-intro_ko.jar"
sign-jar ./fractions/fractions-intro_ko.jar
echo "signing ./fractions/fractions-intro_sl.jar"
sign-jar ./fractions/fractions-intro_sl.jar
echo "signing ./fractions/fractions-intro_kk.jar"
sign-jar ./fractions/fractions-intro_kk.jar
echo "signing ./fractions/build-a-fraction_ar_SA.jar"
sign-jar ./fractions/build-a-fraction_ar_SA.jar
echo "signing ./fractions/build-a-fraction_zh_CN.jar"
sign-jar ./fractions/build-a-fraction_zh_CN.jar
echo "signing ./fractions/fractions-intro_de.jar"
sign-jar ./fractions/fractions-intro_de.jar
echo "signing ./fractions/fraction-matcher_lo.jar"
sign-jar ./fractions/fraction-matcher_lo.jar
echo "signing ./fractions/fractions-intro_fa.jar"
sign-jar ./fractions/fractions-intro_fa.jar
echo "signing ./fractions/build-a-fraction_sq.jar"
sign-jar ./fractions/build-a-fraction_sq.jar
echo "signing ./fractions/fraction-matcher_nl.jar"
sign-jar ./fractions/fraction-matcher_nl.jar
echo "signing ./fractions/fraction-matcher_ca.jar"
sign-jar ./fractions/fraction-matcher_ca.jar
echo "signing ./fractions/build-a-fraction_cs.jar"
sign-jar ./fractions/build-a-fraction_cs.jar
echo "signing ./fractions/fractions-intro_da.jar"
sign-jar ./fractions/fractions-intro_da.jar
echo "signing ./fractions/build-a-fraction_sr.jar"
sign-jar ./fractions/build-a-fraction_sr.jar
echo "signing ./fractions/build-a-fraction_bs.jar"
sign-jar ./fractions/build-a-fraction_bs.jar
echo "signing ./fractions/fractions-intro_bs.jar"
sign-jar ./fractions/fractions-intro_bs.jar
echo "signing ./fractions/build-a-fraction_el.jar"
sign-jar ./fractions/build-a-fraction_el.jar
echo "signing ./fractions/fraction-matcher_fa.jar"
sign-jar ./fractions/fraction-matcher_fa.jar
echo "signing ./fractions/build-a-fraction_vi.jar"
sign-jar ./fractions/build-a-fraction_vi.jar
echo "signing ./fractions/fractions-intro_vi.jar"
sign-jar ./fractions/fractions-intro_vi.jar
echo "signing ./fractions/fraction-matcher_en.jar"
sign-jar ./fractions/fraction-matcher_en.jar
echo "signing ./fractions/fraction-matcher_it.jar"
sign-jar ./fractions/fraction-matcher_it.jar
echo "signing ./fractions/fraction-matcher_af.jar"
sign-jar ./fractions/fraction-matcher_af.jar
echo "signing ./fractions/build-a-fraction_pl.jar"
sign-jar ./fractions/build-a-fraction_pl.jar
echo "signing ./fractions/build-a-fraction_es.jar"
sign-jar ./fractions/build-a-fraction_es.jar
echo "signing ./fractions/build-a-fraction_af.jar"
sign-jar ./fractions/build-a-fraction_af.jar
echo "signing ./fractions/fractions-intro_hu.jar"
sign-jar ./fractions/fractions-intro_hu.jar
echo "signing ./fractions/fractions-intro_pl.jar"
sign-jar ./fractions/fractions-intro_pl.jar
echo "signing ./fractions/fractions-intro_mk.jar"
sign-jar ./fractions/fractions-intro_mk.jar
echo "signing ./fractions/fraction-matcher_zh_CN.jar"
sign-jar ./fractions/fraction-matcher_zh_CN.jar
echo "signing ./fractions/fraction-matcher_ar_SA.jar"
sign-jar ./fractions/fraction-matcher_ar_SA.jar
echo "signing ./fractions/fractions-intro_nl.jar"
sign-jar ./fractions/fractions-intro_nl.jar
echo "signing ./fractions/build-a-fraction_sk.jar"
sign-jar ./fractions/build-a-fraction_sk.jar
echo "signing ./fractions/build-a-fraction_nl.jar"
sign-jar ./fractions/build-a-fraction_nl.jar
echo "signing ./fractions/build-a-fraction_es_ES.jar"
sign-jar ./fractions/build-a-fraction_es_ES.jar
echo "signing ./fractions/fraction-matcher_in.jar"
sign-jar ./fractions/fraction-matcher_in.jar
echo "signing ./fractions/fractions-intro_en.jar"
sign-jar ./fractions/fractions-intro_en.jar
echo "signing ./fractions/fractions-intro_sw.jar"
sign-jar ./fractions/fractions-intro_sw.jar
echo "signing ./fractions/build-a-fraction_th.jar"
sign-jar ./fractions/build-a-fraction_th.jar
echo "signing ./fractions/fraction-matcher_th.jar"
sign-jar ./fractions/fraction-matcher_th.jar
echo "signing ./fractions/fractions-intro_mr.jar"
sign-jar ./fractions/fractions-intro_mr.jar
echo "signing ./fractions/build-a-fraction_tk.jar"
sign-jar ./fractions/build-a-fraction_tk.jar
echo "signing ./fractions/build-a-fraction_ja.jar"
sign-jar ./fractions/build-a-fraction_ja.jar
echo "signing ./fractions/fractions-intro_zh_TW.jar"
sign-jar ./fractions/fractions-intro_zh_TW.jar
echo "signing ./fractions/build-a-fraction_ko.jar"
sign-jar ./fractions/build-a-fraction_ko.jar
echo "signing ./fractions/fractions-intro_es_PE.jar"
sign-jar ./fractions/fractions-intro_es_PE.jar
echo "signing ./fractions/fraction-matcher_fr.jar"
sign-jar ./fractions/fraction-matcher_fr.jar
echo "signing ./fractions/fraction-matcher_da.jar"
sign-jar ./fractions/fraction-matcher_da.jar
echo "signing ./fractions/fractions-intro_tk.jar"
sign-jar ./fractions/fractions-intro_tk.jar
echo "signing ./fractions/fractions_all.jar"
sign-jar ./fractions/fractions_all.jar
echo "signing ./fractions/fraction-matcher_sw.jar"
sign-jar ./fractions/fraction-matcher_sw.jar
echo "signing ./fractions/build-a-fraction_en.jar"
sign-jar ./fractions/build-a-fraction_en.jar
echo "signing ./fractions/fraction-matcher_ko.jar"
sign-jar ./fractions/fraction-matcher_ko.jar
echo "signing ./fractions/fraction-matcher_pt_BR.jar"
sign-jar ./fractions/fraction-matcher_pt_BR.jar
echo "signing ./fractions/fraction-matcher_tr.jar"
sign-jar ./fractions/fraction-matcher_tr.jar
echo "signing ./fractions/fraction-matcher_el.jar"
sign-jar ./fractions/fraction-matcher_el.jar
echo "signing ./fractions/fraction-matcher_tk.jar"
sign-jar ./fractions/fraction-matcher_tk.jar
echo "signing ./fractions/build-a-fraction_lo.jar"
sign-jar ./fractions/build-a-fraction_lo.jar
echo "signing ./fractions/build-a-fraction_zh_TW.jar"
sign-jar ./fractions/build-a-fraction_zh_TW.jar
echo "signing ./fractions/fraction-matcher_pt.jar"
sign-jar ./fractions/fraction-matcher_pt.jar
echo "signing ./fractions/build-a-fraction_sw.jar"
sign-jar ./fractions/build-a-fraction_sw.jar
echo "signing ./fractions/fraction-matcher_de.jar"
sign-jar ./fractions/fraction-matcher_de.jar
echo "signing ./fractions/fraction-matcher_cs.jar"
sign-jar ./fractions/fraction-matcher_cs.jar
echo "signing ./fractions/fraction-matcher_eu.jar"
sign-jar ./fractions/fraction-matcher_eu.jar
echo "signing ./fractions/fractions-intro_iw.jar"
sign-jar ./fractions/fractions-intro_iw.jar
echo "signing ./fractions/build-a-fraction_kk.jar"
sign-jar ./fractions/build-a-fraction_kk.jar
echo "signing ./fractions/fractions-intro_af.jar"
sign-jar ./fractions/fractions-intro_af.jar
echo "signing ./fractions/fractions-intro_es.jar"
sign-jar ./fractions/fractions-intro_es.jar
echo "signing ./fractions/fractions-intro_el.jar"
sign-jar ./fractions/fractions-intro_el.jar
echo "signing ./fractions/build-a-fraction_de.jar"
sign-jar ./fractions/build-a-fraction_de.jar
echo "signing ./moving-man/moving-man_tr.jar"
sign-jar ./moving-man/moving-man_tr.jar
echo "signing ./moving-man/moving-man_tn.jar"
sign-jar ./moving-man/moving-man_tn.jar
echo "signing ./moving-man/moving-man_sr.jar"
sign-jar ./moving-man/moving-man_sr.jar
echo "signing ./moving-man/moving-man_uk.jar"
sign-jar ./moving-man/moving-man_uk.jar
echo "signing ./moving-man/moving-man_pt.jar"
sign-jar ./moving-man/moving-man_pt.jar
echo "signing ./moving-man/moving-man_gl.jar"
sign-jar ./moving-man/moving-man_gl.jar
echo "signing ./moving-man/moving-man_es.jar"
sign-jar ./moving-man/moving-man_es.jar
echo "signing ./moving-man/moving-man_et.jar"
sign-jar ./moving-man/moving-man_et.jar
echo "signing ./moving-man/moving-man_fi.jar"
sign-jar ./moving-man/moving-man_fi.jar
echo "signing ./moving-man/moving-man_mk.jar"
sign-jar ./moving-man/moving-man_mk.jar
echo "signing ./moving-man/moving-man_km.jar"
sign-jar ./moving-man/moving-man_km.jar
echo "signing ./moving-man/moving-man_zh_CN.jar"
sign-jar ./moving-man/moving-man_zh_CN.jar
echo "signing ./moving-man/moving-man_bs.jar"
sign-jar ./moving-man/moving-man_bs.jar
echo "signing ./moving-man/moving-man_sk.jar"
sign-jar ./moving-man/moving-man_sk.jar
echo "signing ./moving-man/moving-man_ja.jar"
sign-jar ./moving-man/moving-man_ja.jar
echo "signing ./moving-man/moving-man_kk.jar"
sign-jar ./moving-man/moving-man_kk.jar
echo "signing ./moving-man/moving-man_es_PE.jar"
sign-jar ./moving-man/moving-man_es_PE.jar
echo "signing ./moving-man/moving-man_ku_TR.jar"
sign-jar ./moving-man/moving-man_ku_TR.jar
echo "signing ./moving-man/moving-man_en.jar"
sign-jar ./moving-man/moving-man_en.jar
echo "signing ./moving-man/moving-man_hu.jar"
sign-jar ./moving-man/moving-man_hu.jar
echo "signing ./moving-man/moving-man_da.jar"
sign-jar ./moving-man/moving-man_da.jar
echo "signing ./moving-man/moving-man_nl.jar"
sign-jar ./moving-man/moving-man_nl.jar
echo "signing ./moving-man/moving-man_all_installer.jar"
sign-jar ./moving-man/moving-man_all_installer.jar
echo "signing ./moving-man/moving-man_ru.jar"
sign-jar ./moving-man/moving-man_ru.jar
echo "signing ./moving-man/moving-man_cs.jar"
sign-jar ./moving-man/moving-man_cs.jar
echo "signing ./moving-man/moving-man_sv.jar"
sign-jar ./moving-man/moving-man_sv.jar
echo "signing ./moving-man/moving-man_ar_SA.jar"
sign-jar ./moving-man/moving-man_ar_SA.jar
echo "signing ./moving-man/moving-man_de.jar"
sign-jar ./moving-man/moving-man_de.jar
echo "signing ./moving-man/moving-man_nb.jar"
sign-jar ./moving-man/moving-man_nb.jar
echo "signing ./moving-man/moving-man_pl.jar"
sign-jar ./moving-man/moving-man_pl.jar
echo "signing ./moving-man/moving-man_fr.jar"
sign-jar ./moving-man/moving-man_fr.jar
echo "signing ./moving-man/moving-man_el.jar"
sign-jar ./moving-man/moving-man_el.jar
echo "signing ./moving-man/moving-man_vi.jar"
sign-jar ./moving-man/moving-man_vi.jar
echo "signing ./moving-man/moving-man_iw.jar"
sign-jar ./moving-man/moving-man_iw.jar
echo "signing ./moving-man/moving-man_pt_BR.jar"
sign-jar ./moving-man/moving-man_pt_BR.jar
echo "signing ./moving-man/moving-man_eu.jar"
sign-jar ./moving-man/moving-man_eu.jar
echo "signing ./moving-man/moving-man_th.jar"
sign-jar ./moving-man/moving-man_th.jar
echo "signing ./moving-man/moving-man_sl.jar"
sign-jar ./moving-man/moving-man_sl.jar
echo "signing ./moving-man/moving-man_zh_TW.jar"
sign-jar ./moving-man/moving-man_zh_TW.jar
echo "signing ./moving-man/moving-man_kn.jar"
sign-jar ./moving-man/moving-man_kn.jar
echo "signing ./moving-man/moving-man_ms.jar"
sign-jar ./moving-man/moving-man_ms.jar
echo "signing ./moving-man/moving-man_ko.jar"
sign-jar ./moving-man/moving-man_ko.jar
echo "signing ./moving-man/moving-man_ar.jar"
sign-jar ./moving-man/moving-man_ar.jar
echo "signing ./moving-man/moving-man_es_CO.jar"
sign-jar ./moving-man/moving-man_es_CO.jar
echo "signing ./moving-man/moving-man_tk.jar"
sign-jar ./moving-man/moving-man_tk.jar
echo "signing ./moving-man/moving-man_fa.jar"
sign-jar ./moving-man/moving-man_fa.jar
echo "signing ./moving-man/moving-man_it.jar"
sign-jar ./moving-man/moving-man_it.jar
echo "signing ./moving-man/moving-man_all.jar"
sign-jar ./moving-man/moving-man_all.jar
echo "signing ./moving-man/moving-man_hr.jar"
sign-jar ./moving-man/moving-man_hr.jar
echo "signing ./moving-man/moving-man_in.jar"
sign-jar ./moving-man/moving-man_in.jar
echo "signing ./moving-man/moving-man_be.jar"
sign-jar ./moving-man/moving-man_be.jar
echo "signing ./friction/friction_es.jar"
sign-jar ./friction/friction_es.jar
echo "signing ./friction/friction_pt_BR.jar"
sign-jar ./friction/friction_pt_BR.jar
echo "signing ./friction/friction_de.jar"
sign-jar ./friction/friction_de.jar
echo "signing ./friction/friction_sr.jar"
sign-jar ./friction/friction_sr.jar
echo "signing ./friction/friction_fr.jar"
sign-jar ./friction/friction_fr.jar
echo "signing ./friction/friction_vi.jar"
sign-jar ./friction/friction_vi.jar
echo "signing ./friction/friction_gl.jar"
sign-jar ./friction/friction_gl.jar
echo "signing ./friction/friction_ko.jar"
sign-jar ./friction/friction_ko.jar
echo "signing ./friction/friction_eu.jar"
sign-jar ./friction/friction_eu.jar
echo "signing ./friction/friction_en.jar"
sign-jar ./friction/friction_en.jar
echo "signing ./friction/friction_el.jar"
sign-jar ./friction/friction_el.jar
echo "signing ./friction/friction_kn.jar"
sign-jar ./friction/friction_kn.jar
echo "signing ./friction/friction_mn.jar"
sign-jar ./friction/friction_mn.jar
echo "signing ./friction/friction_ru.jar"
sign-jar ./friction/friction_ru.jar
echo "signing ./friction/friction_cs.jar"
sign-jar ./friction/friction_cs.jar
echo "signing ./friction/friction_uk.jar"
sign-jar ./friction/friction_uk.jar
echo "signing ./friction/friction_sq.jar"
sign-jar ./friction/friction_sq.jar
echo "signing ./friction/friction_sv.jar"
sign-jar ./friction/friction_sv.jar
echo "signing ./friction/friction_fi.jar"
sign-jar ./friction/friction_fi.jar
echo "signing ./friction/friction_ar.jar"
sign-jar ./friction/friction_ar.jar
echo "signing ./friction/friction_iw.jar"
sign-jar ./friction/friction_iw.jar
echo "signing ./friction/friction_be.jar"
sign-jar ./friction/friction_be.jar
echo "signing ./friction/friction_ar_SA.jar"
sign-jar ./friction/friction_ar_SA.jar
echo "signing ./friction/friction_zh_TW.jar"
sign-jar ./friction/friction_zh_TW.jar
echo "signing ./friction/friction_da.jar"
sign-jar ./friction/friction_da.jar
echo "signing ./friction/friction_es_PE.jar"
sign-jar ./friction/friction_es_PE.jar
echo "signing ./friction/friction_ku_TR.jar"
sign-jar ./friction/friction_ku_TR.jar
echo "signing ./friction/friction_kk.jar"
sign-jar ./friction/friction_kk.jar
echo "signing ./friction/friction_tk.jar"
sign-jar ./friction/friction_tk.jar
echo "signing ./friction/friction_pt.jar"
sign-jar ./friction/friction_pt.jar
echo "signing ./friction/friction_et.jar"
sign-jar ./friction/friction_et.jar
echo "signing ./friction/friction_fa.jar"
sign-jar ./friction/friction_fa.jar
echo "signing ./friction/friction_tr.jar"
sign-jar ./friction/friction_tr.jar
echo "signing ./friction/friction_km.jar"
sign-jar ./friction/friction_km.jar
echo "signing ./friction/friction_lv.jar"
sign-jar ./friction/friction_lv.jar
echo "signing ./friction/friction_nl.jar"
sign-jar ./friction/friction_nl.jar
echo "signing ./friction/friction_ta.jar"
sign-jar ./friction/friction_ta.jar
echo "signing ./friction/friction_it.jar"
sign-jar ./friction/friction_it.jar
echo "signing ./friction/friction_mr.jar"
sign-jar ./friction/friction_mr.jar
echo "signing ./friction/friction_mk.jar"
sign-jar ./friction/friction_mk.jar
echo "signing ./friction/friction_bs.jar"
sign-jar ./friction/friction_bs.jar
echo "signing ./friction/friction_ja.jar"
sign-jar ./friction/friction_ja.jar
echo "signing ./friction/friction_hr.jar"
sign-jar ./friction/friction_hr.jar
echo "signing ./friction/friction_th.jar"
sign-jar ./friction/friction_th.jar
echo "signing ./friction/friction_zh_CN.jar"
sign-jar ./friction/friction_zh_CN.jar
echo "signing ./friction/friction_in.jar"
sign-jar ./friction/friction_in.jar
echo "signing ./friction/friction_pl.jar"
sign-jar ./friction/friction_pl.jar
echo "signing ./friction/friction_sk.jar"
sign-jar ./friction/friction_sk.jar
echo "signing ./friction/friction_hu.jar"
sign-jar ./friction/friction_hu.jar
echo "signing ./build-an-atom/build-an-atom_th.jar"
sign-jar ./build-an-atom/build-an-atom_th.jar
echo "signing ./build-an-atom/build-an-atom_fa.jar"
sign-jar ./build-an-atom/build-an-atom_fa.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_cs.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_cs.jar
echo "signing ./build-an-atom/build-an-atom_es.jar"
sign-jar ./build-an-atom/build-an-atom_es.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_uk.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_uk.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_sv.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_sv.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_es_PE.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_es_PE.jar
echo "signing ./build-an-atom/build-an-atom_bs.jar"
sign-jar ./build-an-atom/build-an-atom_bs.jar
echo "signing ./build-an-atom/build-an-atom_ru.jar"
sign-jar ./build-an-atom/build-an-atom_ru.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_mk.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_mk.jar
echo "signing ./build-an-atom/build-an-atom_lv.jar"
sign-jar ./build-an-atom/build-an-atom_lv.jar
echo "signing ./build-an-atom/build-an-atom_nb.jar"
sign-jar ./build-an-atom/build-an-atom_nb.jar
echo "signing ./build-an-atom/build-an-atom_it.jar"
sign-jar ./build-an-atom/build-an-atom_it.jar
echo "signing ./build-an-atom/build-an-atom_tk.jar"
sign-jar ./build-an-atom/build-an-atom_tk.jar
echo "signing ./build-an-atom/build-an-atom_ar.jar"
sign-jar ./build-an-atom/build-an-atom_ar.jar
echo "signing ./build-an-atom/build-an-atom_sv.jar"
sign-jar ./build-an-atom/build-an-atom_sv.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_nl.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_nl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_fr.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_fr.jar
echo "signing ./build-an-atom/build-an-atom_hr.jar"
sign-jar ./build-an-atom/build-an-atom_hr.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_tk.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_tk.jar
echo "signing ./build-an-atom/build-an-atom_zh_CN.jar"
sign-jar ./build-an-atom/build-an-atom_zh_CN.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_lv.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_lv.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_mi.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_mi.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_pt.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_pt.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_zh_TW.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_zh_TW.jar
echo "signing ./build-an-atom/build-an-atom_ka.jar"
sign-jar ./build-an-atom/build-an-atom_ka.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_sk.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_sk.jar
echo "signing ./build-an-atom/build-an-atom_fi.jar"
sign-jar ./build-an-atom/build-an-atom_fi.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_bs.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_bs.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_hr.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_hr.jar
echo "signing ./build-an-atom/build-an-atom_km.jar"
sign-jar ./build-an-atom/build-an-atom_km.jar
echo "signing ./build-an-atom/build-an-atom_mr.jar"
sign-jar ./build-an-atom/build-an-atom_mr.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_et.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_et.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_pt_BR.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_pt_BR.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_pl.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_pl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_hu.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_hu.jar
echo "signing ./build-an-atom/build-an-atom_gl.jar"
sign-jar ./build-an-atom/build-an-atom_gl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_km.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_km.jar
echo "signing ./build-an-atom/build-an-atom_all.jar"
sign-jar ./build-an-atom/build-an-atom_all.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_nb.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_nb.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_ja.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_ja.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_kk.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_kk.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_el.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_el.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_bg.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_bg.jar
echo "signing ./build-an-atom/build-an-atom_bg.jar"
sign-jar ./build-an-atom/build-an-atom_bg.jar
echo "signing ./build-an-atom/build-an-atom_iw.jar"
sign-jar ./build-an-atom/build-an-atom_iw.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_uz.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_uz.jar
echo "signing ./build-an-atom/build-an-atom_mo.jar"
sign-jar ./build-an-atom/build-an-atom_mo.jar
echo "signing ./build-an-atom/build-an-atom_ja.jar"
sign-jar ./build-an-atom/build-an-atom_ja.jar
echo "signing ./build-an-atom/build-an-atom_sl.jar"
sign-jar ./build-an-atom/build-an-atom_sl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_sq.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_sq.jar
echo "signing ./build-an-atom/build-an-atom_vi.jar"
sign-jar ./build-an-atom/build-an-atom_vi.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_iw.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_iw.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_fi.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_fi.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_en.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_en.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_eu.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_eu.jar
echo "signing ./build-an-atom/build-an-atom_da.jar"
sign-jar ./build-an-atom/build-an-atom_da.jar
echo "signing ./build-an-atom/build-an-atom_pl.jar"
sign-jar ./build-an-atom/build-an-atom_pl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_kn.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_kn.jar
echo "signing ./build-an-atom/build-an-atom_hu.jar"
sign-jar ./build-an-atom/build-an-atom_hu.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_gl.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_gl.jar
echo "signing ./build-an-atom/build-an-atom_eu.jar"
sign-jar ./build-an-atom/build-an-atom_eu.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_es.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_es.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_tr.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_tr.jar
echo "signing ./build-an-atom/build-an-atom_pt_BR.jar"
sign-jar ./build-an-atom/build-an-atom_pt_BR.jar
echo "signing ./build-an-atom/build-an-atom_en.jar"
sign-jar ./build-an-atom/build-an-atom_en.jar
echo "signing ./build-an-atom/build-an-atom_mk.jar"
sign-jar ./build-an-atom/build-an-atom_mk.jar
echo "signing ./build-an-atom/build-an-atom_es_ES.jar"
sign-jar ./build-an-atom/build-an-atom_es_ES.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_es_ES.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_es_ES.jar
echo "signing ./build-an-atom/build-an-atom_in.jar"
sign-jar ./build-an-atom/build-an-atom_in.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_mo.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_mo.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_ka.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_ka.jar
echo "signing ./build-an-atom/build-an-atom_cs.jar"
sign-jar ./build-an-atom/build-an-atom_cs.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_da.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_da.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_ar.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_ar.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_cy.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_cy.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_in.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_in.jar
echo "signing ./build-an-atom/build-an-atom_el.jar"
sign-jar ./build-an-atom/build-an-atom_el.jar
echo "signing ./build-an-atom/build-an-atom_kn.jar"
sign-jar ./build-an-atom/build-an-atom_kn.jar
echo "signing ./build-an-atom/build-an-atom_ko.jar"
sign-jar ./build-an-atom/build-an-atom_ko.jar
echo "signing ./build-an-atom/build-an-atom_de.jar"
sign-jar ./build-an-atom/build-an-atom_de.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_be.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_be.jar
echo "signing ./build-an-atom/build-an-atom_tr.jar"
sign-jar ./build-an-atom/build-an-atom_tr.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_th.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_th.jar
echo "signing ./build-an-atom/build-an-atom_sk.jar"
sign-jar ./build-an-atom/build-an-atom_sk.jar
echo "signing ./build-an-atom/build-an-atom_mi.jar"
sign-jar ./build-an-atom/build-an-atom_mi.jar
echo "signing ./build-an-atom/build-an-atom_cy.jar"
sign-jar ./build-an-atom/build-an-atom_cy.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_ko.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_ko.jar
echo "signing ./build-an-atom/build-an-atom_fr.jar"
sign-jar ./build-an-atom/build-an-atom_fr.jar
echo "signing ./build-an-atom/build-an-atom_sq.jar"
sign-jar ./build-an-atom/build-an-atom_sq.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_mr.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_mr.jar
echo "signing ./build-an-atom/build-an-atom_zh_TW.jar"
sign-jar ./build-an-atom/build-an-atom_zh_TW.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_sr.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_sr.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_de.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_de.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_ru.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_ru.jar
echo "signing ./build-an-atom/build-an-atom_all_installer.jar"
sign-jar ./build-an-atom/build-an-atom_all_installer.jar
echo "signing ./build-an-atom/build-an-atom_uk.jar"
sign-jar ./build-an-atom/build-an-atom_uk.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_fa.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_fa.jar
echo "signing ./build-an-atom/build-an-atom_nl.jar"
sign-jar ./build-an-atom/build-an-atom_nl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_zh_CN.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_zh_CN.jar
echo "signing ./build-an-atom/build-an-atom_et.jar"
sign-jar ./build-an-atom/build-an-atom_et.jar
echo "signing ./build-an-atom/build-an-atom_kk.jar"
sign-jar ./build-an-atom/build-an-atom_kk.jar
echo "signing ./build-an-atom/build-an-atom_pt.jar"
sign-jar ./build-an-atom/build-an-atom_pt.jar
echo "signing ./build-an-atom/build-an-atom_es_PE.jar"
sign-jar ./build-an-atom/build-an-atom_es_PE.jar
echo "signing ./build-an-atom/build-an-atom_be.jar"
sign-jar ./build-an-atom/build-an-atom_be.jar
echo "signing ./build-an-atom/build-an-atom_sr.jar"
sign-jar ./build-an-atom/build-an-atom_sr.jar
echo "signing ./build-an-atom/build-an-atom_uz.jar"
sign-jar ./build-an-atom/build-an-atom_uz.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_sl.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_sl.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_vi.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_vi.jar
echo "signing ./build-an-atom/isotopes-and-atomic-mass_it.jar"
sign-jar ./build-an-atom/isotopes-and-atomic-mass_it.jar
echo "signing ./radiating-charge/radiating-charge_es_PE.jar"
sign-jar ./radiating-charge/radiating-charge_es_PE.jar
echo "signing ./radiating-charge/radiating-charge_zh_TW.jar"
sign-jar ./radiating-charge/radiating-charge_zh_TW.jar
echo "signing ./radiating-charge/radiating-charge_da.jar"
sign-jar ./radiating-charge/radiating-charge_da.jar
echo "signing ./radiating-charge/radiating-charge_ja.jar"
sign-jar ./radiating-charge/radiating-charge_ja.jar
echo "signing ./radiating-charge/radiating-charge_vi.jar"
sign-jar ./radiating-charge/radiating-charge_vi.jar
echo "signing ./radiating-charge/radiating-charge_sr.jar"
sign-jar ./radiating-charge/radiating-charge_sr.jar
echo "signing ./radiating-charge/radiating-charge_kk.jar"
sign-jar ./radiating-charge/radiating-charge_kk.jar
echo "signing ./radiating-charge/radiating-charge_iw.jar"
sign-jar ./radiating-charge/radiating-charge_iw.jar
echo "signing ./radiating-charge/radiating-charge_sk.jar"
sign-jar ./radiating-charge/radiating-charge_sk.jar
echo "signing ./radiating-charge/radiating-charge_eu.jar"
sign-jar ./radiating-charge/radiating-charge_eu.jar
echo "signing ./radiating-charge/radiating-charge_th.jar"
sign-jar ./radiating-charge/radiating-charge_th.jar
echo "signing ./radiating-charge/radiating-charge_es.jar"
sign-jar ./radiating-charge/radiating-charge_es.jar
echo "signing ./radiating-charge/radiating-charge_fr.jar"
sign-jar ./radiating-charge/radiating-charge_fr.jar
echo "signing ./radiating-charge/radiating-charge_mk.jar"
sign-jar ./radiating-charge/radiating-charge_mk.jar
echo "signing ./radiating-charge/radiating-charge_ko.jar"
sign-jar ./radiating-charge/radiating-charge_ko.jar
echo "signing ./radiating-charge/radiating-charge_cs.jar"
sign-jar ./radiating-charge/radiating-charge_cs.jar
echo "signing ./radiating-charge/radiating-charge_tr.jar"
sign-jar ./radiating-charge/radiating-charge_tr.jar
echo "signing ./radiating-charge/radiating-charge_el.jar"
sign-jar ./radiating-charge/radiating-charge_el.jar
echo "signing ./radiating-charge/radiating-charge_nl.jar"
sign-jar ./radiating-charge/radiating-charge_nl.jar
echo "signing ./radiating-charge/radiating-charge_bs.jar"
sign-jar ./radiating-charge/radiating-charge_bs.jar
echo "signing ./radiating-charge/radiating-charge_it.jar"
sign-jar ./radiating-charge/radiating-charge_it.jar
echo "signing ./radiating-charge/radiating-charge_de.jar"
sign-jar ./radiating-charge/radiating-charge_de.jar
echo "signing ./radiating-charge/radiating-charge_en.jar"
sign-jar ./radiating-charge/radiating-charge_en.jar
echo "signing ./radiating-charge/radiating-charge_hu.jar"
sign-jar ./radiating-charge/radiating-charge_hu.jar
echo "signing ./radiating-charge/radiating-charge_zh_CN.jar"
sign-jar ./radiating-charge/radiating-charge_zh_CN.jar
echo "signing ./radiating-charge/radiating-charge_pt_BR.jar"
sign-jar ./radiating-charge/radiating-charge_pt_BR.jar
echo "signing ./radiating-charge/radiating-charge_pl.jar"
sign-jar ./radiating-charge/radiating-charge_pl.jar
echo "signing ./radiating-charge/radiating-charge_fa.jar"
sign-jar ./radiating-charge/radiating-charge_fa.jar
echo "signing ./radiating-charge/radiating-charge_fi.jar"
sign-jar ./radiating-charge/radiating-charge_fi.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_vi.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_vi.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_cs.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_cs.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_zh_TW.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_zh_TW.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_fr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_fr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_vi.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_vi.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_pl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_pl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_en.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_en.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_tr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_tr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ka.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ka.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_gl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_gl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ca.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ca.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_es.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_es.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_el.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_el.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_cs.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_cs.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_eu.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_eu.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_kk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_kk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_gl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_gl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_tr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_tr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_ru.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_ru.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_km.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_km.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_ko.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_ko.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ja.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ja.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_tk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_tk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_in.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_in.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_tk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_tk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_iw.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_iw.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_da.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_da.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_zh_TW.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_zh_TW.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_sl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_sl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_all.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_all.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_cs.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_cs.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_da.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_da.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_da.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_da.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_nl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_nl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_fa.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_fa.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_hu.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_hu.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_th.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_th.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ja.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ja.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sv.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sv.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_hu.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_hu.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_km.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_km.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_pt_BR.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_pt_BR.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_it.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_it.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ko.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ko.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_de.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_de.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ko.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ko.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_eu.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_eu.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_mk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_mk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_fa.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_fa.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_in.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_in.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_zh_TW.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_zh_TW.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_bs.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_bs.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_it.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_it.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_mk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_mk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_zh_CN.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_zh_CN.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_sv.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_sv.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_nl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_nl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_fa.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_fa.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_vi.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_vi.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_en.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_en.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_iw.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_iw.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_bs.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_bs.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_es_PE.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_es_PE.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_pt_BR.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_pt_BR.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_ka.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_ka.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_de.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_de.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_tr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_tr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_it.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_it.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_es.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_es.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_ar.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_ar.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_pt_BR.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_pt_BR.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_de.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_de.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_kk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_kk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_mk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_mk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_tk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_tk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_bs.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_bs.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sv.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sv.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_hu.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_hu.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ka.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ka.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_es.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_es.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_ja.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_ja.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_pl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_pl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_km.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_km.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_iw.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_iw.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ca.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ca.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_in.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_in.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_fr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_fr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ar.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ar.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_es_PE.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_es_PE.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_th.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_th.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_en.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_en.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_nl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_nl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_zh_CN.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_zh_CN.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_sl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_zh_CN.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_zh_CN.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_ca.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_ca.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_sr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_sr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_fr.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_fr.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ru.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_ru.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_pl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_pl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_sk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_sk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_sl.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_kk.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_kk.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ru.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ru.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_th.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_th.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_el.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_el.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_es_PE.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_es_PE.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ar.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-micro_ar.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions_all_installer.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions_all_installer.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_el.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_el.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_eu.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_eu.jar
echo "signing ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_gl.jar"
sign-jar ./sugar-and-salt-solutions/sugar-and-salt-solutions-macro_gl.jar
echo "signing ./quantum-tunneling/quantum-tunneling_all.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_all.jar
echo "signing ./quantum-tunneling/quantum-tunneling_zh_CN.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_zh_CN.jar
echo "signing ./quantum-tunneling/quantum-tunneling_ru.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_ru.jar
echo "signing ./quantum-tunneling/quantum-tunneling_nl.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_nl.jar
echo "signing ./quantum-tunneling/quantum-tunneling_ja.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_ja.jar
echo "signing ./quantum-tunneling/quantum-tunneling_tr.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_tr.jar
echo "signing ./quantum-tunneling/quantum-tunneling_kk.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_kk.jar
echo "signing ./quantum-tunneling/quantum-tunneling_vi.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_vi.jar
echo "signing ./quantum-tunneling/quantum-tunneling_es_PE.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_es_PE.jar
echo "signing ./quantum-tunneling/quantum-tunneling_bs.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_bs.jar
echo "signing ./quantum-tunneling/quantum-tunneling_el.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_el.jar
echo "signing ./quantum-tunneling/quantum-tunneling_tk.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_tk.jar
echo "signing ./quantum-tunneling/quantum-tunneling_eu.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_eu.jar
echo "signing ./quantum-tunneling/quantum-tunneling_hr.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_hr.jar
echo "signing ./quantum-tunneling/quantum-tunneling_de.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_de.jar
echo "signing ./quantum-tunneling/quantum-tunneling_da.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_da.jar
echo "signing ./quantum-tunneling/quantum-tunneling_sk.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_sk.jar
echo "signing ./quantum-tunneling/quantum-tunneling_en.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_en.jar
echo "signing ./quantum-tunneling/quantum-tunneling_mn.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_mn.jar
echo "signing ./quantum-tunneling/quantum-tunneling_iw.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_iw.jar
echo "signing ./quantum-tunneling/quantum-tunneling_es.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_es.jar
echo "signing ./quantum-tunneling/quantum-tunneling_mk.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_mk.jar
echo "signing ./quantum-tunneling/quantum-tunneling_pl.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_pl.jar
echo "signing ./quantum-tunneling/quantum-tunneling_fr.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_fr.jar
echo "signing ./quantum-tunneling/quantum-tunneling_in.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_in.jar
echo "signing ./quantum-tunneling/quantum-tunneling_th.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_th.jar
echo "signing ./quantum-tunneling/quantum-tunneling_it.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_it.jar
echo "signing ./quantum-tunneling/quantum-tunneling_sr.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_sr.jar
echo "signing ./quantum-tunneling/quantum-tunneling_hu.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_hu.jar
echo "signing ./quantum-tunneling/quantum-tunneling_zh_TW.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_zh_TW.jar
echo "signing ./quantum-tunneling/quantum-tunneling_pt.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_pt.jar
echo "signing ./quantum-tunneling/quantum-tunneling_pt_BR.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_pt_BR.jar
echo "signing ./quantum-tunneling/quantum-tunneling_fa.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_fa.jar
echo "signing ./quantum-tunneling/quantum-tunneling_ko.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_ko.jar
echo "signing ./quantum-tunneling/quantum-tunneling_all_installer.jar"
sign-jar ./quantum-tunneling/quantum-tunneling_all_installer.jar
echo "signing ./normal-modes/normal-modes_da.jar"
sign-jar ./normal-modes/normal-modes_da.jar
echo "signing ./normal-modes/normal-modes_de.jar"
sign-jar ./normal-modes/normal-modes_de.jar
echo "signing ./normal-modes/normal-modes_mk.jar"
sign-jar ./normal-modes/normal-modes_mk.jar
echo "signing ./normal-modes/normal-modes_pt_BR.jar"
sign-jar ./normal-modes/normal-modes_pt_BR.jar
echo "signing ./normal-modes/normal-modes_ko.jar"
sign-jar ./normal-modes/normal-modes_ko.jar
echo "signing ./normal-modes/normal-modes_fa.jar"
sign-jar ./normal-modes/normal-modes_fa.jar
echo "signing ./normal-modes/normal-modes_it.jar"
sign-jar ./normal-modes/normal-modes_it.jar
echo "signing ./normal-modes/normal-modes_pl.jar"
sign-jar ./normal-modes/normal-modes_pl.jar
echo "signing ./normal-modes/normal-modes_zh_CN.jar"
sign-jar ./normal-modes/normal-modes_zh_CN.jar
echo "signing ./normal-modes/normal-modes_kn.jar"
sign-jar ./normal-modes/normal-modes_kn.jar
echo "signing ./normal-modes/normal-modes_sr.jar"
sign-jar ./normal-modes/normal-modes_sr.jar
echo "signing ./normal-modes/normal-modes_kk.jar"
sign-jar ./normal-modes/normal-modes_kk.jar
echo "signing ./normal-modes/normal-modes_ja.jar"
sign-jar ./normal-modes/normal-modes_ja.jar
echo "signing ./normal-modes/normal-modes_vi.jar"
sign-jar ./normal-modes/normal-modes_vi.jar
echo "signing ./normal-modes/normal-modes_en.jar"
sign-jar ./normal-modes/normal-modes_en.jar
echo "signing ./normal-modes/normal-modes_es_PE.jar"
sign-jar ./normal-modes/normal-modes_es_PE.jar
echo "signing ./normal-modes/normal-modes_sk.jar"
sign-jar ./normal-modes/normal-modes_sk.jar
echo "signing ./normal-modes/normal-modes_hu.jar"
sign-jar ./normal-modes/normal-modes_hu.jar
echo "signing ./normal-modes/normal-modes_th.jar"
sign-jar ./normal-modes/normal-modes_th.jar
echo "signing ./normal-modes/normal-modes_zh_TW.jar"
sign-jar ./normal-modes/normal-modes_zh_TW.jar
echo "signing ./normal-modes/normal-modes_es.jar"
sign-jar ./normal-modes/normal-modes_es.jar
echo "signing ./normal-modes/normal-modes_tk.jar"
sign-jar ./normal-modes/normal-modes_tk.jar
echo "signing ./normal-modes/normal-modes_iw.jar"
sign-jar ./normal-modes/normal-modes_iw.jar
echo "signing ./normal-modes/normal-modes_in.jar"
sign-jar ./normal-modes/normal-modes_in.jar
echo "signing ./normal-modes/normal-modes_eu.jar"
sign-jar ./normal-modes/normal-modes_eu.jar
echo "signing ./normal-modes/normal-modes_fr.jar"
sign-jar ./normal-modes/normal-modes_fr.jar
echo "signing ./normal-modes/normal-modes_gl.jar"
sign-jar ./normal-modes/normal-modes_gl.jar
echo "signing ./normal-modes/normal-modes_bs.jar"
sign-jar ./normal-modes/normal-modes_bs.jar
echo "signing ./molecule-polarity/molecule-polarity_sv.jar"
sign-jar ./molecule-polarity/molecule-polarity_sv.jar
echo "signing ./molecule-polarity/molecule-polarity_ar.jar"
sign-jar ./molecule-polarity/molecule-polarity_ar.jar
echo "signing ./molecule-polarity/molecule-polarity_all.jar"
sign-jar ./molecule-polarity/molecule-polarity_all.jar
echo "signing ./molecule-polarity/molecule-polarity_gl.jar"
sign-jar ./molecule-polarity/molecule-polarity_gl.jar
echo "signing ./molecule-polarity/molecule-polarity_cs.jar"
sign-jar ./molecule-polarity/molecule-polarity_cs.jar
echo "signing ./molecule-polarity/molecule-polarity_km.jar"
sign-jar ./molecule-polarity/molecule-polarity_km.jar
echo "signing ./molecule-polarity/molecule-polarity_zh_TW.jar"
sign-jar ./molecule-polarity/molecule-polarity_zh_TW.jar
echo "signing ./molecule-polarity/molecule-polarity_all_installer.jar"
sign-jar ./molecule-polarity/molecule-polarity_all_installer.jar
echo "signing ./molecule-polarity/molecule-polarity_es.jar"
sign-jar ./molecule-polarity/molecule-polarity_es.jar
echo "signing ./molecule-polarity/molecule-polarity_es_PE.jar"
sign-jar ./molecule-polarity/molecule-polarity_es_PE.jar
echo "signing ./molecule-polarity/molecule-polarity_pt_BR.jar"
sign-jar ./molecule-polarity/molecule-polarity_pt_BR.jar
echo "signing ./molecule-polarity/molecule-polarity_da.jar"
sign-jar ./molecule-polarity/molecule-polarity_da.jar
echo "signing ./molecule-polarity/molecule-polarity_sl.jar"
sign-jar ./molecule-polarity/molecule-polarity_sl.jar
echo "signing ./molecule-polarity/molecule-polarity_nn.jar"
sign-jar ./molecule-polarity/molecule-polarity_nn.jar
echo "signing ./molecule-polarity/molecule-polarity_iw.jar"
sign-jar ./molecule-polarity/molecule-polarity_iw.jar
echo "signing ./molecule-polarity/molecule-polarity_sk.jar"
sign-jar ./molecule-polarity/molecule-polarity_sk.jar
echo "signing ./molecule-polarity/molecule-polarity_en.jar"
sign-jar ./molecule-polarity/molecule-polarity_en.jar
echo "signing ./molecule-polarity/molecule-polarity_fr.jar"
sign-jar ./molecule-polarity/molecule-polarity_fr.jar
echo "signing ./molecule-polarity/molecule-polarity_nl.jar"
sign-jar ./molecule-polarity/molecule-polarity_nl.jar
echo "signing ./molecule-polarity/molecule-polarity_ko.jar"
sign-jar ./molecule-polarity/molecule-polarity_ko.jar
echo "signing ./molecule-polarity/molecule-polarity_zh_CN.jar"
sign-jar ./molecule-polarity/molecule-polarity_zh_CN.jar
echo "signing ./molecule-polarity/molecule-polarity_el.jar"
sign-jar ./molecule-polarity/molecule-polarity_el.jar
echo "signing ./molecule-polarity/molecule-polarity_fi.jar"
sign-jar ./molecule-polarity/molecule-polarity_fi.jar
echo "signing ./molecule-polarity/molecule-polarity_in.jar"
sign-jar ./molecule-polarity/molecule-polarity_in.jar
echo "signing ./molecule-polarity/molecule-polarity_th.jar"
sign-jar ./molecule-polarity/molecule-polarity_th.jar
echo "signing ./molecule-polarity/molecule-polarity_vi.jar"
sign-jar ./molecule-polarity/molecule-polarity_vi.jar
echo "signing ./molecule-polarity/molecule-polarity_eu.jar"
sign-jar ./molecule-polarity/molecule-polarity_eu.jar
echo "signing ./molecule-polarity/molecule-polarity_nb.jar"
sign-jar ./molecule-polarity/molecule-polarity_nb.jar
echo "signing ./molecule-polarity/molecule-polarity_fa.jar"
sign-jar ./molecule-polarity/molecule-polarity_fa.jar
echo "signing ./molecule-polarity/molecule-polarity_de.jar"
sign-jar ./molecule-polarity/molecule-polarity_de.jar
echo "signing ./molecule-polarity/molecule-polarity_kk.jar"
sign-jar ./molecule-polarity/molecule-polarity_kk.jar
echo "signing ./molecule-polarity/molecule-polarity_bs.jar"
sign-jar ./molecule-polarity/molecule-polarity_bs.jar
echo "signing ./molecule-polarity/molecule-polarity_tk.jar"
sign-jar ./molecule-polarity/molecule-polarity_tk.jar
echo "signing ./molecule-polarity/molecule-polarity_ja.jar"
sign-jar ./molecule-polarity/molecule-polarity_ja.jar
echo "signing ./molecule-polarity/molecule-polarity_mk.jar"
sign-jar ./molecule-polarity/molecule-polarity_mk.jar
echo "signing ./molecule-polarity/molecule-polarity_it.jar"
sign-jar ./molecule-polarity/molecule-polarity_it.jar
echo "signing ./molecule-polarity/molecule-polarity_tr.jar"
sign-jar ./molecule-polarity/molecule-polarity_tr.jar
echo "signing ./molecule-polarity/molecule-polarity_pl.jar"
sign-jar ./molecule-polarity/molecule-polarity_pl.jar
echo "signing ./molecule-polarity/molecule-polarity_sr.jar"
sign-jar ./molecule-polarity/molecule-polarity_sr.jar
echo "signing ./molecule-polarity/molecule-polarity_hu.jar"
sign-jar ./molecule-polarity/molecule-polarity_hu.jar
echo "signing ./mri/mri_hr.jar"
sign-jar ./mri/mri_hr.jar
echo "signing ./mri/mri_es.jar"
sign-jar ./mri/mri_es.jar
echo "signing ./mri/mri_sv.jar"
sign-jar ./mri/mri_sv.jar
echo "signing ./mri/mri_fa.jar"
sign-jar ./mri/mri_fa.jar
echo "signing ./mri/mri_in.jar"
sign-jar ./mri/mri_in.jar
echo "signing ./mri/mri_kn.jar"
sign-jar ./mri/mri_kn.jar
echo "signing ./mri/mri_da.jar"
sign-jar ./mri/mri_da.jar
echo "signing ./mri/mri_ru.jar"
sign-jar ./mri/mri_ru.jar
echo "signing ./mri/mri_bs.jar"
sign-jar ./mri/mri_bs.jar
echo "signing ./mri/mri_pl.jar"
sign-jar ./mri/mri_pl.jar
echo "signing ./mri/mri_all_installer.jar"
sign-jar ./mri/mri_all_installer.jar
echo "signing ./mri/mri_be.jar"
sign-jar ./mri/mri_be.jar
echo "signing ./mri/mri_pt.jar"
sign-jar ./mri/mri_pt.jar
echo "signing ./mri/mri_tr.jar"
sign-jar ./mri/mri_tr.jar
echo "signing ./mri/mri_ko.jar"
sign-jar ./mri/mri_ko.jar
echo "signing ./mri/mri_nl.jar"
sign-jar ./mri/mri_nl.jar
echo "signing ./mri/mri_en.jar"
sign-jar ./mri/mri_en.jar
echo "signing ./mri/mri_hu.jar"
sign-jar ./mri/mri_hu.jar
echo "signing ./mri/mri_zh_TW.jar"
sign-jar ./mri/mri_zh_TW.jar
echo "signing ./mri/mri_tk.jar"
sign-jar ./mri/mri_tk.jar
echo "signing ./mri/mri_de.jar"
sign-jar ./mri/mri_de.jar
echo "signing ./mri/mri_fr.jar"
sign-jar ./mri/mri_fr.jar
echo "signing ./mri/mri_it.jar"
sign-jar ./mri/mri_it.jar
echo "signing ./mri/mri_zh_CN.jar"
sign-jar ./mri/mri_zh_CN.jar
echo "signing ./mri/mri_sk.jar"
sign-jar ./mri/mri_sk.jar
echo "signing ./mri/mri_sr.jar"
sign-jar ./mri/mri_sr.jar
echo "signing ./mri/mri_kk.jar"
sign-jar ./mri/mri_kk.jar
echo "signing ./mri/mri_iw.jar"
sign-jar ./mri/mri_iw.jar
echo "signing ./mri/mri_vi.jar"
sign-jar ./mri/mri_vi.jar
echo "signing ./mri/mri_el.jar"
sign-jar ./mri/mri_el.jar
echo "signing ./mri/mri_es_PE.jar"
sign-jar ./mri/mri_es_PE.jar
echo "signing ./mri/mri_all.jar"
sign-jar ./mri/mri_all.jar
echo "signing ./mri/mri_th.jar"
sign-jar ./mri/mri_th.jar
echo "signing ./mri/mri_eu.jar"
sign-jar ./mri/mri_eu.jar
echo "signing ./mri/mri_mk.jar"
sign-jar ./mri/mri_mk.jar
echo "signing ./mri/mri_ja.jar"
sign-jar ./mri/mri_ja.jar
echo "signing ./mri/mri_pt_BR.jar"
sign-jar ./mri/mri_pt_BR.jar
echo "signing ./fourier/fourier_zh_TW.jar"
sign-jar ./fourier/fourier_zh_TW.jar
echo "signing ./fourier/fourier_all.jar"
sign-jar ./fourier/fourier_all.jar
echo "signing ./fourier/fourier_vi.jar"
sign-jar ./fourier/fourier_vi.jar
echo "signing ./fourier/fourier_tk.jar"
sign-jar ./fourier/fourier_tk.jar
echo "signing ./fourier/fourier_fa.jar"
sign-jar ./fourier/fourier_fa.jar
echo "signing ./fourier/fourier_km.jar"
sign-jar ./fourier/fourier_km.jar
echo "signing ./fourier/fourier_fr.jar"
sign-jar ./fourier/fourier_fr.jar
echo "signing ./fourier/fourier_ja.jar"
sign-jar ./fourier/fourier_ja.jar
echo "signing ./fourier/fourier_hu.jar"
sign-jar ./fourier/fourier_hu.jar
echo "signing ./fourier/fourier_sr.jar"
sign-jar ./fourier/fourier_sr.jar
echo "signing ./fourier/fourier_pt_BR.jar"
sign-jar ./fourier/fourier_pt_BR.jar
echo "signing ./fourier/fourier_el.jar"
sign-jar ./fourier/fourier_el.jar
echo "signing ./fourier/fourier_hr.jar"
sign-jar ./fourier/fourier_hr.jar
echo "signing ./fourier/fourier_en.jar"
sign-jar ./fourier/fourier_en.jar
echo "signing ./fourier/fourier_all_installer.jar"
sign-jar ./fourier/fourier_all_installer.jar
echo "signing ./fourier/fourier_bg.jar"
sign-jar ./fourier/fourier_bg.jar
echo "signing ./fourier/fourier_zh_CN.jar"
sign-jar ./fourier/fourier_zh_CN.jar
echo "signing ./fourier/fourier_ko.jar"
sign-jar ./fourier/fourier_ko.jar
echo "signing ./fourier/fourier_eu.jar"
sign-jar ./fourier/fourier_eu.jar
echo "signing ./fourier/fourier_es_PE.jar"
sign-jar ./fourier/fourier_es_PE.jar
echo "signing ./fourier/fourier_iw.jar"
sign-jar ./fourier/fourier_iw.jar
echo "signing ./fourier/fourier_sk.jar"
sign-jar ./fourier/fourier_sk.jar
echo "signing ./fourier/fourier_pl.jar"
sign-jar ./fourier/fourier_pl.jar
echo "signing ./fourier/fourier_in.jar"
sign-jar ./fourier/fourier_in.jar
echo "signing ./fourier/fourier_et.jar"
sign-jar ./fourier/fourier_et.jar
echo "signing ./fourier/fourier_it.jar"
sign-jar ./fourier/fourier_it.jar
echo "signing ./fourier/fourier_tr.jar"
sign-jar ./fourier/fourier_tr.jar
echo "signing ./fourier/fourier_mk.jar"
sign-jar ./fourier/fourier_mk.jar
echo "signing ./fourier/fourier_de.jar"
sign-jar ./fourier/fourier_de.jar
echo "signing ./fourier/fourier_pt.jar"
sign-jar ./fourier/fourier_pt.jar
echo "signing ./fourier/fourier_da.jar"
sign-jar ./fourier/fourier_da.jar
echo "signing ./fourier/fourier_nl.jar"
sign-jar ./fourier/fourier_nl.jar
echo "signing ./fourier/fourier_bs.jar"
sign-jar ./fourier/fourier_bs.jar
echo "signing ./fourier/fourier_es.jar"
sign-jar ./fourier/fourier_es.jar
echo "signing ./estimation/estimation_hu.jar"
sign-jar ./estimation/estimation_hu.jar
echo "signing ./estimation/estimation_eu.jar"
sign-jar ./estimation/estimation_eu.jar
echo "signing ./estimation/estimation_tk.jar"
sign-jar ./estimation/estimation_tk.jar
echo "signing ./estimation/estimation_da.jar"
sign-jar ./estimation/estimation_da.jar
echo "signing ./estimation/estimation_hr.jar"
sign-jar ./estimation/estimation_hr.jar
echo "signing ./estimation/estimation_it.jar"
sign-jar ./estimation/estimation_it.jar
echo "signing ./estimation/estimation_ko.jar"
sign-jar ./estimation/estimation_ko.jar
echo "signing ./estimation/estimation_en.jar"
sign-jar ./estimation/estimation_en.jar
echo "signing ./estimation/estimation_zh_CN.jar"
sign-jar ./estimation/estimation_zh_CN.jar
echo "signing ./estimation/estimation_sv.jar"
sign-jar ./estimation/estimation_sv.jar
echo "signing ./estimation/estimation_fr.jar"
sign-jar ./estimation/estimation_fr.jar
echo "signing ./estimation/estimation_vi.jar"
sign-jar ./estimation/estimation_vi.jar
echo "signing ./estimation/estimation_nb.jar"
sign-jar ./estimation/estimation_nb.jar
echo "signing ./estimation/estimation_pl.jar"
sign-jar ./estimation/estimation_pl.jar
echo "signing ./estimation/estimation_de.jar"
sign-jar ./estimation/estimation_de.jar
echo "signing ./estimation/estimation_kk.jar"
sign-jar ./estimation/estimation_kk.jar
echo "signing ./estimation/estimation_ja.jar"
sign-jar ./estimation/estimation_ja.jar
echo "signing ./estimation/estimation_sr.jar"
sign-jar ./estimation/estimation_sr.jar
echo "signing ./estimation/estimation_el.jar"
sign-jar ./estimation/estimation_el.jar
echo "signing ./estimation/estimation_es.jar"
sign-jar ./estimation/estimation_es.jar
echo "signing ./estimation/estimation_iw.jar"
sign-jar ./estimation/estimation_iw.jar
echo "signing ./estimation/estimation_nl.jar"
sign-jar ./estimation/estimation_nl.jar
echo "signing ./estimation/estimation_fa.jar"
sign-jar ./estimation/estimation_fa.jar
echo "signing ./estimation/estimation_sk.jar"
sign-jar ./estimation/estimation_sk.jar
echo "signing ./estimation/estimation_zh_TW.jar"
sign-jar ./estimation/estimation_zh_TW.jar
echo "signing ./estimation/estimation_mk.jar"
sign-jar ./estimation/estimation_mk.jar
echo "signing ./estimation/estimation_et.jar"
sign-jar ./estimation/estimation_et.jar
echo "signing ./estimation/estimation_es_PE.jar"
sign-jar ./estimation/estimation_es_PE.jar
echo "signing ./estimation/estimation_tr.jar"
sign-jar ./estimation/estimation_tr.jar
echo "signing ./estimation/estimation_bs.jar"
sign-jar ./estimation/estimation_bs.jar
echo "signing ./estimation/estimation_pt_BR.jar"
sign-jar ./estimation/estimation_pt_BR.jar
echo "signing ./estimation/estimation_ar_SA.jar"
sign-jar ./estimation/estimation_ar_SA.jar
echo "signing ./charges-and-fields/charges-and-fields_de.jar"
sign-jar ./charges-and-fields/charges-and-fields_de.jar
echo "signing ./charges-and-fields/charges-and-fields_tk.jar"
sign-jar ./charges-and-fields/charges-and-fields_tk.jar
echo "signing ./charges-and-fields/charges-and-fields_tr.jar"
sign-jar ./charges-and-fields/charges-and-fields_tr.jar
echo "signing ./charges-and-fields/charges-and-fields_sv.jar"
sign-jar ./charges-and-fields/charges-and-fields_sv.jar
echo "signing ./charges-and-fields/charges-and-fields_hr.jar"
sign-jar ./charges-and-fields/charges-and-fields_hr.jar
echo "signing ./charges-and-fields/charges-and-fields_km.jar"
sign-jar ./charges-and-fields/charges-and-fields_km.jar
echo "signing ./charges-and-fields/charges-and-fields_sr.jar"
sign-jar ./charges-and-fields/charges-and-fields_sr.jar
echo "signing ./charges-and-fields/charges-and-fields_pl.jar"
sign-jar ./charges-and-fields/charges-and-fields_pl.jar
echo "signing ./charges-and-fields/charges-and-fields_mk.jar"
sign-jar ./charges-and-fields/charges-and-fields_mk.jar
echo "signing ./charges-and-fields/charges-and-fields_vi.jar"
sign-jar ./charges-and-fields/charges-and-fields_vi.jar
echo "signing ./charges-and-fields/charges-and-fields_kk.jar"
sign-jar ./charges-and-fields/charges-and-fields_kk.jar
echo "signing ./charges-and-fields/charges-and-fields_bs.jar"
sign-jar ./charges-and-fields/charges-and-fields_bs.jar
echo "signing ./charges-and-fields/charges-and-fields_uk.jar"
sign-jar ./charges-and-fields/charges-and-fields_uk.jar
echo "signing ./charges-and-fields/charges-and-fields_hu.jar"
sign-jar ./charges-and-fields/charges-and-fields_hu.jar
echo "signing ./charges-and-fields/charges-and-fields_es.jar"
sign-jar ./charges-and-fields/charges-and-fields_es.jar
echo "signing ./charges-and-fields/charges-and-fields_bg.jar"
sign-jar ./charges-and-fields/charges-and-fields_bg.jar
echo "signing ./charges-and-fields/charges-and-fields_sl.jar"
sign-jar ./charges-and-fields/charges-and-fields_sl.jar
echo "signing ./charges-and-fields/charges-and-fields_fr.jar"
sign-jar ./charges-and-fields/charges-and-fields_fr.jar
echo "signing ./charges-and-fields/charges-and-fields_en.jar"
sign-jar ./charges-and-fields/charges-and-fields_en.jar
echo "signing ./charges-and-fields/charges-and-fields_it.jar"
sign-jar ./charges-and-fields/charges-and-fields_it.jar
echo "signing ./charges-and-fields/charges-and-fields_mr.jar"
sign-jar ./charges-and-fields/charges-and-fields_mr.jar
echo "signing ./charges-and-fields/charges-and-fields_kn.jar"
sign-jar ./charges-and-fields/charges-and-fields_kn.jar
echo "signing ./charges-and-fields/charges-and-fields_ko.jar"
sign-jar ./charges-and-fields/charges-and-fields_ko.jar
echo "signing ./charges-and-fields/charges-and-fields_be.jar"
sign-jar ./charges-and-fields/charges-and-fields_be.jar
echo "signing ./charges-and-fields/charges-and-fields_th.jar"
sign-jar ./charges-and-fields/charges-and-fields_th.jar
echo "signing ./charges-and-fields/charges-and-fields_ja.jar"
sign-jar ./charges-and-fields/charges-and-fields_ja.jar
echo "signing ./charges-and-fields/charges-and-fields_el.jar"
sign-jar ./charges-and-fields/charges-and-fields_el.jar
echo "signing ./charges-and-fields/charges-and-fields_ta.jar"
sign-jar ./charges-and-fields/charges-and-fields_ta.jar
echo "signing ./charges-and-fields/charges-and-fields_eu.jar"
sign-jar ./charges-and-fields/charges-and-fields_eu.jar
echo "signing ./charges-and-fields/charges-and-fields_nl.jar"
sign-jar ./charges-and-fields/charges-and-fields_nl.jar
echo "signing ./charges-and-fields/charges-and-fields_cs.jar"
sign-jar ./charges-and-fields/charges-and-fields_cs.jar
echo "signing ./charges-and-fields/charges-and-fields_es_PE.jar"
sign-jar ./charges-and-fields/charges-and-fields_es_PE.jar
echo "signing ./charges-and-fields/charges-and-fields_zh_CN.jar"
sign-jar ./charges-and-fields/charges-and-fields_zh_CN.jar
echo "signing ./charges-and-fields/charges-and-fields_fa.jar"
sign-jar ./charges-and-fields/charges-and-fields_fa.jar
echo "signing ./charges-and-fields/charges-and-fields_ku_TR.jar"
sign-jar ./charges-and-fields/charges-and-fields_ku_TR.jar
echo "signing ./charges-and-fields/charges-and-fields_zh_TW.jar"
sign-jar ./charges-and-fields/charges-and-fields_zh_TW.jar
echo "signing ./charges-and-fields/charges-and-fields_iw.jar"
sign-jar ./charges-and-fields/charges-and-fields_iw.jar
echo "signing ./charges-and-fields/charges-and-fields_gl.jar"
sign-jar ./charges-and-fields/charges-and-fields_gl.jar
echo "signing ./charges-and-fields/charges-and-fields_fi.jar"
sign-jar ./charges-and-fields/charges-and-fields_fi.jar
echo "signing ./charges-and-fields/charges-and-fields_ar_SA.jar"
sign-jar ./charges-and-fields/charges-and-fields_ar_SA.jar
echo "signing ./charges-and-fields/charges-and-fields_da.jar"
sign-jar ./charges-and-fields/charges-and-fields_da.jar
echo "signing ./charges-and-fields/charges-and-fields_in.jar"
sign-jar ./charges-and-fields/charges-and-fields_in.jar
echo "signing ./charges-and-fields/charges-and-fields_et.jar"
sign-jar ./charges-and-fields/charges-and-fields_et.jar
echo "signing ./charges-and-fields/charges-and-fields_ro.jar"
sign-jar ./charges-and-fields/charges-and-fields_ro.jar
echo "signing ./charges-and-fields/charges-and-fields_sk.jar"
sign-jar ./charges-and-fields/charges-and-fields_sk.jar
echo "signing ./charges-and-fields/charges-and-fields_ru.jar"
sign-jar ./charges-and-fields/charges-and-fields_ru.jar
echo "signing ./charges-and-fields/charges-and-fields_pt_BR.jar"
sign-jar ./charges-and-fields/charges-and-fields_pt_BR.jar
echo "signing ./charges-and-fields/charges-and-fields_lv.jar"
sign-jar ./charges-and-fields/charges-and-fields_lv.jar
echo "signing ./charges-and-fields/charges-and-fields_ht.jar"
sign-jar ./charges-and-fields/charges-and-fields_ht.jar
echo "signing ./charges-and-fields/charges-and-fields_ar.jar"
sign-jar ./charges-and-fields/charges-and-fields_ar.jar
echo "signing ./charges-and-fields/charges-and-fields_ka.jar"
sign-jar ./charges-and-fields/charges-and-fields_ka.jar
echo "signing ./discharge-lamps/discharge-lamps_fr.jar"
sign-jar ./discharge-lamps/discharge-lamps_fr.jar
echo "signing ./discharge-lamps/discharge-lamps_nb.jar"
sign-jar ./discharge-lamps/discharge-lamps_nb.jar
echo "signing ./discharge-lamps/discharge-lamps_it.jar"
sign-jar ./discharge-lamps/discharge-lamps_it.jar
echo "signing ./discharge-lamps/discharge-lamps_da.jar"
sign-jar ./discharge-lamps/discharge-lamps_da.jar
echo "signing ./discharge-lamps/discharge-lamps_tk.jar"
sign-jar ./discharge-lamps/discharge-lamps_tk.jar
echo "signing ./discharge-lamps/discharge-lamps_all.jar"
sign-jar ./discharge-lamps/discharge-lamps_all.jar
echo "signing ./discharge-lamps/discharge-lamps_hu.jar"
sign-jar ./discharge-lamps/discharge-lamps_hu.jar
echo "signing ./discharge-lamps/discharge-lamps_sr.jar"
sign-jar ./discharge-lamps/discharge-lamps_sr.jar
echo "signing ./discharge-lamps/discharge-lamps_et.jar"
sign-jar ./discharge-lamps/discharge-lamps_et.jar
echo "signing ./discharge-lamps/discharge-lamps_ru.jar"
sign-jar ./discharge-lamps/discharge-lamps_ru.jar
echo "signing ./discharge-lamps/discharge-lamps_ar.jar"
sign-jar ./discharge-lamps/discharge-lamps_ar.jar
echo "signing ./discharge-lamps/discharge-lamps_tr.jar"
sign-jar ./discharge-lamps/discharge-lamps_tr.jar
echo "signing ./discharge-lamps/discharge-lamps_nl.jar"
sign-jar ./discharge-lamps/discharge-lamps_nl.jar
echo "signing ./discharge-lamps/discharge-lamps_iw.jar"
sign-jar ./discharge-lamps/discharge-lamps_iw.jar
echo "signing ./discharge-lamps/discharge-lamps_sk.jar"
sign-jar ./discharge-lamps/discharge-lamps_sk.jar
echo "signing ./discharge-lamps/discharge-lamps_gl.jar"
sign-jar ./discharge-lamps/discharge-lamps_gl.jar
echo "signing ./discharge-lamps/discharge-lamps_pt.jar"
sign-jar ./discharge-lamps/discharge-lamps_pt.jar
echo "signing ./discharge-lamps/discharge-lamps_fa.jar"
sign-jar ./discharge-lamps/discharge-lamps_fa.jar
echo "signing ./discharge-lamps/discharge-lamps_ja.jar"
sign-jar ./discharge-lamps/discharge-lamps_ja.jar
echo "signing ./discharge-lamps/discharge-lamps_zh_CN.jar"
sign-jar ./discharge-lamps/discharge-lamps_zh_CN.jar
echo "signing ./discharge-lamps/discharge-lamps_pt_BR.jar"
sign-jar ./discharge-lamps/discharge-lamps_pt_BR.jar
echo "signing ./discharge-lamps/discharge-lamps_mk.jar"
sign-jar ./discharge-lamps/discharge-lamps_mk.jar
echo "signing ./discharge-lamps/discharge-lamps_zh_TW.jar"
sign-jar ./discharge-lamps/discharge-lamps_zh_TW.jar
echo "signing ./discharge-lamps/discharge-lamps_ka.jar"
sign-jar ./discharge-lamps/discharge-lamps_ka.jar
echo "signing ./discharge-lamps/discharge-lamps_vi.jar"
sign-jar ./discharge-lamps/discharge-lamps_vi.jar
echo "signing ./discharge-lamps/discharge-lamps_ko.jar"
sign-jar ./discharge-lamps/discharge-lamps_ko.jar
echo "signing ./discharge-lamps/discharge-lamps_hr.jar"
sign-jar ./discharge-lamps/discharge-lamps_hr.jar
echo "signing ./discharge-lamps/discharge-lamps_in.jar"
sign-jar ./discharge-lamps/discharge-lamps_in.jar
echo "signing ./discharge-lamps/discharge-lamps_es.jar"
sign-jar ./discharge-lamps/discharge-lamps_es.jar
echo "signing ./discharge-lamps/discharge-lamps_el.jar"
sign-jar ./discharge-lamps/discharge-lamps_el.jar
echo "signing ./discharge-lamps/discharge-lamps_de.jar"
sign-jar ./discharge-lamps/discharge-lamps_de.jar
echo "signing ./discharge-lamps/discharge-lamps_pl.jar"
sign-jar ./discharge-lamps/discharge-lamps_pl.jar
echo "signing ./discharge-lamps/discharge-lamps_sv.jar"
sign-jar ./discharge-lamps/discharge-lamps_sv.jar
echo "signing ./discharge-lamps/discharge-lamps_fi.jar"
sign-jar ./discharge-lamps/discharge-lamps_fi.jar
echo "signing ./discharge-lamps/discharge-lamps_uk.jar"
sign-jar ./discharge-lamps/discharge-lamps_uk.jar
echo "signing ./discharge-lamps/discharge-lamps_th.jar"
sign-jar ./discharge-lamps/discharge-lamps_th.jar
echo "signing ./discharge-lamps/discharge-lamps_ku_TR.jar"
sign-jar ./discharge-lamps/discharge-lamps_ku_TR.jar
echo "signing ./discharge-lamps/discharge-lamps_en.jar"
sign-jar ./discharge-lamps/discharge-lamps_en.jar
echo "signing ./discharge-lamps/discharge-lamps_all_installer.jar"
sign-jar ./discharge-lamps/discharge-lamps_all_installer.jar
echo "signing ./discharge-lamps/discharge-lamps_be.jar"
sign-jar ./discharge-lamps/discharge-lamps_be.jar
echo "signing ./discharge-lamps/discharge-lamps_kk.jar"
sign-jar ./discharge-lamps/discharge-lamps_kk.jar
echo "signing ./discharge-lamps/discharge-lamps_eu.jar"
sign-jar ./discharge-lamps/discharge-lamps_eu.jar
echo "signing ./discharge-lamps/discharge-lamps_bs.jar"
sign-jar ./discharge-lamps/discharge-lamps_bs.jar
echo "signing ./discharge-lamps/discharge-lamps_es_PE.jar"
sign-jar ./discharge-lamps/discharge-lamps_es_PE.jar
echo "signing ./test-project/sim1_en.jar"
sign-jar ./test-project/sim1_en.jar
echo "signing ./test-project/sim1_zh_TW.jar"
sign-jar ./test-project/sim1_zh_TW.jar
echo "signing ./test-project/sim2_ak.jar"
sign-jar ./test-project/sim2_ak.jar
echo "signing ./test-project/sim1_km.jar"
sign-jar ./test-project/sim1_km.jar
echo "signing ./test-project/sim1_ak.jar"
sign-jar ./test-project/sim1_ak.jar
echo "signing ./test-project/sim2_km.jar"
sign-jar ./test-project/sim2_km.jar
echo "signing ./test-project/sim2_hu.jar"
sign-jar ./test-project/sim2_hu.jar
echo "signing ./test-project/test-project_all_installer.jar"
sign-jar ./test-project/test-project_all_installer.jar
echo "signing ./test-project/sim2_zh_TW.jar"
sign-jar ./test-project/sim2_zh_TW.jar
echo "signing ./test-project/sim2_el.jar"
sign-jar ./test-project/sim2_el.jar
echo "signing ./test-project/sim1_hu.jar"
sign-jar ./test-project/sim1_hu.jar
echo "signing ./test-project/sim2_en.jar"
sign-jar ./test-project/sim2_en.jar
echo "signing ./test-project/test-project_all.jar"
sign-jar ./test-project/test-project_all.jar
echo "signing ./test-project/sim1_el.jar"
sign-jar ./test-project/sim1_el.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_pt_BR.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_pt_BR.jar
echo "signing ./ideal-gas/reversible-reactions_el.jar"
sign-jar ./ideal-gas/reversible-reactions_el.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_in.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_in.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_iw.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_iw.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_el.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_el.jar
echo "signing ./ideal-gas/gas-properties_zh_TW.jar"
sign-jar ./ideal-gas/gas-properties_zh_TW.jar
echo "signing ./ideal-gas/gas-properties_lo.jar"
sign-jar ./ideal-gas/gas-properties_lo.jar
echo "signing ./ideal-gas/ideal-gas_all.jar"
sign-jar ./ideal-gas/ideal-gas_all.jar
echo "signing ./ideal-gas/reversible-reactions_nb.jar"
sign-jar ./ideal-gas/reversible-reactions_nb.jar
echo "signing ./ideal-gas/ideal-gas_all_installer.jar"
sign-jar ./ideal-gas/ideal-gas_all_installer.jar
echo "signing ./ideal-gas/reversible-reactions_kn.jar"
sign-jar ./ideal-gas/reversible-reactions_kn.jar
echo "signing ./ideal-gas/gas-properties_fr.jar"
sign-jar ./ideal-gas/gas-properties_fr.jar
echo "signing ./ideal-gas/reversible-reactions_ar_SA.jar"
sign-jar ./ideal-gas/reversible-reactions_ar_SA.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_nl.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_nl.jar
echo "signing ./ideal-gas/gas-properties_nb.jar"
sign-jar ./ideal-gas/gas-properties_nb.jar
echo "signing ./ideal-gas/gas-properties_ka.jar"
sign-jar ./ideal-gas/gas-properties_ka.jar
echo "signing ./ideal-gas/gas-properties_kn.jar"
sign-jar ./ideal-gas/gas-properties_kn.jar
echo "signing ./ideal-gas/gas-properties_km.jar"
sign-jar ./ideal-gas/gas-properties_km.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_cs.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_cs.jar
echo "signing ./ideal-gas/reversible-reactions_eu.jar"
sign-jar ./ideal-gas/reversible-reactions_eu.jar
echo "signing ./ideal-gas/reversible-reactions_zh_TW.jar"
sign-jar ./ideal-gas/reversible-reactions_zh_TW.jar
echo "signing ./ideal-gas/gas-properties_mi.jar"
sign-jar ./ideal-gas/gas-properties_mi.jar
echo "signing ./ideal-gas/gas-properties_ar.jar"
sign-jar ./ideal-gas/gas-properties_ar.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_be.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_be.jar
echo "signing ./ideal-gas/gas-properties_eu.jar"
sign-jar ./ideal-gas/gas-properties_eu.jar
echo "signing ./ideal-gas/reversible-reactions_sv.jar"
sign-jar ./ideal-gas/reversible-reactions_sv.jar
echo "signing ./ideal-gas/gas-properties_es.jar"
sign-jar ./ideal-gas/gas-properties_es.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_kn.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_kn.jar
echo "signing ./ideal-gas/reversible-reactions_iw.jar"
sign-jar ./ideal-gas/reversible-reactions_iw.jar
echo "signing ./ideal-gas/gas-properties_mk.jar"
sign-jar ./ideal-gas/gas-properties_mk.jar
echo "signing ./ideal-gas/gas-properties_pt_BR.jar"
sign-jar ./ideal-gas/gas-properties_pt_BR.jar
echo "signing ./ideal-gas/reversible-reactions_fa.jar"
sign-jar ./ideal-gas/reversible-reactions_fa.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_zh_TW.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_zh_TW.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_tr.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_tr.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_kk.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_kk.jar
echo "signing ./ideal-gas/gas-properties_nl.jar"
sign-jar ./ideal-gas/gas-properties_nl.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_hy.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_hy.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_fr.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_fr.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ht.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ht.jar
echo "signing ./ideal-gas/reversible-reactions_kk.jar"
sign-jar ./ideal-gas/reversible-reactions_kk.jar
echo "signing ./ideal-gas/reversible-reactions_ar.jar"
sign-jar ./ideal-gas/reversible-reactions_ar.jar
echo "signing ./ideal-gas/reversible-reactions_lo.jar"
sign-jar ./ideal-gas/reversible-reactions_lo.jar
echo "signing ./ideal-gas/gas-properties_tk.jar"
sign-jar ./ideal-gas/gas-properties_tk.jar
echo "signing ./ideal-gas/gas-properties_de.jar"
sign-jar ./ideal-gas/gas-properties_de.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ka.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ka.jar
echo "signing ./ideal-gas/reversible-reactions_vi.jar"
sign-jar ./ideal-gas/reversible-reactions_vi.jar
echo "signing ./ideal-gas/reversible-reactions_km.jar"
sign-jar ./ideal-gas/reversible-reactions_km.jar
echo "signing ./ideal-gas/gas-properties_mr.jar"
sign-jar ./ideal-gas/gas-properties_mr.jar
echo "signing ./ideal-gas/reversible-reactions_mo.jar"
sign-jar ./ideal-gas/reversible-reactions_mo.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ja.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ja.jar
echo "signing ./ideal-gas/reversible-reactions_ca.jar"
sign-jar ./ideal-gas/reversible-reactions_ca.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ko.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ko.jar
echo "signing ./ideal-gas/gas-properties_gl.jar"
sign-jar ./ideal-gas/gas-properties_gl.jar
echo "signing ./ideal-gas/gas-properties_lv.jar"
sign-jar ./ideal-gas/gas-properties_lv.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_es.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_es.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_mr.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_mr.jar
echo "signing ./ideal-gas/reversible-reactions_et.jar"
sign-jar ./ideal-gas/reversible-reactions_et.jar
echo "signing ./ideal-gas/reversible-reactions_cs.jar"
sign-jar ./ideal-gas/reversible-reactions_cs.jar
echo "signing ./ideal-gas/gas-properties_fa.jar"
sign-jar ./ideal-gas/gas-properties_fa.jar
echo "signing ./ideal-gas/reversible-reactions_mr.jar"
sign-jar ./ideal-gas/reversible-reactions_mr.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_gl.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_gl.jar
echo "signing ./ideal-gas/gas-properties_fi.jar"
sign-jar ./ideal-gas/gas-properties_fi.jar
echo "signing ./ideal-gas/reversible-reactions_de.jar"
sign-jar ./ideal-gas/reversible-reactions_de.jar
echo "signing ./ideal-gas/gas-properties_tr.jar"
sign-jar ./ideal-gas/gas-properties_tr.jar
echo "signing ./ideal-gas/gas-properties_ko.jar"
sign-jar ./ideal-gas/gas-properties_ko.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_nb.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_nb.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ca.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ca.jar
echo "signing ./ideal-gas/gas-properties_mo.jar"
sign-jar ./ideal-gas/gas-properties_mo.jar
echo "signing ./ideal-gas/reversible-reactions_sq.jar"
sign-jar ./ideal-gas/reversible-reactions_sq.jar
echo "signing ./ideal-gas/reversible-reactions_ht.jar"
sign-jar ./ideal-gas/reversible-reactions_ht.jar
echo "signing ./ideal-gas/gas-properties_hr.jar"
sign-jar ./ideal-gas/gas-properties_hr.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_et.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_et.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_fi.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_fi.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_da.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_da.jar
echo "signing ./ideal-gas/reversible-reactions_it.jar"
sign-jar ./ideal-gas/reversible-reactions_it.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_mo.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_mo.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_sk.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_sk.jar
echo "signing ./ideal-gas/reversible-reactions_be.jar"
sign-jar ./ideal-gas/reversible-reactions_be.jar
echo "signing ./ideal-gas/gas-properties_ru.jar"
sign-jar ./ideal-gas/gas-properties_ru.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_vi.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_vi.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_lv.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_lv.jar
echo "signing ./ideal-gas/reversible-reactions_hr.jar"
sign-jar ./ideal-gas/reversible-reactions_hr.jar
echo "signing ./ideal-gas/reversible-reactions_ko.jar"
sign-jar ./ideal-gas/reversible-reactions_ko.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_pt.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_pt.jar
echo "signing ./ideal-gas/reversible-reactions_tk.jar"
sign-jar ./ideal-gas/reversible-reactions_tk.jar
echo "signing ./ideal-gas/gas-properties_sl.jar"
sign-jar ./ideal-gas/gas-properties_sl.jar
echo "signing ./ideal-gas/reversible-reactions_mn.jar"
sign-jar ./ideal-gas/reversible-reactions_mn.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_fa.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_fa.jar
echo "signing ./ideal-gas/gas-properties_sv.jar"
sign-jar ./ideal-gas/gas-properties_sv.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_th.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_th.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_mk.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_mk.jar
echo "signing ./ideal-gas/gas-properties_ht.jar"
sign-jar ./ideal-gas/gas-properties_ht.jar
echo "signing ./ideal-gas/gas-properties_tn.jar"
sign-jar ./ideal-gas/gas-properties_tn.jar
echo "signing ./ideal-gas/gas-properties_bs.jar"
sign-jar ./ideal-gas/gas-properties_bs.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_sr.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_sr.jar
echo "signing ./ideal-gas/reversible-reactions_th.jar"
sign-jar ./ideal-gas/reversible-reactions_th.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_lo.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_lo.jar
echo "signing ./ideal-gas/gas-properties_cs.jar"
sign-jar ./ideal-gas/gas-properties_cs.jar
echo "signing ./ideal-gas/gas-properties_sr.jar"
sign-jar ./ideal-gas/gas-properties_sr.jar
echo "signing ./ideal-gas/reversible-reactions_en.jar"
sign-jar ./ideal-gas/reversible-reactions_en.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ar_SA.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ar_SA.jar
echo "signing ./ideal-gas/gas-properties_hu.jar"
sign-jar ./ideal-gas/gas-properties_hu.jar
echo "signing ./ideal-gas/reversible-reactions_ka.jar"
sign-jar ./ideal-gas/reversible-reactions_ka.jar
echo "signing ./ideal-gas/reversible-reactions_pt.jar"
sign-jar ./ideal-gas/reversible-reactions_pt.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_hu.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_hu.jar
echo "signing ./ideal-gas/reversible-reactions_hu.jar"
sign-jar ./ideal-gas/reversible-reactions_hu.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_eu.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_eu.jar
echo "signing ./ideal-gas/reversible-reactions_sl.jar"
sign-jar ./ideal-gas/reversible-reactions_sl.jar
echo "signing ./ideal-gas/reversible-reactions_sr.jar"
sign-jar ./ideal-gas/reversible-reactions_sr.jar
echo "signing ./ideal-gas/gas-properties_en.jar"
sign-jar ./ideal-gas/gas-properties_en.jar
echo "signing ./ideal-gas/reversible-reactions_ru.jar"
sign-jar ./ideal-gas/reversible-reactions_ru.jar
echo "signing ./ideal-gas/reversible-reactions_tn.jar"
sign-jar ./ideal-gas/reversible-reactions_tn.jar
echo "signing ./ideal-gas/reversible-reactions_nl.jar"
sign-jar ./ideal-gas/reversible-reactions_nl.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_zh_CN.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_zh_CN.jar
echo "signing ./ideal-gas/gas-properties_in.jar"
sign-jar ./ideal-gas/gas-properties_in.jar
echo "signing ./ideal-gas/reversible-reactions_lv.jar"
sign-jar ./ideal-gas/reversible-reactions_lv.jar
echo "signing ./ideal-gas/reversible-reactions_pt_BR.jar"
sign-jar ./ideal-gas/reversible-reactions_pt_BR.jar
echo "signing ./ideal-gas/gas-properties_et.jar"
sign-jar ./ideal-gas/gas-properties_et.jar
echo "signing ./ideal-gas/reversible-reactions_in.jar"
sign-jar ./ideal-gas/reversible-reactions_in.jar
echo "signing ./ideal-gas/gas-properties_be.jar"
sign-jar ./ideal-gas/gas-properties_be.jar
echo "signing ./ideal-gas/reversible-reactions_da.jar"
sign-jar ./ideal-gas/reversible-reactions_da.jar
echo "signing ./ideal-gas/gas-properties_th.jar"
sign-jar ./ideal-gas/gas-properties_th.jar
echo "signing ./ideal-gas/reversible-reactions_ja.jar"
sign-jar ./ideal-gas/reversible-reactions_ja.jar
echo "signing ./ideal-gas/reversible-reactions_bs.jar"
sign-jar ./ideal-gas/reversible-reactions_bs.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_tn.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_tn.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_mi.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_mi.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_sv.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_sv.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_km.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_km.jar
echo "signing ./ideal-gas/reversible-reactions_sk.jar"
sign-jar ./ideal-gas/reversible-reactions_sk.jar
echo "signing ./ideal-gas/gas-properties_it.jar"
sign-jar ./ideal-gas/gas-properties_it.jar
echo "signing ./ideal-gas/gas-properties_ar_SA.jar"
sign-jar ./ideal-gas/gas-properties_ar_SA.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_bs.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_bs.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_sl.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_sl.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ar.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ar.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_hr.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_hr.jar
echo "signing ./ideal-gas/reversible-reactions_mi.jar"
sign-jar ./ideal-gas/reversible-reactions_mi.jar
echo "signing ./ideal-gas/gas-properties_hy.jar"
sign-jar ./ideal-gas/gas-properties_hy.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_tk.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_tk.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_pl.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_pl.jar
echo "signing ./ideal-gas/reversible-reactions_fr.jar"
sign-jar ./ideal-gas/reversible-reactions_fr.jar
echo "signing ./ideal-gas/gas-properties_ca.jar"
sign-jar ./ideal-gas/gas-properties_ca.jar
echo "signing ./ideal-gas/gas-properties_es_PE.jar"
sign-jar ./ideal-gas/gas-properties_es_PE.jar
echo "signing ./ideal-gas/reversible-reactions_fi.jar"
sign-jar ./ideal-gas/reversible-reactions_fi.jar
echo "signing ./ideal-gas/reversible-reactions_hy.jar"
sign-jar ./ideal-gas/reversible-reactions_hy.jar
echo "signing ./ideal-gas/gas-properties_sk.jar"
sign-jar ./ideal-gas/gas-properties_sk.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_mn.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_mn.jar
echo "signing ./ideal-gas/gas-properties_pl.jar"
sign-jar ./ideal-gas/gas-properties_pl.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_en.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_en.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_de.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_de.jar
echo "signing ./ideal-gas/reversible-reactions_ro.jar"
sign-jar ./ideal-gas/reversible-reactions_ro.jar
echo "signing ./ideal-gas/reversible-reactions_gl.jar"
sign-jar ./ideal-gas/reversible-reactions_gl.jar
echo "signing ./ideal-gas/gas-properties_sq.jar"
sign-jar ./ideal-gas/gas-properties_sq.jar
echo "signing ./ideal-gas/gas-properties_iw.jar"
sign-jar ./ideal-gas/gas-properties_iw.jar
echo "signing ./ideal-gas/reversible-reactions_zh_CN.jar"
sign-jar ./ideal-gas/reversible-reactions_zh_CN.jar
echo "signing ./ideal-gas/gas-properties_ro.jar"
sign-jar ./ideal-gas/gas-properties_ro.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ro.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ro.jar
echo "signing ./ideal-gas/reversible-reactions_tr.jar"
sign-jar ./ideal-gas/reversible-reactions_tr.jar
echo "signing ./ideal-gas/gas-properties_ja.jar"
sign-jar ./ideal-gas/gas-properties_ja.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_ru.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_ru.jar
echo "signing ./ideal-gas/gas-properties_pt.jar"
sign-jar ./ideal-gas/gas-properties_pt.jar
echo "signing ./ideal-gas/gas-properties_mn.jar"
sign-jar ./ideal-gas/gas-properties_mn.jar
echo "signing ./ideal-gas/gas-properties_kk.jar"
sign-jar ./ideal-gas/gas-properties_kk.jar
echo "signing ./ideal-gas/reversible-reactions_es_PE.jar"
sign-jar ./ideal-gas/reversible-reactions_es_PE.jar
echo "signing ./ideal-gas/gas-properties_zh_CN.jar"
sign-jar ./ideal-gas/gas-properties_zh_CN.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_sq.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_sq.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_it.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_it.jar
echo "signing ./ideal-gas/reversible-reactions_mk.jar"
sign-jar ./ideal-gas/reversible-reactions_mk.jar
echo "signing ./ideal-gas/gas-properties_el.jar"
sign-jar ./ideal-gas/gas-properties_el.jar
echo "signing ./ideal-gas/reversible-reactions_es.jar"
sign-jar ./ideal-gas/reversible-reactions_es.jar
echo "signing ./ideal-gas/reversible-reactions_pl.jar"
sign-jar ./ideal-gas/reversible-reactions_pl.jar
echo "signing ./ideal-gas/balloons-and-buoyancy_es_PE.jar"
sign-jar ./ideal-gas/balloons-and-buoyancy_es_PE.jar
echo "signing ./ideal-gas/gas-properties_vi.jar"
sign-jar ./ideal-gas/gas-properties_vi.jar
echo "signing ./ideal-gas/gas-properties_da.jar"
sign-jar ./ideal-gas/gas-properties_da.jar
echo "signing ./pendulum-lab/pendulum-lab_en.jar"
sign-jar ./pendulum-lab/pendulum-lab_en.jar
echo "signing ./pendulum-lab/pendulum-lab_fa.jar"
sign-jar ./pendulum-lab/pendulum-lab_fa.jar
echo "signing ./pendulum-lab/pendulum-lab_ca.jar"
sign-jar ./pendulum-lab/pendulum-lab_ca.jar
echo "signing ./pendulum-lab/pendulum-lab_tr.jar"
sign-jar ./pendulum-lab/pendulum-lab_tr.jar
echo "signing ./pendulum-lab/pendulum-lab_es.jar"
sign-jar ./pendulum-lab/pendulum-lab_es.jar
echo "signing ./pendulum-lab/pendulum-lab_da.jar"
sign-jar ./pendulum-lab/pendulum-lab_da.jar
echo "signing ./pendulum-lab/pendulum-lab_ru.jar"
sign-jar ./pendulum-lab/pendulum-lab_ru.jar
echo "signing ./pendulum-lab/pendulum-lab_ht.jar"
sign-jar ./pendulum-lab/pendulum-lab_ht.jar
echo "signing ./pendulum-lab/pendulum-lab_kk.jar"
sign-jar ./pendulum-lab/pendulum-lab_kk.jar
echo "signing ./pendulum-lab/pendulum-lab_uk.jar"
sign-jar ./pendulum-lab/pendulum-lab_uk.jar
echo "signing ./pendulum-lab/pendulum-lab_pl.jar"
sign-jar ./pendulum-lab/pendulum-lab_pl.jar
echo "signing ./pendulum-lab/pendulum-lab_ku_TR.jar"
sign-jar ./pendulum-lab/pendulum-lab_ku_TR.jar
echo "signing ./pendulum-lab/pendulum-lab_vi.jar"
sign-jar ./pendulum-lab/pendulum-lab_vi.jar
echo "signing ./pendulum-lab/pendulum-lab_cs.jar"
sign-jar ./pendulum-lab/pendulum-lab_cs.jar
echo "signing ./pendulum-lab/pendulum-lab_hr.jar"
sign-jar ./pendulum-lab/pendulum-lab_hr.jar
echo "signing ./pendulum-lab/pendulum-lab_ka.jar"
sign-jar ./pendulum-lab/pendulum-lab_ka.jar
echo "signing ./pendulum-lab/pendulum-lab_et.jar"
sign-jar ./pendulum-lab/pendulum-lab_et.jar
echo "signing ./pendulum-lab/pendulum-lab_sk.jar"
sign-jar ./pendulum-lab/pendulum-lab_sk.jar
echo "signing ./pendulum-lab/pendulum-lab_nl.jar"
sign-jar ./pendulum-lab/pendulum-lab_nl.jar
echo "signing ./pendulum-lab/pendulum-lab_fr.jar"
sign-jar ./pendulum-lab/pendulum-lab_fr.jar
echo "signing ./pendulum-lab/pendulum-lab_ta.jar"
sign-jar ./pendulum-lab/pendulum-lab_ta.jar
echo "signing ./pendulum-lab/pendulum-lab_iw.jar"
sign-jar ./pendulum-lab/pendulum-lab_iw.jar
echo "signing ./pendulum-lab/pendulum-lab_sv.jar"
sign-jar ./pendulum-lab/pendulum-lab_sv.jar
echo "signing ./pendulum-lab/pendulum-lab_tk.jar"
sign-jar ./pendulum-lab/pendulum-lab_tk.jar
echo "signing ./pendulum-lab/pendulum-lab_fi.jar"
sign-jar ./pendulum-lab/pendulum-lab_fi.jar
echo "signing ./pendulum-lab/pendulum-lab_eu.jar"
sign-jar ./pendulum-lab/pendulum-lab_eu.jar
echo "signing ./pendulum-lab/pendulum-lab_mn.jar"
sign-jar ./pendulum-lab/pendulum-lab_mn.jar
echo "signing ./pendulum-lab/pendulum-lab_kn.jar"
sign-jar ./pendulum-lab/pendulum-lab_kn.jar
echo "signing ./pendulum-lab/pendulum-lab_ko.jar"
sign-jar ./pendulum-lab/pendulum-lab_ko.jar
echo "signing ./pendulum-lab/pendulum-lab_sl.jar"
sign-jar ./pendulum-lab/pendulum-lab_sl.jar
echo "signing ./pendulum-lab/pendulum-lab_lv.jar"
sign-jar ./pendulum-lab/pendulum-lab_lv.jar
echo "signing ./pendulum-lab/pendulum-lab_bn.jar"
sign-jar ./pendulum-lab/pendulum-lab_bn.jar
echo "signing ./pendulum-lab/pendulum-lab_sr.jar"
sign-jar ./pendulum-lab/pendulum-lab_sr.jar
echo "signing ./pendulum-lab/pendulum-lab_ms.jar"
sign-jar ./pendulum-lab/pendulum-lab_ms.jar
echo "signing ./pendulum-lab/pendulum-lab_hu.jar"
sign-jar ./pendulum-lab/pendulum-lab_hu.jar
echo "signing ./pendulum-lab/pendulum-lab_ar.jar"
sign-jar ./pendulum-lab/pendulum-lab_ar.jar
echo "signing ./pendulum-lab/pendulum-lab_it.jar"
sign-jar ./pendulum-lab/pendulum-lab_it.jar
echo "signing ./pendulum-lab/pendulum-lab_el.jar"
sign-jar ./pendulum-lab/pendulum-lab_el.jar
echo "signing ./pendulum-lab/pendulum-lab_zh_CN.jar"
sign-jar ./pendulum-lab/pendulum-lab_zh_CN.jar
echo "signing ./pendulum-lab/pendulum-lab_de.jar"
sign-jar ./pendulum-lab/pendulum-lab_de.jar
echo "signing ./pendulum-lab/pendulum-lab_gl.jar"
sign-jar ./pendulum-lab/pendulum-lab_gl.jar
echo "signing ./pendulum-lab/pendulum-lab_bs.jar"
sign-jar ./pendulum-lab/pendulum-lab_bs.jar
echo "signing ./pendulum-lab/pendulum-lab_km.jar"
sign-jar ./pendulum-lab/pendulum-lab_km.jar
echo "signing ./pendulum-lab/pendulum-lab_ja.jar"
sign-jar ./pendulum-lab/pendulum-lab_ja.jar
echo "signing ./pendulum-lab/pendulum-lab_ku.jar"
sign-jar ./pendulum-lab/pendulum-lab_ku.jar
echo "signing ./pendulum-lab/pendulum-lab_lt.jar"
sign-jar ./pendulum-lab/pendulum-lab_lt.jar
echo "signing ./pendulum-lab/pendulum-lab_zh_TW.jar"
sign-jar ./pendulum-lab/pendulum-lab_zh_TW.jar
echo "signing ./pendulum-lab/pendulum-lab_pt_BR.jar"
sign-jar ./pendulum-lab/pendulum-lab_pt_BR.jar
echo "signing ./pendulum-lab/pendulum-lab_in.jar"
sign-jar ./pendulum-lab/pendulum-lab_in.jar
echo "signing ./pendulum-lab/pendulum-lab_mk.jar"
sign-jar ./pendulum-lab/pendulum-lab_mk.jar
echo "signing ./pendulum-lab/pendulum-lab_ar_SA.jar"
sign-jar ./pendulum-lab/pendulum-lab_ar_SA.jar
echo "signing ./pendulum-lab/pendulum-lab_be.jar"
sign-jar ./pendulum-lab/pendulum-lab_be.jar
echo "signing ./pendulum-lab/pendulum-lab_th.jar"
sign-jar ./pendulum-lab/pendulum-lab_th.jar
echo "signing ./pendulum-lab/pendulum-lab_es_PE.jar"
sign-jar ./pendulum-lab/pendulum-lab_es_PE.jar
echo "signing ./pendulum-lab/pendulum-lab_nb.jar"
sign-jar ./pendulum-lab/pendulum-lab_nb.jar
echo "signing ./sim-template/sim-template_all_installer.jar"
sign-jar ./sim-template/sim-template_all_installer.jar
echo "signing ./sim-template/sim-template_en.jar"
sign-jar ./sim-template/sim-template_en.jar
echo "signing ./sim-template/sim-template_all.jar"
sign-jar ./sim-template/sim-template_all.jar
echo "signing ./gene-expression-basics/gene-expression-basics_tk.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_tk.jar
echo "signing ./gene-expression-basics/gene-expression-basics_fr.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_fr.jar
echo "signing ./gene-expression-basics/gene-expression-basics_el.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_el.jar
echo "signing ./gene-expression-basics/gene-expression-basics_kk.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_kk.jar
echo "signing ./gene-expression-basics/gene-expression-basics_vi.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_vi.jar
echo "signing ./gene-expression-basics/gene-expression-basics_mr.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_mr.jar
echo "signing ./gene-expression-basics/gene-expression-basics_sr.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_sr.jar
echo "signing ./gene-expression-basics/gene-expression-basics_da.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_da.jar
echo "signing ./gene-expression-basics/gene-expression-basics_th.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_th.jar
echo "signing ./gene-expression-basics/gene-expression-basics_mk.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_mk.jar
echo "signing ./gene-expression-basics/gene-expression-basics_ta.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_ta.jar
echo "signing ./gene-expression-basics/gene-expression-basics_ja.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_ja.jar
echo "signing ./gene-expression-basics/gene-expression-basics_hu.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_hu.jar
echo "signing ./gene-expression-basics/gene-expression-basics_es_PE.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_es_PE.jar
echo "signing ./gene-expression-basics/gene-expression-basics_all_installer.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_all_installer.jar
echo "signing ./gene-expression-basics/gene-expression-basics_pt_BR.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_pt_BR.jar
echo "signing ./gene-expression-basics/gene-expression-basics_ko.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_ko.jar
echo "signing ./gene-expression-basics/gene-expression-basics_es_ES.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_es_ES.jar
echo "signing ./gene-expression-basics/gene-expression-basics_pl.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_pl.jar
echo "signing ./gene-expression-basics/gene-expression-basics_zh_TW.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_zh_TW.jar
echo "signing ./gene-expression-basics/gene-expression-basics_zh_CN.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_zh_CN.jar
echo "signing ./gene-expression-basics/gene-expression-basics_bs.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_bs.jar
echo "signing ./gene-expression-basics/gene-expression-basics_en.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_en.jar
echo "signing ./gene-expression-basics/gene-expression-basics_tr.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_tr.jar
echo "signing ./gene-expression-basics/gene-expression-basics_all.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_all.jar
echo "signing ./gene-expression-basics/gene-expression-basics_it.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_it.jar
echo "signing ./gene-expression-basics/gene-expression-basics_sv.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_sv.jar
echo "signing ./gene-expression-basics/gene-expression-basics_de.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_de.jar
echo "signing ./gene-expression-basics/gene-expression-basics_nn.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_nn.jar
echo "signing ./gene-expression-basics/gene-expression-basics_nb.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_nb.jar
echo "signing ./gene-expression-basics/gene-expression-basics_iw.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_iw.jar
echo "signing ./gene-expression-basics/gene-expression-basics_eu.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_eu.jar
echo "signing ./gene-expression-basics/gene-expression-basics_fa.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_fa.jar
echo "signing ./gene-expression-basics/gene-expression-basics_sq.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_sq.jar
echo "signing ./gene-expression-basics/gene-expression-basics_es.jar"
sign-jar ./gene-expression-basics/gene-expression-basics_es.jar
echo "signing ./resonance/resonance_nl.jar"
sign-jar ./resonance/resonance_nl.jar
echo "signing ./resonance/resonance_bs.jar"
sign-jar ./resonance/resonance_bs.jar
echo "signing ./resonance/resonance_sr.jar"
sign-jar ./resonance/resonance_sr.jar
echo "signing ./resonance/resonance_pt_BR.jar"
sign-jar ./resonance/resonance_pt_BR.jar
echo "signing ./resonance/resonance_iw.jar"
sign-jar ./resonance/resonance_iw.jar
echo "signing ./resonance/resonance_de.jar"
sign-jar ./resonance/resonance_de.jar
echo "signing ./resonance/resonance_sk.jar"
sign-jar ./resonance/resonance_sk.jar
echo "signing ./resonance/resonance_vi.jar"
sign-jar ./resonance/resonance_vi.jar
echo "signing ./resonance/resonance_en.jar"
sign-jar ./resonance/resonance_en.jar
echo "signing ./resonance/resonance_hu.jar"
sign-jar ./resonance/resonance_hu.jar
echo "signing ./resonance/resonance_fr.jar"
sign-jar ./resonance/resonance_fr.jar
echo "signing ./resonance/resonance_ko.jar"
sign-jar ./resonance/resonance_ko.jar
echo "signing ./resonance/resonance_eu.jar"
sign-jar ./resonance/resonance_eu.jar
echo "signing ./resonance/resonance_mk.jar"
sign-jar ./resonance/resonance_mk.jar
echo "signing ./resonance/resonance_kn.jar"
sign-jar ./resonance/resonance_kn.jar
echo "signing ./resonance/resonance_be.jar"
sign-jar ./resonance/resonance_be.jar
echo "signing ./resonance/resonance_fa.jar"
sign-jar ./resonance/resonance_fa.jar
echo "signing ./resonance/resonance_zh_TW.jar"
sign-jar ./resonance/resonance_zh_TW.jar
echo "signing ./resonance/resonance_ja.jar"
sign-jar ./resonance/resonance_ja.jar
echo "signing ./resonance/resonance_kk.jar"
sign-jar ./resonance/resonance_kk.jar
echo "signing ./resonance/resonance_tk.jar"
sign-jar ./resonance/resonance_tk.jar
echo "signing ./resonance/resonance_zh_CN.jar"
sign-jar ./resonance/resonance_zh_CN.jar
echo "signing ./resonance/resonance_pl.jar"
sign-jar ./resonance/resonance_pl.jar
echo "signing ./resonance/resonance_tr.jar"
sign-jar ./resonance/resonance_tr.jar
echo "signing ./resonance/resonance_cs.jar"
sign-jar ./resonance/resonance_cs.jar
echo "signing ./resonance/resonance_el.jar"
sign-jar ./resonance/resonance_el.jar
echo "signing ./resonance/resonance_sv.jar"
sign-jar ./resonance/resonance_sv.jar
echo "signing ./resonance/resonance_th.jar"
sign-jar ./resonance/resonance_th.jar
echo "signing ./resonance/resonance_es_PE.jar"
sign-jar ./resonance/resonance_es_PE.jar
echo "signing ./resonance/resonance_it.jar"
sign-jar ./resonance/resonance_it.jar
echo "signing ./resonance/resonance_ku_TR.jar"
sign-jar ./resonance/resonance_ku_TR.jar
echo "signing ./resonance/resonance_es.jar"
sign-jar ./resonance/resonance_es.jar
echo "signing ./resonance/resonance_da.jar"
sign-jar ./resonance/resonance_da.jar
echo "signing ./resonance/resonance_ar_SA.jar"
sign-jar ./resonance/resonance_ar_SA.jar
echo "signing ./resonance/resonance_in.jar"
sign-jar ./resonance/resonance_in.jar
echo "signing ./mass-spring-lab/mass-spring-lab_iw.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_iw.jar
echo "signing ./mass-spring-lab/mass-spring-lab_pl.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_pl.jar
echo "signing ./mass-spring-lab/mass-spring-lab_mk.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_mk.jar
echo "signing ./mass-spring-lab/mass-spring-lab_tr.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_tr.jar
echo "signing ./mass-spring-lab/mass-spring-lab_th.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_th.jar
echo "signing ./mass-spring-lab/mass-spring-lab_lv.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_lv.jar
echo "signing ./mass-spring-lab/mass-spring-lab_zh_CN.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_zh_CN.jar
echo "signing ./mass-spring-lab/mass-spring-lab_es_PE.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_es_PE.jar
echo "signing ./mass-spring-lab/mass-spring-lab_nl.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_nl.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ku_TR.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ku_TR.jar
echo "signing ./mass-spring-lab/mass-spring-lab_sk.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_sk.jar
echo "signing ./mass-spring-lab/mass-spring-lab_fi.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_fi.jar
echo "signing ./mass-spring-lab/mass-spring-lab_de.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_de.jar
echo "signing ./mass-spring-lab/mass-spring-lab_nb.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_nb.jar
echo "signing ./mass-spring-lab/mass-spring-lab_eu.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_eu.jar
echo "signing ./mass-spring-lab/mass-spring-lab_es.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_es.jar
echo "signing ./mass-spring-lab/mass-spring-lab_sl.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_sl.jar
echo "signing ./mass-spring-lab/mass-spring-lab_da.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_da.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ar.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ar.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ru.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ru.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ar_SA.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ar_SA.jar
echo "signing ./mass-spring-lab/mass-spring-lab_cs.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_cs.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ka.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ka.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ku.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ku.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ht.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ht.jar
echo "signing ./mass-spring-lab/mass-spring-lab_es_CO.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_es_CO.jar
echo "signing ./mass-spring-lab/mass-spring-lab_hu.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_hu.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ms.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ms.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ta.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ta.jar
echo "signing ./mass-spring-lab/mass-spring-lab_bs.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_bs.jar
echo "signing ./mass-spring-lab/mass-spring-lab_vi.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_vi.jar
echo "signing ./mass-spring-lab/mass-spring-lab_be.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_be.jar
echo "signing ./mass-spring-lab/mass-spring-lab_sr.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_sr.jar
echo "signing ./mass-spring-lab/mass-spring-lab_et.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_et.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ko.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ko.jar
echo "signing ./mass-spring-lab/mass-spring-lab_sq.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_sq.jar
echo "signing ./mass-spring-lab/mass-spring-lab_gl.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_gl.jar
echo "signing ./mass-spring-lab/mass-spring-lab_sv.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_sv.jar
echo "signing ./mass-spring-lab/mass-spring-lab_el.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_el.jar
echo "signing ./mass-spring-lab/mass-spring-lab_uk.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_uk.jar
echo "signing ./mass-spring-lab/mass-spring-lab_fr.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_fr.jar
echo "signing ./mass-spring-lab/mass-spring-lab_ja.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_ja.jar
echo "signing ./mass-spring-lab/mass-spring-lab_tk.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_tk.jar
echo "signing ./mass-spring-lab/mass-spring-lab_en.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_en.jar
echo "signing ./mass-spring-lab/mass-spring-lab_km.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_km.jar
echo "signing ./mass-spring-lab/mass-spring-lab_fa.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_fa.jar
echo "signing ./mass-spring-lab/mass-spring-lab_hr.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_hr.jar
echo "signing ./mass-spring-lab/mass-spring-lab_zh_TW.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_zh_TW.jar
echo "signing ./mass-spring-lab/mass-spring-lab_mn.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_mn.jar
echo "signing ./mass-spring-lab/mass-spring-lab_it.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_it.jar
echo "signing ./mass-spring-lab/mass-spring-lab_pt_BR.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_pt_BR.jar
echo "signing ./mass-spring-lab/mass-spring-lab_in.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_in.jar
echo "signing ./mass-spring-lab/mass-spring-lab_nn.jar"
sign-jar ./mass-spring-lab/mass-spring-lab_nn.jar
echo "signing ./bending-light/bending-light_in.jar"
sign-jar ./bending-light/bending-light_in.jar
echo "signing ./bending-light/bending-light_pl.jar"
sign-jar ./bending-light/bending-light_pl.jar
echo "signing ./bending-light/bending-light_vi.jar"
sign-jar ./bending-light/bending-light_vi.jar
echo "signing ./bending-light/bending-light_sr.jar"
sign-jar ./bending-light/bending-light_sr.jar
echo "signing ./bending-light/bending-light_mr.jar"
sign-jar ./bending-light/bending-light_mr.jar
echo "signing ./bending-light/bending-light_ku.jar"
sign-jar ./bending-light/bending-light_ku.jar
echo "signing ./bending-light/bending-light_zh_TW.jar"
sign-jar ./bending-light/bending-light_zh_TW.jar
echo "signing ./bending-light/bending-light_it.jar"
sign-jar ./bending-light/bending-light_it.jar
echo "signing ./bending-light/bending-light_ro.jar"
sign-jar ./bending-light/bending-light_ro.jar
echo "signing ./bending-light/bending-light_en.jar"
sign-jar ./bending-light/bending-light_en.jar
echo "signing ./bending-light/bending-light_mk.jar"
sign-jar ./bending-light/bending-light_mk.jar
echo "signing ./bending-light/bending-light_ru.jar"
sign-jar ./bending-light/bending-light_ru.jar
echo "signing ./bending-light/bending-light_all_installer.jar"
sign-jar ./bending-light/bending-light_all_installer.jar
echo "signing ./bending-light/bending-light_es.jar"
sign-jar ./bending-light/bending-light_es.jar
echo "signing ./bending-light/bending-light_ko.jar"
sign-jar ./bending-light/bending-light_ko.jar
echo "signing ./bending-light/bending-light_gl.jar"
sign-jar ./bending-light/bending-light_gl.jar
echo "signing ./bending-light/bending-light_bg.jar"
sign-jar ./bending-light/bending-light_bg.jar
echo "signing ./bending-light/bending-light_el.jar"
sign-jar ./bending-light/bending-light_el.jar
echo "signing ./bending-light/bending-light_sk.jar"
sign-jar ./bending-light/bending-light_sk.jar
echo "signing ./bending-light/bending-light_sv.jar"
sign-jar ./bending-light/bending-light_sv.jar
echo "signing ./bending-light/bending-light_hu.jar"
sign-jar ./bending-light/bending-light_hu.jar
echo "signing ./bending-light/bending-light_nb.jar"
sign-jar ./bending-light/bending-light_nb.jar
echo "signing ./bending-light/bending-light_ja.jar"
sign-jar ./bending-light/bending-light_ja.jar
echo "signing ./bending-light/bending-light_iw.jar"
sign-jar ./bending-light/bending-light_iw.jar
echo "signing ./bending-light/bending-light_es_PE.jar"
sign-jar ./bending-light/bending-light_es_PE.jar
echo "signing ./bending-light/bending-light_eu.jar"
sign-jar ./bending-light/bending-light_eu.jar
echo "signing ./bending-light/bending-light_th.jar"
sign-jar ./bending-light/bending-light_th.jar
echo "signing ./bending-light/bending-light_fa.jar"
sign-jar ./bending-light/bending-light_fa.jar
echo "signing ./bending-light/bending-light_fr.jar"
sign-jar ./bending-light/bending-light_fr.jar
echo "signing ./bending-light/bending-light_nl.jar"
sign-jar ./bending-light/bending-light_nl.jar
echo "signing ./bending-light/bending-light_ar.jar"
sign-jar ./bending-light/bending-light_ar.jar
echo "signing ./bending-light/bending-light_pt.jar"
sign-jar ./bending-light/bending-light_pt.jar
echo "signing ./bending-light/bending-light_km.jar"
sign-jar ./bending-light/bending-light_km.jar
echo "signing ./bending-light/bending-light_be.jar"
sign-jar ./bending-light/bending-light_be.jar
echo "signing ./bending-light/bending-light_sl.jar"
sign-jar ./bending-light/bending-light_sl.jar
echo "signing ./bending-light/bending-light_de.jar"
sign-jar ./bending-light/bending-light_de.jar
echo "signing ./bending-light/bending-light_all.jar"
sign-jar ./bending-light/bending-light_all.jar
echo "signing ./bending-light/bending-light_tk.jar"
sign-jar ./bending-light/bending-light_tk.jar
echo "signing ./bending-light/bending-light_fi.jar"
sign-jar ./bending-light/bending-light_fi.jar
echo "signing ./bending-light/bending-light_uk.jar"
sign-jar ./bending-light/bending-light_uk.jar
echo "signing ./bending-light/bending-light_zh_CN.jar"
sign-jar ./bending-light/bending-light_zh_CN.jar
echo "signing ./bending-light/bending-light_kk.jar"
sign-jar ./bending-light/bending-light_kk.jar
echo "signing ./bending-light/bending-light_kn.jar"
sign-jar ./bending-light/bending-light_kn.jar
echo "signing ./bending-light/bending-light_da.jar"
sign-jar ./bending-light/bending-light_da.jar
echo "signing ./bending-light/bending-light_pt_BR.jar"
sign-jar ./bending-light/bending-light_pt_BR.jar
echo "signing ./bending-light/bending-light_tr.jar"
sign-jar ./bending-light/bending-light_tr.jar
echo "signing ./bending-light/bending-light_bs.jar"
sign-jar ./bending-light/bending-light_bs.jar
echo "signing ./bending-light/bending-light_lo.jar"
sign-jar ./bending-light/bending-light_lo.jar
echo "signing ./bending-light/bending-light_cs.jar"
sign-jar ./bending-light/bending-light_cs.jar
echo "signing ./bending-light/bending-light_ka.jar"
sign-jar ./bending-light/bending-light_ka.jar
echo "signing ./color-vision/color-vision_hu.jar"
sign-jar ./color-vision/color-vision_hu.jar
echo "signing ./color-vision/color-vision_cs.jar"
sign-jar ./color-vision/color-vision_cs.jar
echo "signing ./color-vision/color-vision_vi.jar"
sign-jar ./color-vision/color-vision_vi.jar
echo "signing ./color-vision/color-vision_bs.jar"
sign-jar ./color-vision/color-vision_bs.jar
echo "signing ./color-vision/color-vision_mr.jar"
sign-jar ./color-vision/color-vision_mr.jar
echo "signing ./color-vision/color-vision_fa.jar"
sign-jar ./color-vision/color-vision_fa.jar
echo "signing ./color-vision/color-vision_it.jar"
sign-jar ./color-vision/color-vision_it.jar
echo "signing ./color-vision/color-vision_ro.jar"
sign-jar ./color-vision/color-vision_ro.jar
echo "signing ./color-vision/color-vision_be.jar"
sign-jar ./color-vision/color-vision_be.jar
echo "signing ./color-vision/color-vision_sl.jar"
sign-jar ./color-vision/color-vision_sl.jar
echo "signing ./color-vision/color-vision_iw.jar"
sign-jar ./color-vision/color-vision_iw.jar
echo "signing ./color-vision/color-vision_lo.jar"
sign-jar ./color-vision/color-vision_lo.jar
echo "signing ./color-vision/color-vision_ar.jar"
sign-jar ./color-vision/color-vision_ar.jar
echo "signing ./color-vision/color-vision_ta.jar"
sign-jar ./color-vision/color-vision_ta.jar
echo "signing ./color-vision/color-vision_kn.jar"
sign-jar ./color-vision/color-vision_kn.jar
echo "signing ./color-vision/color-vision_ka.jar"
sign-jar ./color-vision/color-vision_ka.jar
echo "signing ./color-vision/color-vision_tk.jar"
sign-jar ./color-vision/color-vision_tk.jar
echo "signing ./color-vision/color-vision_ko.jar"
sign-jar ./color-vision/color-vision_ko.jar
echo "signing ./color-vision/color-vision_zh_CN.jar"
sign-jar ./color-vision/color-vision_zh_CN.jar
echo "signing ./color-vision/color-vision_fi.jar"
sign-jar ./color-vision/color-vision_fi.jar
echo "signing ./color-vision/color-vision_ru.jar"
sign-jar ./color-vision/color-vision_ru.jar
echo "signing ./color-vision/color-vision_uk.jar"
sign-jar ./color-vision/color-vision_uk.jar
echo "signing ./color-vision/color-vision_sv.jar"
sign-jar ./color-vision/color-vision_sv.jar
echo "signing ./color-vision/color-vision_tr.jar"
sign-jar ./color-vision/color-vision_tr.jar
echo "signing ./color-vision/color-vision_el.jar"
sign-jar ./color-vision/color-vision_el.jar
echo "signing ./color-vision/color-vision_ja.jar"
sign-jar ./color-vision/color-vision_ja.jar
echo "signing ./color-vision/color-vision_zh_TW.jar"
sign-jar ./color-vision/color-vision_zh_TW.jar
echo "signing ./color-vision/color-vision_af.jar"
sign-jar ./color-vision/color-vision_af.jar
echo "signing ./color-vision/color-vision_all.jar"
sign-jar ./color-vision/color-vision_all.jar
echo "signing ./color-vision/color-vision_ca.jar"
sign-jar ./color-vision/color-vision_ca.jar
echo "signing ./color-vision/color-vision_bg.jar"
sign-jar ./color-vision/color-vision_bg.jar
echo "signing ./color-vision/color-vision_et.jar"
sign-jar ./color-vision/color-vision_et.jar
echo "signing ./color-vision/color-vision_hr.jar"
sign-jar ./color-vision/color-vision_hr.jar
echo "signing ./color-vision/color-vision_pt.jar"
sign-jar ./color-vision/color-vision_pt.jar
echo "signing ./color-vision/color-vision_mk.jar"
sign-jar ./color-vision/color-vision_mk.jar
echo "signing ./color-vision/color-vision_pl.jar"
sign-jar ./color-vision/color-vision_pl.jar
echo "signing ./color-vision/color-vision_all_installer.jar"
sign-jar ./color-vision/color-vision_all_installer.jar
echo "signing ./color-vision/color-vision_pt_BR.jar"
sign-jar ./color-vision/color-vision_pt_BR.jar
echo "signing ./color-vision/color-vision_th.jar"
sign-jar ./color-vision/color-vision_th.jar
echo "signing ./color-vision/color-vision_en.jar"
sign-jar ./color-vision/color-vision_en.jar
echo "signing ./color-vision/color-vision_da.jar"
sign-jar ./color-vision/color-vision_da.jar
echo "signing ./color-vision/color-vision_in.jar"
sign-jar ./color-vision/color-vision_in.jar
echo "signing ./color-vision/color-vision_sq.jar"
sign-jar ./color-vision/color-vision_sq.jar
echo "signing ./color-vision/color-vision_nl.jar"
sign-jar ./color-vision/color-vision_nl.jar
echo "signing ./color-vision/color-vision_nb.jar"
sign-jar ./color-vision/color-vision_nb.jar
echo "signing ./color-vision/color-vision_kk.jar"
sign-jar ./color-vision/color-vision_kk.jar
echo "signing ./color-vision/color-vision_sr.jar"
sign-jar ./color-vision/color-vision_sr.jar
echo "signing ./color-vision/color-vision_eu.jar"
sign-jar ./color-vision/color-vision_eu.jar
echo "signing ./color-vision/color-vision_km.jar"
sign-jar ./color-vision/color-vision_km.jar
echo "signing ./color-vision/color-vision_lv.jar"
sign-jar ./color-vision/color-vision_lv.jar
echo "signing ./color-vision/color-vision_fr.jar"
sign-jar ./color-vision/color-vision_fr.jar
echo "signing ./color-vision/color-vision_sk.jar"
sign-jar ./color-vision/color-vision_sk.jar
echo "signing ./color-vision/color-vision_es.jar"
sign-jar ./color-vision/color-vision_es.jar
echo "signing ./color-vision/color-vision_de.jar"
sign-jar ./color-vision/color-vision_de.jar
echo "signing ./color-vision/color-vision_hy.jar"
sign-jar ./color-vision/color-vision_hy.jar
echo "signing ./color-vision/color-vision_gl.jar"
sign-jar ./color-vision/color-vision_gl.jar
echo "signing ./color-vision/color-vision_es_PE.jar"
sign-jar ./color-vision/color-vision_es_PE.jar
echo "signing ./color-vision/color-vision_ku_TR.jar"
sign-jar ./color-vision/color-vision_ku_TR.jar
echo "signing ./hydrogen-atom/hydrogen-atom_ca.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_ca.jar
echo "signing ./hydrogen-atom/hydrogen-atom_ta.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_ta.jar
echo "signing ./hydrogen-atom/hydrogen-atom_es_PE.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_es_PE.jar
echo "signing ./hydrogen-atom/hydrogen-atom_pl.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_pl.jar
echo "signing ./hydrogen-atom/hydrogen-atom_ru.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_ru.jar
echo "signing ./hydrogen-atom/hydrogen-atom_zh_CN.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_zh_CN.jar
echo "signing ./hydrogen-atom/hydrogen-atom_gl.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_gl.jar
echo "signing ./hydrogen-atom/hydrogen-atom_zh_TW.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_zh_TW.jar
echo "signing ./hydrogen-atom/hydrogen-atom_de.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_de.jar
echo "signing ./hydrogen-atom/hydrogen-atom_in.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_in.jar
echo "signing ./hydrogen-atom/hydrogen-atom_sk.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_sk.jar
echo "signing ./hydrogen-atom/hydrogen-atom_hu.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_hu.jar
echo "signing ./hydrogen-atom/hydrogen-atom_sr.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_sr.jar
echo "signing ./hydrogen-atom/hydrogen-atom_uk.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_uk.jar
echo "signing ./hydrogen-atom/hydrogen-atom_sl.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_sl.jar
echo "signing ./hydrogen-atom/hydrogen-atom_sv.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_sv.jar
echo "signing ./hydrogen-atom/hydrogen-atom_all.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_all.jar
echo "signing ./hydrogen-atom/hydrogen-atom_be.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_be.jar
echo "signing ./hydrogen-atom/hydrogen-atom_km.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_km.jar
echo "signing ./hydrogen-atom/hydrogen-atom_et.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_et.jar
echo "signing ./hydrogen-atom/hydrogen-atom_iw.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_iw.jar
echo "signing ./hydrogen-atom/hydrogen-atom_nb.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_nb.jar
echo "signing ./hydrogen-atom/hydrogen-atom_es.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_es.jar
echo "signing ./hydrogen-atom/hydrogen-atom_en.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_en.jar
echo "signing ./hydrogen-atom/hydrogen-atom_eu.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_eu.jar
echo "signing ./hydrogen-atom/hydrogen-atom_hr.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_hr.jar
echo "signing ./hydrogen-atom/hydrogen-atom_fa.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_fa.jar
echo "signing ./hydrogen-atom/hydrogen-atom_th.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_th.jar
echo "signing ./hydrogen-atom/hydrogen-atom_ja.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_ja.jar
echo "signing ./hydrogen-atom/hydrogen-atom_da.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_da.jar
echo "signing ./hydrogen-atom/hydrogen-atom_all_installer.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_all_installer.jar
echo "signing ./hydrogen-atom/hydrogen-atom_tk.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_tk.jar
echo "signing ./hydrogen-atom/hydrogen-atom_nl.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_nl.jar
echo "signing ./hydrogen-atom/hydrogen-atom_pt.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_pt.jar
echo "signing ./hydrogen-atom/hydrogen-atom_pt_BR.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_pt_BR.jar
echo "signing ./hydrogen-atom/hydrogen-atom_kk.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_kk.jar
echo "signing ./hydrogen-atom/hydrogen-atom_ko.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_ko.jar
echo "signing ./hydrogen-atom/hydrogen-atom_tr.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_tr.jar
echo "signing ./hydrogen-atom/hydrogen-atom_mk.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_mk.jar
echo "signing ./hydrogen-atom/hydrogen-atom_fr.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_fr.jar
echo "signing ./hydrogen-atom/hydrogen-atom_el.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_el.jar
echo "signing ./hydrogen-atom/hydrogen-atom_fi.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_fi.jar
echo "signing ./hydrogen-atom/hydrogen-atom_kn.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_kn.jar
echo "signing ./hydrogen-atom/hydrogen-atom_vi.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_vi.jar
echo "signing ./hydrogen-atom/hydrogen-atom_it.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_it.jar
echo "signing ./hydrogen-atom/hydrogen-atom_hi.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_hi.jar
echo "signing ./hydrogen-atom/hydrogen-atom_ar.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_ar.jar
echo "signing ./hydrogen-atom/hydrogen-atom_bs.jar"
sign-jar ./hydrogen-atom/hydrogen-atom_bs.jar
echo "signing ./battery-voltage/battery-voltage_be.jar"
sign-jar ./battery-voltage/battery-voltage_be.jar
echo "signing ./battery-voltage/battery-voltage_pl.jar"
sign-jar ./battery-voltage/battery-voltage_pl.jar
echo "signing ./battery-voltage/battery-voltage_mk.jar"
sign-jar ./battery-voltage/battery-voltage_mk.jar
echo "signing ./battery-voltage/battery-voltage_zh_CN.jar"
sign-jar ./battery-voltage/battery-voltage_zh_CN.jar
echo "signing ./battery-voltage/battery-voltage_ja.jar"
sign-jar ./battery-voltage/battery-voltage_ja.jar
echo "signing ./battery-voltage/battery-voltage_ar.jar"
sign-jar ./battery-voltage/battery-voltage_ar.jar
echo "signing ./battery-voltage/battery-voltage_mr.jar"
sign-jar ./battery-voltage/battery-voltage_mr.jar
echo "signing ./battery-voltage/battery-voltage_ku_TR.jar"
sign-jar ./battery-voltage/battery-voltage_ku_TR.jar
echo "signing ./battery-voltage/battery-voltage_eu.jar"
sign-jar ./battery-voltage/battery-voltage_eu.jar
echo "signing ./battery-voltage/battery-voltage_ro.jar"
sign-jar ./battery-voltage/battery-voltage_ro.jar
echo "signing ./battery-voltage/battery-voltage_iw.jar"
sign-jar ./battery-voltage/battery-voltage_iw.jar
echo "signing ./battery-voltage/battery-voltage_sk.jar"
sign-jar ./battery-voltage/battery-voltage_sk.jar
echo "signing ./battery-voltage/battery-voltage_lv.jar"
sign-jar ./battery-voltage/battery-voltage_lv.jar
echo "signing ./battery-voltage/battery-voltage_bs.jar"
sign-jar ./battery-voltage/battery-voltage_bs.jar
echo "signing ./battery-voltage/battery-voltage_bg.jar"
sign-jar ./battery-voltage/battery-voltage_bg.jar
echo "signing ./battery-voltage/battery-voltage_gl.jar"
sign-jar ./battery-voltage/battery-voltage_gl.jar
echo "signing ./battery-voltage/battery-voltage_all_installer.jar"
sign-jar ./battery-voltage/battery-voltage_all_installer.jar
echo "signing ./battery-voltage/battery-voltage_ka.jar"
sign-jar ./battery-voltage/battery-voltage_ka.jar
echo "signing ./battery-voltage/battery-voltage_hy.jar"
sign-jar ./battery-voltage/battery-voltage_hy.jar
echo "signing ./battery-voltage/battery-voltage_ar_SA.jar"
sign-jar ./battery-voltage/battery-voltage_ar_SA.jar
echo "signing ./battery-voltage/battery-voltage_sl.jar"
sign-jar ./battery-voltage/battery-voltage_sl.jar
echo "signing ./battery-voltage/battery-voltage_uk.jar"
sign-jar ./battery-voltage/battery-voltage_uk.jar
echo "signing ./battery-voltage/battery-voltage_all.jar"
sign-jar ./battery-voltage/battery-voltage_all.jar
echo "signing ./battery-voltage/battery-voltage_kn.jar"
sign-jar ./battery-voltage/battery-voltage_kn.jar
echo "signing ./battery-voltage/battery-voltage_vi.jar"
sign-jar ./battery-voltage/battery-voltage_vi.jar
echo "signing ./battery-voltage/battery-voltage_el.jar"
sign-jar ./battery-voltage/battery-voltage_el.jar
echo "signing ./battery-voltage/battery-voltage_ko.jar"
sign-jar ./battery-voltage/battery-voltage_ko.jar
echo "signing ./battery-voltage/battery-voltage_pt.jar"
sign-jar ./battery-voltage/battery-voltage_pt.jar
echo "signing ./battery-voltage/battery-voltage_en.jar"
sign-jar ./battery-voltage/battery-voltage_en.jar
echo "signing ./battery-voltage/battery-voltage_pt_BR.jar"
sign-jar ./battery-voltage/battery-voltage_pt_BR.jar
echo "signing ./battery-voltage/battery-voltage_th.jar"
sign-jar ./battery-voltage/battery-voltage_th.jar
echo "signing ./battery-voltage/battery-voltage_in.jar"
sign-jar ./battery-voltage/battery-voltage_in.jar
echo "signing ./battery-voltage/battery-voltage_nl.jar"
sign-jar ./battery-voltage/battery-voltage_nl.jar
echo "signing ./battery-voltage/battery-voltage_es.jar"
sign-jar ./battery-voltage/battery-voltage_es.jar
echo "signing ./battery-voltage/battery-voltage_mn.jar"
sign-jar ./battery-voltage/battery-voltage_mn.jar
echo "signing ./battery-voltage/battery-voltage_it.jar"
sign-jar ./battery-voltage/battery-voltage_it.jar
echo "signing ./battery-voltage/battery-voltage_ru.jar"
sign-jar ./battery-voltage/battery-voltage_ru.jar
echo "signing ./battery-voltage/battery-voltage_da.jar"
sign-jar ./battery-voltage/battery-voltage_da.jar
echo "signing ./battery-voltage/battery-voltage_fr.jar"
sign-jar ./battery-voltage/battery-voltage_fr.jar
echo "signing ./battery-voltage/battery-voltage_sq.jar"
sign-jar ./battery-voltage/battery-voltage_sq.jar
echo "signing ./battery-voltage/battery-voltage_tr.jar"
sign-jar ./battery-voltage/battery-voltage_tr.jar
echo "signing ./battery-voltage/battery-voltage_kk.jar"
sign-jar ./battery-voltage/battery-voltage_kk.jar
echo "signing ./battery-voltage/battery-voltage_zh_TW.jar"
sign-jar ./battery-voltage/battery-voltage_zh_TW.jar
echo "signing ./battery-voltage/battery-voltage_hu.jar"
sign-jar ./battery-voltage/battery-voltage_hu.jar
echo "signing ./battery-voltage/battery-voltage_es_PE.jar"
sign-jar ./battery-voltage/battery-voltage_es_PE.jar
echo "signing ./battery-voltage/battery-voltage_tk.jar"
sign-jar ./battery-voltage/battery-voltage_tk.jar
echo "signing ./battery-voltage/battery-voltage_et.jar"
sign-jar ./battery-voltage/battery-voltage_et.jar
echo "signing ./battery-voltage/battery-voltage_sr.jar"
sign-jar ./battery-voltage/battery-voltage_sr.jar
echo "signing ./battery-voltage/battery-voltage_hr.jar"
sign-jar ./battery-voltage/battery-voltage_hr.jar
echo "signing ./battery-voltage/battery-voltage_sv.jar"
sign-jar ./battery-voltage/battery-voltage_sv.jar
echo "signing ./battery-voltage/battery-voltage_de.jar"
sign-jar ./battery-voltage/battery-voltage_de.jar
echo "signing ./battery-voltage/battery-voltage_fa.jar"
sign-jar ./battery-voltage/battery-voltage_fa.jar
echo "signing ./battery-voltage/battery-voltage_cs.jar"
sign-jar ./battery-voltage/battery-voltage_cs.jar
echo "signing ./battery-voltage/battery-voltage_km.jar"
sign-jar ./battery-voltage/battery-voltage_km.jar
echo "signing ./lunar-lander/lunar-lander_zh_TW.jar"
sign-jar ./lunar-lander/lunar-lander_zh_TW.jar
echo "signing ./lunar-lander/lunar-lander_kk.jar"
sign-jar ./lunar-lander/lunar-lander_kk.jar
echo "signing ./lunar-lander/lunar-lander_ko.jar"
sign-jar ./lunar-lander/lunar-lander_ko.jar
echo "signing ./lunar-lander/lunar-lander_cs.jar"
sign-jar ./lunar-lander/lunar-lander_cs.jar
echo "signing ./lunar-lander/lunar-lander_ja.jar"
sign-jar ./lunar-lander/lunar-lander_ja.jar
echo "signing ./lunar-lander/lunar-lander_ar.jar"
sign-jar ./lunar-lander/lunar-lander_ar.jar
echo "signing ./lunar-lander/lunar-lander_sr.jar"
sign-jar ./lunar-lander/lunar-lander_sr.jar
echo "signing ./lunar-lander/lunar-lander_nl.jar"
sign-jar ./lunar-lander/lunar-lander_nl.jar
echo "signing ./lunar-lander/lunar-lander_km.jar"
sign-jar ./lunar-lander/lunar-lander_km.jar
echo "signing ./lunar-lander/lunar-lander_tr.jar"
sign-jar ./lunar-lander/lunar-lander_tr.jar
echo "signing ./lunar-lander/lunar-lander_th.jar"
sign-jar ./lunar-lander/lunar-lander_th.jar
echo "signing ./lunar-lander/lunar-lander_in.jar"
sign-jar ./lunar-lander/lunar-lander_in.jar
echo "signing ./lunar-lander/lunar-lander_be.jar"
sign-jar ./lunar-lander/lunar-lander_be.jar
echo "signing ./lunar-lander/lunar-lander_eu.jar"
sign-jar ./lunar-lander/lunar-lander_eu.jar
echo "signing ./lunar-lander/lunar-lander_iw.jar"
sign-jar ./lunar-lander/lunar-lander_iw.jar
echo "signing ./lunar-lander/lunar-lander_tk.jar"
sign-jar ./lunar-lander/lunar-lander_tk.jar
echo "signing ./lunar-lander/lunar-lander_pt.jar"
sign-jar ./lunar-lander/lunar-lander_pt.jar
echo "signing ./lunar-lander/lunar-lander_et.jar"
sign-jar ./lunar-lander/lunar-lander_et.jar
echo "signing ./lunar-lander/lunar-lander_vi.jar"
sign-jar ./lunar-lander/lunar-lander_vi.jar
echo "signing ./lunar-lander/lunar-lander_sk.jar"
sign-jar ./lunar-lander/lunar-lander_sk.jar
echo "signing ./lunar-lander/lunar-lander_mk.jar"
sign-jar ./lunar-lander/lunar-lander_mk.jar
echo "signing ./lunar-lander/lunar-lander_el.jar"
sign-jar ./lunar-lander/lunar-lander_el.jar
echo "signing ./lunar-lander/lunar-lander_en.jar"
sign-jar ./lunar-lander/lunar-lander_en.jar
echo "signing ./lunar-lander/lunar-lander_gl.jar"
sign-jar ./lunar-lander/lunar-lander_gl.jar
echo "signing ./lunar-lander/lunar-lander_da.jar"
sign-jar ./lunar-lander/lunar-lander_da.jar
echo "signing ./lunar-lander/lunar-lander_zh_CN.jar"
sign-jar ./lunar-lander/lunar-lander_zh_CN.jar
echo "signing ./lunar-lander/lunar-lander_pl.jar"
sign-jar ./lunar-lander/lunar-lander_pl.jar
echo "signing ./lunar-lander/lunar-lander_hu.jar"
sign-jar ./lunar-lander/lunar-lander_hu.jar
echo "signing ./lunar-lander/lunar-lander_pt_BR.jar"
sign-jar ./lunar-lander/lunar-lander_pt_BR.jar
echo "signing ./lunar-lander/lunar-lander_ar_SA.jar"
sign-jar ./lunar-lander/lunar-lander_ar_SA.jar
echo "signing ./lunar-lander/lunar-lander_bs.jar"
sign-jar ./lunar-lander/lunar-lander_bs.jar
echo "signing ./lunar-lander/lunar-lander_de.jar"
sign-jar ./lunar-lander/lunar-lander_de.jar
echo "signing ./lunar-lander/lunar-lander_fr.jar"
sign-jar ./lunar-lander/lunar-lander_fr.jar
echo "signing ./lunar-lander/lunar-lander_it.jar"
sign-jar ./lunar-lander/lunar-lander_it.jar
echo "signing ./lunar-lander/lunar-lander_fa.jar"
sign-jar ./lunar-lander/lunar-lander_fa.jar
echo "signing ./lunar-lander/lunar-lander_es_PE.jar"
sign-jar ./lunar-lander/lunar-lander_es_PE.jar
echo "signing ./lunar-lander/lunar-lander_es.jar"
sign-jar ./lunar-lander/lunar-lander_es.jar
echo "signing ./lunar-lander/lunar-lander_sv.jar"
sign-jar ./lunar-lander/lunar-lander_sv.jar
echo "signing ./lunar-lander/lunar-lander_mr.jar"
sign-jar ./lunar-lander/lunar-lander_mr.jar
echo "signing ./lunar-lander/lunar-lander_hr.jar"
sign-jar ./lunar-lander/lunar-lander_hr.jar
echo "signing ./java-common-strings/java-common-strings_en.jar"
sign-jar ./java-common-strings/java-common-strings_en.jar
echo "signing ./java-common-strings/java-common-strings_all_installer.jar"
sign-jar ./java-common-strings/java-common-strings_all_installer.jar
echo "signing ./java-common-strings/java-common-strings_all.jar"
sign-jar ./java-common-strings/java-common-strings_all.jar
echo "signing ./geometric-optics/geometric-optics_vi.jar"
sign-jar ./geometric-optics/geometric-optics_vi.jar
echo "signing ./geometric-optics/geometric-optics_uk.jar"
sign-jar ./geometric-optics/geometric-optics_uk.jar
echo "signing ./geometric-optics/geometric-optics_et.jar"
sign-jar ./geometric-optics/geometric-optics_et.jar
echo "signing ./geometric-optics/geometric-optics_zh_CN.jar"
sign-jar ./geometric-optics/geometric-optics_zh_CN.jar
echo "signing ./geometric-optics/geometric-optics_eu.jar"
sign-jar ./geometric-optics/geometric-optics_eu.jar
echo "signing ./geometric-optics/geometric-optics_lt.jar"
sign-jar ./geometric-optics/geometric-optics_lt.jar
echo "signing ./geometric-optics/geometric-optics_mn.jar"
sign-jar ./geometric-optics/geometric-optics_mn.jar
echo "signing ./geometric-optics/geometric-optics_es_MX.jar"
sign-jar ./geometric-optics/geometric-optics_es_MX.jar
echo "signing ./geometric-optics/geometric-optics_sq.jar"
sign-jar ./geometric-optics/geometric-optics_sq.jar
echo "signing ./geometric-optics/geometric-optics_km.jar"
sign-jar ./geometric-optics/geometric-optics_km.jar
echo "signing ./geometric-optics/geometric-optics_bg.jar"
sign-jar ./geometric-optics/geometric-optics_bg.jar
echo "signing ./geometric-optics/geometric-optics_hr.jar"
sign-jar ./geometric-optics/geometric-optics_hr.jar
echo "signing ./geometric-optics/geometric-optics_kk.jar"
sign-jar ./geometric-optics/geometric-optics_kk.jar
echo "signing ./geometric-optics/geometric-optics_bs.jar"
sign-jar ./geometric-optics/geometric-optics_bs.jar
echo "signing ./geometric-optics/geometric-optics_en.jar"
sign-jar ./geometric-optics/geometric-optics_en.jar
echo "signing ./geometric-optics/geometric-optics_da.jar"
sign-jar ./geometric-optics/geometric-optics_da.jar
echo "signing ./geometric-optics/geometric-optics_lv.jar"
sign-jar ./geometric-optics/geometric-optics_lv.jar
echo "signing ./geometric-optics/geometric-optics_sr.jar"
sign-jar ./geometric-optics/geometric-optics_sr.jar
echo "signing ./geometric-optics/geometric-optics_es_PE.jar"
sign-jar ./geometric-optics/geometric-optics_es_PE.jar
echo "signing ./geometric-optics/geometric-optics_it.jar"
sign-jar ./geometric-optics/geometric-optics_it.jar
echo "signing ./geometric-optics/geometric-optics_pl.jar"
sign-jar ./geometric-optics/geometric-optics_pl.jar
echo "signing ./geometric-optics/geometric-optics_nl.jar"
sign-jar ./geometric-optics/geometric-optics_nl.jar
echo "signing ./geometric-optics/geometric-optics_mr.jar"
sign-jar ./geometric-optics/geometric-optics_mr.jar
echo "signing ./geometric-optics/geometric-optics_gl.jar"
sign-jar ./geometric-optics/geometric-optics_gl.jar
echo "signing ./geometric-optics/geometric-optics_el.jar"
sign-jar ./geometric-optics/geometric-optics_el.jar
echo "signing ./geometric-optics/geometric-optics_ar.jar"
sign-jar ./geometric-optics/geometric-optics_ar.jar
echo "signing ./geometric-optics/geometric-optics_de.jar"
sign-jar ./geometric-optics/geometric-optics_de.jar
echo "signing ./geometric-optics/geometric-optics_fr.jar"
sign-jar ./geometric-optics/geometric-optics_fr.jar
echo "signing ./geometric-optics/geometric-optics_fi.jar"
sign-jar ./geometric-optics/geometric-optics_fi.jar
echo "signing ./geometric-optics/geometric-optics_sk.jar"
sign-jar ./geometric-optics/geometric-optics_sk.jar
echo "signing ./geometric-optics/geometric-optics_in.jar"
sign-jar ./geometric-optics/geometric-optics_in.jar
echo "signing ./geometric-optics/geometric-optics_pt_BR.jar"
sign-jar ./geometric-optics/geometric-optics_pt_BR.jar
echo "signing ./geometric-optics/geometric-optics_tk.jar"
sign-jar ./geometric-optics/geometric-optics_tk.jar
echo "signing ./geometric-optics/geometric-optics_ro.jar"
sign-jar ./geometric-optics/geometric-optics_ro.jar
echo "signing ./geometric-optics/geometric-optics_mk.jar"
sign-jar ./geometric-optics/geometric-optics_mk.jar
echo "signing ./geometric-optics/geometric-optics_iw.jar"
sign-jar ./geometric-optics/geometric-optics_iw.jar
echo "signing ./geometric-optics/geometric-optics_be.jar"
sign-jar ./geometric-optics/geometric-optics_be.jar
echo "signing ./geometric-optics/geometric-optics_uz.jar"
sign-jar ./geometric-optics/geometric-optics_uz.jar
echo "signing ./geometric-optics/geometric-optics_ko.jar"
sign-jar ./geometric-optics/geometric-optics_ko.jar
echo "signing ./geometric-optics/geometric-optics_kn.jar"
sign-jar ./geometric-optics/geometric-optics_kn.jar
echo "signing ./geometric-optics/geometric-optics_th.jar"
sign-jar ./geometric-optics/geometric-optics_th.jar
echo "signing ./geometric-optics/geometric-optics_cs.jar"
sign-jar ./geometric-optics/geometric-optics_cs.jar
echo "signing ./geometric-optics/geometric-optics_ka.jar"
sign-jar ./geometric-optics/geometric-optics_ka.jar
echo "signing ./geometric-optics/geometric-optics_pt.jar"
sign-jar ./geometric-optics/geometric-optics_pt.jar
echo "signing ./geometric-optics/geometric-optics_es.jar"
sign-jar ./geometric-optics/geometric-optics_es.jar
echo "signing ./geometric-optics/geometric-optics_hu.jar"
sign-jar ./geometric-optics/geometric-optics_hu.jar
echo "signing ./geometric-optics/geometric-optics_zh_TW.jar"
sign-jar ./geometric-optics/geometric-optics_zh_TW.jar
echo "signing ./geometric-optics/geometric-optics_sv.jar"
sign-jar ./geometric-optics/geometric-optics_sv.jar
echo "signing ./geometric-optics/geometric-optics_fa.jar"
sign-jar ./geometric-optics/geometric-optics_fa.jar
echo "signing ./geometric-optics/geometric-optics_sl.jar"
sign-jar ./geometric-optics/geometric-optics_sl.jar
echo "signing ./geometric-optics/geometric-optics_ja.jar"
sign-jar ./geometric-optics/geometric-optics_ja.jar
echo "signing ./geometric-optics/geometric-optics_tr.jar"
sign-jar ./geometric-optics/geometric-optics_tr.jar
echo "signing ./geometric-optics/geometric-optics_ku_TR.jar"
sign-jar ./geometric-optics/geometric-optics_ku_TR.jar
echo "signing ./neuron/neuron_all.jar"
sign-jar ./neuron/neuron_all.jar
echo "signing ./neuron/neuron_iw.jar"
sign-jar ./neuron/neuron_iw.jar
echo "signing ./neuron/neuron_zh_CN.jar"
sign-jar ./neuron/neuron_zh_CN.jar
echo "signing ./neuron/neuron_es_PE.jar"
sign-jar ./neuron/neuron_es_PE.jar
echo "signing ./neuron/neuron_es.jar"
sign-jar ./neuron/neuron_es.jar
echo "signing ./neuron/neuron_mk.jar"
sign-jar ./neuron/neuron_mk.jar
echo "signing ./neuron/neuron_tk.jar"
sign-jar ./neuron/neuron_tk.jar
echo "signing ./neuron/neuron_fr.jar"
sign-jar ./neuron/neuron_fr.jar
echo "signing ./neuron/neuron_ka.jar"
sign-jar ./neuron/neuron_ka.jar
echo "signing ./neuron/neuron_sv.jar"
sign-jar ./neuron/neuron_sv.jar
echo "signing ./neuron/neuron_de.jar"
sign-jar ./neuron/neuron_de.jar
echo "signing ./neuron/neuron_hu.jar"
sign-jar ./neuron/neuron_hu.jar
echo "signing ./neuron/neuron_all_installer.jar"
sign-jar ./neuron/neuron_all_installer.jar
echo "signing ./neuron/neuron_km.jar"
sign-jar ./neuron/neuron_km.jar
echo "signing ./neuron/neuron_kk.jar"
sign-jar ./neuron/neuron_kk.jar
echo "signing ./neuron/neuron_sr.jar"
sign-jar ./neuron/neuron_sr.jar
echo "signing ./neuron/neuron_ko.jar"
sign-jar ./neuron/neuron_ko.jar
echo "signing ./neuron/neuron_sk.jar"
sign-jar ./neuron/neuron_sk.jar
echo "signing ./neuron/neuron_et.jar"
sign-jar ./neuron/neuron_et.jar
echo "signing ./neuron/neuron_th.jar"
sign-jar ./neuron/neuron_th.jar
echo "signing ./neuron/neuron_pt_BR.jar"
sign-jar ./neuron/neuron_pt_BR.jar
echo "signing ./neuron/neuron_en.jar"
sign-jar ./neuron/neuron_en.jar
echo "signing ./neuron/neuron_es_ES.jar"
sign-jar ./neuron/neuron_es_ES.jar
echo "signing ./neuron/neuron_eu.jar"
sign-jar ./neuron/neuron_eu.jar
echo "signing ./neuron/neuron_pl.jar"
sign-jar ./neuron/neuron_pl.jar
echo "signing ./neuron/neuron_fa.jar"
sign-jar ./neuron/neuron_fa.jar
echo "signing ./neuron/neuron_zh_TW.jar"
sign-jar ./neuron/neuron_zh_TW.jar
echo "signing ./neuron/neuron_ja.jar"
sign-jar ./neuron/neuron_ja.jar
echo "signing ./neuron/neuron_da.jar"
sign-jar ./neuron/neuron_da.jar
echo "signing ./neuron/neuron_cs.jar"
sign-jar ./neuron/neuron_cs.jar
echo "signing ./neuron/neuron_el.jar"
sign-jar ./neuron/neuron_el.jar
echo "signing ./neuron/neuron_vi.jar"
sign-jar ./neuron/neuron_vi.jar
echo "signing ./neuron/neuron_it.jar"
sign-jar ./neuron/neuron_it.jar
echo "signing ./neuron/neuron_tr.jar"
sign-jar ./neuron/neuron_tr.jar
echo "signing ./neuron/neuron_bs.jar"
sign-jar ./neuron/neuron_bs.jar
echo "signing ./neuron/neuron_ru.jar"
sign-jar ./neuron/neuron_ru.jar
echo "signing ./calculus-grapher/calculus-grapher_eu.jar"
sign-jar ./calculus-grapher/calculus-grapher_eu.jar
echo "signing ./calculus-grapher/calculus-grapher_iw.jar"
sign-jar ./calculus-grapher/calculus-grapher_iw.jar
echo "signing ./calculus-grapher/calculus-grapher_el.jar"
sign-jar ./calculus-grapher/calculus-grapher_el.jar
echo "signing ./calculus-grapher/calculus-grapher_bs.jar"
sign-jar ./calculus-grapher/calculus-grapher_bs.jar
echo "signing ./calculus-grapher/calculus-grapher_ja.jar"
sign-jar ./calculus-grapher/calculus-grapher_ja.jar
echo "signing ./calculus-grapher/calculus-grapher_de.jar"
sign-jar ./calculus-grapher/calculus-grapher_de.jar
echo "signing ./calculus-grapher/calculus-grapher_vi.jar"
sign-jar ./calculus-grapher/calculus-grapher_vi.jar
echo "signing ./calculus-grapher/calculus-grapher_lv.jar"
sign-jar ./calculus-grapher/calculus-grapher_lv.jar
echo "signing ./calculus-grapher/calculus-grapher_in.jar"
sign-jar ./calculus-grapher/calculus-grapher_in.jar
echo "signing ./calculus-grapher/calculus-grapher_mk.jar"
sign-jar ./calculus-grapher/calculus-grapher_mk.jar
echo "signing ./calculus-grapher/calculus-grapher_es.jar"
sign-jar ./calculus-grapher/calculus-grapher_es.jar
echo "signing ./calculus-grapher/calculus-grapher_nl.jar"
sign-jar ./calculus-grapher/calculus-grapher_nl.jar
echo "signing ./calculus-grapher/calculus-grapher_tr.jar"
sign-jar ./calculus-grapher/calculus-grapher_tr.jar
echo "signing ./calculus-grapher/calculus-grapher_sr.jar"
sign-jar ./calculus-grapher/calculus-grapher_sr.jar
echo "signing ./calculus-grapher/calculus-grapher_zh_TW.jar"
sign-jar ./calculus-grapher/calculus-grapher_zh_TW.jar
echo "signing ./calculus-grapher/calculus-grapher_ar.jar"
sign-jar ./calculus-grapher/calculus-grapher_ar.jar
echo "signing ./calculus-grapher/calculus-grapher_es_PE.jar"
sign-jar ./calculus-grapher/calculus-grapher_es_PE.jar
echo "signing ./calculus-grapher/calculus-grapher_hu.jar"
sign-jar ./calculus-grapher/calculus-grapher_hu.jar
echo "signing ./calculus-grapher/calculus-grapher_sv.jar"
sign-jar ./calculus-grapher/calculus-grapher_sv.jar
echo "signing ./calculus-grapher/calculus-grapher_it.jar"
sign-jar ./calculus-grapher/calculus-grapher_it.jar
echo "signing ./calculus-grapher/calculus-grapher_sk.jar"
sign-jar ./calculus-grapher/calculus-grapher_sk.jar
echo "signing ./calculus-grapher/calculus-grapher_zh_CN.jar"
sign-jar ./calculus-grapher/calculus-grapher_zh_CN.jar
echo "signing ./calculus-grapher/calculus-grapher_nb.jar"
sign-jar ./calculus-grapher/calculus-grapher_nb.jar
echo "signing ./calculus-grapher/calculus-grapher_fr.jar"
sign-jar ./calculus-grapher/calculus-grapher_fr.jar
echo "signing ./calculus-grapher/calculus-grapher_pt_BR.jar"
sign-jar ./calculus-grapher/calculus-grapher_pt_BR.jar
echo "signing ./calculus-grapher/calculus-grapher_ko.jar"
sign-jar ./calculus-grapher/calculus-grapher_ko.jar
echo "signing ./calculus-grapher/calculus-grapher_ar_SA.jar"
sign-jar ./calculus-grapher/calculus-grapher_ar_SA.jar
echo "signing ./calculus-grapher/calculus-grapher_et.jar"
sign-jar ./calculus-grapher/calculus-grapher_et.jar
echo "signing ./calculus-grapher/calculus-grapher_tk.jar"
sign-jar ./calculus-grapher/calculus-grapher_tk.jar
echo "signing ./calculus-grapher/calculus-grapher_kk.jar"
sign-jar ./calculus-grapher/calculus-grapher_kk.jar
echo "signing ./calculus-grapher/calculus-grapher_fa.jar"
sign-jar ./calculus-grapher/calculus-grapher_fa.jar
echo "signing ./calculus-grapher/calculus-grapher_es_CO.jar"
sign-jar ./calculus-grapher/calculus-grapher_es_CO.jar
echo "signing ./calculus-grapher/calculus-grapher_pl.jar"
sign-jar ./calculus-grapher/calculus-grapher_pl.jar
echo "signing ./calculus-grapher/calculus-grapher_da.jar"
sign-jar ./calculus-grapher/calculus-grapher_da.jar
echo "signing ./calculus-grapher/calculus-grapher_be.jar"
sign-jar ./calculus-grapher/calculus-grapher_be.jar
echo "signing ./calculus-grapher/calculus-grapher_en.jar"
sign-jar ./calculus-grapher/calculus-grapher_en.jar
echo "signing ./calculus-grapher/calculus-grapher_cs.jar"
sign-jar ./calculus-grapher/calculus-grapher_cs.jar
echo "signing ./force-law-lab/gravity-force-lab_fa.jar"
sign-jar ./force-law-lab/gravity-force-lab_fa.jar
echo "signing ./force-law-lab/gravity-force-lab_ka.jar"
sign-jar ./force-law-lab/gravity-force-lab_ka.jar
echo "signing ./force-law-lab/gravity-force-lab_ja.jar"
sign-jar ./force-law-lab/gravity-force-lab_ja.jar
echo "signing ./force-law-lab/gravity-force-lab_gl.jar"
sign-jar ./force-law-lab/gravity-force-lab_gl.jar
echo "signing ./force-law-lab/gravity-force-lab_et.jar"
sign-jar ./force-law-lab/gravity-force-lab_et.jar
echo "signing ./force-law-lab/gravity-force-lab_sk.jar"
sign-jar ./force-law-lab/gravity-force-lab_sk.jar
echo "signing ./force-law-lab/gravity-force-lab_nb.jar"
sign-jar ./force-law-lab/gravity-force-lab_nb.jar
echo "signing ./force-law-lab/gravity-force-lab_nl.jar"
sign-jar ./force-law-lab/gravity-force-lab_nl.jar
echo "signing ./force-law-lab/gravity-force-lab_in.jar"
sign-jar ./force-law-lab/gravity-force-lab_in.jar
echo "signing ./force-law-lab/gravity-force-lab_da.jar"
sign-jar ./force-law-lab/gravity-force-lab_da.jar
echo "signing ./force-law-lab/gravity-force-lab_de.jar"
sign-jar ./force-law-lab/gravity-force-lab_de.jar
echo "signing ./force-law-lab/gravity-force-lab_el.jar"
sign-jar ./force-law-lab/gravity-force-lab_el.jar
echo "signing ./force-law-lab/force-law-lab_all_installer.jar"
sign-jar ./force-law-lab/force-law-lab_all_installer.jar
echo "signing ./force-law-lab/gravity-force-lab_zh_CN.jar"
sign-jar ./force-law-lab/gravity-force-lab_zh_CN.jar
echo "signing ./force-law-lab/gravity-force-lab_pl.jar"
sign-jar ./force-law-lab/gravity-force-lab_pl.jar
echo "signing ./force-law-lab/gravity-force-lab_zh_TW.jar"
sign-jar ./force-law-lab/gravity-force-lab_zh_TW.jar
echo "signing ./force-law-lab/gravity-force-lab_ar.jar"
sign-jar ./force-law-lab/gravity-force-lab_ar.jar
echo "signing ./force-law-lab/gravity-force-lab_ko.jar"
sign-jar ./force-law-lab/gravity-force-lab_ko.jar
echo "signing ./force-law-lab/gravity-force-lab_mk.jar"
sign-jar ./force-law-lab/gravity-force-lab_mk.jar
echo "signing ./force-law-lab/gravity-force-lab_kn.jar"
sign-jar ./force-law-lab/gravity-force-lab_kn.jar
echo "signing ./force-law-lab/gravity-force-lab_it.jar"
sign-jar ./force-law-lab/gravity-force-lab_it.jar
echo "signing ./force-law-lab/gravity-force-lab_iw.jar"
sign-jar ./force-law-lab/gravity-force-lab_iw.jar
echo "signing ./force-law-lab/gravity-force-lab_bg.jar"
sign-jar ./force-law-lab/gravity-force-lab_bg.jar
echo "signing ./force-law-lab/gravity-force-lab_cs.jar"
sign-jar ./force-law-lab/gravity-force-lab_cs.jar
echo "signing ./force-law-lab/gravity-force-lab_lv.jar"
sign-jar ./force-law-lab/gravity-force-lab_lv.jar
echo "signing ./force-law-lab/gravity-force-lab_kk.jar"
sign-jar ./force-law-lab/gravity-force-lab_kk.jar
echo "signing ./force-law-lab/gravity-force-lab_mr.jar"
sign-jar ./force-law-lab/gravity-force-lab_mr.jar
echo "signing ./force-law-lab/gravity-force-lab_es.jar"
sign-jar ./force-law-lab/gravity-force-lab_es.jar
echo "signing ./force-law-lab/gravity-force-lab_bs.jar"
sign-jar ./force-law-lab/gravity-force-lab_bs.jar
echo "signing ./force-law-lab/gravity-force-lab_sv.jar"
sign-jar ./force-law-lab/gravity-force-lab_sv.jar
echo "signing ./force-law-lab/gravity-force-lab_ar_SA.jar"
sign-jar ./force-law-lab/gravity-force-lab_ar_SA.jar
echo "signing ./force-law-lab/force-law-lab_all.jar"
sign-jar ./force-law-lab/force-law-lab_all.jar
echo "signing ./force-law-lab/gravity-force-lab_ku_TR.jar"
sign-jar ./force-law-lab/gravity-force-lab_ku_TR.jar
echo "signing ./force-law-lab/gravity-force-lab_fr.jar"
sign-jar ./force-law-lab/gravity-force-lab_fr.jar
echo "signing ./force-law-lab/gravity-force-lab_be.jar"
sign-jar ./force-law-lab/gravity-force-lab_be.jar
echo "signing ./force-law-lab/gravity-force-lab_es_PE.jar"
sign-jar ./force-law-lab/gravity-force-lab_es_PE.jar
echo "signing ./force-law-lab/gravity-force-lab_tk.jar"
sign-jar ./force-law-lab/gravity-force-lab_tk.jar
echo "signing ./force-law-lab/gravity-force-lab_es_CO.jar"
sign-jar ./force-law-lab/gravity-force-lab_es_CO.jar
echo "signing ./force-law-lab/gravity-force-lab_hu.jar"
sign-jar ./force-law-lab/gravity-force-lab_hu.jar
echo "signing ./force-law-lab/gravity-force-lab_th.jar"
sign-jar ./force-law-lab/gravity-force-lab_th.jar
echo "signing ./force-law-lab/gravity-force-lab_sr.jar"
sign-jar ./force-law-lab/gravity-force-lab_sr.jar
echo "signing ./force-law-lab/gravity-force-lab_pt_BR.jar"
sign-jar ./force-law-lab/gravity-force-lab_pt_BR.jar
echo "signing ./force-law-lab/gravity-force-lab_tr.jar"
sign-jar ./force-law-lab/gravity-force-lab_tr.jar
echo "signing ./force-law-lab/gravity-force-lab_en.jar"
sign-jar ./force-law-lab/gravity-force-lab_en.jar
echo "signing ./force-law-lab/gravity-force-lab_pt.jar"
sign-jar ./force-law-lab/gravity-force-lab_pt.jar
echo "signing ./force-law-lab/gravity-force-lab_eu.jar"
sign-jar ./force-law-lab/gravity-force-lab_eu.jar
echo "signing ./force-law-lab/gravity-force-lab_vi.jar"
sign-jar ./force-law-lab/gravity-force-lab_vi.jar
echo "signing ./force-law-lab/gravity-force-lab_km.jar"
sign-jar ./force-law-lab/gravity-force-lab_km.jar
echo "signing ./photoelectric/photoelectric_cs.jar"
sign-jar ./photoelectric/photoelectric_cs.jar
echo "signing ./photoelectric/photoelectric_kk.jar"
sign-jar ./photoelectric/photoelectric_kk.jar
echo "signing ./photoelectric/photoelectric_ro.jar"
sign-jar ./photoelectric/photoelectric_ro.jar
echo "signing ./photoelectric/photoelectric_pt.jar"
sign-jar ./photoelectric/photoelectric_pt.jar
echo "signing ./photoelectric/photoelectric_fr.jar"
sign-jar ./photoelectric/photoelectric_fr.jar
echo "signing ./photoelectric/photoelectric_te.jar"
sign-jar ./photoelectric/photoelectric_te.jar
echo "signing ./photoelectric/photoelectric_hr.jar"
sign-jar ./photoelectric/photoelectric_hr.jar
echo "signing ./photoelectric/photoelectric_de.jar"
sign-jar ./photoelectric/photoelectric_de.jar
echo "signing ./photoelectric/photoelectric_all.jar"
sign-jar ./photoelectric/photoelectric_all.jar
echo "signing ./photoelectric/photoelectric_bg.jar"
sign-jar ./photoelectric/photoelectric_bg.jar
echo "signing ./photoelectric/photoelectric_pl.jar"
sign-jar ./photoelectric/photoelectric_pl.jar
echo "signing ./photoelectric/photoelectric_in.jar"
sign-jar ./photoelectric/photoelectric_in.jar
echo "signing ./photoelectric/photoelectric_vi.jar"
sign-jar ./photoelectric/photoelectric_vi.jar
echo "signing ./photoelectric/photoelectric_hu.jar"
sign-jar ./photoelectric/photoelectric_hu.jar
echo "signing ./photoelectric/photoelectric_sk.jar"
sign-jar ./photoelectric/photoelectric_sk.jar
echo "signing ./photoelectric/photoelectric_mk.jar"
sign-jar ./photoelectric/photoelectric_mk.jar
echo "signing ./photoelectric/photoelectric_tk.jar"
sign-jar ./photoelectric/photoelectric_tk.jar
echo "signing ./photoelectric/photoelectric_sq.jar"
sign-jar ./photoelectric/photoelectric_sq.jar
echo "signing ./photoelectric/photoelectric_ko.jar"
sign-jar ./photoelectric/photoelectric_ko.jar
echo "signing ./photoelectric/photoelectric_be.jar"
sign-jar ./photoelectric/photoelectric_be.jar
echo "signing ./photoelectric/photoelectric_ca.jar"
sign-jar ./photoelectric/photoelectric_ca.jar
echo "signing ./photoelectric/photoelectric_bs.jar"
sign-jar ./photoelectric/photoelectric_bs.jar
echo "signing ./photoelectric/photoelectric_zh_TW.jar"
sign-jar ./photoelectric/photoelectric_zh_TW.jar
echo "signing ./photoelectric/photoelectric_es.jar"
sign-jar ./photoelectric/photoelectric_es.jar
echo "signing ./photoelectric/photoelectric_fi.jar"
sign-jar ./photoelectric/photoelectric_fi.jar
echo "signing ./photoelectric/photoelectric_pt_BR.jar"
sign-jar ./photoelectric/photoelectric_pt_BR.jar
echo "signing ./photoelectric/photoelectric_zh_CN.jar"
sign-jar ./photoelectric/photoelectric_zh_CN.jar
echo "signing ./photoelectric/photoelectric_th.jar"
sign-jar ./photoelectric/photoelectric_th.jar
echo "signing ./photoelectric/photoelectric_ta.jar"
sign-jar ./photoelectric/photoelectric_ta.jar
echo "signing ./photoelectric/photoelectric_en.jar"
sign-jar ./photoelectric/photoelectric_en.jar
echo "signing ./photoelectric/photoelectric_et.jar"
sign-jar ./photoelectric/photoelectric_et.jar
echo "signing ./photoelectric/photoelectric_it.jar"
sign-jar ./photoelectric/photoelectric_it.jar
echo "signing ./photoelectric/photoelectric_es_PE.jar"
sign-jar ./photoelectric/photoelectric_es_PE.jar
echo "signing ./photoelectric/photoelectric_ar.jar"
sign-jar ./photoelectric/photoelectric_ar.jar
echo "signing ./photoelectric/photoelectric_nb.jar"
sign-jar ./photoelectric/photoelectric_nb.jar
echo "signing ./photoelectric/photoelectric_fu.jar"
sign-jar ./photoelectric/photoelectric_fu.jar
echo "signing ./photoelectric/photoelectric_da.jar"
sign-jar ./photoelectric/photoelectric_da.jar
echo "signing ./photoelectric/photoelectric_kn.jar"
sign-jar ./photoelectric/photoelectric_kn.jar
echo "signing ./photoelectric/photoelectric_all_installer.jar"
sign-jar ./photoelectric/photoelectric_all_installer.jar
echo "signing ./photoelectric/photoelectric_sv.jar"
sign-jar ./photoelectric/photoelectric_sv.jar
echo "signing ./photoelectric/photoelectric_mr.jar"
sign-jar ./photoelectric/photoelectric_mr.jar
echo "signing ./photoelectric/photoelectric_eu.jar"
sign-jar ./photoelectric/photoelectric_eu.jar
echo "signing ./photoelectric/photoelectric_fa.jar"
sign-jar ./photoelectric/photoelectric_fa.jar
echo "signing ./photoelectric/photoelectric_uk.jar"
sign-jar ./photoelectric/photoelectric_uk.jar
echo "signing ./photoelectric/photoelectric_ja.jar"
sign-jar ./photoelectric/photoelectric_ja.jar
echo "signing ./photoelectric/photoelectric_iw.jar"
sign-jar ./photoelectric/photoelectric_iw.jar
echo "signing ./photoelectric/photoelectric_gl.jar"
sign-jar ./photoelectric/photoelectric_gl.jar
echo "signing ./photoelectric/photoelectric_ka.jar"
sign-jar ./photoelectric/photoelectric_ka.jar
echo "signing ./photoelectric/photoelectric_ar_SA.jar"
sign-jar ./photoelectric/photoelectric_ar_SA.jar
echo "signing ./photoelectric/photoelectric_ku_TR.jar"
sign-jar ./photoelectric/photoelectric_ku_TR.jar
echo "signing ./photoelectric/photoelectric_el.jar"
sign-jar ./photoelectric/photoelectric_el.jar
echo "signing ./photoelectric/photoelectric_hy.jar"
sign-jar ./photoelectric/photoelectric_hy.jar
echo "signing ./photoelectric/photoelectric_sr.jar"
sign-jar ./photoelectric/photoelectric_sr.jar
echo "signing ./photoelectric/photoelectric_ru.jar"
sign-jar ./photoelectric/photoelectric_ru.jar
echo "signing ./photoelectric/photoelectric_nl.jar"
sign-jar ./photoelectric/photoelectric_nl.jar
echo "signing ./photoelectric/photoelectric_tr.jar"
sign-jar ./photoelectric/photoelectric_tr.jar
echo "signing ./sound/sound_bs.jar"
sign-jar ./sound/sound_bs.jar
echo "signing ./sound/sound_pt.jar"
sign-jar ./sound/sound_pt.jar
echo "signing ./sound/sound_sk.jar"
sign-jar ./sound/sound_sk.jar
echo "signing ./sound/sound_sr.jar"
sign-jar ./sound/sound_sr.jar
echo "signing ./sound/sound_ga.jar"
sign-jar ./sound/sound_ga.jar
echo "signing ./sound/sound_it.jar"
sign-jar ./sound/sound_it.jar
echo "signing ./sound/sound_in.jar"
sign-jar ./sound/sound_in.jar
echo "signing ./sound/sound_km.jar"
sign-jar ./sound/sound_km.jar
echo "signing ./sound/sound_hr.jar"
sign-jar ./sound/sound_hr.jar
echo "signing ./sound/sound_iw.jar"
sign-jar ./sound/sound_iw.jar
echo "signing ./sound/sound_nb.jar"
sign-jar ./sound/sound_nb.jar
echo "signing ./sound/sound_ja.jar"
sign-jar ./sound/sound_ja.jar
echo "signing ./sound/sound_kk.jar"
sign-jar ./sound/sound_kk.jar
echo "signing ./sound/sound_pt_BR.jar"
sign-jar ./sound/sound_pt_BR.jar
echo "signing ./sound/sound_eu.jar"
sign-jar ./sound/sound_eu.jar
echo "signing ./sound/sound_et.jar"
sign-jar ./sound/sound_et.jar
echo "signing ./sound/sound_zh_TW.jar"
sign-jar ./sound/sound_zh_TW.jar
echo "signing ./sound/sound_ka.jar"
sign-jar ./sound/sound_ka.jar
echo "signing ./sound/sound_hu.jar"
sign-jar ./sound/sound_hu.jar
echo "signing ./sound/sound_el.jar"
sign-jar ./sound/sound_el.jar
echo "signing ./sound/sound_es_PE.jar"
sign-jar ./sound/sound_es_PE.jar
echo "signing ./sound/sound_ca.jar"
sign-jar ./sound/sound_ca.jar
echo "signing ./sound/sound_tk.jar"
sign-jar ./sound/sound_tk.jar
echo "signing ./sound/sound_de.jar"
sign-jar ./sound/sound_de.jar
echo "signing ./sound/sound_lo.jar"
sign-jar ./sound/sound_lo.jar
echo "signing ./sound/sound_pl.jar"
sign-jar ./sound/sound_pl.jar
echo "signing ./sound/sound_all.jar"
sign-jar ./sound/sound_all.jar
echo "signing ./sound/sound_es.jar"
sign-jar ./sound/sound_es.jar
echo "signing ./sound/sound_th.jar"
sign-jar ./sound/sound_th.jar
echo "signing ./sound/sound_cs.jar"
sign-jar ./sound/sound_cs.jar
echo "signing ./sound/sound_all_installer.jar"
sign-jar ./sound/sound_all_installer.jar
echo "signing ./sound/sound_kn.jar"
sign-jar ./sound/sound_kn.jar
echo "signing ./sound/sound_fa.jar"
sign-jar ./sound/sound_fa.jar
echo "signing ./sound/sound_lv.jar"
sign-jar ./sound/sound_lv.jar
echo "signing ./sound/sound_sv.jar"
sign-jar ./sound/sound_sv.jar
echo "signing ./sound/sound_fr.jar"
sign-jar ./sound/sound_fr.jar
echo "signing ./sound/sound_ku_TR.jar"
sign-jar ./sound/sound_ku_TR.jar
echo "signing ./sound/sound_da.jar"
sign-jar ./sound/sound_da.jar
echo "signing ./sound/sound_en.jar"
sign-jar ./sound/sound_en.jar
echo "signing ./sound/sound_tr.jar"
sign-jar ./sound/sound_tr.jar
echo "signing ./sound/sound_gl.jar"
sign-jar ./sound/sound_gl.jar
echo "signing ./sound/sound_nl.jar"
sign-jar ./sound/sound_nl.jar
echo "signing ./sound/sound_fi.jar"
sign-jar ./sound/sound_fi.jar
echo "signing ./sound/sound_mk.jar"
sign-jar ./sound/sound_mk.jar
echo "signing ./sound/sound_vi.jar"
sign-jar ./sound/sound_vi.jar
echo "signing ./sound/sound_uk.jar"
sign-jar ./sound/sound_uk.jar
echo "signing ./sound/sound_ko.jar"
sign-jar ./sound/sound_ko.jar
echo "signing ./sound/sound_zh_CN.jar"
sign-jar ./sound/sound_zh_CN.jar
echo "signing ./sound/sound_ar_SA.jar"
sign-jar ./sound/sound_ar_SA.jar
echo "signing ./states-of-matter/atomic-interactions_iw.jar"
sign-jar ./states-of-matter/atomic-interactions_iw.jar
echo "signing ./states-of-matter/atomic-interactions_ka.jar"
sign-jar ./states-of-matter/atomic-interactions_ka.jar
echo "signing ./states-of-matter/states-of-matter-basics_uz.jar"
sign-jar ./states-of-matter/states-of-matter-basics_uz.jar
echo "signing ./states-of-matter/states-of-matter-basics_in.jar"
sign-jar ./states-of-matter/states-of-matter-basics_in.jar
echo "signing ./states-of-matter/atomic-interactions_fa.jar"
sign-jar ./states-of-matter/atomic-interactions_fa.jar
echo "signing ./states-of-matter/states-of-matter-basics_ku_TR.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ku_TR.jar
echo "signing ./states-of-matter/states-of-matter_eu.jar"
sign-jar ./states-of-matter/states-of-matter_eu.jar
echo "signing ./states-of-matter/states-of-matter_de.jar"
sign-jar ./states-of-matter/states-of-matter_de.jar
echo "signing ./states-of-matter/states-of-matter-basics_mr.jar"
sign-jar ./states-of-matter/states-of-matter-basics_mr.jar
echo "signing ./states-of-matter/states-of-matter-basics_hu.jar"
sign-jar ./states-of-matter/states-of-matter-basics_hu.jar
echo "signing ./states-of-matter/states-of-matter-basics_en.jar"
sign-jar ./states-of-matter/states-of-matter-basics_en.jar
echo "signing ./states-of-matter/states-of-matter_hu.jar"
sign-jar ./states-of-matter/states-of-matter_hu.jar
echo "signing ./states-of-matter/atomic-interactions_bs.jar"
sign-jar ./states-of-matter/atomic-interactions_bs.jar
echo "signing ./states-of-matter/states-of-matter_ka.jar"
sign-jar ./states-of-matter/states-of-matter_ka.jar
echo "signing ./states-of-matter/atomic-interactions_hu.jar"
sign-jar ./states-of-matter/atomic-interactions_hu.jar
echo "signing ./states-of-matter/atomic-interactions_pl.jar"
sign-jar ./states-of-matter/atomic-interactions_pl.jar
echo "signing ./states-of-matter/atomic-interactions_km.jar"
sign-jar ./states-of-matter/atomic-interactions_km.jar
echo "signing ./states-of-matter/states-of-matter-basics_tk.jar"
sign-jar ./states-of-matter/states-of-matter-basics_tk.jar
echo "signing ./states-of-matter/atomic-interactions_et.jar"
sign-jar ./states-of-matter/atomic-interactions_et.jar
echo "signing ./states-of-matter/states-of-matter-basics_km.jar"
sign-jar ./states-of-matter/states-of-matter-basics_km.jar
echo "signing ./states-of-matter/states-of-matter_ar_SA.jar"
sign-jar ./states-of-matter/states-of-matter_ar_SA.jar
echo "signing ./states-of-matter/atomic-interactions_sk.jar"
sign-jar ./states-of-matter/atomic-interactions_sk.jar
echo "signing ./states-of-matter/states-of-matter-basics_pl.jar"
sign-jar ./states-of-matter/states-of-matter-basics_pl.jar
echo "signing ./states-of-matter/states-of-matter-basics_fr.jar"
sign-jar ./states-of-matter/states-of-matter-basics_fr.jar
echo "signing ./states-of-matter/states-of-matter-basics_et.jar"
sign-jar ./states-of-matter/states-of-matter-basics_et.jar
echo "signing ./states-of-matter/states-of-matter-basics_be.jar"
sign-jar ./states-of-matter/states-of-matter-basics_be.jar
echo "signing ./states-of-matter/atomic-interactions_ko.jar"
sign-jar ./states-of-matter/atomic-interactions_ko.jar
echo "signing ./states-of-matter/states-of-matter_mk.jar"
sign-jar ./states-of-matter/states-of-matter_mk.jar
echo "signing ./states-of-matter/atomic-interactions_tr.jar"
sign-jar ./states-of-matter/atomic-interactions_tr.jar
echo "signing ./states-of-matter/states-of-matter-basics_ar.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ar.jar
echo "signing ./states-of-matter/atomic-interactions_mr.jar"
sign-jar ./states-of-matter/atomic-interactions_mr.jar
echo "signing ./states-of-matter/atomic-interactions_en.jar"
sign-jar ./states-of-matter/atomic-interactions_en.jar
echo "signing ./states-of-matter/atomic-interactions_kn.jar"
sign-jar ./states-of-matter/atomic-interactions_kn.jar
echo "signing ./states-of-matter/states-of-matter-basics_ko.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ko.jar
echo "signing ./states-of-matter/states-of-matter-basics_sq.jar"
sign-jar ./states-of-matter/states-of-matter-basics_sq.jar
echo "signing ./states-of-matter/states-of-matter_cs.jar"
sign-jar ./states-of-matter/states-of-matter_cs.jar
echo "signing ./states-of-matter/states-of-matter-basics_ka.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ka.jar
echo "signing ./states-of-matter/states-of-matter_sk.jar"
sign-jar ./states-of-matter/states-of-matter_sk.jar
echo "signing ./states-of-matter/atomic-interactions_da.jar"
sign-jar ./states-of-matter/atomic-interactions_da.jar
echo "signing ./states-of-matter/states-of-matter_bs.jar"
sign-jar ./states-of-matter/states-of-matter_bs.jar
echo "signing ./states-of-matter/states-of-matter-basics_sv.jar"
sign-jar ./states-of-matter/states-of-matter-basics_sv.jar
echo "signing ./states-of-matter/states-of-matter_zh_CN.jar"
sign-jar ./states-of-matter/states-of-matter_zh_CN.jar
echo "signing ./states-of-matter/atomic-interactions_zh_CN.jar"
sign-jar ./states-of-matter/atomic-interactions_zh_CN.jar
echo "signing ./states-of-matter/atomic-interactions_de.jar"
sign-jar ./states-of-matter/atomic-interactions_de.jar
echo "signing ./states-of-matter/atomic-interactions_ku_TR.jar"
sign-jar ./states-of-matter/atomic-interactions_ku_TR.jar
echo "signing ./states-of-matter/states-of-matter_kn.jar"
sign-jar ./states-of-matter/states-of-matter_kn.jar
echo "signing ./states-of-matter/states-of-matter_zh_TW.jar"
sign-jar ./states-of-matter/states-of-matter_zh_TW.jar
echo "signing ./states-of-matter/states-of-matter_th.jar"
sign-jar ./states-of-matter/states-of-matter_th.jar
echo "signing ./states-of-matter/atomic-interactions_in.jar"
sign-jar ./states-of-matter/atomic-interactions_in.jar
echo "signing ./states-of-matter/atomic-interactions_gl.jar"
sign-jar ./states-of-matter/atomic-interactions_gl.jar
echo "signing ./states-of-matter/states-of-matter-basics_iw.jar"
sign-jar ./states-of-matter/states-of-matter-basics_iw.jar
echo "signing ./states-of-matter/states-of-matter_sr.jar"
sign-jar ./states-of-matter/states-of-matter_sr.jar
echo "signing ./states-of-matter/states-of-matter_vi.jar"
sign-jar ./states-of-matter/states-of-matter_vi.jar
echo "signing ./states-of-matter/states-of-matter_all.jar"
sign-jar ./states-of-matter/states-of-matter_all.jar
echo "signing ./states-of-matter/states-of-matter-basics_es.jar"
sign-jar ./states-of-matter/states-of-matter-basics_es.jar
echo "signing ./states-of-matter/atomic-interactions_mk.jar"
sign-jar ./states-of-matter/atomic-interactions_mk.jar
echo "signing ./states-of-matter/states-of-matter-basics_fa.jar"
sign-jar ./states-of-matter/states-of-matter-basics_fa.jar
echo "signing ./states-of-matter/states-of-matter_ku.jar"
sign-jar ./states-of-matter/states-of-matter_ku.jar
echo "signing ./states-of-matter/atomic-interactions_kk.jar"
sign-jar ./states-of-matter/atomic-interactions_kk.jar
echo "signing ./states-of-matter/atomic-interactions_cs.jar"
sign-jar ./states-of-matter/atomic-interactions_cs.jar
echo "signing ./states-of-matter/states-of-matter-basics_tr.jar"
sign-jar ./states-of-matter/states-of-matter-basics_tr.jar
echo "signing ./states-of-matter/states-of-matter_pt_BR.jar"
sign-jar ./states-of-matter/states-of-matter_pt_BR.jar
echo "signing ./states-of-matter/states-of-matter-basics_nb.jar"
sign-jar ./states-of-matter/states-of-matter-basics_nb.jar
echo "signing ./states-of-matter/atomic-interactions_el.jar"
sign-jar ./states-of-matter/atomic-interactions_el.jar
echo "signing ./states-of-matter/states-of-matter_fi.jar"
sign-jar ./states-of-matter/states-of-matter_fi.jar
echo "signing ./states-of-matter/states-of-matter-basics_th.jar"
sign-jar ./states-of-matter/states-of-matter-basics_th.jar
echo "signing ./states-of-matter/states-of-matter_fr.jar"
sign-jar ./states-of-matter/states-of-matter_fr.jar
echo "signing ./states-of-matter/states-of-matter-basics_ru.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ru.jar
echo "signing ./states-of-matter/atomic-interactions_vi.jar"
sign-jar ./states-of-matter/atomic-interactions_vi.jar
echo "signing ./states-of-matter/atomic-interactions_pt.jar"
sign-jar ./states-of-matter/atomic-interactions_pt.jar
echo "signing ./states-of-matter/atomic-interactions_fr.jar"
sign-jar ./states-of-matter/atomic-interactions_fr.jar
echo "signing ./states-of-matter/atomic-interactions_sq.jar"
sign-jar ./states-of-matter/atomic-interactions_sq.jar
echo "signing ./states-of-matter/states-of-matter_gl.jar"
sign-jar ./states-of-matter/states-of-matter_gl.jar
echo "signing ./states-of-matter/states-of-matter_it.jar"
sign-jar ./states-of-matter/states-of-matter_it.jar
echo "signing ./states-of-matter/states-of-matter_sq.jar"
sign-jar ./states-of-matter/states-of-matter_sq.jar
echo "signing ./states-of-matter/states-of-matter_ru.jar"
sign-jar ./states-of-matter/states-of-matter_ru.jar
echo "signing ./states-of-matter/states-of-matter-basics_eu.jar"
sign-jar ./states-of-matter/states-of-matter-basics_eu.jar
echo "signing ./states-of-matter/states-of-matter-basics_pt_BR.jar"
sign-jar ./states-of-matter/states-of-matter-basics_pt_BR.jar
echo "signing ./states-of-matter/atomic-interactions_es.jar"
sign-jar ./states-of-matter/atomic-interactions_es.jar
echo "signing ./states-of-matter/states-of-matter_kk.jar"
sign-jar ./states-of-matter/states-of-matter_kk.jar
echo "signing ./states-of-matter/states-of-matter-basics_ar_SA.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ar_SA.jar
echo "signing ./states-of-matter/states-of-matter-basics_vi.jar"
sign-jar ./states-of-matter/states-of-matter-basics_vi.jar
echo "signing ./states-of-matter/states-of-matter-basics_sk.jar"
sign-jar ./states-of-matter/states-of-matter-basics_sk.jar
echo "signing ./states-of-matter/states-of-matter-basics_el.jar"
sign-jar ./states-of-matter/states-of-matter-basics_el.jar
echo "signing ./states-of-matter/states-of-matter-basics_sr.jar"
sign-jar ./states-of-matter/states-of-matter-basics_sr.jar
echo "signing ./states-of-matter/atomic-interactions_ru.jar"
sign-jar ./states-of-matter/atomic-interactions_ru.jar
echo "signing ./states-of-matter/states-of-matter_sv.jar"
sign-jar ./states-of-matter/states-of-matter_sv.jar
echo "signing ./states-of-matter/states-of-matter-basics_ms.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ms.jar
echo "signing ./states-of-matter/atomic-interactions_uz.jar"
sign-jar ./states-of-matter/atomic-interactions_uz.jar
echo "signing ./states-of-matter/states-of-matter-basics_fi.jar"
sign-jar ./states-of-matter/states-of-matter-basics_fi.jar
echo "signing ./states-of-matter/states-of-matter_ms.jar"
sign-jar ./states-of-matter/states-of-matter_ms.jar
echo "signing ./states-of-matter/atomic-interactions_th.jar"
sign-jar ./states-of-matter/atomic-interactions_th.jar
echo "signing ./states-of-matter/states-of-matter_fa.jar"
sign-jar ./states-of-matter/states-of-matter_fa.jar
echo "signing ./states-of-matter/atomic-interactions_pt_BR.jar"
sign-jar ./states-of-matter/atomic-interactions_pt_BR.jar
echo "signing ./states-of-matter/atomic-interactions_ms.jar"
sign-jar ./states-of-matter/atomic-interactions_ms.jar
echo "signing ./states-of-matter/states-of-matter_en.jar"
sign-jar ./states-of-matter/states-of-matter_en.jar
echo "signing ./states-of-matter/states-of-matter_all_installer.jar"
sign-jar ./states-of-matter/states-of-matter_all_installer.jar
echo "signing ./states-of-matter/states-of-matter_in.jar"
sign-jar ./states-of-matter/states-of-matter_in.jar
echo "signing ./states-of-matter/states-of-matter_mr.jar"
sign-jar ./states-of-matter/states-of-matter_mr.jar
echo "signing ./states-of-matter/states-of-matter-basics_sl.jar"
sign-jar ./states-of-matter/states-of-matter-basics_sl.jar
echo "signing ./states-of-matter/states-of-matter-basics_it.jar"
sign-jar ./states-of-matter/states-of-matter-basics_it.jar
echo "signing ./states-of-matter/atomic-interactions_sr.jar"
sign-jar ./states-of-matter/atomic-interactions_sr.jar
echo "signing ./states-of-matter/atomic-interactions_ja.jar"
sign-jar ./states-of-matter/atomic-interactions_ja.jar
echo "signing ./states-of-matter/states-of-matter-basics_hr.jar"
sign-jar ./states-of-matter/states-of-matter-basics_hr.jar
echo "signing ./states-of-matter/atomic-interactions_eu.jar"
sign-jar ./states-of-matter/atomic-interactions_eu.jar
echo "signing ./states-of-matter/atomic-interactions_es_PE.jar"
sign-jar ./states-of-matter/atomic-interactions_es_PE.jar
echo "signing ./states-of-matter/atomic-interactions_sl.jar"
sign-jar ./states-of-matter/atomic-interactions_sl.jar
echo "signing ./states-of-matter/atomic-interactions_hr.jar"
sign-jar ./states-of-matter/atomic-interactions_hr.jar
echo "signing ./states-of-matter/states-of-matter-basics_mk.jar"
sign-jar ./states-of-matter/states-of-matter-basics_mk.jar
echo "signing ./states-of-matter/states-of-matter_el.jar"
sign-jar ./states-of-matter/states-of-matter_el.jar
echo "signing ./states-of-matter/states-of-matter_nl.jar"
sign-jar ./states-of-matter/states-of-matter_nl.jar
echo "signing ./states-of-matter/states-of-matter-basics_ja.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ja.jar
echo "signing ./states-of-matter/states-of-matter_km.jar"
sign-jar ./states-of-matter/states-of-matter_km.jar
echo "signing ./states-of-matter/states-of-matter_et.jar"
sign-jar ./states-of-matter/states-of-matter_et.jar
echo "signing ./states-of-matter/states-of-matter-basics_es_PE.jar"
sign-jar ./states-of-matter/states-of-matter-basics_es_PE.jar
echo "signing ./states-of-matter/states-of-matter-basics_bs.jar"
sign-jar ./states-of-matter/states-of-matter-basics_bs.jar
echo "signing ./states-of-matter/atomic-interactions_zh_TW.jar"
sign-jar ./states-of-matter/atomic-interactions_zh_TW.jar
echo "signing ./states-of-matter/states-of-matter-basics_gl.jar"
sign-jar ./states-of-matter/states-of-matter-basics_gl.jar
echo "signing ./states-of-matter/atomic-interactions_nb.jar"
sign-jar ./states-of-matter/atomic-interactions_nb.jar
echo "signing ./states-of-matter/states-of-matter_ku_TR.jar"
sign-jar ./states-of-matter/states-of-matter_ku_TR.jar
echo "signing ./states-of-matter/states-of-matter_uz.jar"
sign-jar ./states-of-matter/states-of-matter_uz.jar
echo "signing ./states-of-matter/states-of-matter_ar.jar"
sign-jar ./states-of-matter/states-of-matter_ar.jar
echo "signing ./states-of-matter/states-of-matter-basics_ku.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ku.jar
echo "signing ./states-of-matter/states-of-matter_iw.jar"
sign-jar ./states-of-matter/states-of-matter_iw.jar
echo "signing ./states-of-matter/states-of-matter-basics_ro.jar"
sign-jar ./states-of-matter/states-of-matter-basics_ro.jar
echo "signing ./states-of-matter/states-of-matter_ro.jar"
sign-jar ./states-of-matter/states-of-matter_ro.jar
echo "signing ./states-of-matter/atomic-interactions_tk.jar"
sign-jar ./states-of-matter/atomic-interactions_tk.jar
echo "signing ./states-of-matter/states-of-matter-basics_zh_CN.jar"
sign-jar ./states-of-matter/states-of-matter-basics_zh_CN.jar
echo "signing ./states-of-matter/states-of-matter_tk.jar"
sign-jar ./states-of-matter/states-of-matter_tk.jar
echo "signing ./states-of-matter/atomic-interactions_be.jar"
sign-jar ./states-of-matter/atomic-interactions_be.jar
echo "signing ./states-of-matter/states-of-matter-basics_nl.jar"
sign-jar ./states-of-matter/states-of-matter-basics_nl.jar
echo "signing ./states-of-matter/states-of-matter_sl.jar"
sign-jar ./states-of-matter/states-of-matter_sl.jar
echo "signing ./states-of-matter/atomic-interactions_ar.jar"
sign-jar ./states-of-matter/atomic-interactions_ar.jar
echo "signing ./states-of-matter/states-of-matter-basics_kn.jar"
sign-jar ./states-of-matter/states-of-matter-basics_kn.jar
echo "signing ./states-of-matter/atomic-interactions_nl.jar"
sign-jar ./states-of-matter/atomic-interactions_nl.jar
echo "signing ./states-of-matter/states-of-matter_hr.jar"
sign-jar ./states-of-matter/states-of-matter_hr.jar
echo "signing ./states-of-matter/states-of-matter-basics_cs.jar"
sign-jar ./states-of-matter/states-of-matter-basics_cs.jar
echo "signing ./states-of-matter/states-of-matter-basics_kk.jar"
sign-jar ./states-of-matter/states-of-matter-basics_kk.jar
echo "signing ./states-of-matter/states-of-matter_ko.jar"
sign-jar ./states-of-matter/states-of-matter_ko.jar
echo "signing ./states-of-matter/states-of-matter-basics_de.jar"
sign-jar ./states-of-matter/states-of-matter-basics_de.jar
echo "signing ./states-of-matter/atomic-interactions_it.jar"
sign-jar ./states-of-matter/atomic-interactions_it.jar
echo "signing ./states-of-matter/states-of-matter_tr.jar"
sign-jar ./states-of-matter/states-of-matter_tr.jar
echo "signing ./states-of-matter/atomic-interactions_fi.jar"
sign-jar ./states-of-matter/atomic-interactions_fi.jar
echo "signing ./states-of-matter/states-of-matter_es_PE.jar"
sign-jar ./states-of-matter/states-of-matter_es_PE.jar
echo "signing ./states-of-matter/states-of-matter_es.jar"
sign-jar ./states-of-matter/states-of-matter_es.jar
echo "signing ./states-of-matter/states-of-matter_ja.jar"
sign-jar ./states-of-matter/states-of-matter_ja.jar
echo "signing ./states-of-matter/atomic-interactions_ro.jar"
sign-jar ./states-of-matter/atomic-interactions_ro.jar
echo "signing ./states-of-matter/states-of-matter-basics_pt.jar"
sign-jar ./states-of-matter/states-of-matter-basics_pt.jar
echo "signing ./states-of-matter/states-of-matter_pl.jar"
sign-jar ./states-of-matter/states-of-matter_pl.jar
echo "signing ./states-of-matter/states-of-matter_nb.jar"
sign-jar ./states-of-matter/states-of-matter_nb.jar
echo "signing ./states-of-matter/atomic-interactions_sv.jar"
sign-jar ./states-of-matter/atomic-interactions_sv.jar
echo "signing ./states-of-matter/states-of-matter_pt.jar"
sign-jar ./states-of-matter/states-of-matter_pt.jar
echo "signing ./states-of-matter/states-of-matter_be.jar"
sign-jar ./states-of-matter/states-of-matter_be.jar
echo "signing ./states-of-matter/atomic-interactions_ku.jar"
sign-jar ./states-of-matter/atomic-interactions_ku.jar
echo "signing ./states-of-matter/states-of-matter-basics_zh_TW.jar"
sign-jar ./states-of-matter/states-of-matter-basics_zh_TW.jar
echo "signing ./states-of-matter/states-of-matter-basics_da.jar"
sign-jar ./states-of-matter/states-of-matter-basics_da.jar
echo "signing ./states-of-matter/atomic-interactions_ar_SA.jar"
sign-jar ./states-of-matter/atomic-interactions_ar_SA.jar
echo "signing ./states-of-matter/states-of-matter_da.jar"
sign-jar ./states-of-matter/states-of-matter_da.jar
echo "signing ./curve-fitting/curve-fitting_ko.jar"
sign-jar ./curve-fitting/curve-fitting_ko.jar
echo "signing ./curve-fitting/curve-fitting_fa.jar"
sign-jar ./curve-fitting/curve-fitting_fa.jar
echo "signing ./curve-fitting/curve-fitting_el.jar"
sign-jar ./curve-fitting/curve-fitting_el.jar
echo "signing ./curve-fitting/curve-fitting_in.jar"
sign-jar ./curve-fitting/curve-fitting_in.jar
echo "signing ./curve-fitting/curve-fitting_tk.jar"
sign-jar ./curve-fitting/curve-fitting_tk.jar
echo "signing ./curve-fitting/curve-fitting_ar_SA.jar"
sign-jar ./curve-fitting/curve-fitting_ar_SA.jar
echo "signing ./curve-fitting/curve-fitting_pl.jar"
sign-jar ./curve-fitting/curve-fitting_pl.jar
echo "signing ./curve-fitting/curve-fitting_it.jar"
sign-jar ./curve-fitting/curve-fitting_it.jar
echo "signing ./curve-fitting/curve-fitting_mk.jar"
sign-jar ./curve-fitting/curve-fitting_mk.jar
echo "signing ./curve-fitting/curve-fitting_nb.jar"
sign-jar ./curve-fitting/curve-fitting_nb.jar
echo "signing ./curve-fitting/curve-fitting_sr.jar"
sign-jar ./curve-fitting/curve-fitting_sr.jar
echo "signing ./curve-fitting/curve-fitting_bs.jar"
sign-jar ./curve-fitting/curve-fitting_bs.jar
echo "signing ./curve-fitting/curve-fitting_et.jar"
sign-jar ./curve-fitting/curve-fitting_et.jar
echo "signing ./curve-fitting/curve-fitting_de.jar"
sign-jar ./curve-fitting/curve-fitting_de.jar
echo "signing ./curve-fitting/curve-fitting_hu.jar"
sign-jar ./curve-fitting/curve-fitting_hu.jar
echo "signing ./curve-fitting/curve-fitting_af.jar"
sign-jar ./curve-fitting/curve-fitting_af.jar
echo "signing ./curve-fitting/curve-fitting_fi.jar"
sign-jar ./curve-fitting/curve-fitting_fi.jar
echo "signing ./curve-fitting/curve-fitting_ca.jar"
sign-jar ./curve-fitting/curve-fitting_ca.jar
echo "signing ./curve-fitting/curve-fitting_fr.jar"
sign-jar ./curve-fitting/curve-fitting_fr.jar
echo "signing ./curve-fitting/curve-fitting_sq.jar"
sign-jar ./curve-fitting/curve-fitting_sq.jar
echo "signing ./curve-fitting/curve-fitting_iw.jar"
sign-jar ./curve-fitting/curve-fitting_iw.jar
echo "signing ./curve-fitting/curve-fitting_tr.jar"
sign-jar ./curve-fitting/curve-fitting_tr.jar
echo "signing ./curve-fitting/curve-fitting_ja.jar"
sign-jar ./curve-fitting/curve-fitting_ja.jar
echo "signing ./curve-fitting/curve-fitting_ka.jar"
sign-jar ./curve-fitting/curve-fitting_ka.jar
echo "signing ./curve-fitting/curve-fitting_hr.jar"
sign-jar ./curve-fitting/curve-fitting_hr.jar
echo "signing ./curve-fitting/curve-fitting_sk.jar"
sign-jar ./curve-fitting/curve-fitting_sk.jar
echo "signing ./curve-fitting/curve-fitting_es_PE.jar"
sign-jar ./curve-fitting/curve-fitting_es_PE.jar
echo "signing ./curve-fitting/curve-fitting_pt_BR.jar"
sign-jar ./curve-fitting/curve-fitting_pt_BR.jar
echo "signing ./curve-fitting/curve-fitting_es.jar"
sign-jar ./curve-fitting/curve-fitting_es.jar
echo "signing ./curve-fitting/curve-fitting_vi.jar"
sign-jar ./curve-fitting/curve-fitting_vi.jar
echo "signing ./curve-fitting/curve-fitting_nl.jar"
sign-jar ./curve-fitting/curve-fitting_nl.jar
echo "signing ./curve-fitting/curve-fitting_en.jar"
sign-jar ./curve-fitting/curve-fitting_en.jar
echo "signing ./curve-fitting/curve-fitting_nn.jar"
sign-jar ./curve-fitting/curve-fitting_nn.jar
echo "signing ./curve-fitting/curve-fitting_eu.jar"
sign-jar ./curve-fitting/curve-fitting_eu.jar
echo "signing ./curve-fitting/curve-fitting_zh_TW.jar"
sign-jar ./curve-fitting/curve-fitting_zh_TW.jar
echo "signing ./curve-fitting/curve-fitting_kk.jar"
sign-jar ./curve-fitting/curve-fitting_kk.jar
echo "signing ./curve-fitting/curve-fitting_da.jar"
sign-jar ./curve-fitting/curve-fitting_da.jar
echo "signing ./curve-fitting/curve-fitting_zh_CN.jar"
sign-jar ./curve-fitting/curve-fitting_zh_CN.jar
echo "signing ./curve-fitting/curve-fitting_ar.jar"
sign-jar ./curve-fitting/curve-fitting_ar.jar
echo "signing ./optical-tweezers/molecular-motors_tk.jar"
sign-jar ./optical-tweezers/molecular-motors_tk.jar
echo "signing ./optical-tweezers/optical-tweezers_tr.jar"
sign-jar ./optical-tweezers/optical-tweezers_tr.jar
echo "signing ./optical-tweezers/optical-tweezers_vi.jar"
sign-jar ./optical-tweezers/optical-tweezers_vi.jar
echo "signing ./optical-tweezers/molecular-motors_pl.jar"
sign-jar ./optical-tweezers/molecular-motors_pl.jar
echo "signing ./optical-tweezers/stretching-dna_eu.jar"
sign-jar ./optical-tweezers/stretching-dna_eu.jar
echo "signing ./optical-tweezers/optical-tweezers_nl.jar"
sign-jar ./optical-tweezers/optical-tweezers_nl.jar
echo "signing ./optical-tweezers/molecular-motors_th.jar"
sign-jar ./optical-tweezers/molecular-motors_th.jar
echo "signing ./optical-tweezers/stretching-dna_da.jar"
sign-jar ./optical-tweezers/stretching-dna_da.jar
echo "signing ./optical-tweezers/molecular-motors_vi.jar"
sign-jar ./optical-tweezers/molecular-motors_vi.jar
echo "signing ./optical-tweezers/molecular-motors_hu.jar"
sign-jar ./optical-tweezers/molecular-motors_hu.jar
echo "signing ./optical-tweezers/optical-tweezers_ka.jar"
sign-jar ./optical-tweezers/optical-tweezers_ka.jar
echo "signing ./optical-tweezers/stretching-dna_hr.jar"
sign-jar ./optical-tweezers/stretching-dna_hr.jar
echo "signing ./optical-tweezers/stretching-dna_sk.jar"
sign-jar ./optical-tweezers/stretching-dna_sk.jar
echo "signing ./optical-tweezers/molecular-motors_ru.jar"
sign-jar ./optical-tweezers/molecular-motors_ru.jar
echo "signing ./optical-tweezers/optical-tweezers_ja.jar"
sign-jar ./optical-tweezers/optical-tweezers_ja.jar
echo "signing ./optical-tweezers/molecular-motors_el.jar"
sign-jar ./optical-tweezers/molecular-motors_el.jar
echo "signing ./optical-tweezers/stretching-dna_kk.jar"
sign-jar ./optical-tweezers/stretching-dna_kk.jar
echo "signing ./optical-tweezers/molecular-motors_km.jar"
sign-jar ./optical-tweezers/molecular-motors_km.jar
echo "signing ./optical-tweezers/optical-tweezers_mn.jar"
sign-jar ./optical-tweezers/optical-tweezers_mn.jar
echo "signing ./optical-tweezers/molecular-motors_pt_BR.jar"
sign-jar ./optical-tweezers/molecular-motors_pt_BR.jar
echo "signing ./optical-tweezers/optical-tweezers_bs.jar"
sign-jar ./optical-tweezers/optical-tweezers_bs.jar
echo "signing ./optical-tweezers/stretching-dna_nl.jar"
sign-jar ./optical-tweezers/stretching-dna_nl.jar
echo "signing ./optical-tweezers/stretching-dna_mk.jar"
sign-jar ./optical-tweezers/stretching-dna_mk.jar
echo "signing ./optical-tweezers/molecular-motors_bs.jar"
sign-jar ./optical-tweezers/molecular-motors_bs.jar
echo "signing ./optical-tweezers/molecular-motors_ja.jar"
sign-jar ./optical-tweezers/molecular-motors_ja.jar
echo "signing ./optical-tweezers/stretching-dna_ka.jar"
sign-jar ./optical-tweezers/stretching-dna_ka.jar
echo "signing ./optical-tweezers/stretching-dna_tk.jar"
sign-jar ./optical-tweezers/stretching-dna_tk.jar
echo "signing ./optical-tweezers/molecular-motors_ar_SA.jar"
sign-jar ./optical-tweezers/molecular-motors_ar_SA.jar
echo "signing ./optical-tweezers/optical-tweezers_eu.jar"
sign-jar ./optical-tweezers/optical-tweezers_eu.jar
echo "signing ./optical-tweezers/molecular-motors_nl.jar"
sign-jar ./optical-tweezers/molecular-motors_nl.jar
echo "signing ./optical-tweezers/molecular-motors_zh_CN.jar"
sign-jar ./optical-tweezers/molecular-motors_zh_CN.jar
echo "signing ./optical-tweezers/molecular-motors_kk.jar"
sign-jar ./optical-tweezers/molecular-motors_kk.jar
echo "signing ./optical-tweezers/optical-tweezers_ko.jar"
sign-jar ./optical-tweezers/optical-tweezers_ko.jar
echo "signing ./optical-tweezers/optical-tweezers_zh_TW.jar"
sign-jar ./optical-tweezers/optical-tweezers_zh_TW.jar
echo "signing ./optical-tweezers/molecular-motors_mn.jar"
sign-jar ./optical-tweezers/molecular-motors_mn.jar
echo "signing ./optical-tweezers/stretching-dna_fa.jar"
sign-jar ./optical-tweezers/stretching-dna_fa.jar
echo "signing ./optical-tweezers/optical-tweezers_zh_CN.jar"
sign-jar ./optical-tweezers/optical-tweezers_zh_CN.jar
echo "signing ./optical-tweezers/optical-tweezers_es_PE.jar"
sign-jar ./optical-tweezers/optical-tweezers_es_PE.jar
echo "signing ./optical-tweezers/optical-tweezers_all_installer.jar"
sign-jar ./optical-tweezers/optical-tweezers_all_installer.jar
echo "signing ./optical-tweezers/stretching-dna_zh_TW.jar"
sign-jar ./optical-tweezers/stretching-dna_zh_TW.jar
echo "signing ./optical-tweezers/optical-tweezers_el.jar"
sign-jar ./optical-tweezers/optical-tweezers_el.jar
echo "signing ./optical-tweezers/molecular-motors_fr.jar"
sign-jar ./optical-tweezers/molecular-motors_fr.jar
echo "signing ./optical-tweezers/optical-tweezers_km.jar"
sign-jar ./optical-tweezers/optical-tweezers_km.jar
echo "signing ./optical-tweezers/molecular-motors_en.jar"
sign-jar ./optical-tweezers/molecular-motors_en.jar
echo "signing ./optical-tweezers/molecular-motors_es.jar"
sign-jar ./optical-tweezers/molecular-motors_es.jar
echo "signing ./optical-tweezers/molecular-motors_da.jar"
sign-jar ./optical-tweezers/molecular-motors_da.jar
echo "signing ./optical-tweezers/stretching-dna_th.jar"
sign-jar ./optical-tweezers/stretching-dna_th.jar
echo "signing ./optical-tweezers/molecular-motors_es_PE.jar"
sign-jar ./optical-tweezers/molecular-motors_es_PE.jar
echo "signing ./optical-tweezers/molecular-motors_sk.jar"
sign-jar ./optical-tweezers/molecular-motors_sk.jar
echo "signing ./optical-tweezers/stretching-dna_in.jar"
sign-jar ./optical-tweezers/stretching-dna_in.jar
echo "signing ./optical-tweezers/optical-tweezers_de.jar"
sign-jar ./optical-tweezers/optical-tweezers_de.jar
echo "signing ./optical-tweezers/stretching-dna_hu.jar"
sign-jar ./optical-tweezers/stretching-dna_hu.jar
echo "signing ./optical-tweezers/stretching-dna_ko.jar"
sign-jar ./optical-tweezers/stretching-dna_ko.jar
echo "signing ./optical-tweezers/optical-tweezers_ru.jar"
sign-jar ./optical-tweezers/optical-tweezers_ru.jar
echo "signing ./optical-tweezers/optical-tweezers_in.jar"
sign-jar ./optical-tweezers/optical-tweezers_in.jar
echo "signing ./optical-tweezers/stretching-dna_mn.jar"
sign-jar ./optical-tweezers/stretching-dna_mn.jar
echo "signing ./optical-tweezers/molecular-motors_pt.jar"
sign-jar ./optical-tweezers/molecular-motors_pt.jar
echo "signing ./optical-tweezers/optical-tweezers_it.jar"
sign-jar ./optical-tweezers/optical-tweezers_it.jar
echo "signing ./optical-tweezers/optical-tweezers_hr.jar"
sign-jar ./optical-tweezers/optical-tweezers_hr.jar
echo "signing ./optical-tweezers/stretching-dna_sr.jar"
sign-jar ./optical-tweezers/stretching-dna_sr.jar
echo "signing ./optical-tweezers/molecular-motors_ka.jar"
sign-jar ./optical-tweezers/molecular-motors_ka.jar
echo "signing ./optical-tweezers/stretching-dna_pt_BR.jar"
sign-jar ./optical-tweezers/stretching-dna_pt_BR.jar
echo "signing ./optical-tweezers/stretching-dna_pt.jar"
sign-jar ./optical-tweezers/stretching-dna_pt.jar
echo "signing ./optical-tweezers/optical-tweezers_en.jar"
sign-jar ./optical-tweezers/optical-tweezers_en.jar
echo "signing ./optical-tweezers/stretching-dna_en.jar"
sign-jar ./optical-tweezers/stretching-dna_en.jar
echo "signing ./optical-tweezers/optical-tweezers_fa.jar"
sign-jar ./optical-tweezers/optical-tweezers_fa.jar
echo "signing ./optical-tweezers/stretching-dna_pl.jar"
sign-jar ./optical-tweezers/stretching-dna_pl.jar
echo "signing ./optical-tweezers/stretching-dna_ar_SA.jar"
sign-jar ./optical-tweezers/stretching-dna_ar_SA.jar
echo "signing ./optical-tweezers/optical-tweezers_mk.jar"
sign-jar ./optical-tweezers/optical-tweezers_mk.jar
echo "signing ./optical-tweezers/stretching-dna_bs.jar"
sign-jar ./optical-tweezers/stretching-dna_bs.jar
echo "signing ./optical-tweezers/molecular-motors_sr.jar"
sign-jar ./optical-tweezers/molecular-motors_sr.jar
echo "signing ./optical-tweezers/optical-tweezers_pt.jar"
sign-jar ./optical-tweezers/optical-tweezers_pt.jar
echo "signing ./optical-tweezers/optical-tweezers_pt_BR.jar"
sign-jar ./optical-tweezers/optical-tweezers_pt_BR.jar
echo "signing ./optical-tweezers/optical-tweezers_da.jar"
sign-jar ./optical-tweezers/optical-tweezers_da.jar
echo "signing ./optical-tweezers/stretching-dna_ja.jar"
sign-jar ./optical-tweezers/stretching-dna_ja.jar
echo "signing ./optical-tweezers/stretching-dna_zh_CN.jar"
sign-jar ./optical-tweezers/stretching-dna_zh_CN.jar
echo "signing ./optical-tweezers/molecular-motors_it.jar"
sign-jar ./optical-tweezers/molecular-motors_it.jar
echo "signing ./optical-tweezers/stretching-dna_tr.jar"
sign-jar ./optical-tweezers/stretching-dna_tr.jar
echo "signing ./optical-tweezers/molecular-motors_mk.jar"
sign-jar ./optical-tweezers/molecular-motors_mk.jar
echo "signing ./optical-tweezers/molecular-motors_tr.jar"
sign-jar ./optical-tweezers/molecular-motors_tr.jar
echo "signing ./optical-tweezers/stretching-dna_el.jar"
sign-jar ./optical-tweezers/stretching-dna_el.jar
echo "signing ./optical-tweezers/stretching-dna_es.jar"
sign-jar ./optical-tweezers/stretching-dna_es.jar
echo "signing ./optical-tweezers/stretching-dna_ru.jar"
sign-jar ./optical-tweezers/stretching-dna_ru.jar
echo "signing ./optical-tweezers/optical-tweezers_ar_SA.jar"
sign-jar ./optical-tweezers/optical-tweezers_ar_SA.jar
echo "signing ./optical-tweezers/stretching-dna_it.jar"
sign-jar ./optical-tweezers/stretching-dna_it.jar
echo "signing ./optical-tweezers/stretching-dna_fr.jar"
sign-jar ./optical-tweezers/stretching-dna_fr.jar
echo "signing ./optical-tweezers/stretching-dna_de.jar"
sign-jar ./optical-tweezers/stretching-dna_de.jar
echo "signing ./optical-tweezers/stretching-dna_es_PE.jar"
sign-jar ./optical-tweezers/stretching-dna_es_PE.jar
echo "signing ./optical-tweezers/optical-tweezers_th.jar"
sign-jar ./optical-tweezers/optical-tweezers_th.jar
echo "signing ./optical-tweezers/molecular-motors_eu.jar"
sign-jar ./optical-tweezers/molecular-motors_eu.jar
echo "signing ./optical-tweezers/molecular-motors_iw.jar"
sign-jar ./optical-tweezers/molecular-motors_iw.jar
echo "signing ./optical-tweezers/molecular-motors_zh_TW.jar"
sign-jar ./optical-tweezers/molecular-motors_zh_TW.jar
echo "signing ./optical-tweezers/optical-tweezers_hu.jar"
sign-jar ./optical-tweezers/optical-tweezers_hu.jar
echo "signing ./optical-tweezers/molecular-motors_hr.jar"
sign-jar ./optical-tweezers/molecular-motors_hr.jar
echo "signing ./optical-tweezers/molecular-motors_fa.jar"
sign-jar ./optical-tweezers/molecular-motors_fa.jar
echo "signing ./optical-tweezers/stretching-dna_km.jar"
sign-jar ./optical-tweezers/stretching-dna_km.jar
echo "signing ./optical-tweezers/molecular-motors_ko.jar"
sign-jar ./optical-tweezers/molecular-motors_ko.jar
echo "signing ./optical-tweezers/molecular-motors_de.jar"
sign-jar ./optical-tweezers/molecular-motors_de.jar
echo "signing ./optical-tweezers/stretching-dna_vi.jar"
sign-jar ./optical-tweezers/stretching-dna_vi.jar
echo "signing ./optical-tweezers/optical-tweezers_pl.jar"
sign-jar ./optical-tweezers/optical-tweezers_pl.jar
echo "signing ./optical-tweezers/optical-tweezers_sr.jar"
sign-jar ./optical-tweezers/optical-tweezers_sr.jar
echo "signing ./optical-tweezers/stretching-dna_iw.jar"
sign-jar ./optical-tweezers/stretching-dna_iw.jar
echo "signing ./optical-tweezers/optical-tweezers_kk.jar"
sign-jar ./optical-tweezers/optical-tweezers_kk.jar
echo "signing ./optical-tweezers/optical-tweezers_iw.jar"
sign-jar ./optical-tweezers/optical-tweezers_iw.jar
echo "signing ./optical-tweezers/optical-tweezers_tk.jar"
sign-jar ./optical-tweezers/optical-tweezers_tk.jar
echo "signing ./optical-tweezers/optical-tweezers_es.jar"
sign-jar ./optical-tweezers/optical-tweezers_es.jar
echo "signing ./optical-tweezers/optical-tweezers_sk.jar"
sign-jar ./optical-tweezers/optical-tweezers_sk.jar
echo "signing ./optical-tweezers/molecular-motors_in.jar"
sign-jar ./optical-tweezers/molecular-motors_in.jar
echo "signing ./optical-tweezers/optical-tweezers_fr.jar"
sign-jar ./optical-tweezers/optical-tweezers_fr.jar
echo "signing ./optical-tweezers/optical-tweezers_all.jar"
sign-jar ./optical-tweezers/optical-tweezers_all.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ko.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ko.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_sr.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_sr.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_fr.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_fr.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_kk.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_kk.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_cs.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_cs.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_es.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_es.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_it.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_it.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_am.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_am.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_tk.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_tk.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_et.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_et.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_eu.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_eu.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_en.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_en.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_nl.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_nl.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_kn.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_kn.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_lv.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_lv.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ru.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ru.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_pt.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_pt.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_in.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_in.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_tr.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_tr.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_pt_BR.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_pt_BR.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_fa.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_fa.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_gl.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_gl.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_zh_CN.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_zh_CN.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_vi.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_vi.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ku_TR.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ku_TR.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_sk.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_sk.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_da.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_da.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ar_SA.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ar_SA.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_iw.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_iw.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_pl.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_pl.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_uk.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_uk.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_hu.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_hu.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_es_PE.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_es_PE.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ka.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ka.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_bg.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_bg.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_hr.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_hr.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_el.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_el.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_mk.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_mk.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_fi.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_fi.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_th.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_th.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_sv.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_sv.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ro.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ro.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_az.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_az.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_zh_TW.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_zh_TW.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_be.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_be.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_mn.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_mn.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ar.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ar.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_bs.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_bs.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_de.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_de.jar
echo "signing ./resistance-in-a-wire/resistance-in-a-wire_ja.jar"
sign-jar ./resistance-in-a-wire/resistance-in-a-wire_ja.jar
echo "signing ./maze-game/maze-game_es_PE.jar"
sign-jar ./maze-game/maze-game_es_PE.jar
echo "signing ./maze-game/maze-game_th.jar"
sign-jar ./maze-game/maze-game_th.jar
echo "signing ./maze-game/maze-game_sv.jar"
sign-jar ./maze-game/maze-game_sv.jar
echo "signing ./maze-game/maze-game_es.jar"
sign-jar ./maze-game/maze-game_es.jar
echo "signing ./maze-game/maze-game_fr.jar"
sign-jar ./maze-game/maze-game_fr.jar
echo "signing ./maze-game/maze-game_it.jar"
sign-jar ./maze-game/maze-game_it.jar
echo "signing ./maze-game/maze-game_nl.jar"
sign-jar ./maze-game/maze-game_nl.jar
echo "signing ./maze-game/maze-game_zh_TW.jar"
sign-jar ./maze-game/maze-game_zh_TW.jar
echo "signing ./maze-game/maze-game_vi.jar"
sign-jar ./maze-game/maze-game_vi.jar
echo "signing ./maze-game/maze-game_ar.jar"
sign-jar ./maze-game/maze-game_ar.jar
echo "signing ./maze-game/maze-game_el.jar"
sign-jar ./maze-game/maze-game_el.jar
echo "signing ./maze-game/maze-game_hu.jar"
sign-jar ./maze-game/maze-game_hu.jar
echo "signing ./maze-game/maze-game_mk.jar"
sign-jar ./maze-game/maze-game_mk.jar
echo "signing ./maze-game/maze-game_lv.jar"
sign-jar ./maze-game/maze-game_lv.jar
echo "signing ./maze-game/maze-game_hr.jar"
sign-jar ./maze-game/maze-game_hr.jar
echo "signing ./maze-game/maze-game_zh_CN.jar"
sign-jar ./maze-game/maze-game_zh_CN.jar
echo "signing ./maze-game/maze-game_tk.jar"
sign-jar ./maze-game/maze-game_tk.jar
echo "signing ./maze-game/maze-game_in.jar"
sign-jar ./maze-game/maze-game_in.jar
echo "signing ./maze-game/maze-game_et.jar"
sign-jar ./maze-game/maze-game_et.jar
echo "signing ./maze-game/maze-game_de.jar"
sign-jar ./maze-game/maze-game_de.jar
echo "signing ./maze-game/maze-game_ko.jar"
sign-jar ./maze-game/maze-game_ko.jar
echo "signing ./maze-game/maze-game_cs.jar"
sign-jar ./maze-game/maze-game_cs.jar
echo "signing ./maze-game/maze-game_all_installer.jar"
sign-jar ./maze-game/maze-game_all_installer.jar
echo "signing ./maze-game/maze-game_da.jar"
sign-jar ./maze-game/maze-game_da.jar
echo "signing ./maze-game/maze-game_es_CO.jar"
sign-jar ./maze-game/maze-game_es_CO.jar
echo "signing ./maze-game/maze-game_kk.jar"
sign-jar ./maze-game/maze-game_kk.jar
echo "signing ./maze-game/maze-game_sk.jar"
sign-jar ./maze-game/maze-game_sk.jar
echo "signing ./maze-game/maze-game_sr.jar"
sign-jar ./maze-game/maze-game_sr.jar
echo "signing ./maze-game/maze-game_iw.jar"
sign-jar ./maze-game/maze-game_iw.jar
echo "signing ./maze-game/maze-game_eu.jar"
sign-jar ./maze-game/maze-game_eu.jar
echo "signing ./maze-game/maze-game_tr.jar"
sign-jar ./maze-game/maze-game_tr.jar
echo "signing ./maze-game/maze-game_all.jar"
sign-jar ./maze-game/maze-game_all.jar
echo "signing ./maze-game/maze-game_fa.jar"
sign-jar ./maze-game/maze-game_fa.jar
echo "signing ./maze-game/maze-game_ja.jar"
sign-jar ./maze-game/maze-game_ja.jar
echo "signing ./maze-game/maze-game_pl.jar"
sign-jar ./maze-game/maze-game_pl.jar
echo "signing ./maze-game/maze-game_km.jar"
sign-jar ./maze-game/maze-game_km.jar
echo "signing ./maze-game/maze-game_gl.jar"
sign-jar ./maze-game/maze-game_gl.jar
echo "signing ./maze-game/maze-game_pt.jar"
sign-jar ./maze-game/maze-game_pt.jar
echo "signing ./maze-game/maze-game_en.jar"
sign-jar ./maze-game/maze-game_en.jar
echo "signing ./plate-tectonics/plate-tectonics_pt_BR.jar"
sign-jar ./plate-tectonics/plate-tectonics_pt_BR.jar
echo "signing ./plate-tectonics/plate-tectonics_ku.jar"
sign-jar ./plate-tectonics/plate-tectonics_ku.jar
echo "signing ./plate-tectonics/plate-tectonics_mi.jar"
sign-jar ./plate-tectonics/plate-tectonics_mi.jar
echo "signing ./plate-tectonics/plate-tectonics_sr.jar"
sign-jar ./plate-tectonics/plate-tectonics_sr.jar
echo "signing ./plate-tectonics/plate-tectonics_el.jar"
sign-jar ./plate-tectonics/plate-tectonics_el.jar
echo "signing ./plate-tectonics/plate-tectonics_de.jar"
sign-jar ./plate-tectonics/plate-tectonics_de.jar
echo "signing ./plate-tectonics/plate-tectonics_bs.jar"
sign-jar ./plate-tectonics/plate-tectonics_bs.jar
echo "signing ./plate-tectonics/plate-tectonics_sk.jar"
sign-jar ./plate-tectonics/plate-tectonics_sk.jar
echo "signing ./plate-tectonics/native_linux.jar"
sign-jar ./plate-tectonics/native_linux.jar
echo "signing ./plate-tectonics/plate-tectonics_mr.jar"
sign-jar ./plate-tectonics/plate-tectonics_mr.jar
echo "signing ./plate-tectonics/plate-tectonics_mk.jar"
sign-jar ./plate-tectonics/plate-tectonics_mk.jar
echo "signing ./plate-tectonics/plate-tectonics_tk.jar"
sign-jar ./plate-tectonics/plate-tectonics_tk.jar
echo "signing ./plate-tectonics/plate-tectonics_nl.jar"
sign-jar ./plate-tectonics/plate-tectonics_nl.jar
echo "signing ./plate-tectonics/plate-tectonics_vi.jar"
sign-jar ./plate-tectonics/plate-tectonics_vi.jar
echo "signing ./plate-tectonics/plate-tectonics_tr.jar"
sign-jar ./plate-tectonics/plate-tectonics_tr.jar
echo "signing ./plate-tectonics/native_macosx.jar"
sign-jar ./plate-tectonics/native_macosx.jar
echo "signing ./plate-tectonics/plate-tectonics_pl.jar"
sign-jar ./plate-tectonics/plate-tectonics_pl.jar
echo "signing ./plate-tectonics/plate-tectonics_zh_TW.jar"
sign-jar ./plate-tectonics/plate-tectonics_zh_TW.jar
echo "signing ./plate-tectonics/plate-tectonics_hu.jar"
sign-jar ./plate-tectonics/plate-tectonics_hu.jar
echo "signing ./plate-tectonics/plate-tectonics_eu.jar"
sign-jar ./plate-tectonics/plate-tectonics_eu.jar
echo "signing ./plate-tectonics/plate-tectonics_is.jar"
sign-jar ./plate-tectonics/plate-tectonics_is.jar
echo "signing ./plate-tectonics/plate-tectonics_sv.jar"
sign-jar ./plate-tectonics/plate-tectonics_sv.jar
echo "signing ./plate-tectonics/plate-tectonics_da.jar"
sign-jar ./plate-tectonics/plate-tectonics_da.jar
echo "signing ./plate-tectonics/plate-tectonics_all.jar"
sign-jar ./plate-tectonics/plate-tectonics_all.jar
echo "signing ./plate-tectonics/plate-tectonics_ja.jar"
sign-jar ./plate-tectonics/plate-tectonics_ja.jar
echo "signing ./plate-tectonics/plate-tectonics_zh_CN.jar"
sign-jar ./plate-tectonics/plate-tectonics_zh_CN.jar
echo "signing ./plate-tectonics/plate-tectonics_ko.jar"
sign-jar ./plate-tectonics/plate-tectonics_ko.jar
echo "signing ./plate-tectonics/plate-tectonics_th.jar"
sign-jar ./plate-tectonics/plate-tectonics_th.jar
echo "signing ./plate-tectonics/plate-tectonics_iw.jar"
sign-jar ./plate-tectonics/plate-tectonics_iw.jar
echo "signing ./plate-tectonics/plate-tectonics_fa.jar"
sign-jar ./plate-tectonics/plate-tectonics_fa.jar
echo "signing ./plate-tectonics/plate-tectonics_all_installer.jar"
sign-jar ./plate-tectonics/plate-tectonics_all_installer.jar
echo "signing ./plate-tectonics/plate-tectonics_fr.jar"
sign-jar ./plate-tectonics/plate-tectonics_fr.jar
echo "signing ./plate-tectonics/plate-tectonics_es_ES.jar"
sign-jar ./plate-tectonics/plate-tectonics_es_ES.jar
echo "signing ./plate-tectonics/plate-tectonics_it.jar"
sign-jar ./plate-tectonics/plate-tectonics_it.jar
echo "signing ./plate-tectonics/native_solaris.jar"
sign-jar ./plate-tectonics/native_solaris.jar
echo "signing ./plate-tectonics/plate-tectonics_es.jar"
sign-jar ./plate-tectonics/plate-tectonics_es.jar
echo "signing ./plate-tectonics/plate-tectonics_cs.jar"
sign-jar ./plate-tectonics/plate-tectonics_cs.jar
echo "signing ./plate-tectonics/native_windows.jar"
sign-jar ./plate-tectonics/native_windows.jar
echo "signing ./plate-tectonics/plate-tectonics_kk.jar"
sign-jar ./plate-tectonics/plate-tectonics_kk.jar
echo "signing ./plate-tectonics/plate-tectonics_es_PE.jar"
sign-jar ./plate-tectonics/plate-tectonics_es_PE.jar
echo "signing ./plate-tectonics/plate-tectonics_en.jar"
sign-jar ./plate-tectonics/plate-tectonics_en.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_zh_TW.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_zh_TW.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_tk.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_tk.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_da.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_da.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_el.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_el.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_hu.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_hu.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_en.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_en.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_pt_BR.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_pt_BR.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_fa.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_fa.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_iw.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_iw.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_sr.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_sr.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_all.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_all.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_sv.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_sv.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_es.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_es.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_kk.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_kk.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_pt.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_pt.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_mk.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_mk.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_es_PE.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_es_PE.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_bs.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_bs.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_nl.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_nl.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_tr.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_tr.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_ja.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_ja.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_zh_CN.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_zh_CN.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_all_installer.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_all_installer.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_ko.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_ko.jar
echo "signing ./self-driven-particle-model/self-driven-particle-model_de.jar"
sign-jar ./self-driven-particle-model/self-driven-particle-model_de.jar
echo "signing ./greenhouse/greenhouse_eu.jar"
sign-jar ./greenhouse/greenhouse_eu.jar
echo "signing ./greenhouse/greenhouse_hr.jar"
sign-jar ./greenhouse/greenhouse_hr.jar
echo "signing ./greenhouse/greenhouse_uk.jar"
sign-jar ./greenhouse/greenhouse_uk.jar
echo "signing ./greenhouse/greenhouse_vi.jar"
sign-jar ./greenhouse/greenhouse_vi.jar
echo "signing ./greenhouse/greenhouse_cs.jar"
sign-jar ./greenhouse/greenhouse_cs.jar
echo "signing ./greenhouse/greenhouse_fa.jar"
sign-jar ./greenhouse/greenhouse_fa.jar
echo "signing ./greenhouse/greenhouse_it.jar"
sign-jar ./greenhouse/greenhouse_it.jar
echo "signing ./greenhouse/greenhouse_th.jar"
sign-jar ./greenhouse/greenhouse_th.jar
echo "signing ./greenhouse/greenhouse_hu.jar"
sign-jar ./greenhouse/greenhouse_hu.jar
echo "signing ./greenhouse/greenhouse_fi.jar"
sign-jar ./greenhouse/greenhouse_fi.jar
echo "signing ./greenhouse/greenhouse_tr.jar"
sign-jar ./greenhouse/greenhouse_tr.jar
echo "signing ./greenhouse/greenhouse_sv.jar"
sign-jar ./greenhouse/greenhouse_sv.jar
echo "signing ./greenhouse/greenhouse_kn.jar"
sign-jar ./greenhouse/greenhouse_kn.jar
echo "signing ./greenhouse/greenhouse_ko.jar"
sign-jar ./greenhouse/greenhouse_ko.jar
echo "signing ./greenhouse/greenhouse_de.jar"
sign-jar ./greenhouse/greenhouse_de.jar
echo "signing ./greenhouse/greenhouse_es_PE.jar"
sign-jar ./greenhouse/greenhouse_es_PE.jar
echo "signing ./greenhouse/greenhouse_ms.jar"
sign-jar ./greenhouse/greenhouse_ms.jar
echo "signing ./greenhouse/greenhouse_sr.jar"
sign-jar ./greenhouse/greenhouse_sr.jar
echo "signing ./greenhouse/greenhouse_nb.jar"
sign-jar ./greenhouse/greenhouse_nb.jar
echo "signing ./greenhouse/greenhouse_all_installer.jar"
sign-jar ./greenhouse/greenhouse_all_installer.jar
echo "signing ./greenhouse/greenhouse_gl.jar"
sign-jar ./greenhouse/greenhouse_gl.jar
echo "signing ./greenhouse/greenhouse_pt_BR.jar"
sign-jar ./greenhouse/greenhouse_pt_BR.jar
echo "signing ./greenhouse/greenhouse_tk.jar"
sign-jar ./greenhouse/greenhouse_tk.jar
echo "signing ./greenhouse/greenhouse_bs.jar"
sign-jar ./greenhouse/greenhouse_bs.jar
echo "signing ./greenhouse/greenhouse_zh_CN.jar"
sign-jar ./greenhouse/greenhouse_zh_CN.jar
echo "signing ./greenhouse/greenhouse_km.jar"
sign-jar ./greenhouse/greenhouse_km.jar
echo "signing ./greenhouse/greenhouse_sq.jar"
sign-jar ./greenhouse/greenhouse_sq.jar
echo "signing ./greenhouse/greenhouse_all.jar"
sign-jar ./greenhouse/greenhouse_all.jar
echo "signing ./greenhouse/greenhouse_sk.jar"
sign-jar ./greenhouse/greenhouse_sk.jar
echo "signing ./greenhouse/greenhouse_ar.jar"
sign-jar ./greenhouse/greenhouse_ar.jar
echo "signing ./greenhouse/greenhouse_es_ES.jar"
sign-jar ./greenhouse/greenhouse_es_ES.jar
echo "signing ./greenhouse/greenhouse_el.jar"
sign-jar ./greenhouse/greenhouse_el.jar
echo "signing ./greenhouse/greenhouse_pl.jar"
sign-jar ./greenhouse/greenhouse_pl.jar
echo "signing ./greenhouse/greenhouse_fr.jar"
sign-jar ./greenhouse/greenhouse_fr.jar
echo "signing ./greenhouse/greenhouse_en.jar"
sign-jar ./greenhouse/greenhouse_en.jar
echo "signing ./greenhouse/greenhouse_ja.jar"
sign-jar ./greenhouse/greenhouse_ja.jar
echo "signing ./greenhouse/greenhouse_zh_TW.jar"
sign-jar ./greenhouse/greenhouse_zh_TW.jar
echo "signing ./greenhouse/greenhouse_nl.jar"
sign-jar ./greenhouse/greenhouse_nl.jar
echo "signing ./greenhouse/greenhouse_be.jar"
sign-jar ./greenhouse/greenhouse_be.jar
echo "signing ./greenhouse/greenhouse_et.jar"
sign-jar ./greenhouse/greenhouse_et.jar
echo "signing ./greenhouse/greenhouse_pt.jar"
sign-jar ./greenhouse/greenhouse_pt.jar
echo "signing ./greenhouse/greenhouse_iw.jar"
sign-jar ./greenhouse/greenhouse_iw.jar
echo "signing ./greenhouse/greenhouse_da.jar"
sign-jar ./greenhouse/greenhouse_da.jar
echo "signing ./greenhouse/greenhouse_mk.jar"
sign-jar ./greenhouse/greenhouse_mk.jar
echo "signing ./greenhouse/greenhouse_es.jar"
sign-jar ./greenhouse/greenhouse_es.jar
echo "signing ./rutherford-scattering/rutherford-scattering_zh_CN.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_zh_CN.jar
echo "signing ./rutherford-scattering/rutherford-scattering_cs.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_cs.jar
echo "signing ./rutherford-scattering/rutherford-scattering_gl.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_gl.jar
echo "signing ./rutherford-scattering/rutherford-scattering_kk.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_kk.jar
echo "signing ./rutherford-scattering/rutherford-scattering_ja.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_ja.jar
echo "signing ./rutherford-scattering/rutherford-scattering_fi.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_fi.jar
echo "signing ./rutherford-scattering/rutherford-scattering_kn.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_kn.jar
echo "signing ./rutherford-scattering/rutherford-scattering_et.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_et.jar
echo "signing ./rutherford-scattering/rutherford-scattering_tr.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_tr.jar
echo "signing ./rutherford-scattering/rutherford-scattering_ny.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_ny.jar
echo "signing ./rutherford-scattering/rutherford-scattering_ko.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_ko.jar
echo "signing ./rutherford-scattering/rutherford-scattering_all.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_all.jar
echo "signing ./rutherford-scattering/rutherford-scattering_eu.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_eu.jar
echo "signing ./rutherford-scattering/rutherford-scattering_vi.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_vi.jar
echo "signing ./rutherford-scattering/rutherford-scattering_es_PE.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_es_PE.jar
echo "signing ./rutherford-scattering/rutherford-scattering_es.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_es.jar
echo "signing ./rutherford-scattering/rutherford-scattering_zh_TW.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_zh_TW.jar
echo "signing ./rutherford-scattering/rutherford-scattering_hr.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_hr.jar
echo "signing ./rutherford-scattering/rutherford-scattering_pt_BR.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_pt_BR.jar
echo "signing ./rutherford-scattering/rutherford-scattering_de.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_de.jar
echo "signing ./rutherford-scattering/rutherford-scattering_sv.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_sv.jar
echo "signing ./rutherford-scattering/rutherford-scattering_ar_SA.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_ar_SA.jar
echo "signing ./rutherford-scattering/rutherford-scattering_it.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_it.jar
echo "signing ./rutherford-scattering/rutherford-scattering_pl.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_pl.jar
echo "signing ./rutherford-scattering/rutherford-scattering_iw.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_iw.jar
echo "signing ./rutherford-scattering/rutherford-scattering_ar.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_ar.jar
echo "signing ./rutherford-scattering/rutherford-scattering_bs.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_bs.jar
echo "signing ./rutherford-scattering/rutherford-scattering_fa.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_fa.jar
echo "signing ./rutherford-scattering/rutherford-scattering_fr.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_fr.jar
echo "signing ./rutherford-scattering/rutherford-scattering_all_installer.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_all_installer.jar
echo "signing ./rutherford-scattering/rutherford-scattering_da.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_da.jar
echo "signing ./rutherford-scattering/rutherford-scattering_in.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_in.jar
echo "signing ./rutherford-scattering/rutherford-scattering_el.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_el.jar
echo "signing ./rutherford-scattering/rutherford-scattering_en.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_en.jar
echo "signing ./rutherford-scattering/rutherford-scattering_uk.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_uk.jar
echo "signing ./rutherford-scattering/rutherford-scattering_pt.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_pt.jar
echo "signing ./rutherford-scattering/rutherford-scattering_mk.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_mk.jar
echo "signing ./rutherford-scattering/rutherford-scattering_th.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_th.jar
echo "signing ./rutherford-scattering/rutherford-scattering_hu.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_hu.jar
echo "signing ./rutherford-scattering/rutherford-scattering_km.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_km.jar
echo "signing ./rutherford-scattering/rutherford-scattering_tk.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_tk.jar
echo "signing ./rutherford-scattering/rutherford-scattering_be.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_be.jar
echo "signing ./rutherford-scattering/rutherford-scattering_sk.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_sk.jar
echo "signing ./rutherford-scattering/rutherford-scattering_nl.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_nl.jar
echo "signing ./rutherford-scattering/rutherford-scattering_sr.jar"
sign-jar ./rutherford-scattering/rutherford-scattering_sr.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_it.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_it.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_de.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_de.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_eu.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_eu.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_kk.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_kk.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_pt_BR.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_pt_BR.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_gl.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_gl.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_vi.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_vi.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_th.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_th.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_zh_TW.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_zh_TW.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_iw.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_iw.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_all.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_all.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_bs.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_bs.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ms.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ms.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ta.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ta.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ar.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ar.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_tr.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_tr.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_da.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_da.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_sk.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_sk.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_es_PE.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_es_PE.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ko.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ko.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_sr.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_sr.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_all_installer.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_all_installer.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_es_ES.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_es_ES.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_sv.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_sv.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ro.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ro.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_cs.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_cs.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_pl.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_pl.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_in.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_in.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_zh_CN.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_zh_CN.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_mk.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_mk.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_en.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_en.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_nl.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_nl.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ku_TR.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ku_TR.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ca.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ca.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_be.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_be.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_hu.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_hu.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_fa.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_fa.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_es.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_es.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_el.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_el.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_fr.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_fr.jar
echo "signing ./forces-and-motion-basics/forces-and-motion-basics_ar_SA.jar"
sign-jar ./forces-and-motion-basics/forces-and-motion-basics_ar_SA.jar
echo "signing ./ph-scale/ph-scale_in.jar"
sign-jar ./ph-scale/ph-scale_in.jar
echo "signing ./ph-scale/ph-scale_mi.jar"
sign-jar ./ph-scale/ph-scale_mi.jar
echo "signing ./ph-scale/ph-scale_da.jar"
sign-jar ./ph-scale/ph-scale_da.jar
echo "signing ./ph-scale/ph-scale_hu.jar"
sign-jar ./ph-scale/ph-scale_hu.jar
echo "signing ./ph-scale/ph-scale_mk.jar"
sign-jar ./ph-scale/ph-scale_mk.jar
echo "signing ./ph-scale/ph-scale_pt.jar"
sign-jar ./ph-scale/ph-scale_pt.jar
echo "signing ./ph-scale/ph-scale_nl.jar"
sign-jar ./ph-scale/ph-scale_nl.jar
echo "signing ./ph-scale/ph-scale_tr.jar"
sign-jar ./ph-scale/ph-scale_tr.jar
echo "signing ./ph-scale/ph-scale_tk.jar"
sign-jar ./ph-scale/ph-scale_tk.jar
echo "signing ./ph-scale/ph-scale_all_installer.jar"
sign-jar ./ph-scale/ph-scale_all_installer.jar
echo "signing ./ph-scale/ph-scale_es.jar"
sign-jar ./ph-scale/ph-scale_es.jar
echo "signing ./ph-scale/ph-scale_eu.jar"
sign-jar ./ph-scale/ph-scale_eu.jar
echo "signing ./ph-scale/ph-scale_th.jar"
sign-jar ./ph-scale/ph-scale_th.jar
echo "signing ./ph-scale/ph-scale_es_PE.jar"
sign-jar ./ph-scale/ph-scale_es_PE.jar
echo "signing ./ph-scale/ph-scale_en.jar"
sign-jar ./ph-scale/ph-scale_en.jar
echo "signing ./ph-scale/ph-scale_hr.jar"
sign-jar ./ph-scale/ph-scale_hr.jar
echo "signing ./ph-scale/ph-scale_ja.jar"
sign-jar ./ph-scale/ph-scale_ja.jar
echo "signing ./ph-scale/ph-scale_fa.jar"
sign-jar ./ph-scale/ph-scale_fa.jar
echo "signing ./ph-scale/ph-scale_ar.jar"
sign-jar ./ph-scale/ph-scale_ar.jar
echo "signing ./ph-scale/ph-scale_sl.jar"
sign-jar ./ph-scale/ph-scale_sl.jar
echo "signing ./ph-scale/ph-scale_el.jar"
sign-jar ./ph-scale/ph-scale_el.jar
echo "signing ./ph-scale/ph-scale_iw.jar"
sign-jar ./ph-scale/ph-scale_iw.jar
echo "signing ./ph-scale/ph-scale_sv.jar"
sign-jar ./ph-scale/ph-scale_sv.jar
echo "signing ./ph-scale/ph-scale_zh_TW.jar"
sign-jar ./ph-scale/ph-scale_zh_TW.jar
echo "signing ./ph-scale/ph-scale_de.jar"
sign-jar ./ph-scale/ph-scale_de.jar
echo "signing ./ph-scale/ph-scale_nb.jar"
sign-jar ./ph-scale/ph-scale_nb.jar
echo "signing ./ph-scale/ph-scale_bs.jar"
sign-jar ./ph-scale/ph-scale_bs.jar
echo "signing ./ph-scale/ph-scale_all.jar"
sign-jar ./ph-scale/ph-scale_all.jar
echo "signing ./ph-scale/ph-scale_fi.jar"
sign-jar ./ph-scale/ph-scale_fi.jar
echo "signing ./ph-scale/ph-scale_fr.jar"
sign-jar ./ph-scale/ph-scale_fr.jar
echo "signing ./ph-scale/ph-scale_ko.jar"
sign-jar ./ph-scale/ph-scale_ko.jar
echo "signing ./ph-scale/ph-scale_ka.jar"
sign-jar ./ph-scale/ph-scale_ka.jar
echo "signing ./ph-scale/ph-scale_pt_BR.jar"
sign-jar ./ph-scale/ph-scale_pt_BR.jar
echo "signing ./ph-scale/ph-scale_sk.jar"
sign-jar ./ph-scale/ph-scale_sk.jar
echo "signing ./ph-scale/ph-scale_et.jar"
sign-jar ./ph-scale/ph-scale_et.jar
echo "signing ./ph-scale/ph-scale_pl.jar"
sign-jar ./ph-scale/ph-scale_pl.jar
echo "signing ./ph-scale/ph-scale_cs.jar"
sign-jar ./ph-scale/ph-scale_cs.jar
echo "signing ./ph-scale/ph-scale_gl.jar"
sign-jar ./ph-scale/ph-scale_gl.jar
echo "signing ./ph-scale/ph-scale_sq.jar"
sign-jar ./ph-scale/ph-scale_sq.jar
echo "signing ./ph-scale/ph-scale_zh_CN.jar"
sign-jar ./ph-scale/ph-scale_zh_CN.jar
echo "signing ./ph-scale/ph-scale_ru.jar"
sign-jar ./ph-scale/ph-scale_ru.jar
echo "signing ./ph-scale/ph-scale_lv.jar"
sign-jar ./ph-scale/ph-scale_lv.jar
echo "signing ./ph-scale/ph-scale_vi.jar"
sign-jar ./ph-scale/ph-scale_vi.jar
echo "signing ./ph-scale/ph-scale_kk.jar"
sign-jar ./ph-scale/ph-scale_kk.jar
echo "signing ./ph-scale/ph-scale_it.jar"
sign-jar ./ph-scale/ph-scale_it.jar
echo "signing ./ph-scale/ph-scale_sr.jar"
sign-jar ./ph-scale/ph-scale_sr.jar
echo "signing ./ph-scale/ph-scale_ar_SA.jar"
sign-jar ./ph-scale/ph-scale_ar_SA.jar
echo "signing ./acid-base-solutions/acid-base-solutions_fi.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_fi.jar
echo "signing ./acid-base-solutions/acid-base-solutions_it.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_it.jar
echo "signing ./acid-base-solutions/acid-base-solutions_nl.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_nl.jar
echo "signing ./acid-base-solutions/acid-base-solutions_all_installer.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_all_installer.jar
echo "signing ./acid-base-solutions/acid-base-solutions_eu.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_eu.jar
echo "signing ./acid-base-solutions/acid-base-solutions_de.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_de.jar
echo "signing ./acid-base-solutions/acid-base-solutions_hu.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_hu.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ku_TR.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ku_TR.jar
echo "signing ./acid-base-solutions/acid-base-solutions_en.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_en.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ru.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ru.jar
echo "signing ./acid-base-solutions/acid-base-solutions_et.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_et.jar
echo "signing ./acid-base-solutions/acid-base-solutions_pt_BR.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_pt_BR.jar
echo "signing ./acid-base-solutions/acid-base-solutions_es_PE.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_es_PE.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ko.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ko.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ca.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ca.jar
echo "signing ./acid-base-solutions/acid-base-solutions_sr.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_sr.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ta.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ta.jar
echo "signing ./acid-base-solutions/acid-base-solutions_bs.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_bs.jar
echo "signing ./acid-base-solutions/acid-base-solutions_el.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_el.jar
echo "signing ./acid-base-solutions/acid-base-solutions_pt.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_pt.jar
echo "signing ./acid-base-solutions/acid-base-solutions_sl.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_sl.jar
echo "signing ./acid-base-solutions/acid-base-solutions_cs.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_cs.jar
echo "signing ./acid-base-solutions/acid-base-solutions_mk.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_mk.jar
echo "signing ./acid-base-solutions/acid-base-solutions_all.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_all.jar
echo "signing ./acid-base-solutions/acid-base-solutions_th.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_th.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ja.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ja.jar
echo "signing ./acid-base-solutions/acid-base-solutions_fa.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_fa.jar
echo "signing ./acid-base-solutions/acid-base-solutions_tr.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_tr.jar
echo "signing ./acid-base-solutions/acid-base-solutions_sq.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_sq.jar
echo "signing ./acid-base-solutions/acid-base-solutions_pl.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_pl.jar
echo "signing ./acid-base-solutions/acid-base-solutions_iw.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_iw.jar
echo "signing ./acid-base-solutions/acid-base-solutions_gl.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_gl.jar
echo "signing ./acid-base-solutions/acid-base-solutions_es.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_es.jar
echo "signing ./acid-base-solutions/acid-base-solutions_in.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_in.jar
echo "signing ./acid-base-solutions/acid-base-solutions_da.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_da.jar
echo "signing ./acid-base-solutions/acid-base-solutions_km.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_km.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ar.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ar.jar
echo "signing ./acid-base-solutions/acid-base-solutions_sk.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_sk.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ka.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ka.jar
echo "signing ./acid-base-solutions/acid-base-solutions_zh_TW.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_zh_TW.jar
echo "signing ./acid-base-solutions/acid-base-solutions_vi.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_vi.jar
echo "signing ./acid-base-solutions/acid-base-solutions_lo.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_lo.jar
echo "signing ./acid-base-solutions/acid-base-solutions_kk.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_kk.jar
echo "signing ./acid-base-solutions/acid-base-solutions_tk.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_tk.jar
echo "signing ./acid-base-solutions/acid-base-solutions_fr.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_fr.jar
echo "signing ./acid-base-solutions/acid-base-solutions_sv.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_sv.jar
echo "signing ./acid-base-solutions/acid-base-solutions_nb.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_nb.jar
echo "signing ./acid-base-solutions/acid-base-solutions_mr.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_mr.jar
echo "signing ./acid-base-solutions/acid-base-solutions_ro.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_ro.jar
echo "signing ./acid-base-solutions/acid-base-solutions_zh_CN.jar"
sign-jar ./acid-base-solutions/acid-base-solutions_zh_CN.jar
echo "signing ./projectile-motion/projectile-motion_iw.jar"
sign-jar ./projectile-motion/projectile-motion_iw.jar
echo "signing ./projectile-motion/projectile-motion_lv.jar"
sign-jar ./projectile-motion/projectile-motion_lv.jar
echo "signing ./projectile-motion/projectile-motion_th.jar"
sign-jar ./projectile-motion/projectile-motion_th.jar
echo "signing ./projectile-motion/projectile-motion_ar_SA.jar"
sign-jar ./projectile-motion/projectile-motion_ar_SA.jar
echo "signing ./projectile-motion/projectile-motion_pt_BR.jar"
sign-jar ./projectile-motion/projectile-motion_pt_BR.jar
echo "signing ./projectile-motion/projectile-motion_sv.jar"
sign-jar ./projectile-motion/projectile-motion_sv.jar
echo "signing ./projectile-motion/projectile-motion_kk.jar"
sign-jar ./projectile-motion/projectile-motion_kk.jar
echo "signing ./projectile-motion/projectile-motion_lt.jar"
sign-jar ./projectile-motion/projectile-motion_lt.jar
echo "signing ./projectile-motion/projectile-motion_hu.jar"
sign-jar ./projectile-motion/projectile-motion_hu.jar
echo "signing ./projectile-motion/projectile-motion_tk.jar"
sign-jar ./projectile-motion/projectile-motion_tk.jar
echo "signing ./projectile-motion/projectile-motion_sk.jar"
sign-jar ./projectile-motion/projectile-motion_sk.jar
echo "signing ./projectile-motion/projectile-motion_zh_CN.jar"
sign-jar ./projectile-motion/projectile-motion_zh_CN.jar
echo "signing ./projectile-motion/projectile-motion_tr.jar"
sign-jar ./projectile-motion/projectile-motion_tr.jar
echo "signing ./projectile-motion/projectile-motion_ja.jar"
sign-jar ./projectile-motion/projectile-motion_ja.jar
echo "signing ./projectile-motion/projectile-motion_be.jar"
sign-jar ./projectile-motion/projectile-motion_be.jar
echo "signing ./projectile-motion/projectile-motion_ka.jar"
sign-jar ./projectile-motion/projectile-motion_ka.jar
echo "signing ./projectile-motion/projectile-motion_da.jar"
sign-jar ./projectile-motion/projectile-motion_da.jar
echo "signing ./projectile-motion/projectile-motion_mk.jar"
sign-jar ./projectile-motion/projectile-motion_mk.jar
echo "signing ./projectile-motion/projectile-motion_km.jar"
sign-jar ./projectile-motion/projectile-motion_km.jar
echo "signing ./projectile-motion/projectile-motion_en.jar"
sign-jar ./projectile-motion/projectile-motion_en.jar
echo "signing ./projectile-motion/projectile-motion_ku_TR.jar"
sign-jar ./projectile-motion/projectile-motion_ku_TR.jar
echo "signing ./projectile-motion/projectile-motion_fr.jar"
sign-jar ./projectile-motion/projectile-motion_fr.jar
echo "signing ./projectile-motion/projectile-motion_et.jar"
sign-jar ./projectile-motion/projectile-motion_et.jar
echo "signing ./projectile-motion/projectile-motion_vi.jar"
sign-jar ./projectile-motion/projectile-motion_vi.jar
echo "signing ./projectile-motion/projectile-motion_bs.jar"
sign-jar ./projectile-motion/projectile-motion_bs.jar
echo "signing ./projectile-motion/projectile-motion_hr.jar"
sign-jar ./projectile-motion/projectile-motion_hr.jar
echo "signing ./projectile-motion/projectile-motion_el.jar"
sign-jar ./projectile-motion/projectile-motion_el.jar
echo "signing ./projectile-motion/projectile-motion_ru.jar"
sign-jar ./projectile-motion/projectile-motion_ru.jar
echo "signing ./projectile-motion/projectile-motion_cs.jar"
sign-jar ./projectile-motion/projectile-motion_cs.jar
echo "signing ./projectile-motion/projectile-motion_zh_TW.jar"
sign-jar ./projectile-motion/projectile-motion_zh_TW.jar
echo "signing ./projectile-motion/projectile-motion_sr.jar"
sign-jar ./projectile-motion/projectile-motion_sr.jar
echo "signing ./projectile-motion/projectile-motion_de.jar"
sign-jar ./projectile-motion/projectile-motion_de.jar
echo "signing ./projectile-motion/projectile-motion_nl.jar"
sign-jar ./projectile-motion/projectile-motion_nl.jar
echo "signing ./projectile-motion/projectile-motion_fa.jar"
sign-jar ./projectile-motion/projectile-motion_fa.jar
echo "signing ./projectile-motion/projectile-motion_es.jar"
sign-jar ./projectile-motion/projectile-motion_es.jar
echo "signing ./projectile-motion/projectile-motion_ta.jar"
sign-jar ./projectile-motion/projectile-motion_ta.jar
echo "signing ./projectile-motion/projectile-motion_ms.jar"
sign-jar ./projectile-motion/projectile-motion_ms.jar
echo "signing ./projectile-motion/projectile-motion_it.jar"
sign-jar ./projectile-motion/projectile-motion_it.jar
echo "signing ./projectile-motion/projectile-motion_in.jar"
sign-jar ./projectile-motion/projectile-motion_in.jar
echo "signing ./projectile-motion/projectile-motion_pl.jar"
sign-jar ./projectile-motion/projectile-motion_pl.jar
echo "signing ./projectile-motion/projectile-motion_eu.jar"
sign-jar ./projectile-motion/projectile-motion_eu.jar
echo "signing ./projectile-motion/projectile-motion_bn.jar"
sign-jar ./projectile-motion/projectile-motion_bn.jar
echo "signing ./projectile-motion/projectile-motion_ko.jar"
sign-jar ./projectile-motion/projectile-motion_ko.jar
echo "signing ./projectile-motion/projectile-motion_kn.jar"
sign-jar ./projectile-motion/projectile-motion_kn.jar
echo "signing ./projectile-motion/projectile-motion_ar.jar"
sign-jar ./projectile-motion/projectile-motion_ar.jar
echo "signing ./projectile-motion/projectile-motion_gl.jar"
sign-jar ./projectile-motion/projectile-motion_gl.jar
echo "signing ./projectile-motion/projectile-motion_ht.jar"
sign-jar ./projectile-motion/projectile-motion_ht.jar
echo "signing ./projectile-motion/projectile-motion_es_PE.jar"
sign-jar ./projectile-motion/projectile-motion_es_PE.jar
echo "signing ./projectile-motion/projectile-motion_pt.jar"
sign-jar ./projectile-motion/projectile-motion_pt.jar
echo "signing ./gene-network/gene-machine-lac-operon_en.jar"
sign-jar ./gene-network/gene-machine-lac-operon_en.jar
echo "signing ./gene-network/gene-machine-lac-operon_af.jar"
sign-jar ./gene-network/gene-machine-lac-operon_af.jar
echo "signing ./gene-network/gene-machine-lac-operon_iw.jar"
sign-jar ./gene-network/gene-machine-lac-operon_iw.jar
echo "signing ./gene-network/gene-machine-lac-operon_mk.jar"
sign-jar ./gene-network/gene-machine-lac-operon_mk.jar
echo "signing ./gene-network/gene-machine-lac-operon_pl.jar"
sign-jar ./gene-network/gene-machine-lac-operon_pl.jar
echo "signing ./gene-network/gene-machine-lac-operon_fa.jar"
sign-jar ./gene-network/gene-machine-lac-operon_fa.jar
echo "signing ./gene-network/gene-machine-lac-operon_es_PE.jar"
sign-jar ./gene-network/gene-machine-lac-operon_es_PE.jar
echo "signing ./gene-network/gene-machine-lac-operon_ja.jar"
sign-jar ./gene-network/gene-machine-lac-operon_ja.jar
echo "signing ./gene-network/gene-machine-lac-operon_pt_BR.jar"
sign-jar ./gene-network/gene-machine-lac-operon_pt_BR.jar
echo "signing ./gene-network/gene-machine-lac-operon_et.jar"
sign-jar ./gene-network/gene-machine-lac-operon_et.jar
echo "signing ./gene-network/gene-machine-lac-operon_tk.jar"
sign-jar ./gene-network/gene-machine-lac-operon_tk.jar
echo "signing ./gene-network/gene-machine-lac-operon_hu.jar"
sign-jar ./gene-network/gene-machine-lac-operon_hu.jar
echo "signing ./gene-network/gene-machine-lac-operon_zh_TW.jar"
sign-jar ./gene-network/gene-machine-lac-operon_zh_TW.jar
echo "signing ./gene-network/gene-machine-lac-operon_el.jar"
sign-jar ./gene-network/gene-machine-lac-operon_el.jar
echo "signing ./gene-network/gene-machine-lac-operon_th.jar"
sign-jar ./gene-network/gene-machine-lac-operon_th.jar
echo "signing ./gene-network/gene-network_all_installer.jar"
sign-jar ./gene-network/gene-network_all_installer.jar
echo "signing ./gene-network/gene-machine-lac-operon_es.jar"
sign-jar ./gene-network/gene-machine-lac-operon_es.jar
echo "signing ./gene-network/gene-machine-lac-operon_ko.jar"
sign-jar ./gene-network/gene-machine-lac-operon_ko.jar
echo "signing ./gene-network/gene-machine-lac-operon_zh_CN.jar"
sign-jar ./gene-network/gene-machine-lac-operon_zh_CN.jar
echo "signing ./gene-network/gene-machine-lac-operon_ar_SA.jar"
sign-jar ./gene-network/gene-machine-lac-operon_ar_SA.jar
echo "signing ./gene-network/gene-machine-lac-operon_it.jar"
sign-jar ./gene-network/gene-machine-lac-operon_it.jar
echo "signing ./gene-network/gene-machine-lac-operon_kk.jar"
sign-jar ./gene-network/gene-machine-lac-operon_kk.jar
echo "signing ./gene-network/gene-machine-lac-operon_sr.jar"
sign-jar ./gene-network/gene-machine-lac-operon_sr.jar
echo "signing ./gene-network/gene-machine-lac-operon_eu.jar"
sign-jar ./gene-network/gene-machine-lac-operon_eu.jar
echo "signing ./gene-network/gene-machine-lac-operon_da.jar"
sign-jar ./gene-network/gene-machine-lac-operon_da.jar
echo "signing ./gene-network/gene-network_all.jar"
sign-jar ./gene-network/gene-network_all.jar
echo "signing ./gene-network/gene-machine-lac-operon_fr.jar"
sign-jar ./gene-network/gene-machine-lac-operon_fr.jar
echo "signing ./gene-network/gene-machine-lac-operon_de.jar"
sign-jar ./gene-network/gene-machine-lac-operon_de.jar
echo "signing ./glaciers/glaciers_pl.jar"
sign-jar ./glaciers/glaciers_pl.jar
echo "signing ./glaciers/glaciers_vi.jar"
sign-jar ./glaciers/glaciers_vi.jar
echo "signing ./glaciers/glaciers_ko.jar"
sign-jar ./glaciers/glaciers_ko.jar
echo "signing ./glaciers/glaciers_all_installer.jar"
sign-jar ./glaciers/glaciers_all_installer.jar
echo "signing ./glaciers/glaciers_pt_BR.jar"
sign-jar ./glaciers/glaciers_pt_BR.jar
echo "signing ./glaciers/glaciers_eu.jar"
sign-jar ./glaciers/glaciers_eu.jar
echo "signing ./glaciers/glaciers_de.jar"
sign-jar ./glaciers/glaciers_de.jar
echo "signing ./glaciers/glaciers_kk.jar"
sign-jar ./glaciers/glaciers_kk.jar
echo "signing ./glaciers/glaciers_sk.jar"
sign-jar ./glaciers/glaciers_sk.jar
echo "signing ./glaciers/glaciers_sr.jar"
sign-jar ./glaciers/glaciers_sr.jar
echo "signing ./glaciers/glaciers_mk.jar"
sign-jar ./glaciers/glaciers_mk.jar
echo "signing ./glaciers/glaciers_en.jar"
sign-jar ./glaciers/glaciers_en.jar
echo "signing ./glaciers/glaciers_fa.jar"
sign-jar ./glaciers/glaciers_fa.jar
echo "signing ./glaciers/glaciers_nl.jar"
sign-jar ./glaciers/glaciers_nl.jar
echo "signing ./glaciers/glaciers_el.jar"
sign-jar ./glaciers/glaciers_el.jar
echo "signing ./glaciers/glaciers_all.jar"
sign-jar ./glaciers/glaciers_all.jar
echo "signing ./glaciers/glaciers_fr.jar"
sign-jar ./glaciers/glaciers_fr.jar
echo "signing ./glaciers/glaciers_zh_CN.jar"
sign-jar ./glaciers/glaciers_zh_CN.jar
echo "signing ./glaciers/glaciers_es_PE.jar"
sign-jar ./glaciers/glaciers_es_PE.jar
echo "signing ./glaciers/glaciers_es.jar"
sign-jar ./glaciers/glaciers_es.jar
echo "signing ./glaciers/glaciers_tr.jar"
sign-jar ./glaciers/glaciers_tr.jar
echo "signing ./glaciers/glaciers_it.jar"
sign-jar ./glaciers/glaciers_it.jar
echo "signing ./glaciers/glaciers_iw.jar"
sign-jar ./glaciers/glaciers_iw.jar
echo "signing ./glaciers/glaciers_th.jar"
sign-jar ./glaciers/glaciers_th.jar
echo "signing ./glaciers/glaciers_et.jar"
sign-jar ./glaciers/glaciers_et.jar
echo "signing ./glaciers/glaciers_da.jar"
sign-jar ./glaciers/glaciers_da.jar
echo "signing ./glaciers/glaciers_hu.jar"
sign-jar ./glaciers/glaciers_hu.jar
echo "signing ./glaciers/glaciers_tk.jar"
sign-jar ./glaciers/glaciers_tk.jar
echo "signing ./glaciers/glaciers_zh_TW.jar"
sign-jar ./glaciers/glaciers_zh_TW.jar
echo "signing ./glaciers/glaciers_ja.jar"
sign-jar ./glaciers/glaciers_ja.jar
echo "signing ./glaciers/glaciers_af.jar"
sign-jar ./glaciers/glaciers_af.jar
echo "signing ./glaciers/glaciers_nb.jar"
sign-jar ./glaciers/glaciers_nb.jar
echo "signing ./glaciers/glaciers_hr.jar"
sign-jar ./glaciers/glaciers_hr.jar
